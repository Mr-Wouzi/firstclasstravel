<?php

namespace User\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Solde
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="User\UserBundle\Entity\SoldeRepository")
 */
class Solde
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="solde", type="float" , nullable=true)
     */
    private $solde;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text" , nullable=true)
     */
    private $observation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agence;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="User")
     * @ORM\JoinColumn(name="admin_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $adminadd;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr=new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set solde
     *
     * @param float $solde
     * @return Solde
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;

        return $this;
    }

    /**
     * Get solde
     *
     * @return float 
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Solde
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Solde
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set agence
     *
     * @param \User\UserBundle\Entity\User $agence
     * @return Solde
     */
    public function setAgence(\User\UserBundle\Entity\User $agence = null)
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * Get agence
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgence()
    {
        return $this->agence;
    }

    /**
     * Set adminadd
     *
     * @param \User\UserBundle\Entity\User $adminadd
     * @return Solde
     */
    public function setAdminadd(\User\UserBundle\Entity\User $adminadd = null)
    {
        $this->adminadd = $adminadd;

        return $this;
    }

    /**
     * Get adminadd
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAdminadd()
    {
        return $this->adminadd;
    }


}
