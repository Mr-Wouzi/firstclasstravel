<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Promotion;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\PromotionType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Hotelmarge;

class MargeController extends Controller {

    public function indexAction($hotelid) {
        $user = $this->getDoctrine()
                ->getRepository('UserUserBundle:User')
                ->findAll();

        $marge = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Hotelmarge')
                ->findByHotel($hotelid);
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            foreach ($marge as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            $pers = $request->request->get("prst");
            foreach ($request->request->get("val") as $key => $value) {
                //Tools::dump($request->request, TRUE);
                $oneuser = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($key);
                $hotelmarge = new Hotelmarge();
                $hotelmarge->setUser($oneuser);
                $hotelmarge->setHotel($this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid));
                $hotelmarge->setMarge($value);
                if (isset($pers[$key]))
                    $hotelmarge->setPrst(true);
                $em->persist($hotelmarge);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('btob_hotel_homepage'));
        }
        return $this->render('BtobHotelBundle:Marge:index.html.twig', array(
                    'marge' => $marge,
                    "hotelid" => $hotelid,
                    'users' => $user
        ));
    }

}
