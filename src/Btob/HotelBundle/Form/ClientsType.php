<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civ', 'choice', array(
                'choices' => array('Mr.' => 'Mr.', 'Mme' => 'Mme', 'Mlle' => 'Mlle'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Civilité  *',
            ))
            ->add('pname', 'text', array('label' => "Prénom *", 'required' => true))
            ->add('name', 'text', array('label' => "Nom  *", 'required' => true))
            ->add('email', 'email', array('label' => "E-mail  *", 'required' => true))
            ->add('tel', 'text', array('label' => "Téléphone  *", 'required' => true))
            ->add('cin', 'text', array('label' => "C.I.N / Passeport  ", 'required' => false))
            ->add('adresse', 'textarea', array('label' => "Adresse", 'required' => false))
           // ->add('cp', 'text', array('label' => "Code postal", 'required' => false))
           // ->add('ville', 'text', array('label' => "Nom de ville", 'required' => false))
            ->add('pays','entity', array(
                'class' => 'BtobHotelBundle:Listpays',
                'required' => false,
                'empty_value' =>false,
                    'label' => "Pays",
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Clients'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_clients';
    }
}
