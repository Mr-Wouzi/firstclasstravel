<?php



namespace Btob\SejourBundle\Controller;



use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use Btob\HotelBundle\Common\Tools;

use Btob\SejourBundle\Entity\Sejour;

use Btob\SejourBundle\Entity\Imgesej;

use Btob\SejourBundle\Form\SejourType;



/**

 * Sejour controller.

 *

 */

class SejourController extends Controller

{



    /**

     * Lists all Sejour entities.

     *

     */



    public function indexAction()

    {

      //  $entities = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findby(array('act' => 1));

        $entities = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findAll();



        return $this->render('BtobSejourBundle:Sejour:index.html.twig', array('entities' => $entities));

    }



    /**

     * Creates a new Sejour entity.

     *

     */

    public function createAction(Request $request)

    {

        $entity = new Sejour();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);

        $request = $this->get('request');

        if ($form->isValid()) {

           


            $em = $this->getDoctrine()->getManager();

           

            $em->persist($entity);

            $em->flush();

            if (is_array($request->request->get("files")))

                foreach ($request->request->get("files") as $key => $value) {

                    if ($value != "") {

                        $img = new Imgesej();

                        $img->setSejour($entity);

                        $img->setImage($value);



                        $em->persist($img);

                        $em->flush();



                    }

                }

            return $this->redirect($this->generateUrl('sejour'));

        }



        return $this->render('BtobSejourBundle:Sejour:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Creates a form to create a Sejour entity.

     *

     * @param Sejour $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createCreateForm(Sejour $entity)

    {

        $form = $this->createForm(new SejourType(), $entity, array(

            'action' => $this->generateUrl('sejour_create'),

            'method' => 'POST',

        ));



       // $form->add('submit', 'submit', array('label' => 'Create'));



        return $form;

    }



    /**

     * Displays a form to create a new Sejour entity.

     *

     */

    public function newAction()

    {

        $entity = new Sejour();

        $form = $this->createCreateForm($entity);



        return $this->render('BtobSejourBundle:Sejour:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Finds and displays a Sejour entity.

     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobSejourBundle:Sejour')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Sejour entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobSejourBundle:Sejour:show.html.twig', array(

            'entity' => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Displays a form to edit an existing Sejour entity.

     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobSejourBundle:Sejour')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Sejour entity.');

        }



        $editForm = $this->createEditForm($entity);

        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobSejourBundle:Sejour:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Creates a form to edit a Sejour entity.

     *

     * @param Sejour $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createEditForm(Sejour $entity)

    {

        $form = $this->createForm(new SejourType(), $entity, array(

            'action' => $this->generateUrl('sejour_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));



      //  $form->add('submit', 'submit', array('label' => 'Update'));



        return $form;

    }



    /**

     * Edits an existing Sejour entity.

     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobSejourBundle:Sejour')->find($id);

        foreach ($entity->getImgesej() as $key => $value) {

            $em->remove($value);

            $em->flush();

        }

        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Sejour entity.');

        }



        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);



        if ($editForm->isValid()) {

            $em->flush();

            if (is_array($request->request->get("files")))

                foreach ($request->request->get("files") as $key => $value) {

                    if ($value != "") {

                        $img = new Imgesej();

                        $img->setSejour($entity);

                        $img->setImage($value);



                        $em->persist($img);

                        $em->flush();



                    }

                }

            return $this->redirect($this->generateUrl('sejour'));

        }



        return $this->render('BtobSejourBundle:Sejour:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Deletes a Sejour entity.

     *

     */

    public function deleteAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSejourBundle:Sejour')->find($id);

        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('sejour'));

    }



    /**

     * Creates a form to delete a Sejour entity by id.

     *

     * @param mixed $id The entity id

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()

            ->setAction($this->generateUrl('sejour_delete', array('id' => $id)))

            ->setMethod('DELETE')

            ->add('submit', 'submit', array('label' => 'Delete'))

            ->getForm();

    }

}

