<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Promotion;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\PromotionType;
use Symfony\Component\HttpFoundation\JsonResponse;

class PromotionController extends Controller {

    public function indexAction($hotelid) {
        $hotel = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Hotel')
                ->find($hotelid);
        return $this->render('BtobHotelBundle:Promotion:index.html.twig', array('entities' => $hotel->getPromotion(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function addAction($hotelid) {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $Promotion = new Promotion();
        $form = $this->createForm(new PromotionType(), $Promotion);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Promotion->setHotel($hotel);
                $em->persist($Promotion);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_promotion_homepage', array("hotelid" => $hotelid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Promotion:form.html.twig', array('form' => $form->createView(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function editAction($id, $hotelid) {
        $request = $this->get('request');
        $Promotion = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Promotion')
                ->find($id);
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PromotionType(), $Promotion);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_promotion_homepage', array("hotelid" => $hotelid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Promotion:form.html.twig', array('form' => $form->createView(), 'id' => $id, "hotelid" => $hotelid, "hotel" => $hotel)
        );
    }

    public function deleteAction(Promotion $Promotion, $hotelid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Promotion) {
            throw new NotFoundHttpException("Promotion non trouvée");
        }
        $em->remove($Promotion);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_promotion_homepage', array("hotelid" => $hotelid)));
    }

}
