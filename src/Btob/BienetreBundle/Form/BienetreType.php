<?php

namespace Btob\BienetreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BienetreType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('dcr')
            ->add('act', NULL, array('label' =>'active', 'required' => false))
            ->add('pindex', NULL, array('label' =>"Inclure ce soin dans la liste promo de la page d'accueil", 'required' => false))
            ->add('titre', 'text', array('label' => "Titre", 'required' => true))
            ->add('prixint', NULL, array('label' =>' Valeur initiale' , 'required' => true))
            ->add('prixpr', NULL, array('label' =>'Valeur promo' , 'required' => false))
            ->add('description', null, array('label' => "Description ", 'required' => false))
            ->add('type', 'choice', array(
                'choices'   => array('Soins' => 'Soins', 'Cures' => 'Cures'),
                'empty_value' => 'Choisissez ',
                'required'  => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\BienetreBundle\Entity\Bienetre'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_bienetrebundle_bienetre';
    }
}
