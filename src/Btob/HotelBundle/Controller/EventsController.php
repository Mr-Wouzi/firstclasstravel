<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Events;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\EventsType;
use Symfony\Component\HttpFoundation\JsonResponse;

class EventsController extends Controller {

    public function indexAction($hotelid) {
        $hotel = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Hotel')
                ->find($hotelid);
        return $this->render('BtobHotelBundle:Events:index.html.twig', array('entities' => $hotel->getEvents(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function addAction($hotelid) {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $Events = new Events();
        $form = $this->createForm(new EventsType(), $Events);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Events->setHotel($hotel);
                $em->persist($Events);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_events_homepage', array("hotelid" => $hotelid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Events:form.html.twig', array('form' => $form->createView(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function editAction($id, $hotelid) {
        $request = $this->get('request');
        $Events = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Events')
                ->find($id);
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new EventsType(), $Events);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_events_homepage', array("hotelid" => $hotelid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Events:form.html.twig', array('form' => $form->createView(), 'id' => $id, "hotelid" => $hotelid, "hotel" => $hotel)
        );
    }

    public function deleteAction(Events $Events, $hotelid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Events) {
            throw new NotFoundHttpException("Events non trouvée");
        }
        $em->remove($Events);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_events_homepage', array("hotelid" => $hotelid)));
    }

}
