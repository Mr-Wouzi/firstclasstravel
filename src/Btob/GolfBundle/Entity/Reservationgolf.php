<?php

namespace Btob\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationgolf
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\GolfBundle\Entity\ReservationgolfRepository")
 */
class Reservationgolf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationgolf")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Golf", inversedBy="reservationgolf")
     * @ORM\JoinColumn(name="golf_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $golf;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationgolf")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbradult", type="integer" , nullable=true)
     */
    private $nbradult;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrenf", type="integer" , nullable=true)
     */
    private $nbrenf;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="text" , nullable=true)
     */
    private $remarque;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Reservationgolf
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }
    public function __construct(){
        $this->dcr=new \DateTime();
    }


    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set nbradult
     *
     * @param integer $nbradult
     * @return Reservationgolf
     */
    public function setNbradult($nbradult)
    {
        $this->nbradult = $nbradult;

        return $this;
    }

    /**
     * Get nbradult
     *
     * @return integer 
     */
    public function getNbradult()
    {
        return $this->nbradult;
    }

    /**
     * Set nbrenf
     *
     * @param integer $nbrenf
     * @return Reservationgolf
     */
    public function setNbrenf($nbrenf)
    {
        $this->nbrenf = $nbrenf;

        return $this;
    }

    /**
     * Get nbrenf
     *
     * @return integer 
     */
    public function getNbrenf()
    {
        return $this->nbrenf;
    }

    /**
     * Set remarque
     *
     * @param string $remarque
     * @return Reservationgolf
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string 
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationgolf
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationgolf
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set golf
     *
     * @param \Btob\GolfBundle\Entity\Golf $golf
     * @return Reservationgolf
     */
    public function setGolf(\Btob\GolfBundle\Entity\Golf $golf = null)
    {
        $this->golf = $golf;

        return $this;
    }

    /**
     * Get golf
     *
     * @return \Btob\GolfBundle\Entity\Golf 
     */
    public function getGolf()
    {
        return $this->golf;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Reservationgolf
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }
}
