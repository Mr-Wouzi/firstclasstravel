<?php

namespace Btob\ClasseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BtobClasseBundle:Default:index.html.twig', array('name' => $name));
    }
}
