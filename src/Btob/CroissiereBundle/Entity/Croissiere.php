<?php







namespace Btob\CroissiereBundle\Entity;







use Doctrine\ORM\Mapping as ORM;











/**



 * Croissiere



 *



 * @ORM\Table()



 * @ORM\Entity(repositoryClass="Btob\CroissiereBundle\Entity\CroissiereRepository")



 */



class Croissiere



{



    /**



     * @var integer



     *



     * @ORM\Column(name="id", type="integer")



     * @ORM\Id



     * @ORM\GeneratedValue(strategy="AUTO")



     */



    private $id;



    /**



     * @ORM\OneToMany(targetEntity="Imgecrs", mappedBy="croissiere")



     */

    protected $imgcr;

    /**

     * @var string

     *

     * @ORM\Column(name="seotitle", type="string", length=255 , nullable=true)

     */

    private $seotitle;

    /**

     * @var string

     *

     * @ORM\Column(name="seodescreption", type="string", length=255 , nullable=true)

     */

    private $seodescreption;

    /**

     * @var string

     *

     * @ORM\Column(name="seokeyword", type="string", length=255 , nullable=true)

     */

    private $seokeyword;





    /**



     * @var boolean



     *



     * @ORM\Column(name="active", type="boolean" , nullable=true)



     */



    private $act;







    /**



     * @var boolean



     *



     * @ORM\Column(name="pub", type="boolean" , nullable=true)



     */



    private $pub;







    /**



     * @var string



     *



     * @ORM\Column(name="region", type="string", length=255 , nullable=true)



     */



    private $region;







    /**



     * @var string



     *



     * @ORM\Column(name="titre", type="string", length=255 , nullable=true)



     */



    private $titre;







    /**



     * @var float



     *



     * @ORM\Column(name="prix", type="float" , nullable=true)



     */



    private $prix;







    /**



     * @var integer



     *



     * @ORM\Column(name="nbrjr", type="integer" , nullable=true)



     */



    private $nbrjr;







    /**



     * @var string



     *



     * @ORM\Column(name="description", type="text" , nullable=true)



     */



    private $description;



    /**



     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="croissiere")



     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")



     */



    protected $arrangement;







    /**



     * Get id



     *



     * @return integer 



     */



    public function getId()



    {



        return $this->id;



    }



    /**

     * Set seotitle

     *

     * @param string $seotitle

     * @return Croissiere

     */

    public function setSeotitle($seotitle)

    {

        $this->seotitle = $seotitle;



        return $this;

    }



    /**

     * Get seotitle

     *

     * @return string

     */

    public function getSeotitle()

    {

        return $this->seotitle;

    }


    /**

     * Set seodescreption

     *

     * @param string $seodescreption

     * @return Croissiere

     */

    public function setSeodescreption($seodescreption)

    {

        $this->seodescreption = $seodescreption;



        return $this;

    }



    /**

     * Get seodescreption

     *

     * @return string

     */

    public function getSeodescreption()

    {

        return $this->seodescreption;

    }


    /**

     * Set seokeyword
     *

     * @param string $seokeyword

     * @return Croissiere

     */

    public function setSeokeyword($seokeyword)

    {

        $this->seokeyword = $seokeyword;



        return $this;

    }



    /**

     * Get seodkeyword

     *

     * @return string

     */

    public function getSeokeyword()

    {

        return $this->seokeyword;

    }




    /**



     * Set pub



     *



     * @param boolean $pub



     * @return Croissiere



     */



    public function setPub($pub)



    {



        $this->pub = $pub;







        return $this;



    }







    /**



     * Get pub



     *



     * @return boolean 



     */



    public function getPub()



    {



        return $this->pub;



    }







    /**



     * Set region



     *



     * @param string $region



     * @return Croissiere



     */



    public function setRegion($region)



    {



        $this->region = $region;







        return $this;



    }







    /**



     * Get region



     *



     * @return string 



     */



    public function getRegion()



    {



        return $this->region;



    }







    /**



     * Set titre



     *



     * @param string $titre



     * @return Croissiere



     */



    public function setTitre($titre)



    {



        $this->titre = $titre;







        return $this;



    }







    /**



     * Get titre



     *



     * @return string 



     */



    public function getTitre()



    {



        return $this->titre;



    }







    /**



     * Set prix



     *



     * @param float $prix



     * @return Croissiere



     */



    public function setPrix($prix)



    {



        $this->prix = $prix;







        return $this;



    }







    /**



     * Get prix



     *



     * @return float 



     */



    public function getPrix()



    {



        return $this->prix;



    }







    /**



     * Set nbrjr



     *



     * @param integer $nbrjr



     * @return Croissiere



     */



    public function setNbrjr($nbrjr)



    {



        $this->nbrjr = $nbrjr;







        return $this;



    }







    /**



     * Get nbrjr



     *



     * @return integer 



     */



    public function getNbrjr()



    {



        return $this->nbrjr;



    }







    /**



     * Set description



     *



     * @param string $description



     * @return Croissiere



     */



    public function setDescription($description)



    {



        $this->description = $description;







        return $this;



    }







    /**



     * Get description



     *



     * @return string 



     */



    public function getDescription()



    {



        return $this->description;



    }







    /**



     * Set arrangement



     *



     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement



     * @return Croissiere



     */



    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)



    {



        $this->arrangement = $arrangement;







        return $this;



    }







    /**



     * Get arrangement



     *



     * @return \Btob\HotelBundle\Entity\Arrangement 



     */



    public function getArrangement()



    {



        return $this->arrangement;



    }







    /**



     * Constructor



     */



    public function __construct()



    {



        $this->imgcr = new \Doctrine\Common\Collections\ArrayCollection();



    }







    /**



     * Add imgcr



     *



     * @param \Btob\CroissiereBundle\Entity\Imgecrs $imgcr



     * @return Croissiere



     */



    public function addImgcr(\Btob\CroissiereBundle\Entity\Imgecrs $imgcr)



    {



        $this->imgcr[] = $imgcr;







        return $this;



    }







    /**



     * Remove imgcr



     *



     * @param \Btob\CroissiereBundle\Entity\Imgecrs $imgcr



     */



    public function removeImgcr(\Btob\CroissiereBundle\Entity\Imgecrs $imgcr)



    {



        $this->imgcr->removeElement($imgcr);



    }







    /**



     * Get imgcr



     *



     * @return \Doctrine\Common\Collections\Collection 



     */



    public function getImgcr()



    {



        return $this->imgcr;



    }







    /**



     * Set act



     *



     * @param boolean $act



     * @return Croissiere



     */



    public function setAct($act)



    {



        $this->act = $act;







        return $this;



    }







    /**



     * Get act



     *



     * @return boolean 



     */



    public function getAct()



    {



        return $this->act;



    }



}



