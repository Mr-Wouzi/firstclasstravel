<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Clients;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClientController extends Controller
{

    public function indexAction()
    {
        $clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
        // test agence connected
        $resa = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findAll();
        $entities = array();
        foreach ($resa as $value) {
            if ($value->getDel()) {
                $entities[] = $value;
            }
        }
        $user = $this->get('security.context')->getToken()->getUser();

        if (in_array("AGENCEID", $user->getRoles())) {
            $tabuser = array();
            foreach ($entities as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabuser[$value->getClient()->getId()] = $value->getClient();
                }
            }
            $clients = $tabuser;
        }
        return $this->render('BtobHotelBundle:Client:index.html.twig', array('entities' => $clients));
    }
    public function detailAction(Clients $client){
        return $this->render('BtobHotelBundle:Client:detail.html.twig', array('entry' => $client));
    }
    public function ajxgetclientAction()
    {
        $request = $this->get('request');
        $cin = $request->request->get('cin');
        $client = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findOneBy(array('cin' => $cin));
        if ($client != null) {
            $tab["civ"] = $client->getCiv();
            $tab["pname"] = $client->getPname();
            $tab["name"] = $client->getName();
            $tab["email"] = $client->getEmail();
            $tab["tel"] = $client->getTel();
            $tab["adresse"] = $client->getAdresse();
            $tab["cp"] = $client->getCp();
            $tab["ville"] = $client->getVille();
            $tab["pays"] = $client->getPays()->getId();
        } else {
            $tab["civ"] = "";
            $tab["pname"] = "";
            $tab["name"] = "";
            $tab["email"] = "";
            $tab["tel"] = "";
            $tab["adresse"] = "";
            $tab["cp"] = "";
            $tab["ville"] = "";
            $tab["pays"] = 1;
        }

//Tools::dump($request->request->get('cin'),true);
        return new JsonResponse($tab);
    }

}
