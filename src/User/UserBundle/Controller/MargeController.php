<?php

namespace User\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use User\UserBundle\Entity\User;
use User\UserBundle\Entity\Usermarge;
use User\UserBundle\Form\UsermargeType;
use Btob\HotelBundle\Entity\Hotelmarge;
use Btob\HotelBundle\Common\Tools;

class MargeController extends Controller
{
    public function indexAction($userid)
    {
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($userid);
        $marge = $user->getUsermarge();
        return $this->render('UserUserBundle:Marge:index.html.twig', array('entities' => $marge, 'userid' => $userid));
    }

    public function addAction($userid)
    {
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($userid);
        $usermarge = new Usermarge();
        $form = $this->createForm(new UsermargeType(), $usermarge);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $em->flush();
                //Tools::dump($usermarge,true);
                $usermarge->setUser($user);
                $em->persist($usermarge);
                $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();
                foreach ($hotel as $value) {
                    $hotelmarge = new Hotelmarge();
                    $hotelmarge->setUsermarge($usermarge);
                    $hotelmarge->setMarge($usermarge->getMarge());
                    $hotelmarge->setPrst($usermarge->getPerst());
                    $hotelmarge->setHotel($value);
                    $em->persist($hotelmarge);
                    $em->flush();
                }

                return $this->redirect($this->generateUrl('user_marge_homepage', array('userid' => $userid)));
            }
        }
        return $this->render('UserUserBundle:Marge:edit.html.twig', array('form' => $form->createView(), 'userid' => $userid));
    }

    public function updateAction($userid, Usermarge $usermarge)
    {
        $request = $this->get('request');


        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new UsermargeType(), $usermarge);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            foreach($usermarge->getHotelmarge() as $value){
                $em->remove($value);
                $em->flush();
            }
            $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();
            foreach ($hotel as $value) {
                $hotelmarge = new Hotelmarge();
                $hotelmarge->setUsermarge($usermarge);
                $hotelmarge->setMarge($usermarge->getMarge());
                $hotelmarge->setPrst($usermarge->getPerst());
                $hotelmarge->setHotel($value);
                $em->persist($hotelmarge);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('user_marge_homepage', array('userid' => $userid)));
        }
        return $this->render('UserUserBundle:Marge:edit.html.twig', array('form' => $form->createView(), 'userid' => $userid, 'id' => $usermarge->getId()));
    }

    public function deleteAction($userid, Usermarge $usermarge)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$usermarge) {
            throw new NotFoundHttpException('marge non trouvée');
        }
        $em->remove($usermarge);
        $em->flush();
        return $this->redirect($this->generateUrl('user_marge_homepage', array('userid' => $userid)));
    }
}