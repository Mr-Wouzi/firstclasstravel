<?php







namespace Btob\CuircuitBundle\Entity;







use Doctrine\ORM\Mapping as ORM;







/**



 * Cuircuit



 *



 * @ORM\Table()



 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\CuircuitRepository")



 */



class Cuircuit



{



    /**



     * @var integer



     *



     * @ORM\Column(name="id", type="integer")



     * @ORM\Id



     * @ORM\GeneratedValue(strategy="AUTO")



     */



    private $id;







    /**



     * @var string



     *



     * @ORM\Column(name="libelle", type="string", length=255 , nullable=true)



     */



    private $libelle;




    /**

     * @var string

     *

     * @ORM\Column(name="seotitle", type="string", length=255 , nullable=true)

     */

    private $seotitle;

    /**

     * @var string

     *

     * @ORM\Column(name="seodescreption", type="string", length=255 , nullable=true)

     */

    private $seodescreption;

    /**

     * @var string

     *

     * @ORM\Column(name="seokeyword", type="string", length=255 , nullable=true)

     */

    private $seokeyword;


    /**



     * @var string



     *



     * @ORM\Column(name="type", type="string", length=255 , nullable=true)



     */



    private $type;



    /**



     * @var date



     *



     * @ORM\Column(name="dcr", type="date")



     */



    private $dcr;







    /**



     * @var string



     *



     * @ORM\Column(name="ville", type="string", length=255 , nullable=true)



     */



    private $ville;







    /**



     * @var integer



     *



     * @ORM\Column(name="nbretoiles", type="integer",  nullable=true , nullable=true)



     */



    private $nbretoiles;







    /**



     * @var float



     *



     * @ORM\Column(name="prix", type="float" , nullable=true)



     */



    private $prix;



    /**



     * @var float



     *



     * @ORM\Column(name="prixavance", type="float" , nullable=true)



     */



    private $prixavance;    




    /**



     * @var boolean



     *



     * @ORM\Column(name="apartir", type="boolean" , nullable=true)



     */



    private $apartir;



    /**



     * @var boolean



     *



     * @ORM\Column(name="active", type="boolean" , nullable=true)



     */



    private $active;







    /**



     * @var boolean



     *



     * @ORM\Column(name="pindex", type="boolean" , nullable=true)



     */



    private $pindex;







    /**



     * @var integer



     *



     * @ORM\Column(name="nbrjr", type="integer" , nullable=true)



     */



    private $nbrjr;



    /**



     * @ORM\OneToMany(targetEntity="Imgcuir", mappedBy="cuircuit")



     */



    protected $imgcuir;



    /**



     * @var string



     *



     * @ORM\Column(name="description", type="text" , nullable=true)



     */



    private $description;

     /**
     * @ORM\OneToMany(targetEntity="Supplementc", mappedBy="cuircuit", cascade={"remove"})
     */
    protected $supplementc;
    

     /**
     * @ORM\OneToMany(targetEntity="Reductionc", mappedBy="cuircuit", cascade={"remove"})
     */
    protected $reductionc;    
    
    
        /**
     * @ORM\OneToMany(targetEntity="Cuircuitprice", mappedBy="cuircuit", cascade={"remove"})
     */
    protected $cuircuitprice;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="ageenfmin", type="string", length=255 , nullable=true)
     */

    private $ageenfmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageenfmax", type="string", length=255 , nullable=true)
     */

    private $ageenfmax;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="agebmin", type="string", length=255 , nullable=true)
     */

    private $agebmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="agebmax", type="string", length=255 , nullable=true)
     */

    private $agebmax;


    public  function  __construct(){



        $this->dcr = new \DateTime();



        $this->imgcuir =new \Doctrine\Common\Collections\ArrayCollection();
        $this->supplementc = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reductionc = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cuircuitprice = new \Doctrine\Common\Collections\ArrayCollection();



    }







    /**



     * Get id



     *



     * @return integer 



     */



    public function getId()



    {



        return $this->id;



    }




    /**

     * Set seotitle

     *

     * @param string $seotitle

     * @return Cuircuit

     */

    public function setSeotitle($seotitle)

    {

        $this->seotitle = $seotitle;



        return $this;

    }



    /**

     * Get seotitle

     *

     * @return string

     */

    public function getSeotitle()

    {

        return $this->seotitle;

    }


    /**

     * Set seodescreption

     *

     * @param string $seodescreption

     * @return Cuircuit

     */

    public function setSeodescreption($seodescreption)

    {

        $this->seodescreption = $seodescreption;



        return $this;

    }



    /**

     * Get seodescreption

     *

     * @return string

     */

    public function getSeodescreption()

    {

        return $this->seodescreption;

    }


    /**

     * Set seokeyword
     *

     * @param string $seokeyword

     * @return Cuircuit

     */

    public function setSeokeyword($seokeyword)

    {

        $this->seokeyword = $seokeyword;



        return $this;

    }



    /**

     * Get seokeyword

     *

     * @return string

     */

    public function getSeokeyword()

    {

        return $this->seokeyword;

    }


    /**



     * Set libelle



     *



     * @param string $libelle



     * @return Cuircuit



     */



    public function setLibelle($libelle)



    {



        $this->libelle = $libelle;







        return $this;



    }







    /**



     * Get libelle



     *



     * @return string 



     */



    public function getLibelle()



    {



        return $this->libelle;



    }



    /**



     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="cuircuit")



     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")



     */



    protected $arrangement;







    /**



     * Set type



     *



     * @param string $type



     * @return Cuircuit



     */



    public function setType($type)



    {



        $this->type = $type;







        return $this;



    }







    /**



     * Get type



     *



     * @return string 



     */



    public function getType()



    {



        return $this->type;



    }







    /**



     * Set ville



     *



     * @param string $ville



     * @return Cuircuit



     */



    public function setVille($ville)



    {



        $this->ville = $ville;







        return $this;



    }







    /**



     * Get ville



     *



     * @return string 



     */



    public function getVille()



    {



        return $this->ville;



    }







    /**



     * Set nbretoiles



     *



     * @param integer $nbretoiles



     * @return Cuircuit



     */



    public function setNbretoiles($nbretoiles)



    {



        $this->nbretoiles = $nbretoiles;







        return $this;



    }







    /**



     * Get nbretoiles



     *



     * @return integer 



     */



    public function getNbretoiles()



    {



        return $this->nbretoiles;



    }







    /**



     * Set prix



     *



     * @param float $prix



     * @return Cuircuit



     */



    public function setPrix($prix)



    {



        $this->prix = $prix;







        return $this;



    }







    /**



     * Get prix



     *



     * @return float 



     */



    public function getPrix()



    {



        return $this->prix;



    }















    /**



     * Set pindex



     *



     * @param boolean $pindex



     * @return Cuircuit



     */



    public function setPindex($pindex)



    {



        $this->pindex = $pindex;







        return $this;



    }







    /**



     * Get pindex



     *



     * @return boolean 



     */



    public function getPindex()



    {



        return $this->pindex;



    }







    /**



     * Set nbrjr



     *



     * @param integer $nbrjr



     * @return Cuircuit



     */



    public function setNbrjr($nbrjr)



    {



        $this->nbrjr = $nbrjr;







        return $this;



    }







    /**



     * Get nbrjr



     *



     * @return integer 



     */



    public function getNbrjr()



    {



        return $this->nbrjr;



    }







    /**



     * Set description



     *



     * @param string $description



     * @return Cuircuit



     */



    public function setDescription($description)



    {



        $this->description = $description;







        return $this;



    }







    /**



     * Get description



     *



     * @return string 



     */



    public function getDescription()



    {



        return $this->description;



    }







    /**



     * Set dcr



     *



     * @param \DateTime $dcr



     * @return Cuircuit



     */



    public function setDcr($dcr)



    {



        $this->dcr = $dcr;







        return $this;



    }







    /**



     * Get dcr



     *



     * @return \DateTime 



     */



    public function getDcr()



    {



        return $this->dcr;



    }















    /**



     * Set arrangement



     *



     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement



     * @return Cuircuit



     */



    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)



    {



        $this->arrangement = $arrangement;







        return $this;



    }







    /**



     * Get arrangement



     *



     * @return \Btob\HotelBundle\Entity\Arrangement 



     */



    public function getArrangement()



    {



        return $this->arrangement;



    }















    /**



     * Set apartir



     *



     * @param boolean $apartir



     * @return Cuircuit



     */



    public function setApartir($apartir)



    {



        $this->apartir = $apartir;







        return $this;



    }







    /**



     * Get apartir



     *



     * @return boolean 



     */



    public function getApartir()



    {



        return $this->apartir;



    }







    /**



     * Add imgcuir



     *



     * @param \Btob\CuircuitBundle\Entity\Imgcuir $imgcuir



     * @return Cuircuit



     */



    public function addImgcuir(\Btob\CuircuitBundle\Entity\Imgcuir $imgcuir)



    {



        $this->imgcuir[] = $imgcuir;







        return $this;



    }







    /**



     * Remove imgcuir



     *



     * @param \Btob\CuircuitBundle\Entity\Imgcuir $imgcuir



     */



    public function removeImgcuir(\Btob\CuircuitBundle\Entity\Imgcuir $imgcuir)



    {



        $this->imgcuir->removeElement($imgcuir);



    }







    /**



     * Get imgcuir



     *



     * @return \Doctrine\Common\Collections\Collection 



     */



    public function getImgcuir()



    {



        return $this->imgcuir;



    }







    /**



     * Set active



     *



     * @param boolean $active



     * @return Cuircuit



     */



    public function setActive($active)



    {



        $this->active = $active;







        return $this;



    }







    /**



     * Get active



     *



     * @return boolean 



     */



    public function getActive()



    {



        return $this->active;



    }


     /**
     * Add supplementc
     *
     * @param \Btob\CuircuitBundle\Entity\Supplementc $supplementc
     * @return Cuircuit
     */
    public function addSupplementc(\Btob\CuircuitBundle\Entity\Supplementc $supplementc)
    {
        $this->supplementc[] = $supplementc;

        return $this;
    }

    /**
     * Remove supplementc
     *
     * @param \Btob\CuircuitBundle\Entity\Supplementc $supplementc
     */
    public function removeSupplementc(\Btob\CuircuitBundle\Entity\Supplementc $supplementc)
    {
        $this->supplementc->removeElement($supplementc);
    }

    /**
     * Get supplementc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplementc()
    {
        return $this->supplementc;
    }
  
    
     /**
     * Add cuircuitprice
     *
     * @param \Btob\CuircuitBundle\Entity\Cuircuitprice $cuircuitprice
     * @return Cuircuit
     */
    public function addCuircuitprice(\Btob\CuircuitBundle\Entity\Cuircuitprice $cuircuitprice)
    {
        $this->cuircuitprice[] = $cuircuitprice;

        return $this;
    }

    /**
     * Remove cuircuitprice
     *
     * @param \Btob\CuircuitBundle\Entity\Cuircuitprice $cuircuitprice
     */
    public function removeCuircuitprice(\Btob\CuircuitBundle\Entity\Cuircuitprice $cuircuitprice)
    {
        $this->cuircuitprice->removeElement($cuircuitprice);
    }

    /**
     * Get Cuircuitprice
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCuircuitprice()
    {
        return $this->cuircuitprice;
    }   
    
    
    
        /**
     * Add reductionc
     *
     * @param \Btob\CuircuitBundle\Entity\Reductionc $reductionc
     * @return Cuircuit
     */
    public function addReductionc(\Btob\CuircuitBundle\Entity\Reductionc $reductionc)
    {
        $this->reductionc[] = $reductionc;

        return $this;
    }

    /**
     * Remove reductionc
     *
     * @param \Btob\CuircuitBundle\Entity\Reductionc $reductionc
     */
    public function removeReductionc(\Btob\CuircuitBundle\Entity\Reductionc $reductionc)
    {
        $this->reductionc->removeElement($reductionc);
    }

    /**
     * Get reductionc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReductionc()
    {
        return $this->reductionc;
    } 
    
    
    
    /**
     * Set prixavance
     *
     * @param float $prixavance
     * @return Cuircuit
     */

    public function setPrixavance($prixavance)
    {
        $this->prixavance = $prixavance;
        return $this;
    }

    /**
     * Get prixavance
     *
     * @return float 
     */

    public function getPrixavance()
    {
        return $this->prixavance;

    }    
    
    
       
     /**
     * Set ageenfmin
     *
     * @param string $ageenfmin
     * @return Cuircuit
     */

    public function setAgeenfmin($ageenfmin)

    {
        $this->ageenfmin = $ageenfmin;
        return $this;

    }



    /**
     * Get ageenfmin
     *
     * @return string 
     */

    public function getAgeenfmin()

    {
        return $this->ageenfmin;

    }
    
    
     /**
     * Set ageenfmax
     *
     * @param string $ageenfmax
     * @return Cuircuit
     */

    public function setAgeenfmax($ageenfmax)

    {
        $this->ageenfmax = $ageenfmax;
        return $this;

    }



    /**
     * Get ageenfmax
     *
     * @return string 
     */

    public function getAgeenfmax()

    {
        return $this->ageenfmax;

    }
    
    
    
    
    
    
         /**
     * Set agebmin
     *
     * @param string $agebmin
     * @return Cuircuit
     */

    public function setAgebmin($agebmin)

    {
        $this->agebmin = $agebmin;
        return $this;

    }



    /**
     * Get agebmin
     *
     * @return string 
     */

    public function getAgebmin()

    {
        return $this->agebmin;

    }
    
    
     /**
     * Set agebmax
     *
     * @param string $agebmax
     * @return Cuircuit
     */

    public function setAgebmax($agebmax)

    {
        $this->agebmax = $agebmax;
        return $this;

    }



    /**
     * Get agebmax
     *
     * @return string 
     */

    public function getAgebmax()

    {
        return $this->agebmax;

    }
    
 

}



