<?php

namespace Btob\VoitureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationvoitureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('act', 'choice', array('label' => 'Statut',
                'choices' => array('3' => 'Valider', '2' => 'Annuler', 'Null' => 'En attente'),
                'preferred_choices' => array('Valider'),
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\VoitureBundle\Entity\Reservationvoiture'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_voiturebundle_reservationvoiture';
    }
}
