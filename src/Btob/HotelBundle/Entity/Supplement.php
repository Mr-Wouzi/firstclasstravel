<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\SupplementRepository")
 */
class Supplement {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=100)
     */
    private $category;    

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;

    /**
     * @ORM\OneToMany(targetEntity="Hotelroomsup", mappedBy="supplement", cascade={"remove"})
     */
    protected $suproom;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Supplement
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Supplement
     */
    public function setAct($act) {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct() {
        return $this->act;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->suproom = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add suproom
     *
     * @param \Btob\HotelBundle\Entity\Hotelroomsup $suproom
     * @return Supplement
     */
    public function addSuproom(\Btob\HotelBundle\Entity\Hotelroomsup $suproom)
    {
        $this->suproom[] = $suproom;

        return $this;
    }

    /**
     * Remove suproom
     *
     * @param \Btob\HotelBundle\Entity\Hotelroomsup $suproom
     */
    public function removeSuproom(\Btob\HotelBundle\Entity\Hotelroomsup $suproom)
    {
        $this->suproom->removeElement($suproom);
    }

    /**
     * Get suproom
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSuproom()
    {
        return $this->suproom;
    }
    
    
    /**
     * Set category
     *
     * @param string $category
     * @return Supplement
     */
    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory() {
        return $this->category;
    }
}
