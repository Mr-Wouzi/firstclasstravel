<?php

namespace Api\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\VoleBundle\Entity\Vol;
use Btob\HotelBundle\Common\Tools;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller{

    public function indexAction(){

		header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');

		$seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Contact'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword());

        $request = $this->get('request');
        if ($request->getMethod() == 'POST'){
        	header('Access-Control-Allow-Origin: *');
            $session = $this->getRequest()->getSession();
            $nom = $request->request->get("nom");
            $tel = $request->request->get("tel");
            $email = $request->request->get("email");
            $sujet = $request->request->get("sujet");
            $message = $request->request->get("Message");


            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');


            $to = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $nom." ".$sujet ;
            $headers = "From: ".$nom." ".$email."\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';            
			$message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 
				<tbody>				  <tr>				   <td>				  
				<a href="http://www.firstclasstravel.ma/">
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
				</a>				   
				</td>				   <td align="right">				    
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>				   </td>			
				</tr>				 </tbody>				</table>
				<table width="90%"  cellspacing="1" border="0">';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Contact :</b></td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $nom . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $tel . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $email . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Sujet</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $sujet . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Message</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $message . '</td>';            
			$message1 .= '</tr>';							
			$message1 .= '<tr>';                			
			$message1 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
				 Angle Bd. Mohamed 5 et Av. Hassen 1er N° 7 Bloc B - Imm. SIBAM2 4000 Marrakech Maroc - <a style="color:#fff;" href="mailto:bnora-fct@menara.ma" target="_blank">bnora-fct@menara.ma</a> - <a style="color:#fff;" href="mailto:bnora-fct@menara.ma" target="_blank">shamid-fct@menara.ma</a> - <a style="color:#fff;" href="mailto:bnora-fct@menara.ma" target="_blank">hmahi-fct@menara.ma</a> <br> <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
				</td>';                			
			$message1 .= '</tr>';            
			$message1 .= '</table>';            
			$message1 .= '</body>';
			mail("infos.firstclass@gmail.com ", $subject, $message1, $headers);
			mail($email, $subject, $message1, $headers);
			//mail("tuninfo@benmacha.tn", $subject, $message1, $headers);
            
	    	$array = array(
				'result' => "success",
				'email' => $to,
	    		);
	    	$response = new JsonResponse($array);
	    	return $response;    

        }else{

	    	$array = array(
	    		'method' => 'GET',
	    		);
	    	$response = new JsonResponse($array);
	    	return $response; 

        }

    }


     public function devisAction(){

		 header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
		 header('Access-Control-Allow-Credentials: true');
		 header('Access-Control-Max-Age: 86400');

		 $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Contact'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword());

        $request = $this->get('request');
        if ($request->getMethod() == 'GET'){
        	header('Access-Control-Allow-Origin: *');
            $session = $this->getRequest()->getSession();
            $nom = $request->request->get("nom");
            $tel = $request->request->get("tel");
            $email = $request->request->get("email");
            $prenom = $request->request->get("prenom");
            $message = $request->request->get("Message");


            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');


            $to = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $prenom." ".$nom." ( Demande devis )" ;
            $headers = "From: ".$nom." ".$email."\n";
            $headers .= 'Cc: '.$email. "\r\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';            
			$message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 
				<tbody>				  <tr>				   <td>				  
				<a href="http://www.firstclasstravel.ma/">
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
				</a>				   
				</td>				   <td align="right">				    
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>				   </td>			
				</tr>				 </tbody>				</table>
				<table width="90%"  cellspacing="1" border="0">';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Demande de devis :</b></td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $nom . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $prenom . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $tel . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $email . '</td>';            
			$message1 .= '</tr>';           
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Message</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $message . '</td>';            
			$message1 .= '</tr>';							
			$message1 .= '<tr>';                			
			$message1 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
				 Angle Bd. Mohamed 5 et Av. Hassen 1er N° 7 Bloc B - Imm. SIBAM2 4000 Marrakech Maroc - <a style="color:#fff;" href="mailto:bnora-fct@menara.ma" target="_blank">bnora-fct@menara.ma</a> - <a style="color:#fff;" href="mailto:bnora-fct@menara.ma" target="_blank">shamid-fct@menara.ma</a> - <a style="color:#fff;" href="mailto:bnora-fct@menara.ma" target="_blank">hmahi-fct@menara.ma</a> <br> <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
				</td>';                			
			$message1 .= '</tr>';            
			$message1 .= '</table>';            
			$message1 .= '</body>';			
            mail("infos.firstclass@gmail.com", $subject, $message1, $headers);
			//mail("tuninfo@benmacha.tn", $subject, $message1, $headers);
			mail($email, $subject, $message1, $headers);

			$array = array(
	    		'result' => "success",
				'email' => $to,
	    		);
	    	$response = new JsonResponse($array);
	    	return $response;    

        }else{

	    	$array = array(
	    		'method' => 'GET',
	    		);
	    	$response = new JsonResponse($array);
	    	return $response; 

        }

    }

}




