<?php 

namespace Btob\ResvoitureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Common\Tools;



use Btob\VoitureBundle\Entity\Voiture;

use Btob\ResvoitureBundle\Entity\Reservationvoiture;



use Btob\HotelBundle\Entity\Clients;



use Btob\VoitureBundle\Form\VoitureType;

use Btob\HotelBundle\Form\ClientsType;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->findAll();



      //  $entities = $this->get('knp_paginator')->paginate($findVoitures,$this->get('request')->query->get('page',1),12);



        return $this->render('BtobResvoitureBundle:Default:index.html.twig', array('entities' => $entities));

    }





    public function paginationAction($i)

    {

        $findVoitures = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->findAll();



        $entities = $this->get('knp_paginator')->paginate($findVoitures,$this->get('request')->query->get('page',$i),12);



        return $this->render('BtobResvoitureBundle:Default:index.html.twig', array('entities' => $entities,'findVoitures' => $findVoitures));

    }



    public function detailAction(Voiture $voiture)

    {

        return $this->render('BtobResvoitureBundle:Default:detail.html.twig', array('entry' => $voiture));

    }





    public function reservationvoitureAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $client = new Clients();



        $form = $this->createForm(new ClientsType(), $client);



        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->find($id);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            $cin = $post["cin"];

            // echo $defaultd .'/'.$defaulta ;exit;

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->handleRequest($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();

                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'), '/'));

                $datef = new \Datetime(Tools::explodedate($request->request->get('datef'), '/'));

                $message = $request->request->get('message');



                $reservation = new Reservationvoiture();

                $reservation->setClient($client);

                $reservation->setVoiture($entities);

                $reservation->setAgent($this->get('security.context')->getToken()->getUser());



                $reservation->setMessage($message);

                $reservation->setDated($dated);

                $reservation->setDatef($datef);



                $em->persist($reservation);

                $em->flush();





                $client = new Clients();



                $form = $this->createForm(new ClientsType(), $client);

                $request->getSession()->getFlashBag()->add('notisejour', 'Votre demande a bien été envoyé. Merci.');



                return $this->render('BtobResvoitureBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));





            }

        }



        return $this->render('BtobResvoitureBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));

    }



    public function listreservationvoitureAction()

    {


        $em = $this->getDoctrine()->getManager();





        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();
        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $this->getDoctrine()->getRepository("BtobResvoitureBundle:Reservationvoiture")->findAll();

        }else{
            $entities = $this->getDoctrine()->getRepository("BtobResvoitureBundle:Reservationvoiture")->findBy(array('agent' => $user));

        }
        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }


        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        return $this->render('BtobResvoitureBundle:Default:listreservationvoiture.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));





    }







 public function mesreservationvoitureAction()

    {

        $entities = $this->getDoctrine()->getRepository("BtobResvoitureBundle:Reservationvoiture")->findAll();

        return $this->render('BtobResvoitureBundle:Default:mesreservations.html.twig', array('entities' => $entities));

    }

















}

