<?php

namespace Btob\GolfBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Common\Tools;

use Btob\GolfBundle\Entity\Golf;
use Btob\GolfBundle\Entity\Reservationgolf;
use Btob\GolfBundle\Entity\Imgegolf;
use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobGolfBundle:Golf")->findby(array('act' => 1));
        return $this->render('BtobGolfBundle:Default:index.html.twig', array('entities' => $entities));
    }

    public function detailAction(Golf $Golf)
    {
        return $this->render('BtobGolfBundle:Default:detail.html.twig', array('entry' => $Golf));
    }
    public function detailreservationAction(Reservationgolf $Reservationgolf)
    {
        return $this->render('BtobGolfBundle:Default:show.html.twig', array('entry' => $Reservationgolf));
    }
    public function deletereservationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BtobGolfBundle:Reservationgolf')->find($id);
        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('golfs_reservation'));
    }
    public function listreservationgolfAction()
    {

        $user = $this->get('security.context')->getToken()->getUser();
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $em->getRepository('BtobSejourBundle:Reservationsejour')->findAll();

        }else{
            $entities = $em->getRepository('BtobSejourBundle:Reservationsejour')->findBy(array('agent' => $user));

        }
        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        return $this->render('BtobSejourBundle:Default:listreservation.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));


    }




    public function reservationsejourAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobGolfBundle:Golf")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $datea = new \Datetime(Tools::explodedate($request->request->get('dated'), '/'));
                $nbrad = $request->request->get('nbrad');
                $nbrenf = $request->request->get('nbrenf');
                $remarque = $request->request->get('remarque');

                $reservation = new Reservationsejour();
                $reservation->setClient($client);
                $reservation->setSejour($entities);
                $reservation->setDated($datea);
                $reservation->setAgent($this->get('security.context')->getToken()->getUser());
                $reservation->setNbradult($nbrad);
                $reservation->setNbrenf($nbrenf);
                $reservation->setRemarque($remarque);
                $em->persist($reservation);
                $em->flush();


                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notisejour', 'Votre message a bien été envoyé. Merci.');

                return $this->render('BtobSejourBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('BtobSejourBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }

}
