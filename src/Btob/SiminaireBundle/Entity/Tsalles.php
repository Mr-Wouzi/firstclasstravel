<?php

namespace Btob\SiminaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tcategories
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SiminaireBundle\Entity\TsallesRepository")
 */
class Tsalles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Reservationsim", inversedBy="tsalles")
     * @ORM\JoinColumn(name="reservationsim_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationsim;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="capacite", type="integer", nullable=true)
     */
    private $capacite;
    
        /**
     * @var string
     *
     * @ORM\Column(name="typesalle", type="string", length=255, nullable=true)
     */
    private $typesalle;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   
    /**
     * Set reservationsim
     *
     * @param \Btob\SiminaireBundle\Entity\Reservationsim $reservationsim
     * @return Tsalles
     */
    public function setReservationsim(\Btob\SiminaireBundle\Entity\Reservationsim $reservationsim = null)
    {
        $this->reservationsim = $reservationsim;

        return $this;
    }

    /**
     * Get reservationsim
     *
     * @return \Btob\SiminaireBundle\Entity\Reservationsim
     */
    public function getReservationsim()
    {
        return $this->reservationsim;
    }
    
    
    
     /**
     * Set capacite
     *
     * @param integer $capacite
     * @return Tsalles
     */
    public function setCapacite($capacite)
    {
        $this->capacite = $capacite;

        return $this;
    }

    /**
     * Get capacite
     *
     * @return integer 
     */
    public function getCapacite()
    {
        return $this->capacite;
    }

    /**
     * Set typesalle
     *
     * @param string $typesalle
     * @return Tsalles
     */
    public function setTypesalle($typesalle)
    {
        $this->typesalle = $typesalle;

        return $this;
    }

    /**
     * Get typesalle
     *
     * @return string 
     */
    public function getTypesalle()
    {
        return $this->typesalle;
    }
}
