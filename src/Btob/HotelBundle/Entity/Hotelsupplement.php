<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotelsupplement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelsupplementRepository")
 */
class Hotelsupplement {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="hotelsupplement")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;

    /**
     * @ORM\ManyToOne(targetEntity="Supplement", inversedBy="hotelsupplement")
     * @ORM\JoinColumn(name="supp_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $supplement;
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Hotelsupplement
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set supplement
     *
     * @param \Btob\HotelBundle\Entity\Supplement $supplement
     * @return Hotelsupplement
     */
    public function setSupplement(\Btob\HotelBundle\Entity\Supplement $supplement = null)
    {
        $this->supplement = $supplement;

        return $this;
    }

    /**
     * Get supplement
     *
     * @return \Btob\HotelBundle\Entity\Supplement 
     */
    public function getSupplement()
    {
        return $this->supplement;
    }
}
