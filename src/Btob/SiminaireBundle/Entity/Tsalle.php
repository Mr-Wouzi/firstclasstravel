<?php

namespace Btob\SiminaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tsalle
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SiminaireBundle\Entity\TsalleRepository")
 */
class Tsalle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Reservationsim", inversedBy="tsalle")
     * @ORM\JoinColumn(name="reservationsim_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationsim;
   /**
     * @var integer
     *
     * @ORM\Column(name="capacite", type="integer", nullable=true)
     */
    private $capacite;

    /**
     * @var array
     *
     * @ORM\Column(name="type", type="text",nullable=true)
     */
    private $type;

   
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reservationsim
     *
     * @param \Btob\SiminaireBundle\Entity\Reservationsim $reservationsim
     * @return Tsalle
     */
    public function setReservationsim(\Btob\SiminaireBundle\Entity\Reservationsim $reservationsim = null)
    {
        $this->reservationsim = $reservationsim;

        return $this;
    }

    /**
     * Get Reservationsim
     *
     * @return \Btob\SiminaireBundle\Entity\Reservationsim
     */
    public function getReservationsim()
    {
        return $this->reservationsim;
    }

    /**
     * Set capacite
     *
     * @param integer $capacite
     * @return Tsalle
     */
    public function setCapacite($capacite)
    {
        $this->capacite = $capacite;

        return $this;
    }

    /**
     * Get capacite
     *
     * @return integer 
     */
    public function getCapacite()
    {
        return $this->capacite;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Reservationsim
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

   
}
