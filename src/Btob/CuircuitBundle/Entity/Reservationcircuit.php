<?php

namespace Btob\CuircuitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationcircuit
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\ReservationcircuitRepository")
 */
class Reservationcircuit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
    
    

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;
    

    /**
     * @ORM\ManyToOne(targetEntity="Cuircuit", inversedBy="reservationcircuit")
     * @ORM\JoinColumn(name="cuircuit_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $cuircuit;
    
    /**
     * @ORM\ManyToOne(targetEntity="Cuircuitprice", inversedBy="reservationcircuit")
     * @ORM\JoinColumn(name="cuircuitprice_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $cuircuitprice;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationcircuit")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationcircuit")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

   
    /**
     * @ORM\OneToMany(targetEntity="Reservationcdetail", mappedBy="reservationcircuit", cascade={"remove"})
     */
    protected $reservationcdetail;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationcircuit
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    

    /**
     * Set total
     *
     * @param float $total
     * @return Reservationcircuit
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * Set cuircuitprice
     *
     * @param \Btob\CuircuitBundle\Entity\Cuircuitprice $cuircuitprice
     * @return Reservationcircuit
     */
    public function setCuircuitprice(\Btob\CuircuitBundle\Entity\Cuircuitprice $cuircuitprice = null)
    {
        $this->cuircuitprice = $cuircuitprice;

        return $this;
    }

    /**
     * Get cuircuitprice
     *
     * @return \Btob\CuircuitBundle\Entity\Cuircuitprice 
     */
    public function getCuircuitprice()
    {
        return $this->cuircuitprice;
    }
    
       
    
    /**
     * Set cuircuit
     *
     * @param \Btob\CuircuitBundle\Entity\Cuircuit $cuircuit
     * @return Reservationcircuit
     */
    public function setCuircuit(\Btob\CuircuitBundle\Entity\Cuircuit $cuircuit = null)
    {
        $this->cuircuit = $cuircuit;

        return $this;
    }

    /**
     * Get cuircuit
     *
     * @return \Btob\CuircuitBundle\Entity\Cuircuit 
     */
    public function getCuircuit()
    {
        return $this->cuircuit;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationcircuit
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Reservationcircuit
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

   


    /**
     * Add reservationcdetail
     *
     * @param \Btob\CuircuitBundle\Entity\Reservationcdetail $reservationcdetail
     * @return Reservationcircuit
     */
    public function addReservationcdetail(\Btob\CuircuitBundle\Entity\Reservationcdetail $reservationcdetail)
    {
        $this->reservationcdetail[] = $reservationcdetail;

        return $this;
    }

    /**
     * Remove reservationcdetail
     *
     * @param \Btob\CuircuitBundle\Entity\Reservationcdetail $reservationcdetail
     */
    public function removeReservationcdetail(\Btob\CuircuitBundle\Entity\Reservationcdetail $reservationcdetail)
    {
        $this->reservationcdetail->removeElement($reservationcdetail);
    }

    /**
     * Get reservationcdetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservationcdetail()
    {
        return $this->reservationcdetail;
    }


}
