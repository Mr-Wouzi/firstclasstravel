<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Room
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\RoomRepository")
 */
class Room {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;

    /**
     * @var integer
     *
     * @ORM\Column(name="capacity", type="integer")
     */
    private $capacity;

    /**
     * @ORM\OneToMany(targetEntity="Hotelroomsup", mappedBy="room", cascade={"remove"})
     */
    protected $roomsup;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Room
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Room
     */
    public function setAct($act) {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct() {
        return $this->act;
    }

    /**
     * Set capacity
     *
     * @param integer $capacity
     * @return Room
     */
    public function setCapacity($capacity) {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity
     *
     * @return integer 
     */
    public function getCapacity() {
        return $this->capacity;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roomsup = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add roomsup
     *
     * @param \Btob\HotelBundle\Entity\Hotelroomsup $roomsup
     * @return Room
     */
    public function addRoomsup(\Btob\HotelBundle\Entity\Hotelroomsup $roomsup)
    {
        $this->roomsup[] = $roomsup;

        return $this;
    }

    /**
     * Remove roomsup
     *
     * @param \Btob\HotelBundle\Entity\Hotelroomsup $roomsup
     */
    public function removeRoomsup(\Btob\HotelBundle\Entity\Hotelroomsup $roomsup)
    {
        $this->roomsup->removeElement($roomsup);
    }

    /**
     * Get roomsup
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoomsup()
    {
        return $this->roomsup;
    }
}
