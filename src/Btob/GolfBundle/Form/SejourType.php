<?php

namespace Btob\GolfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GolfType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('act', NULL, array('label' => 'active', 'required' => false))
            ->add('titre', 'text', array('label' => "Titre de golf", 'required' => true))
            ->add('libelle', 'text', array('label' => "Libellé promotion", 'required' => true))
            ->add('villed', 'text', array('label' => "Ville de départ ", 'required' => true))
            ->add('villea', 'text', array('label' => "ville d'arrivée ", 'required' => true))
            //->add('voyagecart', 'text', array('label' => "Voyages a la carte", 'required' => false))
            ->add('dureegolf', 'number', array('label' => "Durée du golf", 'required' => true))
            ->add('pindex', NULL, array('label' => "Inclure dans la liste des promos", 'required' => false))
            ->add('prix', NULL, array('label' => " A partir de :", 'required' => true))
            ->add('apartir', NULL, array('label' => " ", 'required' => false))
            ->add('pindex', NULL, array('label' => "Promo Index", 'required' => false))
            ->add('themes', NULL, array('label' => "Themes", 'required' => true))
            ->add('arrangement', NULL, array('label' => "Logement", 'required' => false))
            ->add('hotel', NULL, array('label' => "Hôtel : ", 'required' => false))
            ->add('nbretoile', NULL, array('label' => "Nombre d'étoiles :  ", 'required' => false))
            ->add('description', NULL, array('label' => "Description :  ", 'required' => false))/*  ->add('dated', 'date', array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'label' => "Date de séjour :  de ", 'required' => false
            ))
            ->add('datef', 'date', array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'label' => " à :", 'required' => false
            ))*/


        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\GolfBundle\Entity\Golf'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_golfbundle_golf';
    }
}
