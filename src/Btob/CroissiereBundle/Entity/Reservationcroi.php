<?php

namespace Btob\CroissiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationcroi
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CroissiereBundle\Entity\ReservationcroiRepository")
 */
class Reservationcroi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbradultes", type="integer" , nullable=true)
     */
    private $nbradultes;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationcroi")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationcroi")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Croissiere", inversedBy="reservationcroi")
     * @ORM\JoinColumn(name="Croissiere_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $croissiere;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrenf", type="integer" , nullable=true)
     */
    private $nbrenf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="contacte", type="string", length=255 , nullable=true)
     */
    private $contacte;

  /**
     * @var string
     *
     * @ORM\Column(name="villed", type="string", length=255 , nullable=true)
     */
    private $villed;

    /**
     * @var string
     *
     * @ORM\Column(name="classev", type="string", length=255 , nullable=true)
     */
    private $classev;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;


    /**
     * @var string
     *
     * @ORM\Column(name="demande", type="text" , nullable=true)
     */
    private $demande;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255 , nullable=true)
     */
    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="voyage", type="boolean" , nullable=true)
     */
    private $voyage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datem", type="date" , nullable=true)
     */
    private $datem;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbradultes
     *
     * @param integer $nbradultes
     * @return Reservationcroi
     */
    public function setNbradultes($nbradultes)
    {
        $this->nbradultes = $nbradultes;

        return $this;
    }
    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Get nbradultes
     *
     * @return integer 
     */
    public function getNbradultes()
    {
        return $this->nbradultes;
    }

    /**
     * Set nbrenf
     *
     * @param integer $nbrenf
     * @return Reservationcroi
     */
    public function setNbrenf($nbrenf)
    {
        $this->nbrenf = $nbrenf;

        return $this;
    }

    /**
     * Get nbrenf
     *
     * @return integer 
     */
    public function getNbrenf()
    {
        return $this->nbrenf;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Reservationcroi
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set villed
     *
     * @param string $villed
     * @return Reservationcroi
     */
    public function setVilled($villed)
    {
        $this->villed = $villed;

        return $this;
    }

    /**
     * Get villed
     *
     * @return string 
     */
    public function getVilled()
    {
        return $this->villed;
    }

    /**
     * Set classev
     *
     * @param string $classev
     * @return Reservationcroi
     */
    public function setClassev($classev)
    {
        $this->classev = $classev;

        return $this;
    }

    /**
     * Get classev
     *
     * @return string 
     */
    public function getClassev()
    {
        return $this->classev;
    }

    /**
     * Set demande
     *
     * @param string $demande
     * @return Reservationcroi
     */
    public function setDemande($demande)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return string 
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Reservationcroi
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set voyage
     *
     * @param boolean $voyage
     * @return Reservationcroi
     */
    public function setVoyage($voyage)
    {
        $this->voyage = $voyage;

        return $this;
    }

    /**
     * Get voyage
     *
     * @return boolean 
     */
    public function getVoyage()
    {
        return $this->voyage;
    }

    /**
     * Set datem
     *
     * @param \DateTime $datem
     * @return Reservationcroi
     */
    public function setDatem($datem)
    {
        $this->datem = $datem;

        return $this;
    }

    /**
     * Get datem
     *
     * @return \DateTime 
     */
    public function getDatem()
    {
        return $this->datem;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationcroi
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set contacte
     *
     * @param string $contacte
     * @return Reservationcroi
     */
    public function setContacte($contacte)
    {
        $this->contacte = $contacte;

        return $this;
    }

    /**
     * Get contacte
     *
     * @return string 
     */
    public function getContacte()
    {
        return $this->contacte;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Reservationcroi
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationcroi
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set croissiere
     *
     * @param \Btob\CroissiereBundle\Entity\Croissiere $croissiere
     * @return Reservationcroi
     */
    public function setCroissiere(\Btob\CroissiereBundle\Entity\Croissiere $croissiere = null)
    {
        $this->croissiere = $croissiere;

        return $this;
    }

    /**
     * Get croissiere
     *
     * @return \Btob\CroissiereBundle\Entity\Croissiere 
     */
    public function getCroissiere()
    {
        return $this->croissiere;
    }
}
