<?php

namespace Btob\BienetreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationbienetre
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\BienetreBundle\Entity\ReservationbienetreRepository")
 */
class Reservationbienetre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datear", type="date", nullable=true)
     */
    private $datear;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrpr", type="integer", nullable=true)
     */
    private $nbrpr;

   /**
     * @var string
     *
     * @ORM\Column(name="heur",  type="string", length=255, nullable=true)
     */
    private $heur;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrbien", type="integer", nullable=true)
     */
    private $nbrbien;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationbienetre")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Bienetre", inversedBy="reservationbienetre")
     * @ORM\JoinColumn(name="bienetre_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $bienetre;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationbienetre")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datear
     *
     * @param \DateTime $datear
     * @return Reservationbienetre
     */
    public function setDatear($datear)
    {
        $this->datear = $datear;

        return $this;
    }
    /**
     * construct
     */
    public function __construct(){
        $this->dcr=new \DateTime();
    }


    /**
     * Get datear
     *
     * @return \DateTime 
     */
    public function getDatear()
    {
        return $this->datear;
    }

    /**
     * Set nbrpr
     *
     * @param integer $nbrpr
     * @return Reservationbienetre
     */
    public function setNbrpr($nbrpr)
    {
        $this->nbrpr = $nbrpr;

        return $this;
    }

    /**
     * Get nbrpr
     *
     * @return integer 
     */
    public function getNbrpr()
    {
        return $this->nbrpr;
    }



    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationbienetre
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationbienetre
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Reservationbienetre
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set bienetre
     *
     * @param \Btob\BienetreBundle\Entity\Bienetre $bienetre
     * @return Reservationbienetre
     */
    public function setBienetre(\Btob\BienetreBundle\Entity\Bienetre $bienetre = null)
    {
        $this->bienetre = $bienetre;

        return $this;
    }

    /**
     * Get bienetre
     *
     * @return \Btob\BienetreBundle\Entity\Bienetre 
     */
    public function getBienetre()
    {
        return $this->bienetre;
    }

    /**
     * Set heur
     *
     * @param string $heur
     * @return Reservationbienetre
     */
    public function setHeur($heur)
    {
        $this->heur = $heur;

        return $this;
    }

    /**
     * Get heur
     *
     * @return string 
     */
    public function getHeur()
    {
        return $this->heur;
    }

    /**
     * Set nbrbien
     *
     * @param integer $nbrbien
     * @return Reservationbienetre
     */
    public function setNbrbien($nbrbien)
    {
        $this->nbrbien = $nbrbien;

        return $this;
    }

    /**
     * Get nbrbien
     *
     * @return integer 
     */
    public function getNbrbien()
    {
        return $this->nbrbien;
    }
}
