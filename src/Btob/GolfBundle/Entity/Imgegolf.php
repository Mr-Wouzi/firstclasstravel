<?php

namespace Btob\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgesej
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\GolfBundle\Entity\ImgegolfRepository")
 */
class Imgegolf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="Golf", inversedBy="imgegolf")
     * @ORM\JoinColumn(name="golf_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $golf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }


    /**
     * Set image
     *
     * @param string $image
     * @return Imgegolf
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgegolf
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set golf
     *
     * @param \Btob\GolfBundle\Entity\Golf $golf
     * @return Imgegolf
     */
    public function setGolf(\Btob\GolfBundle\Entity\Golf $golf = null)
    {
        $this->golf = $golf;

        return $this;
    }

    /**
     * Get golf
     *
     * @return \Btob\GolfBundle\Entity\Golf
     */
    public function getSejour()
    {
        return $this->golf;
    }
}
