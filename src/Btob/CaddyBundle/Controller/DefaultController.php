<?php

namespace Btob\CaddyBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Reservation;
use Btob\HotelBundle\Entity\Notification;
use Btob\HotelBundle\Entity\Reservationdetail;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $caddy = array();
        if (!$session->has('caddy')) {
            $session->set('caddy', $caddy);
        } else {
            $caddy = $session->get('caddy');
        }
        return $this->render('BtobCaddyBundle:Default:index.html.twig', array('caddy' => $caddy));
    }

    public function deleteAction($name)
    {
        $session = $this->getRequest()->getSession();
        $caddy = $session->get('caddy');
        unset($caddy[$name]);
        if ($name == 'resahotel') {
            unset($caddy['notif']);
        }
        $session->set('caddy', $caddy);
        return $this->redirect($this->generateUrl('btob_caddy_homepage'));

    }

    public function validationAction()
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        $caddy = $session->get('caddy');
        foreach ($caddy as $key => $value) {
            if ($key == "resahotel") {
                foreach ($value as $valhotel) {
                    $idhotel = $valhotel->getHotel()->getId();
                    $iduser = $valhotel->getUser()->getId();
                    $idclient = $valhotel->getClient()->getId();
                    $reservation = new Reservation();
                    $reservation->setAgeenfant($valhotel->getAgeenfant());
                    $reservation->setClient($this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->find($idclient));
                    $reservation->setDated($valhotel->getDated());
                    $reservation->setDatef($valhotel->getDatef());
                    $reservation->setEtat($valhotel->getEtat());
                    $reservation->setHotel($this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($idhotel));
                    $reservation->setNamead($valhotel->getNamead());
                    $reservation->setNameenf($valhotel->getNameenf());
                    $reservation->setNbnuit($valhotel->getNbnuit());
                    $reservation->setTotal($valhotel->getTotal());
                    $reservation->setTotalint($valhotel->getTotalint());
                    $reservation->setUser($this->getDoctrine()->getRepository("UserUserBundle:User")->find($iduser));
                    $em->persist($reservation);
                    $em->flush();
                    foreach ($valhotel->getReservationdetail() as $kx => $vx) {
                        $resadetail = new Reservationdetail();
                        $resadetail->setAd($vx->getAd());
                        $resadetail->setPrice($vx->getPrice());
                        $resadetail->setArrangement($vx->getArrangement());
                        $resadetail->setEnf($vx->getEnf());
                        $resadetail->setEvent($vx->getEvent());
                        $resadetail->setRoomname($vx->getRoomname());
                        $resadetail->setSupp($vx->getSupp());
                        $resadetail->setEvent($vx->getEvent());
                        $resadetail->setReservation($reservation);
                        $em->persist($resadetail);
                        $em->flush();
                    }
                }
            }
            if ($key == "notif") {
                foreach ($value as $notification) {
                    $iduser = $notification->getUser()->getId();
                    $notif = new Notification();
                    $notif->setUser($this->getDoctrine()->getRepository("UserUserBundle:User")->find($iduser));
                    $notif->setNotif($notification->getNotif());
                    $em->persist($notif);
                    $em->flush();
                }
            }
        }
        //$caddy=array();
        $session->remove('caddy');
        return $this->render('BtobCaddyBundle:Default:validation.html.twig', array());
    }
}
