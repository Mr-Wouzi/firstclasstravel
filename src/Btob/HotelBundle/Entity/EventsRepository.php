<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * EventsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EventsRepository extends EntityRepository
{
    public function getEventHotel($hotelId,$dated,$datef){
        $qb = $this->createQueryBuilder('r')
            ->where("r.dated <='" . $datef->format("Y-m-d") . "'")
            ->andWhere("r.dated >= '" . $dated->format("Y-m-d") . "'")
            ->andWhere("r.hotel= $hotelId")
            ->getQuery()
            ->getResult();
        //echo $qb->getQuery()->getSQL();exit;
        return $qb;
    }
}
