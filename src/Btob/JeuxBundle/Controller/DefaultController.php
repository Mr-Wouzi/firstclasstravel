<?php

namespace Btob\JeuxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;

use Btob\JeuxBundle\Entity\Choix;
use Btob\JeuxBundle\Entity\Jeu;
use Btob\JeuxBundle\Entity\Reponse;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\CuircuitBundle\Entity\Resacircui;
use Symfony\Component\Validator\Constraints\DateTime;
class DefaultController extends Controller
{



    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $Question = $this->getDoctrine()->getRepository("BtobJeuxBundle:Jeu")->findBy( array(),array(),
                     1 );

        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $question = $request->request->get('question');
                $optionsRadios = $request->request->get('optionsRadios');
                if($optionsRadios != null){
                $Reponse = new Reponse();
                    $Reponse->setChoix($optionsRadios);
                    $Reponse->setQuestion($question);
                    $Reponse->setAgent($this->get('security.context')->getToken()->getUser());
                    $Reponse->setClient($client);
                    $em->persist($Reponse);
                    $em->flush();


                    $client = new Clients();

                    $form = $this->createForm(new ClientsType(), $client);
                    $request->getSession()->getFlashBag()->add('notiQuestion', 'Votre message a bien été envoyé. Merci.');


                }
                return $this->redirect($this->generateUrl('btob_jeux_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobJeuxBundle:Default:reservation.html.twig', array('form' => $form->createView() , 'Question'=>$Question));
    }


    public function listreponseAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobJeuxBundle:Reponse")->findAll();
        return $this->render('BtobJeuxBundle:Default:listreservation.html.twig', array('entities' => $entities));
    }

    public function deletereponseAction( $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BtobJeuxBundle:Reponse')->find($id);

        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('btob_jeux_list_homepage'));
    }



}
