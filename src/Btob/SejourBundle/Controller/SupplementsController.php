<?php

namespace Btob\SejourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\SejourBundle\Entity\Supplements;
use Symfony\Component\HttpFoundation\Request;
use Btob\SejourBundle\Form\SupplementsType;
use Symfony\Component\HttpFoundation\JsonResponse;

class SupplementsController extends Controller {

    public function indexAction($sejourid) {
        $sejour = $this->getDoctrine()
                ->getRepository('BtobSejourBundle:Sejour')
                ->find($sejourid);
        return $this->render('BtobSejourBundle:Supplements:index.html.twig', array('entities' => $sejour->getSupplements(), "sejourid" => $sejourid));
    }

    public function addAction($sejourid) {
        $sejour = $this->getDoctrine()->getRepository('BtobSejourBundle:Sejour')->find($sejourid);
        $Supplements = new Supplements();
        $form = $this->createForm(new SupplementsType(), $Supplements);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Supplements->setSejour($sejour);
                $em->persist($Supplements);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplements_homepage', array("sejourid" => $sejourid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobSejourBundle:Supplements:form.html.twig', array('form' => $form->createView(), "sejourid" => $sejourid));
    }

    public function editAction($id, $sejourid) {
        $request = $this->get('request');
        $Supplements = $this->getDoctrine()
                ->getRepository('BtobSejourBundle:Supplements')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementsType(), $Supplements);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplements_homepage', array("sejourid" => $sejourid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobSejourBundle:Supplements:form.html.twig', array('form' => $form->createView(), 'id' => $id, "sejourid" => $sejourid)
        );
    }

    public function deleteAction(Supplements $Supplements, $sejourid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Supplements) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
        $em->remove($Supplements);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplements_homepage', array("sejourid" => $sejourid)));
    }

}
