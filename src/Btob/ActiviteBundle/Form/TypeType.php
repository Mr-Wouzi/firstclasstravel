<?php

namespace Btob\ActiviteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', NULL, array('label' =>'Type', 'required' => false))
            ->add('act', NULL, array('label' =>'active', 'required' => false))


        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\ActiviteBundle\Entity\Type'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_activitebundle_type';
    }
}
