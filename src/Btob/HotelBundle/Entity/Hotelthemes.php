<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotelthemes
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelthemesRepository")
 */
class Hotelthemes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="hotelthemes")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;
    /**
     * @ORM\ManyToOne(targetEntity="Themes", inversedBy="hotelthemes")
     * @ORM\JoinColumn(name="themes_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $themes;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Hotelthemes
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set themes
     *
     * @param \Btob\HotelBundle\Entity\Themes $themes
     * @return Hotelthemes
     */
    public function setThemes(\Btob\HotelBundle\Entity\Themes $themes = null)
    {
        $this->themes = $themes;

        return $this;
    }

    /**
     * Get themes
     *
     * @return \Btob\HotelBundle\Entity\Themes 
     */
    public function getThemes()
    {
        return $this->themes;
    }
}
