<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;use Btob\OmraBundle\Entity\Omra;use Btob\OmraBundle\Entity\Reservationomra;

use Btob\HotelBundle\Common\Tools;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use User\UserBundle\Entity\User;use Btob\CuircuitBundle\Entity\Cuircuit;

use Btob\CuircuitBundle\Entity\Resacircui;use Btob\BienetreBundle\Entity\Reservationbienetre;

use Btob\BienetreBundle\Entity\Bienetre;
use Symfony\Component\HttpFoundation\Request;


class OmraController extends Controller


{







    public function indexAction(Request $request)



    {

        $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Omra'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;

        $omra = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findAll();

  $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $omra,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );

        $em = $this->getDoctrine()->getManager();







        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();



        return $this->render('FrontBtobBundle:Omra:index.html.twig', array(



            'entities' => $entities,



            'banner' => $banner,







        ));



    }



    // omra







    public function detailAction(Omra $omra)



    {

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($omra->getSeotitle())
            ->addMeta('name', 'description', $omra->getSeodescreption())
            ->addMeta( 'name', 'keywords',$omra->getSeokeyword())
        ;

        return $this->render('FrontBtobBundle:Omra:detail.html.twig', array('entry' => $omra));



    }



    public function reservationAction(Omra $omra)



    {



        $em = $this->getDoctrine()->getManager();


        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($omra->getSeotitle())
            ->addMeta('name', 'description', $omra->getSeodescreption())
            ->addMeta( 'name', 'keywords',$omra->getSeokeyword())
        ;
        $client = new Clients();



        $form = $this->createForm(new ClientsType(), $client);



        $request = $this->get('request');



        if ($request->getMethod() == 'POST') {



            $post = $request->request->get('btob_hotelbundle_clients');



            //Tools::dump($post["cin"],true);



            $cin = $post["cin"];



            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));



            $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');



            if ($testclient != null) {



                $client = $testclient;



            }



            $form->bind($request);



            if ($form->isValid()) {



                $em->persist($client);



                $em->flush();



                $resa=new Reservationomra();







                $date = Tools::explodedate($request->request->get("dated"),'/');



                $resa->setDated(new \DateTime($date));



                $resa->setUser($User);



                $resa->setClient($client);



                $resa->setBebe($request->request->get("bebe"));



                $resa->setAdulte($request->request->get("adulte"));



                $resa->setEnfant($request->request->get("enfant"));



                $resa->setComment($request->request->get("body"));



                $resa->setOmra($omra);



                $em->persist($resa);



                $em->flush();

                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

                  

                $to = $resa->getClient()->getEmail();
                //$to = "afef.tuninfo@gmail.com";
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = "MENA TOURS: Réservation Omra" ;
                $headers = "From:MENATOURS - Réservation Vol <contact@menatours.com>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message1 .='


				' .'
                      <img id="site-logo" src="http://www.menatours.com/front/images/logo.png" alt="Mena Tours">
                    <br /><br />
				 Cher Madame/Monsieur,<br /><br />
				 Merci pour la confiance, nous notons votre réservation qui reste en instance de confirmation jusqu’au payement.<br />
				 Moyens de payement soit par :<br />
				- Payement directement à notre agence MENA TOURS.<br />
				 Adresse : 21 Avenue USA - 1er étage immeuble les Jasmins Belvédère - 1002 Tunis - Tunisie.<br />
				- Virement bancaire avec l’envoi d’un justificatif de payement ( virement scanné et envoyé par email au contact@menatours.com ou par fax au (+ 216) 71 844 335).<br />
					
					Un Bon Voucher vous serait remis sitôt payement effectué.<br /><br />
					<b>Contact</b><br />
<b>Adresse:</b> 21 Avenue USA - 1er étage immeuble les Jasmins Belvédère - 1002 Tunis - Tunisie<br />
<b>Tel:</b> (+216) 71 844 335 / (+216) 31 332 332<br />
<b>Mobile:</b> (+216) 53 605 073 / (+216) 58 558 502<br />
<b>Fax:</b> (+ 216) 71 844 335<br />
<b>E-mail:</b> contact@menatours.com<br />
<b>Website:</b> www.menatours.com


  </body>';


                mail($to, $subject, $message1, $headers);


                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                //$too = "afef.tuninfo@gmail.com";
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservation Omra" ;
                $header = "From:MENATOURS - Réservation Omra <".$to.">\n";
                $header .= "Reply-To:" .$resa->getUser()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='


                      <img id="site-logo" src="http://www.menatours.com/front/images/logo.png" alt="Mena Tours">
                    <br /><br />


				 '  . $resa->getUser()->getName() .',<br>
				 vous avez reçu une réservation omra  , merci de consulter votre backoffice .




  </body>';


                mail($too, $subjects, $message2, $header);

            } else {
                echo $form->getErrors();
            }

                $request->getSession()->getFlashBag()->add('notifomrafront', 'Votre réservation a bien été envoyé. Merci.');



                return $this->redirect($this->generateUrl('front_btob_omra_homepage'));







        }



        return $this->render('FrontBtobBundle:Omra:reservation.html.twig', array('form' => $form->createView(),'entry'=>$omra));



    }















    public function validationAction(){



        return $this->render('BtobOmraBundle:Default:validation.html.twig', array());



    }











}



