<?php

namespace Btob\TeamBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TeamBundle\Entity\TeamRepository")
 */
class Team
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255 , nullable=true)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255 , nullable=true)
     */
    private $type;
    /**
     * @var date
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255 , nullable=true)
     */
    private $ville;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbretoiles", type="integer",  nullable=true , nullable=true)
     */
    private $nbretoiles;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float" , nullable=true)
     */
    private $prix;

    /**
     * @var boolean
     *
     * @ORM\Column(name="apartir", type="boolean" , nullable=true)
     */
    private $apartir;
    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean" , nullable=true)
     */
    private $active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pindex", type="boolean" , nullable=true)
     */
    private $pindex;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrjr", type="integer" , nullable=true)
     */
    private $nbrjr;
    /**
     * @ORM\OneToMany(targetEntity="Imgteam", mappedBy="team")
     */
    protected $imgteam;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text" , nullable=true)
     */
    private $description;
    public  function  __construct(){
        $this->dcr = new \DateTime();
        $this->imgteam =new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Team
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="team")
     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $arrangement;

    /**
     * Set type
     *
     * @param string $type
     * @return Team
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Team
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set nbretoiles
     *
     * @param integer $nbretoiles
     * @return Team
     */
    public function setNbretoiles($nbretoiles)
    {
        $this->nbretoiles = $nbretoiles;

        return $this;
    }

    /**
     * Get nbretoiles
     *
     * @return integer 
     */
    public function getNbretoiles()
    {
        return $this->nbretoiles;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Team
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }



    /**
     * Set pindex
     *
     * @param boolean $pindex
     * @return Team
     */
    public function setPindex($pindex)
    {
        $this->pindex = $pindex;

        return $this;
    }

    /**
     * Get pindex
     *
     * @return boolean 
     */
    public function getPindex()
    {
        return $this->pindex;
    }

    /**
     * Set nbrjr
     *
     * @param integer $nbrjr
     * @return Team
     */
    public function setNbrjr($nbrjr)
    {
        $this->nbrjr = $nbrjr;

        return $this;
    }

    /**
     * Get nbrjr
     *
     * @return integer 
     */
    public function getNbrjr()
    {
        return $this->nbrjr;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Team
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Team
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }



    /**
     * Set arrangement
     *
     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement
     * @return Team
     */
    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return \Btob\HotelBundle\Entity\Arrangement 
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }



    /**
     * Set apartir
     *
     * @param boolean $apartir
     * @return Team
     */
    public function setApartir($apartir)
    {
        $this->apartir = $apartir;

        return $this;
    }

    /**
     * Get apartir
     *
     * @return boolean 
     */
    public function getApartir()
    {
        return $this->apartir;
    }

    /**
     * Add imgteam
     *
     * @param \Btob\TeamBundle\Entity\Imgteam $imgteam
     * @return Team
     */
    public function addImgteam(\Btob\TeamBundle\Entity\Imgteam $imgteam)
    {
        $this->imgteam[] = $imgteam;

        return $this;
    }

    /**
     * Remove imgteam
     *
     * @param \Btob\TeamBundle\Entity\Imgteam $imgteam
     */
    public function removeImgteam(\Btob\TeamBundle\Entity\Imgteam $imgteam)
    {
        $this->imgteam->removeElement($imgteam);
    }

    /**
     * Get imgteam
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImgteam()
    {
        return $this->imgteam;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Team
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
}
