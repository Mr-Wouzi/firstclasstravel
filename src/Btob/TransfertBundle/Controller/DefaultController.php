<?php

namespace Btob\TransfertBundle\Controller;

use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\TransfertBundle\Entity\Transfert;
use Btob\TransfertBundle\Entity\Tcategories;
use Btob\TransfertBundle\Form\TransfertType;
use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $transfert = new Transfert();
        $form = $this->createForm(new ClientsType(), $client);
        $formTrans = $this->createForm(new TransfertType(), $transfert);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            $dated = new \Datetime($request->request->get('dated'));
            $datea = new \Datetime(Tools::explodedate($request->request->get('datea'), '/'));

           // $datea = new \Datetime($request->request->get('datea'));
            $defaultd = $request->request->get('tp-defaultd');
            $defaulta = $request->request->get('tp-defaulta');
            $organisme = $request->request->get('organisme');
            $pointd = $request->request->get('pointd');
            $villed = $request->request->get('villed');
            $villea = $request->request->get('pointd');
            $cat = $request->request->get('cat');
            $nb = $request->request->get('nb');


            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();


                $transfert->setClient($client);
                $transfert->setHeurd($defaultd);
                $transfert->setHeura($defaulta);
                $transfert->setOrganisme($organisme);
                $transfert->setDated($dated);
                $transfert->setDatea($datea);
                $transfert->setVillea($villed);
                $transfert->setVilled($villea);
                $transfert->setPointd($pointd);

                $transfert->setAgent($this->get('security.context')->getToken()->getUser());

                $em->persist($transfert);
                $em->flush();

                foreach( $cat as $key=>$value){
                    $catg = new Tcategories();
                    $catg->setTransfert($transfert);
                    $catg->setNbr($nb[$key]);
                    $catg->setCategories($cat[$key]);

                    $em->persist($catg);
                    $em->flush();

                }
                $client = new Clients();
                $transfert = new Transfert();
                $form = $this->createForm(new ClientsType(), $client);
                $formTrans = $this->createForm(new TransfertType(), $transfert);

                // return $this->redirect($this->generateUrl('btob_omra_validation'));
                $request->getSession()->getFlashBag()->add('notice', 'Votre message a bien été envoyé. Merci.');

                return $this->render('BtobTransfertBundle:Default:index.html.twig', array('client' => $form->createView(), 'transfert' => $formTrans->createView(),));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobTransfertBundle:Default:index.html.twig', array('client' => $form->createView(), 'transfert' => $formTrans->createView(),));
    }
}
