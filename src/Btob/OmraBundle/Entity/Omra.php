<?php





namespace Btob\OmraBundle\Entity;





use Doctrine\ORM\Mapping as ORM;





/**


 * omra


 *


 * @ORM\Table()


 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\OmraRepository")


 */


class Omra


{


    /**


     * @var integer


     *


     * @ORM\Column(name="id", type="integer")


     * @ORM\Id


     * @ORM\GeneratedValue(strategy="AUTO")


     */


    private $id;





    /**


     * @ORM\OneToMany(targetEntity="Imgomra", mappedBy="omra")


     */


    protected $imgomra;





    /**


     * @var string


     *


     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)


     */


    private $libelle;





 





    /**


     * @var string


     *


     * @ORM\Column(name="description", type="text", nullable=true )


     */


    private $description;





 





    /**


     * @var float


     *


     * @ORM\Column(name="prix", type="float", nullable=true)


     */


    private $prix;





    /**

     * @var string

     *

     * @ORM\Column(name="seotitle", type="string", length=255 , nullable=true)

     */

    private $seotitle;

    /**

     * @var string

     *

     * @ORM\Column(name="seodescreption", type="string", length=255 , nullable=true)

     */

    private $seodescreption;

    /**

     * @var string

     *

     * @ORM\Column(name="seokeyword", type="string", length=255 , nullable=true)

     */

    private $seokeyword;


















    /**


     * Get id


     *


     * @return integer


     */


    public function getId()


    {


        return $this->id;


    }



    /**

     * Set seotitle

     *

     * @param string $seotitle

     * @return Omra

     */

    public function setSeotitle($seotitle)

    {

        $this->seotitle = $seotitle;



        return $this;

    }



    /**

     * Get seotitle

     *

     * @return string

     */

    public function getSeotitle()

    {

        return $this->seotitle;

    }


    /**

     * Set seodescreption

     *

     * @param string $seodescreption

     * @return Omra

     */

    public function setSeodescreption($seodescreption)

    {

        $this->seodescreption = $seodescreption;



        return $this;

    }



    /**

     * Get seodescreption

     *

     * @return string

     */

    public function getSeodescreption()

    {

        return $this->seodescreption;

    }


    /**

     * Set seokeyword
     *

     * @param string $seokeyword

     * @return Omra

     */

    public function setSeokeyword($seokeyword)

    {

        $this->seokeyword = $seokeyword;



        return $this;

    }



    /**

     * Get seodkeyword

     *

     * @return string

     */

    public function getSeokeyword()

    {

        return $this->seokeyword;

    }




    /**


     * Set titre


     *


     * @param string $titre


     * @return omra


     */


    public function setTitre($titre)


    {


        $this->titre = $titre;





        return $this;


    }





    /**


     * Get titre


     *


     * @return string


     */


    public function getTitre()


    {


        return $this->titre;


    }





    /**


     * Set libelle


     *


     * @param string $libelle


     * @return omra


     */


    public function setLibelle($libelle)


    {


        $this->libelle = $libelle;





        return $this;


    }





    /**


     * Get libelle


     *


     * @return string


     */


    public function getLibelle()


    {


        return $this->libelle;


    }





    /**


     * Set equipements


     *


     * @param string $equipements


     * @return omra


     */


    public function setEquipements($equipements)


    {


        $this->equipements = $equipements;





        return $this;


    }





    /**


     * Get equipements


     *


     * @return string


     */


    public function getEquipements()


    {


        return $this->equipements;


    }





    /**


     * Set description


     *


     * @param string $description


     * @return omra


     */


    public function setDescription($description)


    {


        $this->description = $description;





        return $this;


    }





    /**


     * Get description


     *


     * @return string


     */


    public function getDescription()


    {


        return $this->description;


    }





    /**


     * Set class


     *


     * @param string $class


     * @return omra


     */


    public function setClass($class)


    {


        $this->class = $class;





        return $this;


    }





    /**


     * Get class


     *


     * @return string


     */


    public function getClass()


    {


        return $this->class;


    }





    /**


     * Set optionvoiture


     *


     * @param string $optionvoiture


     * @return omra


     */


    public function setOptionvoiture($optionvoiture)


    {


        $this->optionvoiture = $optionvoiture;





        return $this;


    }





    /**


     * Get optionvoiture


     *


     * @return string


     */


    public function getOptionvoiture()


    {


        return $this->optionvoiture;


    }





    /**


     * Set prix


     *


     * @param float $prix


     * @return omra


     */


    public function setPrix($prix)


    {


        $this->prix = $prix;





        return $this;


    }





    /**


     * Get prix


     *


     * @return float


     */


    public function getPrix()


    {


        return $this->prix;


    }





    /**


     * Set libelleproms


     *


     * @param string $libelleproms


     * @return omra


     */


    public function setLibelleproms($libelleproms)


    {


        $this->libelleproms = $libelleproms;





        return $this;


    }





    /**


     * Get libelleproms


     *


     * @return string


     */


    public function getLibelleproms()


    {


        return $this->libelleproms;


    }


    public function __toString() {


        return (string) $this->getClass();





    }














    /**


     * Constructor


     */


    public function __construct()


    {


        $this->imgomra = new \Doctrine\Common\Collections\ArrayCollection();


    }





    /**


     * Add imgomra


     *


     * @param \Btob\OmraBundle\Entity\Imgomra $imgomra


     * @return omra


     */


    public function addImgomra(\Btob\OmraBundle\Entity\Imgomra $imgomra)


    {


        $this->imgomra[] = $imgomra;


    


        return $this;


    }





    /**


     * Remove imgomra


     *


     * @param \Btob\OmraBundle\Entity\Imgomra $imgomra


     */


    public function removeImgomra(\Btob\OmraBundle\Entity\Imgomra $imgomra)


    {


        $this->imgomra->removeElement($imgomra);


    }





    /**


     * Get imgomra


     *


     * @return \Doctrine\Common\Collections\Collection 


     */


    public function getImgomra()


    {


        return $this->imgomra;


    }


}


