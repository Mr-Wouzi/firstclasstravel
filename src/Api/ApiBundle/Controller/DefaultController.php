<?php

namespace Api\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends Controller
{
    public function indexAction()
    {
		$data = array(
			'powred' => 'WebCarre',
			'link' => 'http://www.webcarre.ma/',
		    'version' => '1.0.0',
		);
		$response = new JsonResponse($data);
		return $response;
    }


    public function infoAction(){
    	$array = array();
		$response = new JsonResponse($array);
		return $response;
    }


    public function hotelsearchAction(){

    	/********************************************
    	PArametre d'entrer:
    		Date d'entere et sortie
    		nombre de chambre 
    		nombre d'adulte
    		nombre enfant

    	resultat sou forme Array
    		ID               : integer
    		nom hotel        : String
    		lien image hotel : String
    		nombre etoil     : integer
    		prie             : Double
    		prootion         : Boolean

    	********************************************/

    	$array = array();

    	/************** Resultat Statique **********/	
    	$hotel1 = array(
    		'id' => '1' ,
    		'name' => 'Sofitel Lounge',
    		'picture' => './medias/Kenzi Farah Hotel/Kenzi_Farah_8583_copie.jpg',
    		'stars' => '5',
    		'promo' => 'true',
    		'price' => '380' );

    	array_push($array, $hotel1);

    	$hotel2 = array(
    		'id' => '2' ,
    		'name' => 'Blue Sea le Printemps',
    		'picture' => './medias/Kenzi Farah Hotel/Kenzi_Farah_8583_copie.jpg',
    		'stars' => '2',
    		'promo' => 'false',
    		'price' => '410' );

    	array_push($array, $hotel2);
    	/************** Resultat Statique **********/	


		$response = new JsonResponse($array);
		return $response;
    }
    

    public function hotelAction(){
    	$array = array();
		$response = new JsonResponse($array);
		return $response;

    }


    public function hotelinfoAction($id){
    	$array = array(
    		'id' => $id,
    		'name' => 'Blue Sea le Printemps',
    		'picture' => './medias/Kenzi Farah Hotel/Kenzi_Farah_8583_copie.jpg',
    		'stars' => '2',
    		);
    	$response = new JsonResponse($array);
    	return $response;  
    }


}
