<?php

namespace Btob\SejourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\SejourBundle\Entity\Sejourprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\SejourBundle\Form\SejourpriceType;
use Symfony\Component\HttpFoundation\JsonResponse;



class SejourpriceController extends Controller
{

    public function indexAction($sejourid)
    {
        $sejour = $this->getDoctrine()
            ->getRepository('BtobSejourBundle:Sejour')
            ->find($sejourid);
	    $name =  $sejour->getLibelle();
        $price = array();
        foreach ($sejour->getSejourprice() as $value) {
           
                $price[] = $value;
           
        }
        return $this->render('BtobSejourBundle:Sejourprice:index.html.twig', array(
            "entities" => $price,
            "sejourid" => $sejourid,
	    "name" => $name,
        ));
    }


    public function addAction($sejourid)
    {
        $sejour = $this->getDoctrine()->getRepository('BtobSejourBundle:Sejour')->find($sejourid);
      
        $allsejourprice = $this->getDoctrine()->getRepository('BtobSejourBundle:Sejourprice')->findBy(array('sejour' => $sejour));
      
        

        $sejourprice = new Sejourprice();
        $form = $this->createForm(new SejourpriceType(), $sejourprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $sejourprice->setSejour($sejour);
                $em->persist($sejourprice);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_sejourprice_homepage', array("sejourid" => $sejourid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobSejourBundle:Sejourprice:form.html.twig', array(
            'form' => $form->createView(),
            "sejour" => $sejour

        ));
    }



    public function editAction($id, $sejourid)
    {
        $sejour = $this->getDoctrine()->getRepository('BtobSejourBundle:Sejour')->find($sejourid);
        $request = $this->get('request');
        $sejourprice = $this->getDoctrine()
            ->getRepository('BtobSejourBundle:Sejourprice')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SejourpriceType(), $sejourprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $allsejourprice = $this->getDoctrine()->getRepository('BtobSejourBundle:Sejourprice')->findBy(array('sejour' => $sejour));
      
    
       
        if ($form->isValid()) {

            $sejourprice->setDmj(new \DateTime());
            $em->flush();


            return $this->redirect($this->generateUrl('btob_sejourprice_homepage', array("sejourid" => $sejourid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobSejourBundle:Sejourprice:edit.html.twig', array('form' => $form->createView(), 'price' => $sejourprice, 'id' => $id, "sejour" => $sejour)
        );
    }

  
    public function deleteAction(Sejourprice $sejourprice, $sejourid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$sejourprice) {
            throw new NotFoundHttpException("Sejourprice non trouvée");
        }
        $em->remove($sejourprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_sejourprice_homepage', array("sejourid" => $sejourid)));
    }



}












