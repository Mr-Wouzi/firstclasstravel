<?php

namespace Btob\CuircuitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuitprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\CuircuitBundle\Form\CuircuitpriceType;
use Symfony\Component\HttpFoundation\JsonResponse;



class CuircuitpriceController extends Controller
{

    public function indexAction($cuircuitid)
    {
        $cuircuit = $this->getDoctrine()
            ->getRepository('BtobCuircuitBundle:Cuircuit')
            ->find($cuircuitid);
	    $name =  $cuircuit->getLibelle();
        $price = array();
        foreach ($cuircuit->getCuircuitprice() as $value) {
           
                $price[] = $value;
           
        }
        return $this->render('BtobCuircuitBundle:Cuircuitprice:index.html.twig', array(
            "entities" => $price,
            "cuircuitid" => $cuircuitid,
	    "name" => $name,
        ));
    }


    public function addAction($cuircuitid)
    {
        $cuircuit = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuit')->find($cuircuitid);
      
        $allcuircuitprice = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuitprice')->findBy(array('cuircuit' => $cuircuit));
      
        

        $cuircuitprice = new Cuircuitprice();
        $form = $this->createForm(new CuircuitpriceType(), $cuircuitprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $cuircuitprice->setCuircuit($cuircuit);
                $em->persist($cuircuitprice);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_cuircuitprice_homepage', array("cuircuitid" => $cuircuitid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCuircuitBundle:Cuircuitprice:form.html.twig', array(
            'form' => $form->createView(),
            "cuircuit" => $cuircuit

        ));
    }



    public function editAction($id, $cuircuitid)
    {
        $cuircuit = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuit')->find($cuircuitid);
        $request = $this->get('request');
        $cuircuitprice = $this->getDoctrine()
            ->getRepository('BtobCuircuitBundle:Cuircuitprice')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new CuircuitpriceType(), $cuircuitprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $allcuircuitprice = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuitprice')->findBy(array('cuircuit' => $cuircuit));
      
    
       
        if ($form->isValid()) {

            $cuircuitprice->setDmj(new \DateTime());
            $em->flush();


            return $this->redirect($this->generateUrl('btob_cuircuitprice_homepage', array("cuircuitid" => $cuircuitid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobCuircuitBundle:Cuircuitprice:edit.html.twig', array('form' => $form->createView(), 'price' => $cuircuitprice, 'id' => $id, "cuircuit" => $cuircuit)
        );
    }

  
    public function deleteAction(Cuircuitprice $cuircuitprice, $cuircuitid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$cuircuitprice) {
            throw new NotFoundHttpException("Circuitprice non trouvée");
        }
        $em->remove($cuircuitprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_cuircuitprice_homepage', array("cuircuitid" => $cuircuitid)));
    }



}












