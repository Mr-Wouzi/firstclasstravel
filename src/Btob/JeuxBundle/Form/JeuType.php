<?php

namespace Btob\JeuxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class JeuType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('dcr')

            ->add('question', NULL, array('label' =>'question', 'required' => false))

            ->add('description')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\JeuxBundle\Entity\Jeu'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_jeuxbundle_jeu';
    }
}
