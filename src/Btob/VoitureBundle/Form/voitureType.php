<?php

namespace Btob\VoitureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class voitureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text', array('label' => "Titre ", 'required' => true))
            ->add('libelle', 'text', array('label' => "Libelle ", 'required' => true))
            ->add('equipements', 'textarea', array('label' => "Equipements :", 'required' => false))
            ->add('description', 'text', array('label' => "Description", 'required' => false))
            ->add('class','entity', array(
                'class' => 'BtobClasseBundle:Classe',
                'property' => 'nom',
            ))

            ->add('optionvoiture','entity', array(
                'class' => 'BtobOptionvoitureBundle:Opvoiture',
                'property' => 'nom',
            ))
            ->add('prix', 'text', array('label' => "Prix ", 'required' => false))
            ->add('libelleproms', 'text', array('label' => "Libelle Proms :", 'required' => false))
        ;

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\VoitureBundle\Entity\Voiture'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_voiturebundle_voiture';
    }
}
