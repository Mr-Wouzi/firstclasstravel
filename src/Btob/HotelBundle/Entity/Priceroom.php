<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Priceroom
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PriceroomRepository")
 */
class Priceroom
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;

    /**
     * @ORM\ManyToOne(targetEntity="Hotelprice", inversedBy="Priceroom", cascade={"persist"})
     * @ORM\JoinColumn(name="price_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotelprice;
    /**
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="Priceroom", cascade={"persist"})
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $room;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     * @return Priceroom
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set hotelprice
     *
     * @param \Btob\HotelBundle\Entity\Hotelprice $hotelprice
     * @return Priceroom
     */
    public function setHotelprice(\Btob\HotelBundle\Entity\Hotelprice $hotelprice = null)
    {
        $this->hotelprice = $hotelprice;

        return $this;
    }

    /**
     * Get hotelprice
     *
     * @return \Btob\HotelBundle\Entity\Hotelprice 
     */
    public function getHotelprice()
    {
        return $this->hotelprice;
    }

    /**
     * Set room
     *
     * @param \Btob\HotelBundle\Entity\Room $room
     * @return Priceroom
     */
    public function setRoom(\Btob\HotelBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \Btob\HotelBundle\Entity\Room 
     */
    public function getRoom()
    {
        return $this->room;
    }
}
