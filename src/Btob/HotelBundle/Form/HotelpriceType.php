<?php 

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HotelpriceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price',NULL, array('required' => true, 'label' => "Prix de weekday"))//,'data' => '0'
            ->add('pricew',NULL, array('required' => true, 'label' => "Prix de weekend"))
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date debut de période",'format' => 'dd/MM/yyyy'))
            ->add('dates', 'date', array('widget' => 'single_text','label' => "Date fin de période",'format' => 'dd/MM/yyyy'))
            ->add('nbnuit',NULL, array('required' => true, 'label' => "Nombre min de nuitées"))//,'data' => '1'
            ->add('nbperso',NULL, array('required' => true, 'label' => "Allotement"))
            ->add('delaiannul',NULL, array('required' => true, 'label' => "Délai d'annulation"))//,'data' => '2'
            ->add('retro',NULL, array('required' => true, 'label' => "Délai retrocession"))//,'data' => '3'
            ->add('supsingle',null, array('required' => true, 'label' => "Supplément Single"))
            ->add('perssupsingle',NULL, array('required' => false, 'label' => "Pourcentage"))
            ->add('margess',null, array('required' => true, 'label' => "Marge SS"))
            ->add('persss',NULL, array('required' => false, 'label' => "Pourcentage M"))    
            ->add('red3lit',NULL, array('required' => false, 'label' => false))
            ->add('red4lit',NULL, array('required' => false, 'label' => false))
            ->add('pers3lit',NULL, array('required' => false, 'label' => false))
            ->add('pers4lit',NULL, array('required' => false, 'label' => false))
            ->add('majminstay',NULL, array('required' => false, 'label' => false))
            ->add('persmajminstay',NULL, array('required' => false, 'label' => false))
            ->add('redminstay',NULL, array('required' => false, 'label' => false))
            ->add('persredminstay',NULL, array('required' => false, 'label' => false))

            ->add('enf12ad',NULL, array('required' => false, 'label' => false))
            ->add('persenf12ad',NULL, array('required' => false, 'label' => false))

            ->add('enf11ad',NULL, array('required' => false, 'label' => false))
            ->add('persenf11ad',NULL, array('required' => false, 'label' => false))

            ->add('enf21ad',NULL, array('required' => false, 'label' => false))
            ->add('persenf21ad',NULL, array('required' => false, 'label' => false))

            ->add('enf22ad',NULL, array('required' => false, 'label' => false))
            ->add('persenf22ad',NULL, array('required' => false, 'label' => false))

            ->add('minenf2',NULL, array('required' => false, 'label' => false))
            ->add('persminenf2',NULL, array('required' => false, 'label' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Hotelprice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_hotelprice';
    }
}
