<?php

namespace Btob\DashBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Devise
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\DashBundle\Entity\DeviseRepository")
 */
class Devise {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="devise", type="string", length=20)
     */
    private $devise;

    /**
     * @var string
     *
     * @ORM\Column(name="symb", type="string", length=20)
     */
    private $symb;

    /**
     * @var float
     *
     * @ORM\Column(name="currency", type="float",nullable=true)
     */
    private $currency;

    /**
     * @var integer
     *
     * @ORM\Column(name="chiffre", type="integer")
     */
    private $chiffre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="def", type="boolean")
     */
    private $def;

    public function __toString() {

        return $this->devise . "(" . $this->symb . ")";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set devise
     *
     * @param string $devise
     * @return Devise
     */
    public function setDevise($devise) {
        $this->devise = $devise;

        return $this;
    }

    /**
     * Get devise
     *
     * @return string 
     */
    public function getDevise() {
        return $this->devise;
    }

    /**
     * Set symb
     *
     * @param string $symb
     * @return Devise
     */
    public function setSymb($symb) {
        $this->symb = $symb;

        return $this;
    }

    /**
     * Get symb
     *
     * @return string 
     */
    public function getSymb() {
        return $this->symb;
    }

    /**
     * Set currency
     *
     * @param float $currency
     * @return Devise
     */
    public function setCurrency($currency) {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return float 
     */
    public function getCurrency() {
        return $this->currency;
    }

    /**
     * Set chiffre
     *
     * @param integer $chiffre
     * @return Devise
     */
    public function setChiffre($chiffre) {
        $this->chiffre = $chiffre;

        return $this;
    }

    /**
     * Get chiffre
     *
     * @return integer 
     */
    public function getChiffre() {
        return $this->chiffre;
    }

    /**
     * Set def
     *
     * @param boolean $def
     * @return Devise
     */
    public function setDef($def) {
        $this->def = $def;

        return $this;
    }

    /**
     * Get def
     *
     * @return boolean 
     */
    public function getDef() {
        return $this->def;
    }

}
