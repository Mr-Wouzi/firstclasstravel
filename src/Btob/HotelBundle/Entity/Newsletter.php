<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletter
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\NewsletterRepository")
 */
class Newsletter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datecre", type="date", nullable=true)
     */
    private $datecre;

    /**
     * @var string
     *
     * @ORM\Column(name="fromname", type="string", length=45, nullable=true)
     */
    private $fromname;

    /**
     * @var string
     *
     * @ORM\Column(name="fromaddress", type="string", length=45, nullable=true)
     */
    private $fromaddress;

    /**
     * @var string
     *
     * @ORM\Column(name="replyto", type="string", length=45, nullable=true)
     */
    private $replyto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isnotify", type="boolean" , nullable=true)
     */
    private $isnotify;

    /**
     * @var string
     *
     * @ORM\Column(name="notifyaddress", type="string", length=45, nullable=true)
     */
    private $notifyaddress;

    /**
     * @var string
     *
     * @ORM\Column(name="txtall", type="text", nullable=true)
     */
    private $txtall;

    

    /**
     * @var boolean
     *
     * @ORM\Column(name="statue", type="boolean" , nullable=true)
     */
    private $statue;

    



    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Newsletter
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set datecre
     *
     * @param \DateTime $datecre
     * @return Newsletter
     */
    public function setDatecre($datecre)
    {
        $this->datecre = $datecre;
    
        return $this;
    }

    /**
     * Get datecre
     *
     * @return \DateTime 
     */
    public function getDatecre()
    {
        return $this->datecre;
    }

    /**
     * Set fromname
     *
     * @param string $fromname
     * @return Newsletter
     */
    public function setFromname($fromname)
    {
        $this->fromname = $fromname;
    
        return $this;
    }

    /**
     * Get fromname
     *
     * @return string 
     */
    public function getFromname()
    {
        return $this->fromname;
    }

    /**
     * Set fromaddress
     *
     * @param string $fromaddress
     * @return Newsletter
     */
    public function setFromaddress($fromaddress)
    {
        $this->fromaddress = $fromaddress;
    
        return $this;
    }

    /**
     * Get fromaddress
     *
     * @return string 
     */
    public function getFromaddress()
    {
        return $this->fromaddress;
    }

    /**
     * Set replyto
     *
     * @param string $replyto
     * @return Newsletter
     */
    public function setReplyto($replyto)
    {
        $this->replyto = $replyto;
    
        return $this;
    }

    /**
     * Get replyto
     *
     * @return string 
     */
    public function getReplyto()
    {
        return $this->replyto;
    }

    /**
     * Set isnotify
     *
     * @param boolean $isnotify
     * @return Newsletter
     */
    public function setIsnotify($isnotify)
    {
        $this->isnotify = $isnotify;

        return $this;
    }

    /**
     * Get isnotify
     *
     * @return boolean 
     */
    public function getIsnotify()
    {
        return $this->isnotify;
    }

    /**
     * Set notifyaddress
     *
     * @param string $notifyaddress
     * @return Newsletter
     */
    public function setNotifyaddress($notifyaddress)
    {
        $this->notifyaddress = $notifyaddress;
    
        return $this;
    }

    /**
     * Get notifyaddress
     *
     * @return string 
     */
    public function getNotifyaddress()
    {
        return $this->notifyaddress;
    }

    /**
     * Set txtall
     *
     * @param string $txtall
     * @return Newsletter
     */
    public function setTxtall($txtall)
    {
        $this->txtall = $txtall;
    
        return $this;
    }

    /**
     * Get txtall
     *
     * @return string 
     */
    public function getTxtall()
    {
        return $this->txtall;
    }

    /**
     * Set statue
     *
     * @param boolean $statue
     * @return Child
     */
    public function setStatue($statue)
    {
        $this->statue = $statue;

        return $this;
    }

    /**
     * Get statue
     *
     * @return boolean 
     */
    public function getStatue()
    {
        return $this->statue;
    }

   

}
