<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Options;
use Btob\HotelBundle\Form\OptionsType;

class OptionsController extends Controller {

    public function indexAction() {
        $options = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Options')
                ->findAll();
        return $this->render('BtobHotelBundle:Options:index.html.twig', array('entities' => $options));
    }

    public function addAction() {
        $options = new Options();
        $form = $this->createForm(new OptionsType(), $options);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($options);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_options_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Options:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $options = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Options')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new OptionsType(), $options);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_options_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Options:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Options $options) {
        $em = $this->getDoctrine()->getManager();

        if (!$options) {
            throw new NotFoundHttpException("Theme non trouvée");
        }
        $em->remove($options);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_options_homepage'));
    }

}
