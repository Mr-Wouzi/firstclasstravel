<?php
namespace Front\BtobBundle\Controller;

use Front\BtobBundle\Controller\Connect2PayClient;
use Front\BtobBundle\Controller\C2PValidate;
use Front\BtobBundle\Controller\CartProduct;
use Front\BtobBundle\Controller\Connect2PayCurrencyHelper;
use Front\BtobBundle\Controller\TransactionStatus;
/**
 * Client class for the connect2pay payment page.
 * The normal workflow is as follow:
 * - Instantiate the class
 * - Set all the required parameters of the transaction
 * - Call prepareTransaction() to create the transaction
 * - Call getCustomerRedirectURL() and redirect the customer to this URL
 * - If receiving result via callback, use handleCallbackStatus to initialize the status from the POST request
 * - If receiving result via customer redirection, use handleRedirectStatus to initialize the status from the POST data
 *
 * This class does not do any sanitization on received data.
 * This must be done externally.
 * Every text must be encoded as UTF-8 when passed to this class.
 *
 * PHP dependencies:
 * PHP >= 5.2.0
 * PHP CURL module
 * PHP Mcrypt module
 *
 * @version 2.0.4 (20140702)
 * @author Jérôme Schell <jsh@payxpert.com>
 * @author Yann Finck <yann@iiiaaa.fr>
 * @copyright 2011-2014 Payxpert
 *
 */


class CartProduct {
  // Fields are public otherwise json_encode can't see them...
  public $cartProductId;
  public $cartProductName;
  public $cartProductUnitPrice;
  public $cartProductQuantity;
  public $cartProductBrand;
  public $cartProductMPN;
  public $cartProductCategoryName;
  public $cartProductCategoryID;

  public function getCartProductId() {
    return $this->cartProductId;
  }

  public function setCartProductId($cartProductId) {
    $this->cartProductId = $cartProductId;
    return $this;
  }

  public function getCartProductName() {
    return $this->cartProductName;
  }

  public function setCartProductName($cartProductName) {
    $this->cartProductName = $cartProductName;
    return $this;
  }

  public function getCartProductUnitPrice() {
    return $this->cartProductUnitPrice;
  }

  public function setCartProductUnitPrice($cartProductUnitPrice) {
    $this->cartProductUnitPrice = $cartProductUnitPrice;
    return $this;
  }

  public function getCartProductQuantity() {
    return $this->cartProductQuantity;
  }

  public function setCartProductQuantity($cartProductQuantity) {
    $this->cartProductQuantity = $cartProductQuantity;
    return $this;
  }

  public function getCartProductBrand() {
    return $this->cartProductBrand;
  }

  public function setCartProductBrand($cartProductBrand) {
    $this->cartProductBrand = $cartProductBrand;
    return $this;
  }

  public function getCartProductMPN() {
    return $this->cartProductMPN;
  }

  public function setCartProductMPN($cartProductMPN) {
    $this->cartProductMPN = $cartProductMPN;
    return $this;
  }

  public function getCartProductCategoryName() {
    return $this->cartProductCategoryName;
  }

  public function setCartProductCategoryName($cartProductCategoryName) {
    $this->cartProductCategoryName = $cartProductCategoryName;
    return $this;
  }

  public function getCartProductCategoryID() {
    return $this->cartProductCategoryID;
  }

  public function setCartProductCategoryID($cartProductCategoryID) {
    $this->cartProductCategoryID = $cartProductCategoryID;
    return $this;
  }
}

