<?php



namespace Btob\HotelBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Entity\Configseo;

use Btob\HotelBundle\Form\ConfigseoType;



class ConfigseoController extends Controller {



    public function indexAction() {

        $configseo = $this->getDoctrine()

                ->getRepository('BtobHotelBundle:Configseo')

                ->findAll();

        return $this->render('BtobHotelBundle:Configseo:index.html.twig', array('entities' => $configseo));

    }



    public function addAction() {

        $configseo = new Configseo();

        $form = $this->createForm(new ConfigseoType(), $configseo);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {



            $form->bind($request);



            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                $em->persist($configseo);

                $em->flush();

                return $this->redirect($this->generateUrl('btob_configseo_homepage'));

            } else {

                echo $form->getErrors();

            }

        }

        return $this->render('BtobHotelBundle:Configseo:form.html.twig', array('form' => $form->createView()));

    }



    public function editAction($id) {

        $request = $this->get('request');

        $configseo = $this->getDoctrine()

                ->getRepository('BtobHotelBundle:Configseo')

                ->find($id);



        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new ConfigseoType(), $configseo);

        $form->handleRequest($request);



        if ($form->isValid()) {

            $em->flush();



            return $this->redirect($this->generateUrl('btob_configseo_homepage'));

        } else {

            echo $form->getErrors();

        }

        return $this->render('BtobHotelBundle:Configseo:form.html.twig', array('form' => $form->createView(), 'id' => $id,'configseo'=>$configseo)

        );

    }



    public function deleteAction(Configseo $configseo) {

        $em = $this->getDoctrine()->getManager();



        if (!$configseo) {

            throw new NotFoundHttpException("Configseo non trouvée");

        }

        $em->remove($configseo);

        $em->flush();

        return $this->redirect($this->generateUrl('btob_configseo_homepage'));

    }



}

