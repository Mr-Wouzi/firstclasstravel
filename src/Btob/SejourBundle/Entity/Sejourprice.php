<?php

namespace Btob\SejourBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sejourprice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SejourBundle\Entity\SejourpriceRepository")
 */
class Sejourprice {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="pricead", type="float" , nullable=true)
     */
    private $pricead;

     /**
     * @var float
     *
     * @ORM\Column(name="priceenf", type="float" , nullable=true)
     */
    private $priceenf;

     /**
     * @var float
     *
     * @ORM\Column(name="priceb", type="float" , nullable=true)
     */
    private $priceb;   
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dates", type="date" , nullable=true)
     */
    private $dates;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Sejour", inversedBy="supplements")
     * @ORM\JoinColumn(name="sejour_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $sejour;

    /**
     * @var float
     *
     * @ORM\Column(name="supsingle", type="float" , nullable=true)
     */
    private $supsingle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="perssupsingle", type="boolean" , nullable=true)
     */

    private $perssupsingle;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pricead
     *
     * @param float $pricead
     * @return Sejourprice
     */
    public function setPricead($pricead)
    {
        $this->pricead = $pricead;

        return $this;
    }

    /**
     * Get pricead
     *
     * @return float 
     */
    public function getPricead()
    {
        return $this->pricead;
    }

    /**
     * Set priceenf
     *
     * @param float $priceenf
     * @return Sejourprice
     */
    public function setPriceenf($priceenf)
    {
        $this->priceenf = $priceenf;

        return $this;
    }

    /**
     * Get priceenf
     *
     * @return float 
     */
    public function getPriceenf()
    {
        return $this->priceenf;
    }   
    
    
    /**
     * Set priceb
     *
     * @param float $priceb
     * @return Sejourprice
     */
    public function setPriceb($priceb)
    {
        $this->priceb = $priceb;

        return $this;
    }

    /**
     * Get priceb
     *
     * @return float 
     */
    public function getPriceb()
    {
        return $this->priceb;
    } 
    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Sejourprice
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dates
     *
     * @param \DateTime $dates
     * @return Sejourprice
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * Get dates
     *
     * @return \DateTime 
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Sejourprice
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Sejourprice
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set sejour
     *
     * @param \Btob\SejourBundle\Entity\Sejour $sejour
     * @return Sejourprice
     */
    public function setSejour(\Btob\SejourBundle\Entity\Sejour $sejour = null)
    {
        $this->sejour = $sejour;

        return $this;
    }

    /**
     * Get sejour
     *
     * @return \Btob\SejourBundle\Entity\Sejour 
     */
    public function getSejour()
    {
        return $this->sejour;
    }
    
    
        /**
     * Set supsingle
     *
     * @param float $supsingle
     * @return Sejourprice
     */
    public function setSupsingle($supsingle)
    {
        $this->supsingle = $supsingle;

        return $this;
    }

    /**
     * Get supsingle
     *
     * @return float 
     */
    public function getSupsingle()
    {
        return $this->supsingle;
    }

    /**
     * Set perssupsingle
     *
     * @param boolean $perssupsingle
     * @return Sejourprice
     */
    public function setPerssupsingle($perssupsingle)
    {
        $this->perssupsingle = $perssupsingle;

        return $this;

    }

    /**
     * Get perssupsingle
     *
     * @return boolean
     */
    public function getPerssupsingle()
    {
        return $this->perssupsingle;
    }

}
