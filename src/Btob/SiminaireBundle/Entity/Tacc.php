<?php

namespace Btob\SiminaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**

 * Tacc

 *

 * @ORM\Table()

 * @ORM\Entity(repositoryClass="Btob\SiminaireBundle\Entity\TaccRepository")

 */
class Tacc {

    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */
    private $id;

    /**

     * @ORM\ManyToOne(targetEntity="Reservationcon", inversedBy="tacc")

     * @ORM\JoinColumn(name="reservationcon_id", referencedColumnName="id",onDelete="CASCADE")

     */
    protected $reservationcon;

    /**

     * @var integer

     *

     * @ORM\Column(name="namead", type="string", length=255, nullable=true)

     */
    private $namead;

    /**

     * @var array

     *

     * @ORM\Column(name="prenomad", type="string", length=255, nullable=true)

     */
    private $prenomad;

    /**

     * Get id

     *

     * @return integer

     */
    public function getId() {

        return $this->id;
    }

    /**

     * Set reservationcon

     *

     * @param \Btob\SiminaireBundle\Entity\Reservationcon $reservationcon

     * @return Tacc

     */
    public function setReservationcon(\Btob\SiminaireBundle\Entity\Reservationcon $reservationcon = null) {

        $this->reservationcon = $reservationcon;



        return $this;
    }

    /**

     * Get Reservationcon

     *

     * @return \Btob\SiminaireBundle\Entity\Reservationcon

     */
    public function getReservationcon() {

        return $this->reservationcon;
    }

    /**

     * Set namead

     *

     * @param string $namead

     * @return Reservationcon

     */
    public function setNamead($namead) {

        $this->namead = $namead;



        return $this;
    }

    /**

     * Get namead

     *

     * @return string 

     */
    public function getNamead() {

        return $this->namead;
    }

    /**

     * Set prenomad

     *

     * @param string $prenomad

     * @return Reservationcon

     */
    public function setPrenomad($prenomad) {

        $this->prenomad = $prenomad;



        return $this;
    }

    /**

     * Get prenomad

     *

     * @return string 

     */
    public function getPrenomad() {

        return $this->prenomad;
    }

}
