<?php



namespace Btob\HotelBundle\Entity;



use Doctrine\ORM\Mapping as ORM;



/**

 * Configseo

 *

 * @ORM\Table()

 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\ConfigseoRepository")

 */

class Configseo

{

    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */

    private $id;



    /**

     * @var string

     *

     * @ORM\Column(name="page", type="string", length=100 , nullable=true)

     */

    private $page;



    /**

     * @var string

     *

     * @ORM\Column(name="seotitle", type="string", length=255 , nullable=true)

     */

    private $seotitle;

    /**

     * @var string

     *

     * @ORM\Column(name="seodescreption", type="string", length=255 , nullable=true)

     */

    private $seodescreption;

    /**

     * @var string

     *

     * @ORM\Column(name="seokeyword", type="string", length=255 , nullable=true)

     */

    private $seokeyword;






    /**

     * Get id

     *

     * @return integer

     */

    public function getId()

    {

        return $this->id;

    }



    /**

     * Set page

     *

     * @param string $page

     * @return Configseo

     */

    public function setPage($page)

    {

        $this->page = $page;



        return $this;

    }



    /**

     * Get page

     *

     * @return string

     */

    public function getPage()

    {

        return $this->page;

    }


    /**

     * Set seotitle

     *

     * @param string $seotitle

     * @return Configseo

     */

    public function setSeotitle($seotitle)

    {

        $this->seotitle = $seotitle;



        return $this;

    }



    /**

     * Get seotitle

     *

     * @return string

     */

    public function getSeotitle()

    {

        return $this->seotitle;

    }


    /**

     * Set seodescreption

     *

     * @param string $seodescreption

     * @return Configseo

     */

    public function setSeodescreption($seodescreption)

    {

        $this->seodescreption = $seodescreption;



        return $this;

    }



    /**

     * Get seodescreption

     *

     * @return string

     */

    public function getSeodescreption()

    {

        return $this->seodescreption;

    }


    /**

     * Set seokeyword
     *

     * @param string $seokeyword

     * @return Configseo

     */

    public function setSeokeyword($seokeyword)

    {

        $this->seokeyword = $seokeyword;



        return $this;

    }



    /**

     * Get seodkeyword

     *

     * @return string

     */

    public function getSeokeyword()

    {

        return $this->seokeyword;

    }



    public function __toString()

    {

        return $this->page;

    }


}

