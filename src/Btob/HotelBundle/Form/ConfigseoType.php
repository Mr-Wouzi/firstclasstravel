<?php



namespace Btob\HotelBundle\Form;



use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;



class ConfigseoType extends AbstractType

{

    /**

     * @param FormBuilderInterface $builder

     * @param array $options

     */

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder

            ->add('page', 'hidden')
            ->add('seotitle', Null, array('label' => "Seo Title", 'required' => true))
            ->add('seodescreption', Null, array('label' => "Seo Description ", 'required' => true))
            ->add('seokeyword', Null, array('label' => "Seo Keyword", 'required' => true))


        ;

    }

    

    /**

     * @param OptionsResolverInterface $resolver

     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => 'Btob\HotelBundle\Entity\Configseo'

        ));

    }



    /**

     * @return string

     */

    public function getName()

    {

        return 'btob_hotelbundle_configseo';

    }

}

