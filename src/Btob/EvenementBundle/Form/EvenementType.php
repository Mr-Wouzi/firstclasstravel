<?php



namespace Btob\EvenementBundle\Form;



use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;



class EvenementType extends AbstractType

{

    /**

     * @param FormBuilderInterface $builder

     * @param array $options

     */

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder

            ->add('act', NULL, array('label' => 'active', 'required' => false))

            ->add('titre', 'text', array('label' => "Titre d'evenement", 'required' => true))


            ->add('villed', 'text', array('label' => "Ville", 'required' => true))


            //->add('voyagecart', 'text', array('label' => "Voyages a la carte", 'required' => false))

             ->add('ageenfmin', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Min Enfant  *',
            ))
            ->add('ageenfmax', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Max Enfant  *',
            ))
            ->add('agebmin', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Min Bébé  *',
            ))
            ->add('agebmax', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Max Bébé  *',
            ))     


            ->add('prix', NULL, array('label' => " A partir de :", 'required' => true))
                
            ->add('prixavance', NULL, array('label' => 'Avance(En pourcentage) ', 'required' => true))
    


            ->add('pindex', NULL, array('label' => "Promo Index", 'required' => false))

            ->add('themes', NULL, array('label' => "Themes", 'required' => true))

            ->add('arrangement', NULL, array('label' => "Logement", 'required' => false))

            ->add('hotel', NULL, array('label' => "Hôtel : ", 'required' => false))

            ->add('nbretoile', NULL, array('label' => "Nombre d'étoiles :  ", 'required' => false))

            ->add('description', NULL, array('label' => "Description :  ", 'required' => false))

            ->add('seotitle', Null, array('label' => "Seo Title", 'required' => true))
            ->add('seodescreption', Null, array('label' => "Seo Description ", 'required' => true))
            ->add('seokeyword', Null, array('label' => "Seo Keyword", 'required' => true))


        ;

    }



    /**

     * @param OptionsResolverInterface $resolver

     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => 'Btob\EvenementBundle\Entity\Evenement'

        ));

    }



    /**

     * @return string

     */

    public function getName()

    {

        return 'btob_evenementbundle_evenement';

    }

}

