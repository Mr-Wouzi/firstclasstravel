<?php







namespace Front\BtobBundle\Controller;







use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use Btob\HotelBundle\Entity\Clients;



use Btob\HotelBundle\Form\ClientsType;



use Btob\VoleBundle\Entity\Vol;



use Btob\HotelBundle\Common\Tools;use Symfony\Component\HttpFoundation\Request;



class ContactController extends Controller



{



    public function indexAction()



    {
        $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Contact'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;
        $request = $this->get('request');
        if ($request->getMethod() == 'POST')
        {

            $session = $this->getRequest()->getSession();
            $nom = $request->request->get("nom");
            $tel = $request->request->get("tel");
            $email = $request->request->get("email");
            $sujet = $request->request->get("sujet");
            $message = $request->request->get("Message");


            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');


            $to = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $nom." - ".$sujet ;
            $headers = "From: ".$nom." ".$email."\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';            
			$message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 
				<tbody>				  <tr>				   <td>				  
				<a href="http://www.firstclasstravel.ma/">
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
				</a>				   
				</td>				   <td align="right">				    
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>				   </td>			
				</tr>				 </tbody>				</table>
				<table width="90%"  cellspacing="1" border="0">';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Contact :</b></td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $nom . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $tel . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $email . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Sujet</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $sujet . '</td>';            
			$message1 .= '</tr>';            
			$message1 .= '<tr>';            
			$message1 .= '<td height="30"><b style="padding-left:10px;">Message</b></td>';            
			$message1 .= '<td height="30"><b>:</b></td>';            
			$message1 .= '<td height="30">' . $message . '</td>';            
			$message1 .= '</tr>';							
			$message1 .= '<tr>';                			
			$message1 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed 5 et Av. Hassen 1er N° 7 Bloc B - Imm. SIBAM2 4000 Marrakech Maroc 
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                        <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
 </td>';                			
			$message1 .= '</tr>';            
			$message1 .= '</table>';            
			$message1 .= '</body>';			
            //mail($to, $subject, $message1, $headers);     
            mail('infos.firstclass@gmail.com', $subject, $message1, $headers);
            mail('shamid-fct@menara.ma', $subject, $message1, $headers);
            mail('hmahi-fct@menara.ma', $subject, $message1, $headers);
            mail('firas.benkharrat@gmail.com', $subject, $message1, $headers);      
			//mail("afef.tuninfo@gmail.com", $subject, $message1, $headers);




            $request->getSession()->getFlashBag()->add('noticcontact', 'Votre message a bien été envoyé. Merci.');
            return $this->redirect($this->generateUrl('front_contacts_homepage'));  }


        return $this->render('FrontBtobBundle:Contact:index.html.twig');



    }



}



