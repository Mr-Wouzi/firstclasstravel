<?php

namespace Btob\ActiviteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ActiviteBundle\Entity\TypeRepository")
 */
class Type
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;
    /**
     * @ORM\OneToMany(targetEntity="Activite", mappedBy="type")
     */
    protected $activite;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean", nullable=true)
     */
    private $act;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;



    public  function  __construct(){
        $this->dcr = new \DateTime();

    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Type
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Type
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Type
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Add activite
     *
     * @param \Btob\ActiviteBundle\Entity\Activite $activite
     * @return Type
     */
    public function addActivite(\Btob\ActiviteBundle\Entity\Activite $activite)
    {
        $this->activite[] = $activite;

        return $this;
    }

    /**
     * Remove activite
     *
     * @param \Btob\ActiviteBundle\Entity\Activite $activite
     */
    public function removeActivite(\Btob\ActiviteBundle\Entity\Activite $activite)
    {
        $this->activite->removeElement($activite);
    }

    /**
     * Get activite
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActivite()
    {
        return $this->activite;
    }
    public  function  __toString(){
        return $this->type;
    }
}
