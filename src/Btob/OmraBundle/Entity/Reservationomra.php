<?php



namespace Btob\OmraBundle\Entity;



use Doctrine\ORM\Mapping as ORM;



/**

 * Reservationomra

 *

 * @ORM\Table()

 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\ReservationomraRepository")

 */

class Reservationomra

{

    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */

    private $id;



    /**

     * @var \DateTime

     *

     * @ORM\Column(name="dcr", type="date")

     */

    private $dcr;





    /**

     * @var integer

     *

     * @ORM\Column(name="adulte", type="integer" , nullable=true)

     */

    private $adulte;



    /**

     * @var integer

     *

     * @ORM\Column(name="enfant", type="integer" , nullable=true)

     */

    private $enfant;



    /**

     * @var integer

     *

     * @ORM\Column(name="bebe", type="integer" , nullable=true)

     */

    private $bebe;



    /**

     * @var string

     *

     * @ORM\Column(name="body", type="text" , nullable=true)

     */

    private $comment;

/**

     * @var integer

     *

     * @ORM\Column(name="etat", type="integer" , nullable=true)

     */

    private $etat;

    /**

     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationomra")

     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")

     */

    protected $client;

    /**

     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationomra")

     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")

     */

    protected $user;

    /**

     * @ORM\ManyToOne(targetEntity="Omra", inversedBy="reservationomra")

     * @ORM\JoinColumn(name="omra_id", referencedColumnName="id",onDelete="CASCADE")

     */

    protected $omra;

    /**

     * construct

     */

    public function __construct(){

        $this->dcr=new \DateTime();

    }



    /**

     * Get id

     *

     * @return integer 

     */

    public function getId()

    {

        return $this->id;

    }



    /**

     * Set dcr

     *

     * @param \DateTime $dcr

     * @return Reservationomra

     */

    public function setDcr($dcr)

    {

        $this->dcr = $dcr;



        return $this;

    }



    /**

     * Get dcr

     *

     * @return \DateTime 

     */

    public function getDcr()

    {

        return $this->dcr;

    }



    /**

     * Set adulte

     *

     * @param integer $adulte

     * @return Reservationomra

     */

    public function setAdulte($adulte)

    {

        $this->adulte = $adulte;



        return $this;

    }



    /**

     * Get adulte

     *

     * @return integer 

     */

    public function getAdulte()

    {

        return $this->adulte;

    }



    /**

     * Set enfant

     *

     * @param integer $enfant

     * @return Reservationomra

     */

    public function setEnfant($enfant)

    {

        $this->enfant = $enfant;



        return $this;

    }



    /**

     * Get enfant

     *

     * @return integer 

     */

    public function getEnfant()

    {

        return $this->enfant;

    }



    /**

     * Set bebe

     *

     * @param integer $bebe

     * @return Reservationomra

     */

    public function setBebe($bebe)

    {

        $this->bebe = $bebe;



        return $this;

    }



    /**

     * Get bebe

     *

     * @return integer 

     */

    public function getBebe()

    {

        return $this->bebe;

    }



    /**

     * Set comment

     *

     * @param string $comment

     * @return Reservationomra

     */

    public function setComment($comment)

    {

        $this->comment = $comment;



        return $this;

    }



    /**

     * Get comment

     *

     * @return string 

     */

    public function getComment()

    {

        return $this->comment;

    }



    /**

     * Set client

     *

     * @param \Btob\HotelBundle\Entity\Clients $client

     * @return Reservationomra

     */

    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)

    {

        $this->client = $client;



        return $this;

    }



    /**

     * Get client

     *

     * @return \Btob\HotelBundle\Entity\Clients 

     */

    public function getClient()

    {

        return $this->client;

    }

    /**

     * Set dated

     *

     * @param \DateTime $dated

     * @return Resacircui

     */

    public function setDated($dated)

    {

        $this->dated = $dated;



        return $this;

    }


    /**

     * Set user

     *

     * @param \User\UserBundle\Entity\User $user

     * @return Reservationomra

     */

    public function setUser(\User\UserBundle\Entity\User $user = null)

    {

        $this->user = $user;



        return $this;

    }



    /**

     * Get user

     *

     * @return \User\UserBundle\Entity\User 

     */

    public function getUser()

    {

        return $this->user;

    }



    /**

     * Set omra

     *

     * @param \Btob\OmraBundle\Entity\Omra $omra

     * @return Reservationomra

     */

    public function setOmra(\Btob\OmraBundle\Entity\Omra $omra = null)

    {

        $this->omra = $omra;



        return $this;

    }



    /**

     * Get omra

     *

     * @return \Btob\OmraBundle\Entity\Omra 

     */

    public function getOmra()

    {

        return $this->omra;

    }
	
	/**

     * Get etat

     *

     * @return integer 

     */

    public function getEtat()

    {

        return $this->etat;

    }
	
	
	/**

     * Set etat

     *

     * @param string $etat

     * @return Reservationomra

     */

    public function setEtat($etat)

    {

        $this->etat = $etat;



        return $this;

    }
	
}

