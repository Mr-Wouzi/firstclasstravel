<?php

namespace Btob\SiminaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationcon
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SiminaireBundle\Entity\ReservationconRepository")
 */
class Reservationcon {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationcon")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbchsingle", type="integer", nullable=true)
     */
    private $nbchsingle;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbnuitchsingle", type="integer", nullable=true)
     */
    private $nbnuitchsingle;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbchdouble", type="integer", nullable=true)
     */
    private $nbchdouble;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbnuitchdouble", type="integer", nullable=true)
     */
    private $nbnuitchdouble;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbchtriple", type="integer", nullable=true)
     */
    private $nbchtriple;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbnuitchtriple", type="integer", nullable=true)
     */
    private $nbnuitchtriple;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date", nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dater", type="date", nullable=true)
     */
    private $dater;

    /**
     * @ORM\OneToMany(targetEntity="Tacc", mappedBy="reservationcon")
     */
    protected $reservationcon;

    /**
     * @var string
     *
     * @ORM\Column(name="heurd", type="string", length=255 , nullable=true)
     */
    private $heurd;

    /**
     * @var string
     *
     * @ORM\Column(name="heura", type="string", length=255 , nullable=true)
     */
    private $heura;

    /**
     * @var array
     *
     * @ORM\Column(name="hotel1", type="text",nullable=true)
     */
    private $hotel1;

    /**
     * @var array
     *
     * @ORM\Column(name="hotel2", type="text",nullable=true)
     */
    private $hotel2;

    /**
     * @var string
     *
     * @ORM\Column(name="vold", type="string", length=255 , nullable=true)
     */
    private $vold;

    /**

     * @var string

     *

     * @ORM\Column(name="vola", type="string", length=255 , nullable=true)

     */
    private $vola;

    /**

     * @var boolean

     *

     * @ORM\Column(name="transfert", type="boolean", nullable=true)

     */
    private $transfert;

    /**

     * @var array

     *

     * @ORM\Column(name="type", type="text",nullable=true)

     */
    private $type;

    /**

     * @var string

     *

     * @ORM\Column(name="civ", type="string", length=10 , nullable=true)

     */
    private $civ;

    /**

     * @var string

     *

     * @ORM\Column(name="pname", type="string", length=50 , nullable=true)

     */
    private $pname;

    /**

     * @var string

     *

     * @ORM\Column(name="name", type="string", length=50 , nullable=true)

     */
    private $name;

    /**

     * @var string

     *

     * @ORM\Column(name="email", type="string", length=255 , nullable=true)

     */
    private $email;

    /**

     * @var string

     *

     * @ORM\Column(name="tel", type="string", length=40 , nullable=true)

     */
    private $tel;

    /**

     * @var string

     *

     * @ORM\Column(name="adresse", type="string", length=255,nullable=true)

     */
    private $adresse;

    /**

     * @var string

     *

     * @ORM\Column(name="cp", type="string", length=12,nullable=true)

     */
    private $cp;

    /**

     * @var string

     *

     * @ORM\Column(name="ville", type="string", length=100,nullable=true)

     */
    private $ville;

    /**

     * @var string

     *

     * @ORM\Column(name="pays", type="string", length=100,nullable=true)

     */
    private $pays;

    /**

     * @var string

     *

     * @ORM\Column(name="organisme", type="string", length=100,nullable=true)

     */
    private $organisme;

    /**

     * @var string

     *

     * @ORM\Column(name="fax", type="string", length=100,nullable=true)

     */
    private $fax;

    /**

     * Get id

     *

     * @return integer

     */
    public function getId() {

        return $this->id;
    }

    /**

     * Set agent

     *

     * @param \User\UserBundle\Entity\User $agent

     * @return Reservationcon

     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null) {

        $this->agent = $agent;



        return $this;
    }

    /**

     * Get agent

     *

     * @return \User\UserBundle\Entity\User

     */
    public function getAgent() {

        return $this->agent;
    }

    /**

     * Constructor

     */
    public function __construct() {



        $this->dcr = new \DateTime();
    }

    /**

     * Set dcr

     *

     * @param \DateTime $dcr

     * @return Reservationcon

     */
    public function setDcr($dcr) {

        $this->dcr = $dcr;



        return $this;
    }

    /**

     * Get dcr

     *

     * @return \DateTime

     */
    public function getDcr() {

        return $this->dcr;
    }

    /**

     * Set nbchsingle

     *

     * @param integer $nbchsingle

     * @return Reservationcon

     */
    public function setNbchsingle($nbchsingle) {

        $this->nbchsingle = $nbchsingle;



        return $this;
    }

    /**

     * Get nbchsingle

     *

     * @return integer

     */
    public function getNbchsingle() {

        return $this->nbchsingle;
    }

    /**

     * Set nbchdouble

     *

     * @param integer $nbchdouble

     * @return Reservationcon

     */
    public function setNbchdouble($nbchdouble) {

        $this->nbchdouble = $nbchdouble;



        return $this;
    }

    /**

     * Get nbchdouble

     *

     * @return integer

     */
    public function getNbchdouble() {

        return $this->nbchdouble;
    }

    /**

     * Set nbchtriple

     *

     * @param integer $nbchtriple

     * @return Reservationcon

     */
    public function setNbchtriple($nbchtriple) {

        $this->nbchtriple = $nbchtriple;



        return $this;
    }

    /**

     * Get nbchtriple

     *

     * @return integer

     */
    public function getNbchtriple() {

        return $this->nbchtriple;
    }

    /**

     * Set dated

     *

     * @param \DateTime $dated

     * @return Reservationcon

     */
    public function setDated($dated) {

        $this->dated = $dated;



        return $this;
    }

    /**

     * Get dated

     *

     * @return \DateTime

     */
    public function getDated() {

        return $this->dated;
    }

    /**

     * Set dater

     *

     * @param \DateTime $dater

     * @return Reservationcon

     */
    public function setDater($dater) {

        $this->dater = $dater;



        return $this;
    }

    /**

     * Get dater

     *

     * @return \DateTime

     */
    public function getDater() {

        return $this->dater;
    }

    /**

     * Add reservationcon

     *

     * @param \Btob\SiminaireBundle\Entity\Tacc $reservationcon

     * @return Reservationcon

     */
    public function addReservationcon(\Btob\SiminaireBundle\Entity\Tacc $reservationcon) {



        $this->reservationcon[] = $reservationcon;







        return $this;
    }

    /**



     * Remove reservationcon



     *



     * @param \Btob\SiminaireBundle\Entity\Tacc $reservationcon



     */
    public function removeReservationcon(\Btob\SiminaireBundle\Entity\Tacc $reservationcon) {



        $this->reservationcon->removeElement($reservationcon);
    }

    /**



     * Get reservationcon



     *



     * @return \Doctrine\Common\Collections\Collection



     */
    public function getReservationcon() {



        return $this->reservationcon;
    }

    /**

     * Set nbnuitchsingle

     *

     * @param integer $nbnuitchsingle

     * @return Reservationcon

     */
    public function setNbnuitchsingle($nbnuitchsingle) {

        $this->nbnuitchsingle = $nbnuitchsingle;



        return $this;
    }

    /**

     * Get nbnuitchsingle

     *

     * @return integer

     */
    public function getNbnuitchsingle() {

        return $this->nbnuitchsingle;
    }

    /**

     * Set nbnuitchdouble

     *

     * @param integer $nbnuitchdouble

     * @return Reservationcon

     */
    public function setNbnuitchdouble($nbnuitchdouble) {

        $this->nbnuitchdouble = $nbnuitchdouble;



        return $this;
    }

    /**

     * Get nbnuitchdouble

     *

     * @return integer

     */
    public function getNbnuitchdouble() {

        return $this->nbnuitchdouble;
    }

    /**

     * Set nbnuitchtriple

     *

     * @param integer $nbnuitchtriple

     * @return Reservationcon

     */
    public function setNbnuitchtriple($nbnuitchtriple) {

        $this->nbnuitchtriple = $nbnuitchtriple;



        return $this;
    }

    /**

     * Get nbnuitchtriple

     *

     * @return integer

     */
    public function getNbnuitchtriple() {

        return $this->nbnuitchtriple;
    }

    /**

     * Set hotel1

     *

     * @param string $hotel1

     * @return Reservationcon

     */
    public function setHotel1($hotel1) {

        $this->hotel1 = $hotel1;



        return $this;
    }

    /**

     * Get hotel1

     *

     * @return string

     */
    public function getHotel1() {

        return $this->hotel1;
    }

    /**

     * Set hotel2

     *

     * @param string $hotel2

     * @return Reservationcon

     */
    public function setHotel2($hotel2) {

        $this->hotel2 = $hotel2;



        return $this;
    }

    /**

     * Get hotel2

     *

     * @return string

     */
    public function getHotel2() {

        return $this->hotel2;
    }

    /**

     * Set transfert

     *

     * @param boolean $transfert

     * @return Reservationcon

     */
    public function setTransfert($transfert) {

        $this->transfert = $transfert;



        return $this;
    }

    /**

     * Get transfert

     *

     * @return boolean

     */
    public function getTransfert() {

        return $this->transfert;
    }

    /**

     * Set type

     *

     * @param string $type

     * @return Reservationcon

     */
    public function setType($type) {

        $this->type = $type;



        return $this;
    }

    /**

     * Get type

     *

     * @return string

     */
    public function getType() {

        return $this->type;
    }

    /**

     * Set heurd

     *

     * @param string $heurd

     * @return Reservationcon

     */
    public function setHeurd($heurd) {

        $this->heurd = $heurd;



        return $this;
    }

    /**

     * Get heurd

     *

     * @return string

     */
    public function getHeurd() {

        return $this->heurd;
    }

    /**

     * Set heura

     *

     * @param string $heura

     * @return Reservationcon

     */
    public function setHeura($heura) {

        $this->heura = $heura;



        return $this;
    }

    /**

     * Get heura

     *

     * @return string

     */
    public function getHeura() {

        return $this->heura;
    }

    /**

     * Set vold

     *

     * @param string $vold

     * @return Reservationcon

     */
    public function setVold($vold) {

        $this->vold = $vold;



        return $this;
    }

    /**

     * Get vold

     *

     * @return string

     */
    public function getVold() {

        return $this->vold;
    }

    /**

     * Set vola

     *

     * @param string $vola

     * @return Reservationcon

     */
    public function setVola($vola) {

        $this->vola = $vola;



        return $this;
    }

    /**

     * Get vola

     *

     * @return string

     */
    public function getVola() {

        return $this->vola;
    }

    /**

     * Set civ

     *

     * @param string $civ

     * @return Reservationcon

     */
    public function setCiv($civ) {

        $this->civ = $civ;



        return $this;
    }

    /**

     * Get civ

     *

     * @return string

     */
    public function getCiv() {

        return $this->civ;
    }

    /**

     * Set pname

     *

     * @param string $pname

     * @return Reservationcon

     */
    public function setPname($pname) {

        $this->pname = $pname;



        return $this;
    }

    /**

     * Get pname

     *

     * @return string

     */
    public function getPname() {

        return $this->pname;
    }

    /**

     * Set name

     *

     * @param string $name

     * @return Reservationcon

     */
    public function setName($name) {

        $this->name = $name;



        return $this;
    }

    /**

     * Get name

     *

     * @return string

     */
    public function getName() {

        return $this->name;
    }

    /**

     * Set email

     *

     * @param string $email

     * @return Reservationcon

     */
    public function setEmail($email) {

        $this->email = $email;



        return $this;
    }

    /**

     * Get email

     *

     * @return string

     */
    public function getEmail() {

        return $this->email;
    }

    /**

     * Set tel

     *

     * @param string $tel

     * @return Reservationcon

     */
    public function setTel($tel) {

        $this->tel = $tel;



        return $this;
    }

    /**

     * Get tel

     *

     * @return string

     */
    public function getTel() {

        return $this->tel;
    }

    /**

     * Set adresse

     *

     * @param string $adresse

     * @return Reservationcon

     */
    public function setAdresse($adresse) {

        $this->adresse = $adresse;



        return $this;
    }

    /**

     * Get adresse

     *

     * @return string

     */
    public function getAdresse() {

        return $this->adresse;
    }

    /**

     * Set cp

     *

     * @param string $cp

     * @return Reservationcon

     */
    public function setCp($cp) {

        $this->cp = $cp;



        return $this;
    }

    /**

     * Get cp

     *

     * @return string

     */
    public function getCp() {

        return $this->cp;
    }

    /**

     * Set ville

     *

     * @param string $ville

     * @return Reservationcon

     */
    public function setVille($ville) {

        $this->ville = $ville;



        return $this;
    }

    /**

     * Get ville

     *

     * @return string

     */
    public function getVille() {

        return $this->ville;
    }

    /**

     * Set pays

     *

     * @param string $pays

     * @return Reservationcon

     */
    public function setPays($pays) {

        $this->pays = $pays;



        return $this;
    }

    /**

     * Get pays

     *

     * @return string

     */
    public function getPays() {

        return $this->pays;
    }

    /**

     * Set organisme

     *

     * @param string $organisme

     * @return Reservationcon

     */
    public function setOrganisme($organisme) {

        $this->organisme = $organisme;



        return $this;
    }

    /**

     * Get organisme

     *

     * @return string

     */
    public function getOrganisme() {

        return $this->organisme;
    }

    /**

     * Set fax

     *

     * @param string $fax

     * @return Reservationcon

     */
    public function setFax($fax) {

        $this->fax = $fax;



        return $this;
    }

    /**

     * Get fax

     *

     * @return string

     */
    public function getFax() {

        return $this->fax;
    }

}
