<?php



namespace Btob\SejourBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use Btob\HotelBundle\Common\Tools;



use Btob\SejourBundle\Entity\Sejour;

use Btob\SejourBundle\Entity\Reservationsejour;

use Btob\SejourBundle\Entity\Imgesej;

use Btob\HotelBundle\Entity\Clients;



use Btob\HotelBundle\Form\ClientsType;

use Symfony\Component\HttpFoundation\Request;


use Btob\SejourBundle\Entity\Reservationsdetail;

class DefaultController extends Controller

{

    public function indexAction(Request $request)

    {

         $sejourprices = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->listPrice();

        $entities = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findby(array('act' => 1));
          $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $sejourprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );   
         $user = $this->get('security.context')->getToken()->getUser();

        return $this->render('BtobSejourBundle:Default:index.html.twig', array('entities' => $entities,'sejourprices' => $sejourprices,'user' => $user));

    }



    public function detailAction(Sejour $Sejour)

    {
       $sejourperiode = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->findBySejour($Sejour->getId());

        return $this->render('BtobSejourBundle:Default:detail.html.twig', array('entry' => $Sejour,'periods' => $sejourperiode));

    }

    
    
    public function personalisationAction(Sejour $sejour)

    {

       $em = $this->getDoctrine()->getManager();
       
       $sejourprices = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->findBySejour($sejour->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {
            
            $sejourprices = $request->request->get("sejourprice");
            $session->set('sejourprice', $sejourprices);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
            $nbbebe = $request->request->get("nbbebe");
            $session->set('nbbebe', $nbbebe);
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
            
          return $this->redirect($this->generateUrl('btob_inscrip_reservation_sejour', array('id'=>$sejour->getId())));

            
        }
       
       
       
       
    return $this->render('BtobSejourBundle:Default:personalisation.html.twig', array('entry' => $sejour,'sejourprices' => $sejourprices));

    
    }
    
    
    public function inscriptionAction(Sejour $sejour)

    {

       $em = $this->getDoctrine()->getManager();
       
       $sejourprices = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->findBySejour($sejour->getId());
       $sejoursupp = $this->getDoctrine()->getRepository("BtobSejourBundle:Supplements")->findSupplementBySejour($sejour->getId());
       

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $sejourprice = $session->get("sejourprice");
        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbbebe = $session->get("nbbebe");
        $nbenf = $session->get("nbenf");
        
        $Array = array();
        
        
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
                //bebe
                if(isset($request->request->get("nameb")[$i]))
                {
                     $bebe=array();
                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)
                 {
                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];
                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];
                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];
                
                
                if(isset($request->request->get("suppb")[$i][$l]))
                 {
                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['suppb']=null;  
                 }
                 
                 
                 
                 
                 }
                 
                 $Array[$i]['bebe']=$bebe;
                }
                
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('sejourprice', $sejourprice);
            
          

          return $this->redirect($this->generateUrl('btob_sejour_sejour_reservation_homepage', array('id'=>$sejour->getId())));

            
        }
       
       
       
       
    return $this->render('BtobSejourBundle:Default:inscription.html.twig', array('entry' => $sejour,'sejoursupp' => $sejoursupp,'sejourprices' => $sejourprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf));

    
    }
    
    
    
    
    public function detailreservationAction(Reservationsejour $Reservationsejour)

    {

        return $this->render('BtobSejourBundle:Default:show.html.twig', array('entry' => $Reservationsejour));

    }

    public function deletereservationAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSejourBundle:Reservationsejour')->find($id);

        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('sejours_reservation'));

    }

    public function listreservationsejourAction()

    {



        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();

        $dt->modify('-2 month');

        $dcrfrom = $dt->format("d/m/Y");

        $dt->modify('+4 month');

        $dcrto = $dt->format("d/m/Y");

        $etat = "";

        $client = "";

        $numres = "";

        $dt = new \DateTime();

        $dt->modify('-2 month');

        $datedfrom = $dt->format("d/m/Y");

        $dt->modify('+4 month');

        $datefto = $dt->format("d/m/Y");

        $agence = "";

        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $dcrto = $request->request->get('dcrto');

            $dcrfrom = $request->request->get('dcrfrom');

            $etat = $request->request->get('etat');

            $client = trim($request->request->get('client'));

            $numres = trim($request->request->get('numres'));



            $agence = $request->request->get('agence');

        }

        $em = $this->getDoctrine()->getManager();

        // reset notification

        $user = $this->get('security.context')->getToken()->getUser();

        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));



        foreach ($notification as $value) {

            $value->setNotif(0);

            $em->persist($value);

            $em->flush();

        }

        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')

        {

            $entities = $em->getRepository('BtobSejourBundle:Reservationsejour')->findAll();



        }else{

            $entities = $em->getRepository('BtobSejourBundle:Reservationsejour')->findBy(array('user' => $user));



        }
        
               if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getUser()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }


        if ($client != "") {

            $tabsearch = array();

            foreach ($entities as $value) {

                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {

                    $tabsearch[] = $value;

                }

            }

            $entities = $tabsearch;

        }

        if ($dcrfrom != "") {

            $tabsearch = array();

            $tab = explode('/', $dcrfrom);

            $dtx = $tab[2] . $tab[1] . $tab[0];

            foreach ($entities as $value) {

                $dty = $value->getDcr()->format('Ymd');

                if (($dty - $dtx) >= 0) {

                    $tabsearch[] = $value;

                }

            }

            $entities = $tabsearch;

        }

        if ($dcrto != "") {

            $tabsearch = array();

            $tab = explode('/', $dcrto);

            $dtx = $tab[2] . $tab[1] . $tab[0];

            foreach ($entities as $value) {

                $dty = $value->getDcr()->format('Ymd');

                if (($dtx - $dty) >= 0) {

                    $tabsearch[] = $value;

                }

            }

            $entities = $tabsearch;

        }

       

       


        return $this->render('BtobSejourBundle:Default:listreservation.html.twig', array(

            'entities' => $entities,

            'dcrto' => $dcrto,

            'dcrfrom' => $dcrfrom,

            'etat' => $etat,

            'client' => $client,

            'numres' => $numres,

            'datefto' => $datefto,

            'datedfrom' => $datedfrom,

            'agence' => $agence,

            'users' => $users,

        ));





    }








    
  public function reservationsejourAction(Sejour $sejour)

    {

        $em = $this->getDoctrine()->getManager();
       
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $sejourperiode = $session->get("sejourprice");
        
        
       $pricesej =   $this->getDoctrine()->getRepository('BtobSejourBundle:Sejourprice')->find($sejourperiode);   

       
        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           
          
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
             
                 
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              
              
              
              
              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
                  if(isset($valb['suppb']))
                  {
                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
              $totalsuppb+= $suppbp;
              
              
              
              
           }
           }
           
        
          $ArrBase[$key]['price'] = $this->getDoctrine()
                        ->getRepository('BtobSejourBundle:Sejourprice')
                        ->calculsej($pricesej,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $sejourprice = $session->get("sejourprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            $User = $this->get('security.context')->getToken()->getUser();

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();
                

                
                
                
                
                
                
                
                

                $resa=new Reservationsejour();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setSejour($sejour);
                $resa->setSejourprice($pricesej);
                
                $resa->setTotal($total);
                  
                $em->persist($resa);

                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
       
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
           
           $resadetail=new Reservationsdetail();
           $resadetail->setReservationsejour($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                 
                  
                
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               
           $resadetails=new Reservationsdetail();
           $resadetails->setReservationsejour($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
            
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
               
               
               $resadetailb=new Reservationsdetail();
           $resadetailb->setReservationsejour($resa);
           $resadetailb->setChambre($key+1);
           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);
          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);
          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);
                  if(isset($valb['suppb']))
                  {

                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));
              $totalsuppb+= $suppbp;
              
              
             
                 
                  
                  
              

            $em->persist($resadetailb);
            $em->flush();


              
           }
           }
           
        
          $ArrBase[$key]['price'] = $this->getDoctrine()
                        ->getRepository('BtobSejourBundle:Sejourprice')
                        ->calculsej($pricesej,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         
          $total+= $ArrBase[$key]['price']; 
          
          

           
       }    
                
                
                
                
                
                
                
                
                
                
                
                
             
                
               

            } else {
                echo $form->getErrors();
            }
                $request->getSession()->getFlashBag()->add('notisejourb', 'Votre réservation a bien été envoyé. Merci.');

                return $this->redirect($this->generateUrl('btob_sejour_sejour_homepage'));



        }

        return $this->render('BtobSejourBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$sejour,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total));

    }
   
}

