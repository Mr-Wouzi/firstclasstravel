<?php

namespace Front\BtobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Btob\HotelBundle\Common\Tools;
use Btob\VoitureBundle\Entity\Voiture;
use Btob\VoitureBundle\Entity\Imgv;
use Btob\VoitureBundle\Form\VoitureType;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\VoitureBundle\Entity\Reservationvoiture;

class VoitureController extends Controller
{


    public function indexAction()
    {


        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->findAll();
        return $this->render('FrontBtobBundle:Voiture:index.html.twig', array('entities' => $entities));
    }
    public function detailAction(Voiture $voiture)
    {
        return $this->render('FrontBtobBundle:Voiture:detail.html.twig', array('entry' => $voiture));
    }
    public function reservationvoitureAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'), '/'));
                $datef = new \Datetime(Tools::explodedate($request->request->get('datef'), '/'));
                $message = $request->request->get('message');

                $reservation = new Reservationvoiture();
                $reservation->setClient($client);
                $reservation->setVoiture($entities);
                $reservation->setAgent($this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB'));

                $reservation->setMessage($message);
                $reservation->setDated($dated);
                $reservation->setDatef($datef);

                $em->persist($reservation);
                $em->flush();


                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notisejour', 'Votre demande a bien été envoyé. Merci.');

                return $this->render('FrontBtobBundle:Voiture:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('FrontBtobBundle:Voiture:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }



    public function listreservationvoitureAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobResvoitureBundle:Reservationvoiture")->findAll();
        return $this->render('FrontBtobBundle:Voiture:listreservationvoiture.html.twig', array('entities' => $entities));
    }

}
