<?php

namespace Btob\BienetreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bienetre
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\BienetreBundle\Entity\BienetreRepository")
 */
class Bienetre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="Imagbien", mappedBy="bienetre")
     */
    protected $imgbien;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean" , nullable=true)
     */
    private $act;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pindex", type="boolean" , nullable=true)
     */
    private $pindex;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255 , nullable=true)
     */
    private $titre;

    /**
     * @var float
     *
     * @ORM\Column(name="prixint", type="float" , nullable=true)
     */
    private $prixint;

    /**
     * @var float
     *
     * @ORM\Column(name="prixpr", type="float" , nullable=true)
     */
    private $prixpr;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text" , nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255 , nullable=true)
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    public  function  __construct(){
        $this->dcr = new \DateTime();

    }
    /**
     * Set act
     *
     * @param boolean $act
     * @return Bienetre
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set pindex
     *
     * @param boolean $pindex
     * @return Bienetre
     */
    public function setPindex($pindex)
    {
        $this->pindex = $pindex;

        return $this;
    }

    /**
     * Get pindex
     *
     * @return boolean 
     */
    public function getPindex()
    {
        return $this->pindex;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Bienetre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set prixint
     *
     * @param float $prixint
     * @return Bienetre
     */
    public function setPrixint($prixint)
    {
        $this->prixint = $prixint;

        return $this;
    }

    /**
     * Get prixint
     *
     * @return float 
     */
    public function getPrixint()
    {
        return $this->prixint;
    }

    /**
     * Set prixpr
     *
     * @param float $prixpr
     * @return Bienetre
     */
    public function setPrixpr($prixpr)
    {
        $this->prixpr = $prixpr;

        return $this;
    }

    /**
     * Get prixpr
     *
     * @return float 
     */
    public function getPrixpr()
    {
        return $this->prixpr;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Bienetre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Bienetre
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Bienetre
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Add imgbien
     *
     * @param \Btob\BienetreBundle\Entity\Imagbien $imgbien
     * @return Bienetre
     */
    public function addImgbien(\Btob\BienetreBundle\Entity\Imagbien $imgbien)
    {
        $this->imgbien[] = $imgbien;

        return $this;
    }

    /**
     * Remove imgbien
     *
     * @param \Btob\BienetreBundle\Entity\Imagbien $imgbien
     */
    public function removeImgbien(\Btob\BienetreBundle\Entity\Imagbien $imgbien)
    {
        $this->imgbien->removeElement($imgbien);
    }

    /**
     * Get imgbien
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImgbien()
    {
        return $this->imgbien;
    }
}
