<?php

namespace Btob\SejourBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationsejour
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SejourBundle\Entity\ReservationsejourRepository")
 */
class Reservationsejour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
    
    

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;
    

    /**
     * @ORM\ManyToOne(targetEntity="Sejour", inversedBy="reservationsejour")
     * @ORM\JoinColumn(name="sejour_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $sejour;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sejourprice", inversedBy="reservationsejour")
     * @ORM\JoinColumn(name="sejourprice_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $sejourprice;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationsejour")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationsejour")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

   
    /**
     * @ORM\OneToMany(targetEntity="Reservationsdetail", mappedBy="reservationsejour", cascade={"remove"})
     */
    protected $reservationsdetail;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationsejour
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    

    /**
     * Set total
     *
     * @param float $total
     * @return Reservationsejour
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * Set sejourprice
     *
     * @param \Btob\SejourBundle\Entity\Sejourprice $sejourprice
     * @return Reservationcircuit
     */
    public function setSejourprice(\Btob\SejourBundle\Entity\Sejourprice $sejourprice = null)
    {
        $this->sejourprice = $sejourprice;

        return $this;
    }

    /**
     * Get sejourprice
     *
     * @return \Btob\SejourBundle\Entity\Sejourprice 
     */
    public function getSejourprice()
    {
        return $this->sejourprice;
    }
    
       
    
    /**
     * Set sejour
     *
     * @param \Btob\SejourBundle\Entity\Sejour $sejour
     * @return Reservationsejour
     */
    public function setSejour(\Btob\SejourBundle\Entity\Sejour $sejour = null)
    {
        $this->sejour = $sejour;

        return $this;
    }

    /**
     * Get sejour
     *
     * @return \Btob\SejourBundle\Entity\Sejour 
     */
    public function getSejour()
    {
        return $this->sejour;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationsejour
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Reservationsejour
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

   


    /**
     * Add reservationsdetail
     *
     * @param \Btob\SejourBundle\Entity\Reservationsdetail $reservationsdetail
     * @return Reservationsejour
     */
    public function addReservationsdetail(\Btob\SejourBundle\Entity\Reservationsdetail $reservationsdetail)
    {
        $this->reservationsdetail[] = $reservationsdetail;

        return $this;
    }

    /**
     * Remove reservationsdetail
     *
     * @param \Btob\SejourBundle\Entity\Reservationsdetail $reservationsdetail
     */
    public function removeReservationsdetail(\Btob\SejourBundle\Entity\Reservationsdetail $reservationsdetail)
    {
        $this->reservationsdetail->removeElement($reservationsdetail);
    }

    /**
     * Get reservationsdetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservationsdetail()
    {
        return $this->reservationsdetail;
    }


}
