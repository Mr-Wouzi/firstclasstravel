<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Amg;
use Btob\HotelBundle\Form\AmgType;

class AmgController extends Controller
{

    public function indexAction()
    {
        $amg = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Amg')
            ->findAll();
        return $this->render('BtobHotelBundle:Amg:index.html.twig', array('entities' => $amg));
    }

    public function addAction()
    {
        $amg = new Amg();
        $form = $this->createForm(new AmgType(), $amg);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $amg->setFile($request->request->get('files'));
                $em->persist($amg);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_amenagement_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Amg:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id)
    {
        $request = $this->get('request');
        $amg = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Amg')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new AmgType(), $amg);
        $file = $amg->getFile();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $amg->setFile($request->request->get("files"));
            $em->flush();

            return $this->redirect($this->generateUrl('btob_amenagement_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Amg:form.html.twig', array('form' => $form->createView(), 'id' => $id, 'file' => $file)
        );
    }

    public function deleteAction(Amg $amg)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$amg) {
            throw new NotFoundHttpException('amenagement non trouvée');
        }
        $em->remove($amg);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_amenagement_homepage'));
    }

}
