<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pays
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PaysRepository")
 */
class Pays {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;

    /**
     * @ORM\OneToMany(targetEntity="Ville", mappedBy="pays", cascade={"remove"})
     */
    protected $ville;

    /**
     * @ORM\OneToMany(targetEntity="Hotel", mappedBy="pays", cascade={"remove"})
     */
    protected $hotel;

    public function __toString() {

        return "" . $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ville = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotel = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Pays
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Pays
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Add ville
     *
     * @param \Btob\HotelBundle\Entity\Ville $ville
     * @return Pays
     */
    public function addVille(\Btob\HotelBundle\Entity\Ville $ville)
    {
        $this->ville[] = $ville;

        return $this;
    }

    /**
     * Remove ville
     *
     * @param \Btob\HotelBundle\Entity\Ville $ville
     */
    public function removeVille(\Btob\HotelBundle\Entity\Ville $ville)
    {
        $this->ville->removeElement($ville);
    }

    /**
     * Get ville
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Add hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Pays
     */
    public function addHotel(\Btob\HotelBundle\Entity\Hotel $hotel)
    {
        $this->hotel[] = $hotel;

        return $this;
    }

    /**
     * Remove hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     */
    public function removeHotel(\Btob\HotelBundle\Entity\Hotel $hotel)
    {
        $this->hotel->removeElement($hotel);
    }

    /**
     * Get hotel
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotel()
    {
        return $this->hotel;
    }
}
