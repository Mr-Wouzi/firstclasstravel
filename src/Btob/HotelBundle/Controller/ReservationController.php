<?php

namespace Btob\HotelBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Resacomment;
use Btob\HotelBundle\Entity\Notification;
use User\UserBundle\Entity\Solde;
use Symfony\Component\HttpFoundation\JsonResponse;

class ReservationController extends Controller
{
    public function dashAction()
    {
        //echo 'dashmessage';
        $dt = new \DateTime();
        $dtweek = $dt->modify('-7 day');
        $tab = array();
        $tab = array();


        for ($i = 0; $i < 7; ++$i) {
            $dt = new \DateTime();
            $dt->modify("-$i day");
            $tab[$dt->format('Ymd')] = array();
        }
        $dtx = new \DateTime();
        $user = $this->get('security.context')->getToken()->getUser();
        $resaweek = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->getResaDash($dtx, $dtweek);
       // if (in_array('AGENCEID', $user->getRoles()) || in_array('SALES', $user->getRoles())) {
            $tabx = array();
            foreach ($resaweek as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabx[] = $value;
                }
            }
            $resaweek = $tabx;
        //}
        foreach ($resaweek as $value) {
            $tab[$value->getDcr()->format('Ymd')][] = 1;
        }
        ksort($tab);
        $ArrFinal = array();
        $i = 1;
        foreach ($tab as $key => $value) {
            $ArrFinal[strtotime($key)] = count($value);
            ++$i;
        }


        return new JsonResponse($ArrFinal);

    }

    public function indexAction()
    {
        //echo 'indexmessage';
        // filtre de recherch
        $hotelname = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "1000";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $hotelname = trim($request->request->get('hotelname'));
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        //var_dump($notification->getId());
        $resa = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findAll();
        $entities = array();
        foreach ($resa as $value) {
            if ($value->getDel()) {
                $entities[] = $value;
            }
        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {
            if ($agence != "") {
                $id = $agence;
            } else {
                $id = $user->getId();
            }
            $tabuser = array();
            foreach ($entities as $value) {
                if ($value->getUser()->getId() == $id) {
                    $tabuser[] = $value;
                }
            }
            $entities = $tabuser;
        }
        
        
        //filteretat
        if($etat!="1000")
        {
             $tabsearch = array();
             foreach ($entities as $value) {
                if ($value->getEtat() == $etat) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        
        
        
        // test nom d'hôtel
        if ($hotelname != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if ($value->getHotel()->getName() == $hotelname) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        // test client $client
        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDatef()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        return $this->render('BtobHotelBundle:Reservation:index.html.twig', array(
            'entities' => $entities,
            'hotelname' => $hotelname,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));
    }

    public function detailAction(Reservation $reservation)
    {
        $namead = json_decode($reservation->getNamead(), true);
        $agead = json_decode($reservation->getAgeadult(), true);
        $nameenf = json_decode($reservation->getNameenf(), true);
        $ageenfant = json_decode($reservation->getAgeenfant(), true);
        return $this->render('BtobHotelBundle:Reservation:detail.html.twig', array(
            'entry' => $reservation,
            'namead' => $namead,
            'agead' => $agead,
            'nameenf' => $nameenf,
            'ageenfant' => $ageenfant,
        ));
    }

    public function deleteAction(Reservation $reservation)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation->setDel(false);
        $em->persist($reservation);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_reservationback_homepage'));
    }

    public function ajxaddcommentAction()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $id = $request->request->get("id");
        $action = $request->request->get("action");
        $user = $this->get('security.context')->getToken()->getUser();
        $reservation = $resa = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($id);
        $comment = new Resacomment();
        $comment->setAction($action);
        $comment->setUser($user);
        $comment->setReservation($reservation);
        $comment->setIp($ip);
        $em->persist($comment);
        $em->flush();
        exit;
    }

    public function actiAction(Reservation $reservation, $action)
    {

        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $reservation->setEtat($action);
        $em->persist($reservation);
        $em->flush();
        // action =2
        $agence=$reservation->getUser()->getUsername();

        $comment = new Resacomment();
        $user = $this->get('security.context')->getToken()->getUser();
        if ($action == 2) {
            $comment->setAction("Réservation traité");


            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $reservation->getUser()->getName().": Réservation traité... " ;
            $headers = "From: FirstClassTravel\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				  <tr>				   <td>				    <img src="http://bbc-weather.net/first/public_html/front/images/logo.png"/>				   </td>				   <td align="right">				    <img src="http://bbc-weather.net/first/public_html/front/images/telmail.png"/>				   </td>				  </tr>				 </tbody>				</table>
				<br /> <b>Cher Madame/Monsieur,</b><br />
				 Merci pour la confiance, votre  Réservation est traitée.<br /></body>';


            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');
  

        } elseif ($action == 3) {

            //decrimentation de stock de chambre
            $dated = $reservation->getDated();
            $hotel = $reservation->getHotel();
            $agence = $reservation->getUser();
            $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($dated, $hotel->getId(), $agence);
            foreach ($price as $hotelprice) {

                $roomprice = $hotelprice->getPriceroom();
                foreach ($roomprice as $valroomprice) {
                    foreach ($reservation->getReservationdetail() as $valdetail) {
                        if ($valdetail->getRoomname() == $valroomprice->getRoom()->getName()) {
                            $html = "le stocke de " . $valdetail->getRoomname() . " était " . $valroomprice->getQte() . " et maintenant est ";
                            $qte = $valroomprice->getQte() - 1;

                            if ($qte > 0) {
                                $valroomprice->setQte($qte);
                                $html = $html . $qte;
                            } else {
                                $valroomprice->setQte(0);
                                $html .= "0";
                            }

                            $em->persist($valroomprice);
                            $em->flush();
                            $comm = new Resacomment();
                            $comm->setReservation($reservation);
                            $comm->setUser($user);
                            $comm->setAction($html);
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
                        }
                    }

                }

            }


            // ajout de commentaire
            $comment->setAction("Réservation Payeé");

            // envoie d'email de confirmation de paiement
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $reservation->getUser()->getName().": Réservation Payeé... " ;
            $headers = "From: FirstClassTravel\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				  <tr>				   <td>				    <img src="http://bbc-weather.net/first/public_html/front/images/logo.png"/>				   </td>				   <td align="right">				    <img src="http://bbc-weather.net/first/public_html/front/images/telmail.png"/>				   </td>				  </tr>				 </tbody>				</table>
				<br /> <b>Cher Madame/Monsieur,</b><br />
				 Merci pour la confiance, votre  Réservation est Payeé.<br /></body>';


            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');



           // mail($too, $subjects, $message2, $header);
            // déminuer le solde de l'agence
            $solde = new Solde();
            $solde->setObservation("Paiement de la commande " . Tools::RefResa($reservation->getRef(), $reservation->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($reservation->getUser());
            $solde->setSolde(($reservation->getTotal() * (-1)));
            $em->persist($solde);
            $newsolde = $reservation->getUser()->getSold() - $reservation->getTotal();
            $currenuser = $reservation->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);

        } elseif ($action == 0) {
            $comment->setAction("Réservation Annulée");

            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $reservation->getUser()->getName().": Réservation Annulée... " ;
            $headers = "From: FirstClassTravel\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				  <tr>				   <td>				    <img src="http://bbc-weather.net/first/public_html/front/images/logo.png"/>				   </td>				   <td align="right">				    <img src="http://bbc-weather.net/first/public_html/front/images/telmail.png"/>				   </td>				  </tr>				 </tbody>				</table>
				<br /> <b>Cher Madame/Monsieur,</b><br />
				 Merci pour la confiance, votre  Réservation est Annulée.<br /></body>';


            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

  

        } elseif ($action == 1) {
            $comment->setAction('Réservation Confirmer');
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $reservation->getUser()->getName().": Réservation Confirmée... " ;
            $headers = "From: FirstClassTravel\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				  <tr>				   <td>				    <img src="http://bbc-weather.net/first/public_html/front/images/logo.png"/>				   </td>				   <td align="right">				    <img src="http://bbc-weather.net/first/public_html/front/images/telmail.png"/>				   </td>				  </tr>				 </tbody>				</table>
				<br /> <b>Cher Madame/Monsieur,</b><br />
				 Merci pour la confiance, votre  Réservation est Confirmée.<br /></body>';
          


            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

          
        }
        $comment->setUser($user);
        $comment->setReservation($reservation);
        $comment->setIp($ip);
        $em->persist($comment);
        $em->flush();
        // notification
        $notif = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findAll();
        $tst = false;
        $not = null;
        foreach ($notif as $value) {
            if ($reservation->getUser()->getId() == $value->getUser()->getId()) {
                $not = $value;
                $tst = true;
            }
        }
        if ($tst) {
            $note = $not->getNotif() + 1;
            $not->setNotif($note);
        } else {
            $not = new Notification();
            $not->setNotif(1);
            $not->setUser($reservation->getUser());
        }
        $em->persist($not);
        $em->flush();

        return $this->redirect($this->generateUrl('btob_reservationback_detail_homepage', array('id' => $reservation->getId())));
    }

    public function voucherAction(Reservation $reservation)
    {
        // Commentaire
        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $comment = new Resacomment();
        $comment->setAction("Impression de Voucher");
        $comment->setUser($user);
        $comment->setReservation($reservation);
        $comment->setIp($ip);
        $em->persist($comment);
        $em->flush();
        // fin commentaire
        $pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
        $namead = json_decode($reservation->getNamead(), true);
        $nameenf = json_decode($reservation->getNameenf(), true);
        $ageenfant = json_decode($reservation->getAgeenfant(), true);
        $pagefooter = "";
        
		   $dataimg = $reservation->getHotel()->getHotelimg();
       $img = "/back/img/dummy_150x150.gif";
                    $j = 0;
                    foreach ($dataimg as $keyimg => $valimg) {
                        if ($j == 0)
                            $img = $valimg->getFile();
                        if ($valimg->getPriori())
                            $img = $valimg->getFile();
                        ++$j;
                    }
					
        // echo $recaphtml;exit;
        //echo $reservation->getRecap();exit;
        $html = $this->renderView('BtobHotelBundle:Reservation:pdf.html.twig', array(
            'reservation' => $reservation,
            'namead' => $namead,
            'nameenf' => $nameenf,
            'pagefooter' => $pagefooter,
            'ageenfant' => $ageenfant,
            'user' => $user,
            'img' => $img,
			
        ));

        $pdf->writeHTML($html);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }

    public function ajxrefAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $id = $request->request->get("id");
        $ref = $request->request->get("ref");
        $resa = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($id);
        $resa->setRef($ref);
        $em->persist($resa);
        $em->flush();
        exit;
    }
    
    
     public function statsAction()
    {
        //echo 'indexmessage';
        // filtre de recherch
        $hotelname = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $hotelname = trim($request->request->get('hotelname'));
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        //var_dump($notification->getId());
        $resa = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findEtat();
        $entities = array();
        foreach ($resa as $value) {
            if ($value->getDel()) {
                $entities[] = $value;
            }
        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {
            if ($agence != "") {
                $id = $agence;
            } else {
                $id = $user->getId();
            }
            $tabuser = array();
            foreach ($entities as $value) {
                if ($value->getUser()->getId() == $id) {
                    $tabuser[] = $value;
                }
            }
            $entities = $tabuser;
        }
        // test nom d'hôtel
        if ($hotelname != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if ($value->getHotel()->getName() == $hotelname) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        // test client $client
        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDatef()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        return $this->render('BtobHotelBundle:Reservation:stats.html.twig', array(
            'entities' => $entities,
            'hotelname' => $hotelname,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));
    }
    
    
}
