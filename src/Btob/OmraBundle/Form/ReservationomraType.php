<?php

namespace Btob\OmraBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationomraType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('etat', 'choice', array('label' => 'Statut',
                'choices' => array('3' => 'Valider', '2' => 'Annuler', 'Null' => 'En attente'),
                'preferred_choices' => array('Valider'),
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\OmraBundle\Entity\Reservationomra'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_omrabundle_reservationomra';
    }
}
