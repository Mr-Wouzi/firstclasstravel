<?php

namespace Btob\SiminaireBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TsalleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TsalleRepository extends EntityRepository
{
     public function findByRessim($reservationsimid)
    {
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.reservationsim', 'u')
            ->andWhere('u.id =:id')
            ->setParameter('id', $reservationsimid);
        return $query->getQuery()->getResult();
    }
}
