<?php



namespace User\UserBundle\Form;



use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;



class UserType extends AbstractType

{

    /**

     * @param FormBuilderInterface $builder

     * @param array $options

     */

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder

            ->add('name', 'text', array('label' => "Nom de l'agence"))

            //->add('datenaiss')

            //->add('dcr')

            ->add('devise', 'entity', array(

                'class' => 'BtobDashBundle:Devise',

                'empty_value' => 'Choisissez une devise',

                'required' => true,

                'label' => "Devise",

            ))

            ->add('marcher', 'entity', array(

                'class' => 'BtobHotelBundle:Marcher',

                'empty_value' => 'Choisissez un marché',

                'required' => true,

                'label' => "Marché",

            ))

            ->add('responsable', 'text', array('label' => "Responsable", "required" => false))

            ->add('adresse', 'text', array('label' => "Adresse", "required" => false))

            ->add('cp', 'text', array('label' => "Code postal", "required" => false))

            ->add('tel', 'text', array('label' => "Téléphone", "required" => false))

            ->add('fax', 'text', array('label' => "Fax", "required" => false))

            ->add('matriculefiscal', 'text', array('label' => "Matricule Fiscal", "required" => false))

            ->add('observation', 'text', array('label' => "Observation", "required" => false))

            //->add('logo', 'text', array('label' => "logo", "required" => false))

            ->add('marge', 'text', array('label' => "Marge Hotel",'data' => '0', "required" => false))

            ->add('sold', 'text', array('label' => "Sold de l'agence", "required" => false))

            ->add('prst', null, array('label' => "Pourcentage marge Hôtel", "required" => false))

            ->add('illimite', null, array('label' => "Sold illimité", "required" => false))

           // ->add('marque', 'text', array('label' => "Marge marque blanche", "required" => false))

            //->add('prstmarque', null, array('label' => "Pourcentage marque blanche", "required" => false))





            ///////////////////////////////////////////////////////////////////////////////////

            ->add('actomra', null, array('label' => "Activation omra", "required" => false))
             ->add('margeomra', 'text', array('label' => "Marge omra",'data' => '0', "required" => true))
            ->add('prstomra', null, array('label' => "Pourcentage", "required" => false))
            ->add('margecircuit', 'text', array('label' => "Marge circuit",'data' => '0', "required" => true))
            ->add('prstcircuit', null, array('label' => "Pourcentage", "required" => false))
            ->add('actcircuit', null, array('label' => "Activation circuit", "required" => false))

            ->add('margesejour', 'text', array('label' => "Marge séjour",'data' => '0', "required" => true))
             ->add('prstsejour', null, array('label' => "Pourcentage", "required" => false))
            ->add('actsejour', null, array('label' => "Activation séjour", "required" => false))

                
             ->add('margeevenement', 'text', array('label' => "Marge evenement",'data' => '0', "required" => true))
             ->add('prstevenement', null, array('label' => "Pourcentage", "required" => false))
            ->add('actevenement', null, array('label' => "Activation evenement", "required" => false))
   
                
                
            ->add('margevole', 'text', array('label' => "Marge vole",'data' => '0', "required" => false))

            ->add('actvole', null, array('label' => "Activation vole", "required" => false))

            //->add('logo', 'text', array('label' => "logo", "required" => false))

            ->add('margecroissiere', 'text', array('label' => "Marge croissiére",'data' => '0', "required" => true))
             ->add('prstcroissiere', null, array('label' => "Pourcentage", "required" => false))
            ->add('actcroissiere', null, array('label' => "Activation croissiére", "required" => false))

            ->add('margevoiture', 'text', array('label' => "Marge voiture",'data' => '0','data' => "0", "required" => true))
             ->add('prstvoiture', null, array('label' => "Pourcentage", "required" => false))
            ->add('actvoiture', null, array('label' => "Activation voiture", "required" => false))

             ->add('actparc', null, array('label' => "Activation parc", "required" => false))
             ->add('margeparc', 'text', array('label' => "Marge parc",'data' => '0','data' => "0", "required" => true))
            ->add('prstparc', null, array('label' => "Pourcentage", "required" => false))    
                
            ->add('acttransfert', null, array('label' => "Activation transfert", "required" => false))

            ->add('actbienetre', null, array('label' => "Activation bien être", "required" => false))
             ->add('margebienetre', 'text', array('label' => "Marge Bien etre",'data' => '0','data' => "0", "required" => true))
             ->add('prstbienetre', null, array('label' => "Pourcentage", "required" => false))
            
            ->add('actseminaire', null, array('label' => "Activation séminaire", "required" => false))

            ->add('actgrandjeu', null, array('label' => "Activation grand jeu", "required" => false))

            ->add('actactivite', null, array('label' => "Activation activité", "required" => false))

        ;

    }

    

    /**

     * @param OptionsResolverInterface $resolver

     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => 'User\UserBundle\Entity\User'

        ));

    }


    /**

     * @return string

     */

    public function getName()

    {

        return 'user_userbundle_user';

    }

}

