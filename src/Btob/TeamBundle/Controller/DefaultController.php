<?php

namespace Btob\TeamBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\TeamBundle\Entity\Team;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\TeamBundle\Entity\Resateam;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobTeamBundle:Team")->findby(array('active'=>1));
        return $this->render('BtobTeamBundle:Default:index.html.twig', array('entities' => $entities));
    }

    public function detailAction(Team $team)
    {
        return $this->render('BtobTeamBundle:Default:detail.html.twig', array('entry' => $team));
    }

    public function reservationAction(Team $team)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $resa=new Resateam();

                $date = Tools::explodedate($request->request->get("dated"),'/');
                $resa->setDated(new \DateTime($date));
                $resa->setUser($this->get('security.context')->getToken()->getUser());
                $resa->setClient($client);
                $resa->setBebe($request->request->get("bebe"));
                $resa->setAdulte($request->request->get("adulte"));
                $resa->setEnfant($request->request->get("enfant"));
                $resa->setComment($request->request->get("body"));
                $resa->setTeam($team);
                $em->persist($resa);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_team_validation'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobTeamBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$team));
    }
    public function validationAction(){
        return $this->render('BtobTeamBundle:Default:validation.html.twig', array());
    }
}
