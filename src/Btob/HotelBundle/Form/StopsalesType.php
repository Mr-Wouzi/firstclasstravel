<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use User\UserBundle\Entity\UserRepository;
class StopsalesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dated', 'date', array('widget' => 'single_text', 'label' => "Date debut", 'format' => 'dd/MM/yyyy'))
            ->add('dates', 'date', array('widget' => 'single_text', 'label' => "Date debut", 'format' => 'dd/MM/yyyy'))
            ->add('act', null, array('label' => "Active?", 'required' => false))
            ->add('user', 'entity', array(
                'class' => 'UserUserBundle:User',
                /*'query_builder'=>function(UserRepository $r) {
                    return $r->findBy(array('Roles','AGENCID'));
                },*/
                'empty_value' => 'Choisissez une agence',
                'required' => true,
                'label' => "Agence",
                'multiple'=>false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Stopsales'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_stopsales';
    }
}
