<?php
namespace Front\BtobBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\OmraBundle\Entity\Omra;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\User;
use Btob\EvenementBundle\Entity\Evenement;
use Symfony\Component\HttpFoundation\Request;

use Btob\EvenementBundle\Entity\Reservationevenement;

use Btob\EvenementBundle\Entity\Reservationevdetail;
class EvenementController extends Controller
{

    public function indexAction(Request $request)
    {
        $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Evenements'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;

       $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->listPrice();

          $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $evenementprices,
            $request->query->get('page', 1)/*page number*/,
            9 /*limit per page*/
        );
        
        
        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        return $this->render('FrontBtobBundle:Evenement:index.html.twig', array(
            "convert" => $convert,
            "convertd" => $convertd,
            'entities' => $entities,
            'evenementprices' => $evenementprices,
            'banner' => $banner,
            ));
    }

    public function detailAction(Evenement $Evenement)
    {
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($Evenement->getSeotitle())
            ->addMeta('name', 'description', $Evenement->getSeodescreption())
            ->addMeta( 'name', 'keywords',$Evenement->getSeokeyword())
        ;
        
          $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');

       $evenementperiode = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->findByEvenement($Evenement->getId());

        return $this->render('FrontBtobBundle:Evenement:detail.html.twig', array("convert" => $convert,
            "convertd" => $convertd,'entry' => $Evenement,'periods' => $evenementperiode));
    }


    
    
    public function personalisationAction(Evenement $evenement)

    {

       $em = $this->getDoctrine()->getManager();
       
       $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->findByEvenement($evenement->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

          $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');

        if ($request->getMethod() == 'POST') {
            
            $evenementprices = $request->request->get("evenementprice");
            $session->set('evenementprice', $evenementprices);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
            $nbbebe = $request->request->get("nbbebe");
            $session->set('nbbebe', $nbbebe);
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
            
          return $this->redirect($this->generateUrl('front_inscrip_reservation_evenement', array('id'=>$evenement->getId())));

            
        }
       
       
       
       
    return $this->render('FrontBtobBundle:Evenement:personalisation.html.twig', array("convert" => $convert,
            "convertd" => $convertd,'entry' => $evenement,'evenementprices' => $evenementprices));

    
    }
    
    
    
    
    public function inscriptionAction(Evenement $evenement)

    {

       $em = $this->getDoctrine()->getManager();
       
       $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->findByEvenement($evenement->getId());
       $evenementsupp = $this->getDoctrine()->getRepository("BtobEvenementBundle:Supplementev")->findSupplementByEvenement($evenement->getId());
       
          $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');


       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $evenementprice = $session->get("evenementprice");
        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbbebe = $session->get("nbbebe");
        $nbenf = $session->get("nbenf");
        
        $Array = array();
        
        
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
                //bebe
                if(isset($request->request->get("nameb")[$i]))
                {
                     $bebe=array();
                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)
                 {
                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];
                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];
                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];
                
                
                if(isset($request->request->get("suppb")[$i][$l]))
                 {
                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['suppb']=null;  
                 }
                 
                 
                 
                 
                 }
                 
                 $Array[$i]['bebe']=$bebe;
                }
                
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('evenementprice', $evenementprice);
            
          

          return $this->redirect($this->generateUrl('front_evenement_reservation_homepage', array('id'=>$evenement->getId())));

            
        }
       
       
       
       
    return $this->render('FrontBtobBundle:Evenement:inscription.html.twig', array("convert" => $convert,
            "convertd" => $convertd,'entry' => $evenement,'evenementsupp' => $evenementsupp,'evenementprices' => $evenementprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf));

    
    }
    
    
    
  
    
     public function reservationevenementAction(Evenement $evenement)

    {

        $em = $this->getDoctrine()->getManager();
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($evenement->getSeotitle())
            ->addMeta('name', 'description', $evenement->getSeodescreption())
            ->addMeta( 'name', 'keywords',$evenement->getSeokeyword())
        ;
        $session = $this->getRequest()->getSession();

          $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $evenementperiode = $session->get("evenementprice");
        
        
       $priceev =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenementprice')->find($evenementperiode);   

       
        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           
          
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
             
                 
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPriceenf(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              
              
              
              
              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
                  if(isset($valb['suppb']))
                  {
                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPriceb();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
              $totalsuppb+= $suppbp;
              
              
              
              
           }
           }
           
        
          $ArrBase[$key]['price'] = $this->getDoctrine()
                        ->getRepository('BtobEvenementBundle:Evenementprice')
                        ->calculev($priceev,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $evenementprice = $session->get("evenementprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();
                

                
                
                
                
                
                
                
                

                $resa=new Reservationevenement();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setEvenement($evenement);
                $resa->setEvenementprice($priceev);
                
                $resa->setTotal($total);
                  
                $em->persist($resa);

                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
       
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
           
           $resadetail=new Reservationevdetail();
           $resadetail->setReservationevenement($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                 
                  
                
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               
           $resadetails=new Reservationevdetail();
           $resadetails->setReservationevenement($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPriceenf(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
            
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
               
               
               $resadetailb=new Reservationevdetail();
           $resadetailb->setReservationevenement($resa);
           $resadetailb->setChambre($key+1);
           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);
          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);
          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);
                  if(isset($valb['suppb']))
                  {

                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPriceb();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));
              $totalsuppb+= $suppbp;
              
              
             
                 
                  
                  
              

            $em->persist($resadetailb);
            $em->flush();


              
           }
           }
           
        
          $ArrBase[$key]['price'] = $this->getDoctrine()
                        ->getRepository('BtobEvenementBundle:Evenementprice')
                        ->calculev($priceev,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         
          $total+= $ArrBase[$key]['price']; 
          
          

           
       }    
                
                
                
                
                
                
                
                
                
                
                
    $reservationdetails = $this->getDoctrine()->getRepository("BtobEvenementBundle:Reservationevdetail")->findByResv($resa->getId());   

                
                
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $resa->getClient()->getEmail();
               


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "First Class Travel: Réservation Evénements & soirées" ;
                $headers = "From:First-Class-Travel infos.firstclass@gmail.com\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				  <tr>				
				<td>				   
				<a href="http://www.firstclasstravel.ma/">
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
				</a>		
				</td>				   <td align="right">				  
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>				
				</td>				  </tr>				 </tbody>				</table>';            			      
                $message1 .='
				 Cher Madame/Monsieur,<br />
				 Merci pour votre réservation. Votre demande a bien été envoyée, nous traiterons votre demande dans les plus brefs délais.<br />
				 <table width="90%"  cellspacing="1" border="0">';
				$message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre commande :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" width="170"><b style="padding-left:10px;">Date d\'événement </b></td>';
                $message1 .= '<td height="30" width="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $evenement->getTitre().' : '.$resa->getEvenementprice()->getDated()->format('Y-m-d') . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3">';
				$message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0" style="margin-left:10px;">';
				$message1 .= '<tr>';
				$message1 .= '<td><strong>Chambre</strong></td>';
				$message1 .= '<td><strong>Type</strong></td>';
				$message1 .= '<td><strong>Nom & prénom</strong></td>';
				$message1 .= '<td><strong>Age</strong></td>';
				$message1 .= '<td><strong>Suppliments</strong></td>';
				$message1 .= '</tr>';
                foreach( $reservationdetails as $kc=>$valuc){
          
                if($valuc->getNamead()!= NULL){
				$message1 .= '<tr>';
				$message1 .= '<td>'. $valuc->getChambre() .'</td>';
				$message1 .= '<td>Adulte</td>';
				$message1 .= '<td>'. $valuc->getNamead(). ' ' .$valuc->getPrenomad() .'</td>';
				$message1 .= '<td>'. $valuc->getAgead() .'</td>';
				if($valuc->getSuppad()!= 'null'){
				$message1 .= '<td>'. $valuc->getSuppad() .'</td>';
                }else{
				$message1 .= '<td></td>';
				}
				$message1 .= '</tr>';
				}
                
                if($valuc->getNameenf()!= NULL){				
				$message1 .= '<tr>';
				$message1 .= '<td>'. $valuc->getChambre() .'</td>';
				$message1 .= '<td>Enfant</td>';
				$message1 .= '<td>'. $valuc->getNameenf(). ' ' .$valuc->getPrenomenf() .'</td>';
				$message1 .= '<td>'. $valuc->getAgeenf() .'</td>';
				if($valuc->getSuppenf()!= 'null'){
				$message1 .= '<td>'. $valuc->getSuppenf() .'</td>';
                }else{
				$message1 .= '<td></td>';
				}
				$message1 .= '</tr>';
				}
                 
                
                if($valuc->getNameb()!= NULL){      
				$message1 .= '<tr>';
				$message1 .= '<td>'. $valuc->getChambre() .'</td>';
				$message1 .= '<td>Bébé</td>';
				$message1 .= '<td>'. $valuc->getNameb(). ' ' .$valuc->getPrenomb() .'</td>';
				$message1 .= '<td>'. $valuc->getAgeb() .'</td>';
				if($valuc->getSuppb()!= 'null'){
				$message1 .= '<td>'. $valuc->getSuppb() .'</td>';
                }else{
				$message1 .= '<td></td>';
				}
				$message1 .= '</tr>';
                }
				
				}
				   
                $message1 .= '</table></td></tr>';
				
				$message1 .= '<tr>';                
				$message1 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed V et Av. Hassen I Bloc B - Imm. SIBAM2 40020 - Marrakech
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                        <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
 </td>';                
				$message1 .= '</tr>';
                $message1 .= '</table>';
				$message1 .= '</body>';
                  

                mail($to, $subject, $message1, $headers);
                //mail('afef.tuninfo@gmail.com', $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservation Evénements & soirées" ;
                $header = "From:First-Class-Travel infos.firstclass@gmail.com\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				  <tr>				
			    <td>				    
			    <a href="http://www.firstclasstravel.ma/">
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
				</a>	
			    </td>				   <td align="right">				
			    <img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>			
			    </td>				  </tr>				 </tbody>				</table>';            			

				$message2 .='
                 <table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Commande :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Période voyage organisé </b></td>';
                $message2 .= '<td height="30" width="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $evenement->getTitre().' : '.$resa->getEvenementprice()->getDated()->format('Y-m-d') . '</td>';
                $message2 .= '</tr>';
				$message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">C.I.N / Passeport</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getCin() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civilité</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' .$client->getCiv() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getPname() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $to . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getTel() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getPays() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getAdresse() . '</td>';
                $message2 .= '</tr>';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3">';
				$message2 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0" style="margin-left:10px;">';
				$message2 .= '<tr>';
				$message2 .= '<td height="30"><strong>Chambre</strong></td>';
				$message2 .= '<td height="30"><strong>Type</strong></td>';
				$message2 .= '<td height="30"><strong>Nom & prénom</strong></td>';
				$message2 .= '<td height="30"><strong>Age</strong></td>';
				$message2 .= '<td height="30"><strong>Suppliments</strong></td>';
				$message2 .= '</tr>';
				
				foreach( $reservationdetails as $kc=>$valuc){
                       
                
                if($valuc->getNamead()!= NULL){
				$message2 .= '<tr>';
				$message2 .= '<td height="30">'. $valuc->getChambre() .'</td>';
				$message2 .= '<td height="30">Adulte</td>';
				$message2 .= '<td height="30">'. $valuc->getNamead(). ' ' .$valuc->getPrenomad() .'</td>';
				$message2 .= '<td height="30">'. $valuc->getAgead() .'</td>';
				if($valuc->getSuppad()!= 'null'){
				$message2 .= '<td height="30">'. $valuc->getSuppad() .'</td>';
                }else{
				$message2 .= '<td height="30"></td>';
				}
				$message2 .= '</tr>';
				}
                
                if($valuc->getNameenf()!= NULL){				
				$message2 .= '<tr>';
				$message2 .= '<td height="30">'. $valuc->getChambre() .'</td>';
				$message2 .= '<td height="30">Enfant</td>';
				$message2 .= '<td height="30">'. $valuc->getNameenf(). ' ' .$valuc->getPrenomenf() .'</td>';
				$message2 .= '<td height="30">'. $valuc->getAgeenf() .'</td>';
				if($valuc->getSuppenf()!= 'null'){
				$message2 .= '<td height="30">'. $valuc->getSuppenf() .'</td>';
                }else{
				$message2 .= '<td height="30"></td>';
				}
				$message2 .= '</tr>';
				}
                 
                
                if($valuc->getNameb()!= NULL){      
				$message2 .= '<tr>';
				$message2 .= '<td height="30">'. $valuc->getChambre() .'</td>';
				$message2 .= '<td height="30">Bébé</td>';
				$message2 .= '<td height="30">'. $valuc->getNameb(). ' ' .$valuc->getPrenomb() .'</td>';
				$message2 .= '<td height="30">'. $valuc->getAgeb() .'</td>';
				if($valuc->getSuppb()!= 'null'){
				$message2 .= '<td height="30">'. $valuc->getSuppb() .'</td>';
                }else{
				$message2 .= '<td height="30"></td>';
				}
				$message2 .= '</tr>';
                }
				 
                };
				   
                $message2 .= '</table></td></tr>';
				
				$message2 .= '<tr>';                
				$message2 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed V et Av. Hassen I Bloc B - Imm. SIBAM2 40020 - Marrakech
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                        <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
 </td>';                
				$message2 .= '</tr>';
                $message2 .= '</table>';
				$message2 .= '</body>';

                mail($too, $subjects, $message2, $header);
                mail('infos.firstclass@gmail.com', $subjects, $message2, $header);
                mail('shamid-fct@menara.ma', $subjects, $message2, $header);
                mail('hmahi-fct@menara.ma', $subjects, $message2, $header);
                mail('firas.benkharrat@gmail.com', $subjects, $message2, $header);
                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);
                
               

            } else {
                echo $form->getErrors();
            }
                $request->getSession()->getFlashBag()->add('notievenementf', 'Votre réservation a été bien prise en compte. Notre conseiller vous contactera dans les plus bref délais.');

                return $this->redirect($this->generateUrl('front_evenement_homepage'));



        }

        return $this->render('FrontBtobBundle:Evenement:reservation.html.twig', array("convert" => $convert,
            "convertd" => $convertd,'form' => $form->createView(),'entry'=>$evenement,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total));

    }


}
