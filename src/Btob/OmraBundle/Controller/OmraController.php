<?php

namespace Btob\OmraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Btob\OmraBundle\Entity\Omra;
use Btob\OmraBundle\Entity\Imgomra;
use Btob\OmraBundle\Form\OmraType;

/**
 * omra controller.
 *
 * @Route("/omra")
 */
class OmraController extends Controller
{

    /**
     * Lists all omra entities.
     *
     * @Route("/", name="omra")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobOmraBundle:Omra')->findAll();

        return $this->render('BtobOmraBundle:Omra:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new omra entity.
     *
     * @Route("/", name="omra_create")
     * @Method("POST")
     * @Template("BtobOmraBundle:omra:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new omra();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
       // $entity->upload();


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('omra_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a omra entity.
     *
     * @param omra $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(omra $entity)
    {
        $form = $this->createForm(new omraType(), $entity, array(
            'action' => $this->generateUrl('omra_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new omra entity.
     *
     * @Route("/new", name="omra_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new omra();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a omra entity.
     *
     * @Route("/{id}", name="omra_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobOmraBundle:Omra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find omra entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing omra entity.
     *
     * @Route("/{id}/edit", name="omra_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobOmraBundle:Omra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find omra entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a omra entity.
    *
    * @param omra $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(omra $entity)
    {
        $form = $this->createForm(new omraType(), $entity, array(
            'action' => $this->generateUrl('omra_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing omra entity.
     *
     * @Route("/{id}", name="omra_update")
     * @Method("PUT")
     * @Template("BtobOmraBundle:omra:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobOmraBundle:Omra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find omra entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('omra_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a omra entity.
     *
     * @Route("/{id}", name="omra_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Omra $omra) {
        $em = $this->getDoctrine()->getManager();

        if (!$omra) {
            throw new NotFoundHttpException("omra non trouvée");
        }
        $em->remove($omra);
        $em->flush();
        return $this->redirect($this->generateUrl('omra'));
    }


    /**
     * Creates a form to delete a omra entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('omra_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    public function delete1Action(Omra $omra) {
        $em = $this->getDoctrine()->getManager();

        if (!$omra) {
            throw new NotFoundHttpException("omra non trouvée");
        }
        $em->remove($omra);
        $em->flush();
        return $this->redirect($this->generateUrl('omra'));
    }

    public function adAction() {
        $omra2 = new Omra();
        $form = $this->createForm(new omraType(), $omra2);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
                $em = $this->getDoctrine()->getManager();
                $em->persist($omra2);
                $em->flush();
                if (is_array($request->request->get("files")))
                    foreach ($request->request->get("files") as $key => $value) {
                        if ($value != "") {
                            $img = new Imgomra();
                            $img->setomra($omra2);
                            $img->setImage($value);
                            $em->persist($img);
                            $em->flush();

                        }
                    }
                return $this->redirect($this->generateUrl('omra'));

        }
        return $this->render('BtobOmraBundle:Omra:form.html.twig', array('form' => $form->createView() , 'entity'=>$omra2));
    }

    public function edit1Action($id) {/*
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $omra = $em->getRepository('BtobOmraBundle:Omra')->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new omraType(), $omra);
        $form->handleRequest($request);

        if ($form->isValid()) {
            foreach ($omra->getImgomra() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgomra();
                        $img->setomra($omra);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('omra'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobOmraBundle:Omra:form.html.twig', array('form' => $form->createView(), 'id' => $id,'entity'=>$omra)
        );
    }
*/



        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $omra = $em->getRepository('BtobOmraBundle:Omra')->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new OmraType(), $omra);
        $form->handleRequest($request);

        if ($form->isValid()) {
            foreach ($omra->getImgomra() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgomra();
                        $img->setOmra($omra);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('omra'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobOmraBundle:Omra:form.html.twig', array('form' => $form->createView(), 'id' => $id,'entity'=>$omra)
        );

}
}
