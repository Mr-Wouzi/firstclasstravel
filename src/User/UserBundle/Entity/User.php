<?php

namespace User\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * * @ORM\Table(name="User")
 * @ORM\Entity (repositoryClass="User\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100,nullable=true)
     */
    private $name;
    /**
     * @var date
     *
     * @ORM\Column(name="datenaiss", type="date",nullable=true)
     */
    private $datenaiss;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dcr", type="datetime",nullable=true)
     */
    private $dcr;

    /**
     * @ORM\ManyToOne(targetEntity="Btob\DashBundle\Entity\Devise", inversedBy="User")
     * @ORM\JoinColumn(name="devise_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $devise;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Marcher", inversedBy="User")
     * @ORM\JoinColumn(name="marcher_id", referencedColumnName="id",onDelete="CASCADE",nullable=true)
     */
    protected $marcher;


    /**
     * @var string
     *
     * @ORM\Column(name="responsable", type="string", length=255,nullable=true)
     */
    private $responsable;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255,nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=10,nullable=true)
     */
    private $cp;
    /**
     * @var float
     *
     * @ORM\Column(name="sold", type="float", nullable=true)
     */
    private $sold;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=25,nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=25,nullable=true)
     */
    private $fax;
    /**
     * @var string
     *
     * @ORM\Column(name="matriculefiscal", type="string", length=50,nullable=true)
     */
    private $matriculefiscal;
    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="string", length=255,nullable=true)
     */
    private $observation;
    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255,nullable=true)
     */
    private $logo;
    /**
     * @var boolean
     *
     * @ORM\Column(name="illimite", type="boolean" , nullable=true)
     */
    private $illimite;
    
    /**
     * @ORM\OneToMany(targetEntity="Btob\HotelBundle\Entity\Notification", mappedBy="user", cascade={"remove"})
     */
    protected $notification;

    /**
     * @ORM\OneToMany(targetEntity="Btob\HotelBundle\Entity\Hotelmarge", mappedBy="user", cascade={"remove"})
     */
    protected $hotelmarge;

    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float",nullable=true)
     */
    private $marge;
    /**
     * @var boolean
     *
     * @ORM\Column(name="prst", type="boolean" , nullable=true)
     */
    private $prst;
	/**
     * @var float
     *
     * @ORM\Column(name="margeomra", type="float",nullable=true)
     */
    private $margeomra;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prstomra", type="boolean" , nullable=true)
     */
    private $prstomra;
	/**
     * @var boolean
     *
     * @ORM\Column(name="actomra", type="boolean" , nullable=true)
     */
    private $actomra;
	
	
    /**
     * @var boolean
     *
     * @ORM\Column(name="actparc", type="boolean" , nullable=true)
     */
    private $actparc;
	/**
     * @var float
     *
     * @ORM\Column(name="margecircuit", type="float",nullable=true)
     */
    private $margecircuit;
   /**
     * @var boolean
     *
     * @ORM\Column(name="prstcircuit", type="boolean" , nullable=true)
     */
    private $prstcircuit;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="actcircuit", type="boolean" , nullable=true)
     */
    private $actcircuit;





    /**
     * @var float
     *
     * @ORM\Column(name="margesejour", type="float",nullable=true)
     */
    private $margesejour;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="prstsejour", type="boolean" , nullable=true)
     */
    private $prstsejour;
    /**
     * @var boolean
     *
     * @ORM\Column(name="actsejour", type="boolean" , nullable=true)
     */
    private $actsejour;
    
    
    
     /**
     * @var float
     *
     * @ORM\Column(name="margeevenement", type="float",nullable=true)
     */
    private $margeevenement;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="prstevenement", type="boolean" , nullable=true)
     */
    private $prstevenement;
    /**
     * @var boolean
     *
     * @ORM\Column(name="actevenement", type="boolean" , nullable=true)
     */
    private $actevenement;



    /**
     * @var float
     *
     * @ORM\Column(name="margevole", type="float",nullable=true)
     */
    private $margevole;
    /**
     * @var boolean
     *
     * @ORM\Column(name="actvole", type="boolean" , nullable=true)
     */
    private $actvole;


    /**
     * @var float
     *
     * @ORM\Column(name="margecroissiere", type="float",nullable=true)
     */
    private $margecroissiere;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="prstcroissiere", type="boolean" , nullable=true)
     */
    private $prstcroissiere;
    /**
     * @var boolean
     *
     * @ORM\Column(name="actcroissiere", type="boolean" , nullable=true)
     */
    private $actcroissiere;

    /**
     * @var float
     *
     * @ORM\Column(name="margevoiture", type="float",nullable=true)
     */
    private $margevoiture;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="prstvoiture", type="boolean" , nullable=true)
     */
    private $prstvoiture;
    /**
     * @var boolean
     *
     * @ORM\Column(name="actvoiture", type="boolean" , nullable=true)
     */
    private $actvoiture;


    /**
     * @var boolean
     *
     * @ORM\Column(name="acttransfert", type="boolean" , nullable=true)
     */
    private $acttransfert;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actbienetre", type="boolean" , nullable=true)
     */
    private $actbienetre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actseminaire", type="boolean" , nullable=true)
     */
    private $actseminaire;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actgrandjeu", type="boolean" , nullable=true)
     */
    private $actgrandjeu;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actactivite", type="boolean" , nullable=true)
     */
    private $actactivite;

/**
     * @var float
     *
     * @ORM\Column(name="margebienetre", type="float",nullable=true)
     */
    private $margebienetre;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="prstbienetre", type="boolean" , nullable=true)
     */
    private $prstbienetre;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="margeparc", type="float",nullable=true)
     */
    private $margeparc;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="prstparc", type="boolean" , nullable=true)
     */
    private $prstparc;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->notification = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelmarge = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set datenaiss
     *
     * @param \DateTime $datenaiss
     * @return User
     */
    public function setDatenaiss($datenaiss)
    {
        $this->datenaiss = $datenaiss;

        return $this;
    }

    /**
     * Get datenaiss
     *
     * @return \DateTime 
     */
    public function getDatenaiss()
    {
        return $this->datenaiss;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return User
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     * @return User
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string 
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return User
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set sold
     *
     * @param float $sold
     * @return User
     */
    public function setSold($sold)
    {
        $this->sold = $sold;

        return $this;
    }

    /**
     * Get sold
     *
     * @return float 
     */
    public function getSold()
    {
        return $this->sold;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return User
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set matriculefiscal
     *
     * @param string $matriculefiscal
     * @return User
     */
    public function setMatriculefiscal($matriculefiscal)
    {
        $this->matriculefiscal = $matriculefiscal;

        return $this;
    }

    /**
     * Get matriculefiscal
     *
     * @return string 
     */
    public function getMatriculefiscal()
    {
        return $this->matriculefiscal;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return User
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return User
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set illimite
     *
     * @param boolean $illimite
     * @return User
     */
    public function setIllimite($illimite)
    {
        $this->illimite = $illimite;

        return $this;
    }

    /**
     * Get illimite
     *
     * @return boolean 
     */
    public function getIllimite()
    {
        return $this->illimite;
    }

    /**
     * Set marge
     *
     * @param float $marge
     * @return User
     */
    public function setMarge($marge)
    {
        $this->marge = $marge;

        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */
    public function getMarge()
    {
        return $this->marge;
    }

    /**
     * Set prst
     *
     * @param boolean $prst
     * @return User
     */
    public function setPrst($prst)
    {
        $this->prst = $prst;

        return $this;
    }

    /**
     * Get prst
     *
     * @return boolean 
     */
    public function getPrst()
    {
        return $this->prst;
    }

    /**
     * Set devise
     *
     * @param \Btob\DashBundle\Entity\Devise $devise
     * @return User
     */
    public function setDevise(\Btob\DashBundle\Entity\Devise $devise = null)
    {
        $this->devise = $devise;

        return $this;
    }

    /**
     * Get devise
     *
     * @return \Btob\DashBundle\Entity\Devise 
     */
    public function getDevise()
    {
        return $this->devise;
    }

    /**
     * Set marcher
     *
     * @param \Btob\HotelBundle\Entity\Marcher $marcher
     * @return User
     */
    public function setMarcher(\Btob\HotelBundle\Entity\Marcher $marcher = null)
    {
        $this->marcher = $marcher;

        return $this;
    }

    /**
     * Get marcher
     *
     * @return \Btob\HotelBundle\Entity\Marcher 
     */
    public function getMarcher()
    {
        return $this->marcher;
    }

    /**
     * Add notification
     *
     * @param \Btob\HotelBundle\Entity\Notification $notification
     * @return User
     */
    public function addNotification(\Btob\HotelBundle\Entity\Notification $notification)
    {
        $this->notification[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param \Btob\HotelBundle\Entity\Notification $notification
     */
    public function removeNotification(\Btob\HotelBundle\Entity\Notification $notification)
    {
        $this->notification->removeElement($notification);
    }

    /**
     * Get notification
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Add hotelmarge
     *
     * @param \Btob\HotelBundle\Entity\Hotelmarge $hotelmarge
     * @return User
     */
    public function addHotelmarge(\Btob\HotelBundle\Entity\Hotelmarge $hotelmarge)
    {
        $this->hotelmarge[] = $hotelmarge;

        return $this;
    }

    /**
     * Remove hotelmarge
     *
     * @param \Btob\HotelBundle\Entity\Hotelmarge $hotelmarge
     */
    public function removeHotelmarge(\Btob\HotelBundle\Entity\Hotelmarge $hotelmarge)
    {
        $this->hotelmarge->removeElement($hotelmarge);
    }

    /**
     * Get hotelmarge
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelmarge()
    {
        return $this->hotelmarge;
    }
	
	
	 /**
     * Set margeomra
     *
     * @param float $margeomra
     * @return User
     */
    public function setMargeomra($margeomra)
    {
        $this->margeomra = $margeomra;

        return $this;
    }

    /**
     * Get margeomra
     *
     * @return float 
     */
    public function getMargeomra()
    {
        return $this->margeomra;
    }
	 /**
     * Set actomra
     *
     * @param boolean $actomra
     * @return User
     */
    public function setActomra($actomra)
    {
        $this->actomra = $actomra;

        return $this;
    }

    /**
     * Get actomra
     *
     * @return boolean 
     */
    public function getActomra()
    {
        return $this->actomra;
    }
	
	
	
    /**
     * Set margecircuit
     *
     * @param float $margecircuit
     * @return User
     */
    public function setMargecircuit($margecircuit)
    {
        $this->margecircuit = $margecircuit;

        return $this;
    }

    /**
     * Get margecircuit
     *
     * @return float 
     */
    public function getMargecircuit()
    {
        return $this->margecircuit;
    }

    /**
     * Set actcircuit
     *
     * @param boolean $actcircuit
     * @return User
     */
    public function setActcircuit($actcircuit)
    {
        $this->actcircuit = $actcircuit;

        return $this;
    }

    /**
     * Get actcircuit
     *
     * @return boolean 
     */
    public function getActcircuit()
    {
        return $this->actcircuit;
    }

    /**
     * Set margesejour
     *
     * @param float $margesejour
     * @return User
     */
    public function setMargesejour($margesejour)
    {
        $this->margesejour = $margesejour;

        return $this;
    }

    /**
     * Get margesejour
     *
     * @return float 
     */
    public function getMargesejour()
    {
        return $this->margesejour;
    }

    /**
     * Set actsejour
     *
     * @param boolean $actsejour
     * @return User
     */
    public function setActsejour($actsejour)
    {
        $this->actsejour = $actsejour;

        return $this;
    }

    /**
     * Get actsejour
     *
     * @return boolean 
     */
    public function getActsejour()
    {
        return $this->actsejour;
    }

    /**
     * Set margevole
     *
     * @param float $margevole
     * @return User
     */
    public function setMargevole($margevole)
    {
        $this->margevole = $margevole;

        return $this;
    }

    /**
     * Get margevole
     *
     * @return float 
     */
    public function getMargevole()
    {
        return $this->margevole;
    }

    /**
     * Set actvole
     *
     * @param boolean $actvole
     * @return User
     */
    public function setActvole($actvole)
    {
        $this->actvole = $actvole;

        return $this;
    }

    /**
     * Get actvole
     *
     * @return boolean 
     */
    public function getActvole()
    {
        return $this->actvole;
    }

    /**
     * Set margecroissiere
     *
     * @param float $margecroissiere
     * @return User
     */
    public function setMargecroissiere($margecroissiere)
    {
        $this->margecroissiere = $margecroissiere;

        return $this;
    }

    /**
     * Get margecroissiere
     *
     * @return float 
     */
    public function getMargecroissiere()
    {
        return $this->margecroissiere;
    }

    /**
     * Set actcroissiere
     *
     * @param boolean $actcroissiere
     * @return User
     */
    public function setActcroissiere($actcroissiere)
    {
        $this->actcroissiere = $actcroissiere;

        return $this;
    }

    /**
     * Get actcroissiere
     *
     * @return boolean 
     */
    public function getActcroissiere()
    {
        return $this->actcroissiere;
    }

    /**
     * Set margevoiture
     *
     * @param float $margevoiture
     * @return User
     */
    public function setMargevoiture($margevoiture)
    {
        $this->margevoiture = $margevoiture;

        return $this;
    }

    /**
     * Get margevoiture
     *
     * @return float 
     */
    public function getMargevoiture()
    {
        return $this->margevoiture;
    }

    /**
     * Set actvoiture
     *
     * @param boolean $actvoiture
     * @return User
     */
    public function setActvoiture($actvoiture)
    {
        $this->actvoiture = $actvoiture;

        return $this;
    }

    /**
     * Get actvoiture
     *
     * @return boolean 
     */
    public function getActvoiture()
    {
        return $this->actvoiture;
    }

    /**
     * Set acttransfert
     *
     * @param boolean $acttransfert
     * @return User
     */
    public function setActtransfert($acttransfert)
    {
        $this->acttransfert = $acttransfert;

        return $this;
    }

    /**
     * Get acttransfert
     *
     * @return boolean 
     */
    public function getActtransfert()
    {
        return $this->acttransfert;
    }

    /**
     * Set actbienetre
     *
     * @param boolean $actbienetre
     * @return User
     */
    public function setActbienetre($actbienetre)
    {
        $this->actbienetre = $actbienetre;

        return $this;
    }

    /**
     * Get actbienetre
     *
     * @return boolean 
     */
    public function getActbienetre()
    {
        return $this->actbienetre;
    }

    /**
     * Set actseminaire
     *
     * @param boolean $actseminaire
     * @return User
     */
    public function setActseminaire($actseminaire)
    {
        $this->actseminaire = $actseminaire;

        return $this;
    }

    /**
     * Get actseminaire
     *
     * @return boolean 
     */
    public function getActseminaire()
    {
        return $this->actseminaire;
    }

    /**
     * Set actgrandjeu
     *
     * @param boolean $actgrandjeu
     * @return User
     */
    public function setActgrandjeu($actgrandjeu)
    {
        $this->actgrandjeu = $actgrandjeu;

        return $this;
    }

    /**
     * Get actgrandjeu
     *
     * @return boolean 
     */
    public function getActgrandjeu()
    {
        return $this->actgrandjeu;
    }

    /**
     * Set actactivite
     *
     * @param boolean $actactivite
     * @return User
     */
    public function setActactivite($actactivite)
    {
        $this->actactivite = $actactivite;

        return $this;
    }

    /**
     * Get actactivite
     *
     * @return boolean 
     */
    public function getActactivite()
    {
        return $this->actactivite;
    }

    
        /**
     * Set prstsejour
     *
     * @param boolean $prstsejour
     * @return User
     */
    public function setPrstsejour($prstsejour)
    {
        $this->prstsejour = $prstsejour;

        return $this;
    }

    /**
     * Get prstsejour
     *
     * @return boolean 
     */
    public function getPrstsejour()
    {
        return $this->prstsejour;
    }
    
    
        /**
     * Set prstcircuit
     *
     * @param boolean $prstcircuit
     * @return User
     */
    public function setPrstcircuit($prstcircuit)
    {
        $this->prstcircuit = $prstcircuit;

        return $this;
    }

    /**
     * Get prstcircuit
     *
     * @return boolean 
     */
    public function getPrstcircuit()
    {
        return $this->prstcircuit;
    }


    
  
    
        /**
     * Set prstomra
     *
     * @param boolean $prstomra
     * @return User
     */
    public function setPrstomra($prstomra)
    {
        $this->prstomra = $prstomra;

        return $this;
    }

    /**
     * Get prstomra
     *
     * @return boolean 
     */
    public function getPrstomra()
    {
        return $this->prstomra;
    }

    
        /**
     * Set prstcroissiere
     *
     * @param boolean $prstcroissiere
     * @return User
     */
    public function setPrstcroissiere($prstcroissiere)
    {
        $this->prstcroissiere = $prstcroissiere;

        return $this;
    }

    /**
     * Get prstcroissiere
     *
     * @return boolean 
     */
    public function getPrstcroissiere()
    {
        return $this->prstcroissiere;
    }

    
     /**
     * Set prstvoiture
     *
     * @param boolean $prstvoiture
     * @return User
     */
    public function setPrstvoiture($prstvoiture)
    {
        $this->prstvoiture = $prstvoiture;

        return $this;
    }

    /**
     * Get prstvoiture
     *
     * @return boolean 
     */
    public function getPrstvoiture()
    {
        return $this->prstvoiture;
    }

    
     /**
     * Set prstbienetre
     *
     * @param boolean $prstbienetre
     * @return User
     */
    public function setPrstbienetre($prstbienetre)
    {
        $this->prstbienetre = $prstbienetre;

        return $this;
    }

    /**
     * Get prstbienetre
     *
     * @return boolean 
     */
    public function getPrstbienetre()
    {
        return $this->prstbienetre;
    }

    
    /**
     * Set margebienetre
     *
     * @param float $margebienetre
     * @return User
     */
    public function setMargebienetre($margebienetre)
    {
        $this->margebienetre = $margebienetre;

        return $this;
    }

    /**
     * Get margeomra
     *
     * @return float 
     */
    public function getMargebienetre()
    {
        return $this->margebienetre;
    }
    
    
    /**
     * Set actparc
     *
     * @param boolean $actparc
     * @return User
     */
    public function setActparc($actparc)
    {
        $this->actparc = $actparc;

        return $this;
    }

    /**
     * Get actparc
     *
     * @return boolean 
     */
    public function getActparc()
    {
        return $this->actparc;
    }
    
    
        /**
     * Set margeparc
     *
     * @param float $margeparc
     * @return User
     */
    public function setMargeparc($margeparc)
    {
        $this->margeparc = $margeparc;

        return $this;
    }

    /**
     * Get margeparc
     *
     * @return float 
     */
    public function getMargeparc()
    {
        return $this->margeparc;
    }
    
      /**
     * Set prstparc
     *
     * @param boolean $prstparc
     * @return User
     */
    public function setPrstparc($prstparc)
    {
        $this->prstparc = $prstparc;

        return $this;
    }

    /**
     * Get prstparc
     *
     * @return boolean 
     */
    public function getPrstparc()
    {
        return $this->prstparc;
    } 
    
    
        /**
     * Set margeevenement
     *
     * @param float $margeevenement
     * @return User
     */
    public function setMargeevenement($margeevenement)
    {
        $this->margeevenement = $margeevenement;

        return $this;
    }

    /**
     * Get margeevenement
     *
     * @return float 
     */
    public function getMargeevenement()
    {
        return $this->margeevenement;
    }

    /**
     * Set actevenement
     *
     * @param boolean $actevenement
     * @return User
     */
    public function setActevenement($actevenement)
    {
        $this->actevenement = $actevenement;

        return $this;
    }

    /**
     * Get actevenement
     *
     * @return boolean 
     */
    public function getActevenement()
    {
        return $this->actevenement;
    }

     /**
     * Set prstevenement
     *
     * @param boolean $prstevenement
     * @return User
     */
    public function setPrstevenement($prstevenement)
    {
        $this->prstevenement = $prstevenement;

        return $this;
    }

    /**
     * Get prstevenement
     *
     * @return boolean 
     */
    public function getPrstevenement()
    {
        return $this->prstevenement;
    }	
}
