/**
 * Created by Salah Chtioui on 16/01/2015.
 */
// disable all other room options
$(document).ready(function () {
    $(".dontshow").hide();
    $(".dontshow select").attr('disabled', 'disabled');
    $(".dontshow input").attr('disabled', 'disabled');
});


// fonction qui gére le nombre des chambre
function Addrowtotable(div, nbr) {
    rowCount = $("#" + div + " tr").length;
    nb = (rowCount - 1) / 2;
    tot = (nbr - nb)
    var tr1 = $("#" + div + " tr:nth-child(2)");
    var tr2 = $("#" + div + " tr:nth-child(3)");
    htm = "";
    if (tot > 0) {
        for (i = 0; i < tot; i++) {
            line1 = tr1.html();
            line1 = replaceAll("dontshow", "", line1);
            line1 = replaceAll(" 1 :", "&nbsp;" + (nb + 1 + i) + " :", line1);
            line1 = replaceAll('[\1\[\]]', "["+(nb + 1 + i)+"]", line1);
            //line1 = replaceAll('[\[\]]\[\]', (nb + 1 + i) + "[]", line1);
            line1 = replaceAll(",1", "," + (nb + 1 + i), line1);
            line1 = replaceAll('disabled="disabled"', "", line1);

            line2 = tr2.html();
            line2 = replaceAll(" 1 :", "&nbsp;" + (nb + 1 + i) + " :", line2);
            line2 = replaceAll('[1]', (nb + 1 + i), line2);
            line2 = replaceAll('[1][]', (nb + 1 + i) + "[]", line2);
            line2 = replaceAll(",1", "," + (nb + 1 + i), line2);
            line2 = replaceAll("dontshow", "", line2);
            line2 = replaceAll('disabled="disabled"', "", line2);

            htm = htm + "<tr>" + line1 + "</tr>" + "<tr>" + line2 + "</tr>";
        }
    }
    else if (tot < 0) {
        tot = tot * (-1);
        for (i = 0; i < tot; i++) {
            $("#" + div + " tr:last-child").remove();
            $("#" + div + " tr:last-child").remove();
        }
    }
    $("#" + div + " tbody").append(htm)
}

// fonction qui gére le nombre des other room
function Addrowtotable2(div, nbr) {
    nbr= parseInt(nbr)+1;
    console.log(nbr)
    rowCount = $("#" + div + " tr").length;
    nb = (rowCount - 1) / 2;
    tot = (nbr - nb)
    var tr1 = $("#" + div + " tr:nth-child(2)");
    var tr2 = $("#" + div + " tr:nth-child(3)");
    htm = "";
    if (tot > 0) {
        for (i = 0; i < tot; i++) {
            line1 = tr1.html();
            line1 = replaceAll("dontshow", "", line1);
            line1 = replaceAll(" 1 :", "&nbsp;" + (nb + 1 + i) + " :", line1);
            line1 = replaceAll('[\1\[\]]', "["+(nb + 1 + i)+"]", line1);
            //line1 = replaceAll('[\[\]]\[\]', (nb + 1 + i) + "[]", line1);
            line1 = replaceAll(",1", "," + (nb + 1 + i), line1);
            line1 = replaceAll('disabled="disabled"', "", line1);

            line2 = tr2.html();
            line2 = replaceAll(" 1 :", "&nbsp;" + (nb + 1 + i) + " :", line2);
            //line2 = replaceAll('[1]', (nb + 1 + i), line2);
            //line2 = replaceAll('[1][]', (nb + 1 + i) + "[]", line2);

            line1 = replaceAll('[\\[\]]', "["+(nb + 1 + i)+"]", line1);
            line2 = replaceAll(",1", "," + (nb + 1 + i), line2);
            line2 = replaceAll("dontshow", "", line2);
            line2 = replaceAll('disabled="disabled"', "", line2);

            htm = htm + "<tr>" + line1 + "</tr>" + "<tr>" + line2 + "</tr>";
        }
    }
    else if (tot < 0) {
        tot = tot * (-1);
        for (i = 0; i < tot; i++) {
            $("#" + div + " tr:last-child").remove();
            $("#" + div + " tr:last-child").remove();
        }
    }
    $("#" + div + " tbody").append(htm)
}
/*
 * deux fonction qui calcule le nombbre d'adulte et des enfants
 */

function AdulteCalcul(select, max, idrow, id) {
    $selectad = $(select);
    valselectad = $selectad.val();
    parents = $selectad.parent().parent();
    valselectenf = max - valselectad;
    parents.children('td').eq(2).find('select').val(valselectenf);
    ChildCalcul(parents.children('td').eq(2).find('select'), max, idrow, id);
}
function ChildCalcul(select, max, idrow, id) {
    $selectenf = $(select);
    valselectenf = $selectenf.val();
    parents = $selectenf.parent().parent();
    valselectad = max - valselectenf;
    parents.children('td').eq(1).find('select').val(valselectad);
    parents.children('td').eq(2).find('div').remove();
    htm = '<div class="span12">';
    for (i = 1; i <= valselectenf; i++) {
        htm += '<label> Age Enfant ' + i + ' <select name="children[' + idrow + '][' + id + '][]" style=" width: 50px;" >' +
        '<option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option>' +
        '<option value="6">6</option><option value="7">7</option>' +
        '<option value="8">8</option><option value="9">9</option><option value="10">10</option>' +
        '<option value="11">11</option><option value="12">12</option></select> an(s)</label>';
    }
    htm += '</div>';
    parents.children('td').eq(2).append(htm);
}
// function qui remplace tous les instance
function replaceAll(find, replace, str) {

    return str.replace(new RegExp(find, 'g'), replace);
}