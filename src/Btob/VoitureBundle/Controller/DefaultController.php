<?php

namespace Btob\VoitureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;

use Btob\VoitureBundle\Entity\Voiture;
use Btob\VoitureBundle\Entity\Reservationvoiture;

use Btob\HotelBundle\Entity\Clients;

use Btob\VoitureBundle\Form\voitureType;
use Btob\HotelBundle\Form\ClientsType;

class DefaultController extends Controller
{
    public function indexAction()
    {

        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:voiture")->findAll();
        return $this->render('BtobVoitureBundle:Default:index.html.twig', array('entities' => $entities));
    }

    public function detailAction(voiture $voiture)
    {
        return $this->render('BtobVoitureBundle:Default:detail.html.twig', array('entry' => $voiture));
    }



    public function reservationvoitureAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:voiture")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'), '/'));
                $datef = new \Datetime(Tools::explodedate($request->request->get('datef'), '/'));
                $message = $request->request->get('message');

                $reservation = new Reservationvoiture();
                $reservation->setClient($client);
                $reservation->setVoiture($entities);
                $reservation->setAgent($this->get('security.context')->getToken()->getUser());

                $reservation->setMessage($message);
                $reservation->setDated($dated);
                $reservation->setDatef($datef);

                $em->persist($reservation);
                $em->flush();


                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notisejour', 'Votre demande a bien été envoyé. Merci.');

                return $this->render('BtobVoitureBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('BtobVoitureBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }


    public function listreservationvoitureAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:Reservationvoiture")->findAll();
        return $this->render('BtobVoitureBundle:Default:listreservationvoiture.html.twig', array('entities' => $entities));
    }



    public function deletereservationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BtobVoitureBundle:Reservationvoiture')->find($id);
        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('voiture_reservation'));
    }


    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Reservationvoiture')->find($id);

       // $form = $this->createForm(new ClientsType(), $client);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservation voiture entity.');
        }

        $editForm = $this->createForm(new voitureType(), $entity);


        return $this->render('BtobVoitureBundle:Defaut:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }


}
