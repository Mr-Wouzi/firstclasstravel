<?php

namespace Btob\BienetreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\BienetreBundle\Entity\Bienetre;
use Btob\BienetreBundle\Entity\Imagbien;
use Btob\BienetreBundle\Entity\Reservationbienetre;
use Btob\BienetreBundle\Form\BienetreType;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->findby(array('act' => 1));
        return $this->render('BtobBienetreBundle:Default:index.html.twig', array('entities' => $entities));
    }

    public function listreservationbienAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities =  $this->getDoctrine()->getRepository("BtobBienetreBundle:Reservationbienetre")->findAll();

        }else{
            $entities =  $this->getDoctrine()->getRepository("BtobBienetreBundle:Reservationbienetre")->findBy(array('agent' => $user));

        }


        return $this->render('BtobBienetreBundle:Bienetre:listreservation.html.twig', array('entities' => $entities));
    }

    public function detailAction(Bienetre $Bienetre)
    {
        return $this->render('BtobBienetreBundle:Default:detail.html.twig', array('entry' => $Bienetre));
    }


  public function detailreservationbienAction(Reservationbienetre $Reservationbienetre)
    {
        return $this->render('BtobBienetreBundle:Bienetre:detailreservation.html.twig', array('entry' => $Reservationbienetre));
    }




    public function reservationbienAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $reservation = new Reservationbienetre();
                $datea = new \Datetime(Tools::explodedate($request->request->get('datea'), '/'));
                $defaulta = $request->request->get('defaulta');
                $nbrpr = $request->request->get('nbrpr');
                $nbrbien = $request->request->get('nbrbien');

                $reservation->setAgent($this->get('security.context')->getToken()->getUser());
                $reservation->setClient($client);
                $reservation->setBienetre($entities);
                $reservation->setDatear($datea);
                $reservation->setNbrbien($nbrbien);
                $reservation->setNbrpr($nbrpr);
                $reservation->setHeur($defaulta);
                $em->persist($reservation);
                $em->flush();
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notiBienetre', 'Votre message a bien été envoyé. Merci.');

                return $this->render('BtobBienetreBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('BtobBienetreBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }
}
