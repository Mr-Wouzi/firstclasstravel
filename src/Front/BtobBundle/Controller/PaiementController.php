<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use Btob\HotelBundle\Common\Tools;

use Btob\HotelBundle\Entity\Reservation;

use Btob\HotelBundle\Entity\Reservationdetail;

use Btob\HotelBundle\Entity\Notification;

use Symfony\Component\HttpFoundation\Request;

use Btob\CuircuitBundle\Entity\Reservationcircuit;
use Btob\ParcBundle\Entity\Reservationparc;
use Btob\SpaBundle\Entity\Reservationspa;
use Btob\CroissiereBundle\Entity\Reservationcroi;
use Btob\TransfertBundle\Entity\Reservationtransfert;
use Btob\SejourBundle\Entity\Reservationsejour;



class PaiementController extends Controller

{
	public function indexAction()

    {
		

					
		
	
	return $this->render('FrontBtobBundle:Paiement:success.html.twig' );

    }
    
	
	
	public function notificationAction()

    {
		
		$em = $this->getDoctrine()->getManager();
		
		$request = $this->get('request');
		/* $ref = $request->request->get('ref');
		$act = $request->request->get('act');
		$par = $request->request->get('par'); */
		
		//$ref ='230';
		//$act = 'ANNULATION';
		$ref = 0; 
		$act = 0; 
		$par = 0; 
		if (isset($_GET['Reference'])){
			$ref = $_GET['Reference'];
		}
		if (isset($_GET['Action'])){
			$act = $_GET['Action']; 
		}
		if (isset($_GET['Param'])){
			$par = $_GET['Param']; 
		}
		
		/*
		var_dump($ref);
		var_dump($act);
		var_dump($par);
		die();
		*/
		
		$prefix = substr($ref, 0, 3);
		$real_id = substr($ref, 3);


		switch($prefix) {
			case "CIR":
					$reservation = $this->getDoctrine()->getRepository('BtobCircuitBundle:Reservationcircuit')->find($real_id);
					break; 
			case "PAR":
					$reservation = $this->getDoctrine()->getRepository('BtobParcBundle:Reservationparc')->find($real_id);
					break; 
			case "BIE":
					$reservation = $this->getDoctrine()->getRepository('BtobSpaBundle:Reservationspa')->find($real_id);
					break; 
			case "CRO":
					$reservation = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Reservationcroi')->find($real_id);
					break; 
			case "TRA":
					$reservation = $this->getDoctrine()->getRepository('BtobTransfertBundle:Reservationtransfert')->find($real_id);
					break; 
			case "HOT":
					$reservation = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($real_id);
					break; 
                        case "EVE":
					$reservation = $this->getDoctrine()->getRepository('BtobEvenementBundle:Reservationevenement')->find($real_id);
					break; 
                        case "SEJ":
					$reservation = $this->getDoctrine()->getRepository('BtobSejourBundle:Reservationsejour')->find($real_id);
					break; 
		}
		
		
		
		switch ($act) {
				 case "DETAIL":
					   // accéder à la base et récuperer le montant
						//$reservation = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($ref);
						$montant = 0;
						if($reservation)
						{
							$montant_payer = $reservation->getAvance();
							$montant = number_format($montant_payer, 3, '.', '');
						}
					   echo "Reference=".$ref."&Action=".$act."&Reponse=".$montant;    
					   break;
					
				 case "ERREUR":                  
						// accéder à la base et mettre à jour l’état de la transaction 
						//$reservation = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($ref);
						$reservation->setEtat(2);
						$reservation->setResultatfinal($act);
                                                $em->persist($reservation);
                                                $em->flush();
						echo "Reference=".$ref."&Action=".$act."&Reponse=OK";      
						break;         
						 
				 case "ACCORD":                   
						// accéder à la base, enregistrer le numéro d’autorisation (dans param)  
						//$reservation = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($ref);
						$reservation->setEtat(3);
						$reservation->setResultatfinal($act);
						$reservation->setNumautoris($par);
                                                $em->persist($reservation);
                                                $em->flush();
						echo "Reference=".$ref."&Action=".$act."&Reponse=ok";     
						break; 
								 
				  case "REFUS":                  
						// accéder à la base et mettre à jour l’état de la transaction    
						//$reservation = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($ref);
						$reservation->setEtat(4);
						$reservation->setResultatfinal($act);
                                                $em->persist($reservation);
                                                $em->flush();
						echo "Reference=".$ref."&Action=".$act."&Reponse=ok";     
						break;         
						 
				  case "ANNULATION":                
						// accéder à la base et mettre à jour l’état de la transaction     
						//$reservation = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($ref);
						$reservation->setEtat(5);
						$reservation->setResultatfinal($act);
                                                $em->persist($reservation);
                                                $em->flush();
						echo "Reference=".$ref. "&Action=".$act. "&Reponse=OK";    
						break; 
		} 	
		
		
        return $this->render('FrontBtobBundle:Paiement:notification.html.twig' );

    }
	
	public function successAction()

    {
		
		
        return $this->render('FrontBtobBundle:Paiement:success.html.twig' );

    }

	
	public function failureAction()

    {
		

        return $this->render('FrontBtobBundle:Paiement:failure.html.twig' );

    }
}