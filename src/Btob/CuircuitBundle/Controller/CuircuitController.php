<?php 
namespace Btob\CuircuitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\CuircuitBundle\Entity\Imgcuir;
use Btob\CuircuitBundle\Form\CuircuitType;

/**
 * Cuircuit controller.
 *
 */
class CuircuitController extends Controller
{

    /**
     * Lists all Cuircuit entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobCuircuitBundle:Cuircuit')->findAll();
        $entities=array_reverse($entities);
        return $this->render('BtobCuircuitBundle:Cuircuit:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Cuircuit entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Cuircuit();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgcuir();
                        $img->setCuircuit($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
         //   return $this->redirect($this->generateUrl('cuircuit_show', array('id' => $entity->getId())));
            return $this->redirect($this->generateUrl('cuircuit'));
        }

        return $this->render('BtobCuircuitBundle:Cuircuit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Cuircuit entity.
     *
     * @param Cuircuit $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cuircuit $entity)
    {
        $form = $this->createForm(new CuircuitType(), $entity, array(
            'action' => $this->generateUrl('cuircuit_create'),
            'method' => 'POST',
        ));

     //   $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Cuircuit entity.
     *
     */
    public function newAction()
    {
        $entity = new Cuircuit();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobCuircuitBundle:Cuircuit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Cuircuit entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobCuircuitBundle:Cuircuit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cuircuit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobCuircuitBundle:Cuircuit:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Cuircuit entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobCuircuitBundle:Cuircuit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cuircuit entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobCuircuitBundle:Cuircuit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Cuircuit entity.
    *
    * @param Cuircuit $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Cuircuit $entity)
    {
        $form = $this->createForm(new CuircuitType(), $entity, array(
            'action' => $this->generateUrl('cuircuit_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

    //    $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Cuircuit entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobCuircuitBundle:Cuircuit')->find($id);
        foreach ($entity->getImgcuir() as $key => $value) {
            $em->remove($value);
            $em->flush();
        }
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cuircuit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgcuir();
                        $img->setCuircuit($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
          //  return $this->redirect($this->generateUrl('cuircuit_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('cuircuit'));
        }

        return $this->render('BtobCuircuitBundle:Cuircuit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Cuircuit entity.
     *
     */
    public function deleteAction($id)
    {

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobCuircuitBundle:Cuircuit')->find($id);


            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('cuircuit'));
    }

    /**
     * Creates a form to delete a Cuircuit entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cuircuit_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
