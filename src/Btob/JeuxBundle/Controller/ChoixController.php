<?php

namespace Btob\JeuxBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\JeuxBundle\Entity\Choix;
use Btob\JeuxBundle\Entity\Jeu;
use Btob\JeuxBundle\Form\ChoixType;

/**
 * Choix controller.
 *
 */
class ChoixController extends Controller
{

    /**
     * Lists all Choix entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobJeuxBundle:Choix')->findAll();

        return $this->render('BtobJeuxBundle:Choix:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Choix entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Choix();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('choix_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobJeuxBundle:Choix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Choix entity.
     *
     * @param Choix $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Choix $entity)
    {
        $form = $this->createForm(new ChoixType(), $entity, array(
            'action' => $this->generateUrl('choix_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Choix entity.
     *
     */
    public function newAction()
    {
        $entity = new Choix();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobJeuxBundle:Choix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Choix entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobJeuxBundle:Choix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Choix entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobJeuxBundle:Choix:show.html.twig', array(
            'entity'      => $entity,

            'delete_form' => $deleteForm->createView(),
        ));
    }

   public function newsAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobJeuxBundle:Jeu')->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {


            $choix = $request->request->get('choix');

            foreach( $choix as $key=>$value){
                if($choix[$key] != null){
              $newchoix  = new Choix();
                $newchoix->setChoix($choix[$key]);
                $newchoix->setQustion($entity);
                $em->persist($newchoix);
                $em->flush();
                }
            }
            return $this->redirect($this->generateUrl('jeu_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobJeuxBundle:Choix:new.html.twig', array(
            'entity'      => $entity,

                   ));
    }

    /**
     * Displays a form to edit an existing Choix entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobJeuxBundle:Choix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Choix entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobJeuxBundle:Choix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Choix entity.
    *
    * @param Choix $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Choix $entity)
    {
        $form = $this->createForm(new ChoixType(), $entity, array(
            'action' => $this->generateUrl('choix_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       // $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Choix entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobJeuxBundle:Choix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Choix entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

      //      return $this->redirect($this->generateUrl('choix_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('jeu_show', array('id' => $entity->getQustion()->getId())));
        }

        return $this->render('BtobJeuxBundle:Choix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Choix entity.
     *
     */
    public function deleteAction( $id)
    {
             $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobJeuxBundle:Choix')->find($id);

             $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('jeu_show', array('id' => $entity->getQustion()->getId())));
    }

    /**
     * Creates a form to delete a Choix entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('choix_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
