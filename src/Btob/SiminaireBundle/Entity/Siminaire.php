<?php

namespace Btob\SiminaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Siminaire
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SiminaireBundle\Entity\SiminaireRepository")
 */
class Siminaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="Imgs", mappedBy="siminaire")
     */
    protected $imgsim;
    /**
     * @var boolean
     *
     * @ORM\Column(name="pub", type="boolean", nullable=true)
     */
    private $pub;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", nullable=true)
     */
    private $prix;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrjr", type="integer", nullable=true)
     */
    private $nbrjr;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set region
     *
     * @param string $region
     * @return Siminaire
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }


    /**
     * Set titre
     *
     * @param string $titre
     * @return Siminaire
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }



    /**
     * Set nbrjr
     *
     * @param integer $nbrjr
     * @return Siminaire
     */
    public function setNbrjr($nbrjr)
    {
        $this->nbrjr = $nbrjr;

        return $this;
    }

    /**
     * Get nbrjr
     *
     * @return integer 
     */
    public function getNbrjr()
    {
        return $this->nbrjr;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Siminaire
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set pub
     *
     * @param boolean $pub
     * @return Siminaire
     */
    public function setPub($pub)
    {
        $this->pub = $pub;

        return $this;
    }

    /**
     * Get pub
     *
     * @return boolean 
     */
    public function getPub()
    {
        return $this->pub;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Siminaire
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->imgsim = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dcr=new \DateTime();
    }

    /**
     * Add imgsim
     *
     * @param \Btob\SiminaireBundle\Entity\Imgs $imgsim
     * @return Siminaire
     */
    public function addImgsim(\Btob\SiminaireBundle\Entity\Imgs $imgsim)
    {
        $this->imgsim[] = $imgsim;

        return $this;
    }

    /**
     * Remove imgsim
     *
     * @param \Btob\SiminaireBundle\Entity\Imgs $imgsim
     */
    public function removeImgsim(\Btob\SiminaireBundle\Entity\Imgs $imgsim)
    {
        $this->imgsim->removeElement($imgsim);
    }

    /**
     * Get imgsim
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImgsim()
    {
        return $this->imgsim;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Siminaire
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }
}
