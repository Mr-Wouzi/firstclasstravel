<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationdetail
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\ReservationdetailRepository")
 */
class Reservationdetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;
    /**
     * @var integer
     *
     * @ORM\Column(name="enf", type="integer")
     */
    private $enf;
    /**
     * @var integer
     *
     * @ORM\Column(name="ad", type="integer")
     */
    private $ad;
    /**
     * @var text
     *
     * @ORM\Column(name="arrangement", type="text")
     */
    protected $arrangement;
    /**
     * @var text
     *
     * @ORM\Column(name="roomname", type="text")
     */
    protected $roomname;
    /**
     * @ORM\ManyToOne(targetEntity="Reservation", inversedBy="Resercationdetail")
     * @ORM\JoinColumn(name="res_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservation;
    /**
     * @var text
     *
     * @ORM\Column(name="supp", type="text")
     */
    private $supp;
    /**
     * @var text
     *
     * @ORM\Column(name="event", type="text")
     */
    private $event;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Reservationdetail
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set enf
     *
     * @param integer $enf
     * @return Reservationdetail
     */
    public function setEnf($enf)
    {
        $this->enf = $enf;

        return $this;
    }

    /**
     * Get enf
     *
     * @return integer 
     */
    public function getEnf()
    {
        return $this->enf;
    }

    /**
     * Set ad
     *
     * @param integer $ad
     * @return Reservationdetail
     */
    public function setAd($ad)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return integer 
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set arrangement
     *
     * @param string $arrangement
     * @return Reservationdetail
     */
    public function setArrangement($arrangement)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return string 
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }

    /**
     * Set roomname
     *
     * @param string $roomname
     * @return Reservationdetail
     */
    public function setRoomname($roomname)
    {
        $this->roomname = $roomname;

        return $this;
    }

    /**
     * Get roomname
     *
     * @return string 
     */
    public function getRoomname()
    {
        return $this->roomname;
    }

    /**
     * Set supp
     *
     * @param string $supp
     * @return Reservationdetail
     */
    public function setSupp($supp)
    {
        $this->supp = $supp;

        return $this;
    }

    /**
     * Get supp
     *
     * @return string 
     */
    public function getSupp()
    {
        return $this->supp;
    }

    /**
     * Set event
     *
     * @param string $event
     * @return Reservationdetail
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return string 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set reservation
     *
     * @param \Btob\HotelBundle\Entity\Reservation $reservation
     * @return Reservationdetail
     */
    public function setReservation(\Btob\HotelBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \Btob\HotelBundle\Entity\Reservation 
     */
    public function getReservation()
    {
        return $this->reservation;
    }
}
