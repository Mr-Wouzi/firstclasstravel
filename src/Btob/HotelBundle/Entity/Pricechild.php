<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pricechild
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PricechildRepository")
 */
class Pricechild
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pers", type="boolean")
     */
    private $pers;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Hotelprice", inversedBy="Pricechild", cascade={"persist"})
     * @ORM\JoinColumn(name="price_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotelprice;

    /**
     * @ORM\ManyToOne(targetEntity="Hotelchild", inversedBy="Pricechild", cascade={"persist"})
     * @ORM\JoinColumn(name="hotchild_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotelchild;

    public function __construct()
    {
        $this->pers = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Pricechild
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set pers
     *
     * @param boolean $pers
     * @return Pricechild
     */
    public function setPers($pers)
    {
        $this->pers = $pers;

        return $this;
    }

    /**
     * Get pers
     *
     * @return boolean 
     */
    public function getPers()
    {
        return $this->pers;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Pricechild
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set hotelprice
     *
     * @param \Btob\HotelBundle\Entity\Hotelprice $hotelprice
     * @return Pricechild
     */
    public function setHotelprice(\Btob\HotelBundle\Entity\Hotelprice $hotelprice = null)
    {
        $this->hotelprice = $hotelprice;

        return $this;
    }

    /**
     * Get hotelprice
     *
     * @return \Btob\HotelBundle\Entity\Hotelprice 
     */
    public function getHotelprice()
    {
        return $this->hotelprice;
    }

    /**
     * Set hotelchild
     *
     * @param \Btob\HotelBundle\Entity\Hotelchild $hotelchild
     * @return Pricechild
     */
    public function setHotelchild(\Btob\HotelBundle\Entity\Hotelchild $hotelchild = null)
    {
        $this->hotelchild = $hotelchild;

        return $this;
    }

    /**
     * Get hotelchild
     *
     * @return \Btob\HotelBundle\Entity\Hotelchild 
     */
    public function getHotelchild()
    {
        return $this->hotelchild;
    }
}
