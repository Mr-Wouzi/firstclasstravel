<?php

namespace Btob\TeamBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Null;

class TeamType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', 'text', array('label' => "Libelle", 'required' => true))
        
           // ->add('dcr')
            ->add('ville', 'text', array('label' => "Ville", 'required' => true))
          /*  ->add('nbretoiles', 'choice', array(
                'choices' => array('3' => '3', '4' => '4', '5' => '5', '6' => 'Charme'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label' => 'Nombre d\'étoile',
            ))*/
            ->add('prix', NULL, array('label' => ' ', 'required' => true))
            ->add('apartir', NULL, array('label' =>' ', 'required' => false))
            ->add('active', NULL, array('label' =>' ', 'required' => false))
            ->add('pindex', NULL, array('label' =>' ' , 'required' => false))
            ->add('arrangement', NULL, array('label' => "Logement", 'required' => true))
            ->add('nbrjr', NULL, array('label' => "Nombre des jours", 'required' => true))
          //  ->add('description')
            ->add('description', null, array('label' => "Description longue", 'required' => false))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\TeamBundle\Entity\Team'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_teambundle_team';
    }
}
