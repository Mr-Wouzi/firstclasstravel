<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotelroom
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelroomRepository")
 */
class Hotelroom {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="hotelroom")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;

    /**
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="hotelroom")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $room;

    /**
     * @var integer
     *
     * @ORM\Column(name="ad", type="integer")
     */
    private $ad;

    /**
     * @var integer
     *
     * @ORM\Column(name="enf", type="integer")
     */
    private $enf;

    /**
     * @var integer
     *
     * @ORM\Column(name="bb", type="integer")
     */
    private $bb;

    /**
     * @var boolean
     *
     * @ORM\Column(name="personne", type="boolean")
     */
    private $personne;

    public function __construct() {
        $this->personne = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ad
     *
     * @param integer $ad
     * @return Hotelroom
     */
    public function setAd($ad)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return integer 
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set enf
     *
     * @param integer $enf
     * @return Hotelroom
     */
    public function setEnf($enf)
    {
        $this->enf = $enf;

        return $this;
    }

    /**
     * Get enf
     *
     * @return integer 
     */
    public function getEnf()
    {
        return $this->enf;
    }

    /**
     * Set bb
     *
     * @param integer $bb
     * @return Hotelroom
     */
    public function setBb($bb)
    {
        $this->bb = $bb;

        return $this;
    }

    /**
     * Get bb
     *
     * @return integer 
     */
    public function getBb()
    {
        return $this->bb;
    }

    /**
     * Set personne
     *
     * @param boolean $personne
     * @return Hotelroom
     */
    public function setPersonne($personne)
    {
        $this->personne = $personne;

        return $this;
    }

    /**
     * Get personne
     *
     * @return boolean 
     */
    public function getPersonne()
    {
        return $this->personne;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Hotelroom
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set room
     *
     * @param \Btob\HotelBundle\Entity\Room $room
     * @return Hotelroom
     */
    public function setRoom(\Btob\HotelBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \Btob\HotelBundle\Entity\Room 
     */
    public function getRoom()
    {
        return $this->room;
    }
}
