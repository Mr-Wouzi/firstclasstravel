<?php

namespace Btob\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Golf
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\GolfBundle\Entity\GolfRepository")
 */
class Golf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date", nullable=true)
     */
    private $dcr;
    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean" , nullable=true)
     */
    private $act;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datef", type="date" , nullable=true)
     */
    private $datef;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255 , nullable=true)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="dureegolf", type="string", length=255 , nullable=true)
     */
    private $dureegolf;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float" , nullable=true)
     */
    private $prix;
    /**
     * @ORM\OneToMany(targetEntity="Imgegolf", mappedBy="golf")
     */
    protected $imgegolf;

    /**
     * @var boolean
     *
     * @ORM\Column(name="apartir", type="boolean" , nullable=true)
     */
    private $apartir;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pindex", type="boolean" , nullable=true)
     */
    private $pindex;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Themes", inversedBy="golf")
     * @ORM\JoinColumn(name="themes_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $themes;

    /**
     * @var string
     *
     * @ORM\Column(name="villed", type="string", length=255 , nullable=true)
     */
    private $villed;

    /**
     * @var string
     *
     * @ORM\Column(name="voyagecart", type="string", length=255 , nullable=true)
     */
    private $voyagecart;


    /**
     * @var string
     *
     * @ORM\Column(name="villea", type="string", length=255 , nullable=true)
     */
    private $villea;

    /**
     * @var string
     *
     * @ORM\Column(name="hotel", type="string", length=255 , nullable=true)
     */
    private $hotel;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbretoile", type="integer" , nullable=true)
     */
    private $nbretoile;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text" , nullable=true)
     */
    private $description;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="golf")
     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $arrangement;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Golf
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Golf
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     * @return Golf
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime 
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Golf
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set dureegolf
     *
     * @param string $dureegolf
     * @return Golf
     */
    public function setDureegolf($dureegolf)
    {
        $this->dureegolf = $dureegolf;

        return $this;
    }

    /**
     * Get dureegolf
     *
     * @return string 
     */
    public function getDureegolf()
    {
        return $this->dureegolf;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Golf
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set apartir
     *
     * @param boolean $apartir
     * @return Sejour
     */
    public function setApartir($apartir)
    {
        $this->apartir = $apartir;

        return $this;
    }

    /**
     * Get apartir
     *
     * @return boolean 
     */
    public function getApartir()
    {
        return $this->apartir;
    }

    /**
     * Set pindex
     *
     * @param boolean $pindex
     * @return Golf
     */
    public function setPindex($pindex)
    {
        $this->pindex = $pindex;

        return $this;
    }

    /**
     * Get pindex
     *
     * @return boolean 
     */
    public function getPindex()
    {
        return $this->pindex;
    }

    /**
     * Set villed
     *
     * @param string $villed
     * @return Golf
     */
    public function setVilled($villed)
    {
        $this->villed = $villed;

        return $this;
    }

    /**
     * Get villed
     *
     * @return string 
     */
    public function getVilled()
    {
        return $this->villed;
    }

    /**
     * Set voyagecart
     *
     * @param string $voyagecart
     * @return Golf
     */
    public function setVoyagecart($voyagecart)
    {
        $this->voyagecart = $voyagecart;

        return $this;
    }

    /**
     * Get voyagecart
     *
     * @return string 
     */
    public function getVoyagecart()
    {
        return $this->voyagecart;
    }

    /**
     * Set hotel
     *
     * @param string $hotel
     * @return Golf
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return string 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set nbretoile
     *
     * @param integer $nbretoile
     * @return Golf
     */
    public function setNbretoile($nbretoile)
    {
        $this->nbretoile = $nbretoile;

        return $this;
    }

    /**
     * Get nbretoile
     *
     * @return integer 
     */
    public function getNbretoile()
    {
        return $this->nbretoile;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return Golf
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Golf
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Golf
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }



    /**
     * Add imgegolf
     *
     * @param \Btob\GolfBundle\Entity\Imgegolf $imgegolf
     * @return Golf
     */
    public function addImgegolf(\Btob\GolfBundle\Entity\Imgegolf $imgegolf)
    {
        $this->imgegolf[] = $imgegolf;

        return $this;
    }

    /**
     * Remove imgegolf
     *
     * @param \Btob\GolfBundle\Entity\Imgegolf $imgegolf
     */
    public function removeImgegolf(\Btob\GolfBundle\Entity\Imgegolf $imgegolf)
    {
        $this->imgesej->removeElement($imgegolf);
    }

    /**
     * Get imgegolf
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImgegolf()
    {
        return $this->imgegolf;
    }

    /**
     * Set villea
     *
     * @param string $villea
     * @return Golf
     */
    public function setVillea($villea)
    {
        $this->villea = $villea;

        return $this;
    }

    /**
     * Get villea
     *
     * @return string 
     */
    public function getVillea()
    {
        return $this->villea;
    }

    /**
     * Set themes
     *
     * @param \Btob\HotelBundle\Entity\Themes $themes
     * @return Sejour
     */
    public function setThemes(\Btob\HotelBundle\Entity\Themes $themes = null)
    {
        $this->themes = $themes;

        return $this;
    }

    /**
     * Get themes
     *
     * @return \Btob\HotelBundle\Entity\Themes 
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * Set arrangement
     *
     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement
     * @return Golf
     */
    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return \Btob\HotelBundle\Entity\Arrangement 
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }
}
