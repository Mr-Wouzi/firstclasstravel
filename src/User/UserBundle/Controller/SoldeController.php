<?php
/**
 * Created by PhpStorm.
 * User: poste 9
 * Date: 28/01/2015
 * Time: 09:28
 */

namespace User\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use User\UserBundle\Entity\Solde;
use User\UserBundle\Form\SoldeType;
use Btob\HotelBundle\Common\Tools;
use Symfony\Component\HttpFoundation\Request;


class SoldeController extends Controller
{
    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UserUserBundle:User')->findRole();
        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Speaker entity.');
        }

        return $this->render('UserUserBundle:Solde:home.html.twig', array(
            'entities' => $entities,
        ));
    }

public function agenceAction($userid,Request $request)
{
    $em = $this->getDoctrine()->getManager();

    $value = $request->get('_route_params');
    $x= (int)$value["userid"];
    $nameagence= $this->getDoctrine()
        ->getRepository('UserUserBundle:User')
        ->find($x);

    $solde = $this->getDoctrine()
        ->getRepository('UserUserBundle:Solde')
        ->findByAgence($userid);

    if(in_array('AGENCEID',$this->get('security.context')->getToken()->getUser()->getRoles())){
        $tab=array();
        foreach($solde as $value){
            if($value->getAgence()->getId()==$this->get('security.context')->getToken()->getUser()->getId()){
                $tab[]=$value;
            }
        }
        $solde=$tab;

    }
    $solde=array_reverse($solde);

    return $this->render('UserUserBundle:Solde:agence.html.twig', array(
        'entities' => $solde,
        "nameagence" => $nameagence
    ));
}



    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UserUserBundle:User')->findRole();
        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Speaker entity.');
        }

        return $this->render('UserUserBundle:Solde:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function addAction(Request $request)
    {

  //      die();
        $solde = new Solde();
        $form = $this->createForm(new SoldeType(), $solde);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $agent = $this->get('security.context')->getToken()->getUser();
                $us = $solde->getAgence();
                $oldesolde = $us->getSold();
                $newsolde = $oldesolde + $solde->getSolde();
                $us->setSold($newsolde);
                $solde->setAdminadd($agent);
                $em->persist($solde);
                $em->persist($us);
                $em->flush();

                $userid =$solde->getAgence()->getId();
                return $this->redirect($this->generateUrl('user_solde_agence', array('userid' => $userid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('UserUserBundle:Solde:form.html.twig', array('form' => $form->createView()));
    }
}