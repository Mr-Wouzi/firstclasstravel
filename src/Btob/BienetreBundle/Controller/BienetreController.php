<?php

namespace Btob\BienetreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\BienetreBundle\Entity\Bienetre;
use Btob\BienetreBundle\Entity\Reservationbienetre;
use Btob\BienetreBundle\Entity\Imagbien;
use Btob\BienetreBundle\Form\BienetreType;

/**
 * Bienetre controller.
 *
 */
class BienetreController extends Controller
{

    /**
     * Lists all Bienetre entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobBienetreBundle:Bienetre')->findAll();

        return $this->render('BtobBienetreBundle:Bienetre:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Bienetre entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Bienetre();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imagbien();
                        $img->setBienetre($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
        //    return $this->redirect($this->generateUrl('bienetre_show', array('id' => $entity->getId())));
            return $this->redirect($this->generateUrl('bienetre'));

        }

        return $this->render('BtobBienetreBundle:Bienetre:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Bienetre entity.
     *
     * @param Bienetre $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Bienetre $entity)
    {
        $form = $this->createForm(new BienetreType(), $entity, array(
            'action' => $this->generateUrl('bienetre_create'),
            'method' => 'POST',
        ));

      //  $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Bienetre entity.
     *
     */
    public function newAction()
    {
        $entity = new Bienetre();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobBienetreBundle:Bienetre:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Bienetre entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBienetreBundle:Bienetre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bienetre entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobBienetreBundle:Bienetre:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Bienetre entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBienetreBundle:Bienetre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bienetre entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobBienetreBundle:Bienetre:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Bienetre entity.
    *
    * @param Bienetre $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Bienetre $entity)
    {
        $form = $this->createForm(new BienetreType(), $entity, array(
            'action' => $this->generateUrl('bienetre_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       // $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Bienetre entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBienetreBundle:Bienetre')->find($id);
        foreach ($entity->getImgbien() as $key => $value) {
            $em->remove($value);
            $em->flush();
        }
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bienetre entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imagbien();
                        $img->setBienetre($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('bienetre'));
        }

        return $this->render('BtobBienetreBundle:Bienetre:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Bienetre entity.
     *
     */
    public function deleteAction( $id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobBienetreBundle:Bienetre')->find($id);

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('bienetre'));
    }

   public function deletereservationAction( $id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobBienetreBundle:Bienetre')->find($id);

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('bienetre'));
    }

  public function deletereservationadminAction($id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobBienetreBundle:Reservationbienetre')->find($id);

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('btob_bienetre_list_reservation_homepage'));
    }

    /**
     * Creates a form to delete a Bienetre entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bienetre_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
