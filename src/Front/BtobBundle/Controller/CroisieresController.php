<?php
namespace Front\BtobBundle\Controller;

use Btob\CroissiereBundle\Entity\Reservationcroi;

use Btob\HotelBundle\Common\Tools;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class CroisieresController extends Controller



{



    public function indexAction(Request $request)



    {

        $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Croisières'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;

        $croissieres = $this->getDoctrine()->getRepository("BtobCroissiereBundle:Croissiere")->findby(array('act' => 1));



  $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $croissieres,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );
        



        $em = $this->getDoctrine()->getManager();







        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();







        return $this->render('FrontBtobBundle:Croissiere:index.html.twig', array(



            'entities' => $entities,



            'banner' => $banner,



            ));



			



    }







    public function detailAction($id)



    {



        $entities = $this->getDoctrine()->getRepository("BtobCroissiereBundle:Croissiere")->find($id);



        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($entities->getSeotitle())
            ->addMeta('name', 'description', $entities->getSeodescreption())
            ->addMeta( 'name', 'keywords',$entities->getSeokeyword())
        ;




        return $this->render('FrontBtobBundle:Croissiere:detail.html.twig', array('entry' => $entities));



    }







    public function reservationAction($id)



    {



        $em = $this->getDoctrine()->getManager();
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');



        $client = new Clients();







        $form = $this->createForm(new ClientsType(), $client);







        $entities = $this->getDoctrine()->getRepository("BtobCroissiereBundle:Croissiere")->find($id);


        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($entities->getSeotitle())
            ->addMeta('name', 'description', $entities->getSeodescreption())
            ->addMeta( 'name', 'keywords',$entities->getSeokeyword())
        ;
        $request = $this->get('request');



        if ($request->getMethod() == 'POST') {



            $post = $request->request->get('btob_hotelbundle_clients');



            $cin = $post["cin"];



            // echo $defaultd .'/'.$defaulta ;exit;



            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));



            if ($testclient != null) {



                $client = $testclient;



            }



            $form->bind($request);



            if ($form->isValid()) {



                $em->persist($client);



                $em->flush();







                $Reservationcroi = new Reservationcroi();



                $adulte = $request->request->get('adulte');



                $enfant = $request->request->get('enfant');



                $depart = $request->request->get('depart');



                $voyage = $request->request->get('voyage');



                $body = $request->request->get('body');



                $Comment = $request->request->get('Comment');



                $vmariage = $request->request->get('vmariage');



                $contacte = $request->request->get('contacte');







                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));



                if ($request->request->get('mariage') != null){



                    $mariage = new \Datetime(Tools::explodedate($request->request->get('mariage'),'/'));



                    $Reservationcroi->setDatem($mariage);



                }



                $Reservationcroi->setClient($client);



                $Reservationcroi->setNbradultes($adulte);



                $Reservationcroi->setNbrenf($enfant);



                $Reservationcroi->setDated($dated);



                $Reservationcroi->setVilled($depart);



                $Reservationcroi->setClassev($voyage);



                $Reservationcroi->setDemande($body);



                $Reservationcroi->setComment($Comment);



                $Reservationcroi->setVoyage($vmariage);



                $Reservationcroi->setAgent($User);







                $Reservationcroi->setContacte($contacte);



                $Reservationcroi->setCroissiere($entities);



                $em->persist($Reservationcroi);



                $em->flush();



                $client = new Clients();







                $form = $this->createForm(new ClientsType(), $client);

                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $Reservationcroi->getClient()->getEmail();
                //$to = "afef.tuninfo@gmail.com";
                
				$mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = "MENA TOURS: Réservation Croisière" ;
                $headers = "From:MENATOURS - Réservation Croisière <contact@menatours.com>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message1 .='



				' .'
                      <img id="site-logo" src="http://www.menatours.com/front/images/logo.png" alt="Mena Tours">
                   <br /><br />
				 Cher Madame/Monsieur,<br /><br />
				 Merci pour la confiance, nous notons votre réservation qui reste en instance de confirmation jusqu’au payement.<br />
				 Moyens de payement soit par :<br />
				- Payement directement à notre agence MENA TOURS.<br />
				 Adresse : 21 Avenue USA - 1er étage immeuble les Jasmins Belvédère - 1002 Tunis - Tunisie.<br />
				- Virement bancaire avec l’envoi d’un justificatif de payement ( virement scanné et envoyé par email au contact@menatours.com ou par fax au (+ 216) 71 844 335).<br />
					
					Un Bon Voucher vous serait remis sitôt payement effectué.<br /><br />
					<b>Contact</b><br />
<b>Adresse:</b> 21 Avenue USA - 1er étage immeuble les Jasmins Belvédère - 1002 Tunis - Tunisie<br />
<b>Tel:</b> (+216) 71 844 335 / (+216) 31 332 332<br />
<b>Mobile:</b> (+216) 53 605 073 / (+216) 58 558 502<br />
<b>Fax:</b> (+ 216) 71 844 335<br />
<b>E-mail:</b> contact@menatours.com<br />
<b>Website:</b> www.menatours.com



  </body>';


                mail($to, $subject, $message1, $headers);


                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();			
                //$too = "afef.tuninfo@gmail.com";
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservation  Croisière" ;
                $header = "From:MENATOURS - Réservation Croisière <".$to.">\n";
                $header .= "Reply-To:" .$Reservationcroi->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='

                      <img id="site-logo" src="http://www.menatours.com/front/images/logo.png" alt="Mena Tours">
                   <br /><br />

				 '  . $Reservationcroi->getAgent()->getName() .',<br>
				 vous avez reçu une réservation croisière  , merci de consulter votre backoffice .




  </body>';


                mail($too, $subjects, $message2, $header);



                $request->getSession()->getFlashBag()->add('noticreserv', 'Votre message a bien été envoyé. Merci.');







                return $this->render('FrontBtobBundle:Croissiere:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));











            }



        }







        return $this->render('FrontBtobBundle:Croissiere:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));



    }



}



