<?php

namespace Btob\JeuxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Choix
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\JeuxBundle\Entity\ChoixRepository")
 */
class Choix
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="choix", type="string", length=255, nullable=true)
     */
    private $choix;
    /**
     * @ORM\ManyToOne(targetEntity="Jeu", inversedBy="choix")
     * @ORM\JoinColumn(name="jeu_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $qustion;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set choix
     *
     * @param string $choix
     * @return Choix
     */
    public function setChoix($choix)
    {
        $this->choix = $choix;

        return $this;
    }

    /**
     * Get choix
     *
     * @return string 
     */
    public function getChoix()
    {
        return $this->choix;
    }

    /**
     * Set qustion
     *
     * @param \Btob\JeuxBundle\Entity\Jeu $qustion
     * @return Choix
     */
    public function setQustion(\Btob\JeuxBundle\Entity\Jeu $qustion = null)
    {
        $this->qustion = $qustion;

        return $this;
    }

    /**
     * Get qustion
     *
     * @return \Btob\JeuxBundle\Entity\Jeu 
     */
    public function getQustion()
    {
        return $this->qustion;
    }
}
