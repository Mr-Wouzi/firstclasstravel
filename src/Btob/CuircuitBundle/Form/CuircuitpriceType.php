<?php 

namespace Btob\CuircuitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CuircuitpriceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pricead',NULL, array('required' => true, 'label' => "Prix Adulte"))
            ->add('priceenf',NULL, array('required' => true, 'label' => "Prix Enfant"))//,'data' => '0'
            ->add('priceb',NULL, array('required' => true, 'label' => "Prix Bébé"))//,'data' => '0'
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date debut de période",'format' => 'dd/MM/yyyy'))
            ->add('dates', 'date', array('widget' => 'single_text','label' => "Date fin de période",'format' => 'dd/MM/yyyy'))
            ->add('nbnuit',NULL, array('required' => true, 'label' => "Nombre de nuitées"))//,'data' => '1'
            ->add('supsingle',null, array('required' => true, 'label' => "Supplément Single"))
            ->add('perssupsingle',NULL, array('required' => false, 'label' => "Pourcentage"))
            ->add('red3lit',NULL, array('required' => false, 'label' => false))
            ->add('red4lit',NULL, array('required' => false, 'label' => false))
            ->add('pers3lit',NULL, array('required' => false, 'label' => false))
            ->add('pers4lit',NULL, array('required' => false, 'label' => false))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\CuircuitBundle\Entity\Cuircuitprice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_cuircuitbundle_cuircuitprice';
    }
}
