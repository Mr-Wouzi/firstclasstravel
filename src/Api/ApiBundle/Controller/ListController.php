<?php

namespace Api\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\VoleBundle\Entity\Vol;
use Btob\HotelBundle\Common\Tools;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Btob\HotelBundle\Entity\Reservation;
use Btob\HotelBundle\Entity\Reservationdetail;
use Twig_Loader_Filesystem;
use Twig_Environment;

session_start();

class ListController extends Controller{

    public function searchAction(){
        try{
           header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
        }catch (Exception $e){

        }

        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');

        
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $marge = $user->getHotelmarge();
        $agence = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $agenceact =$agence->getId();

        $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Hotels'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $marge = $user->getHotelmarge();
        $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');

        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');


        $request = $this->get('request');
        //            Tools::dump($request->request);

        $session = $this->getRequest()->getSession();

        /*
         *  donner de formulaire
         */
        // list des arrangement
        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findActiv();
        // list des pays
        $pays = $this->getDoctrine()->getRepository('BtobHotelBundle:Pays')->findActiv();

        // valeur de submit

        $nbjour = 1;
        $paysid = $request->request->get("pays");
        $ville = $request->request->get("ville");
        $nom = $request->request->get("nom");
        $theme = intval($request->request->get("theme"));

        $trie = $request->request->get("ville");
        $try = $request->request->get("testinput");
        $price_min = $request->request->get("price_min");
        $price_max = $request->request->get("price_max");
        $star = $request->request->get("star");
        if($star==0)
        {
            $star =0;
        }
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $request->request->get("dated");
        $dt->modify('+1 day');
        $datef = $request->request->get("datef");

        $ArrFinal = array();
        $filterhotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findActiv();


        $ad = $request->request->get("ad");
        $enf = $request->request->get("enf");
        $arr = $request->request->get("arrr");

        $children = $request->request->get("children");


        if ($request->getMethod() == 'POST') {


            //Tools::dump($request->request);
            $paysid = $request->request->get("pays");
            $session->set('paysid', $paysid);
            $_SESSION['paysid'] = $paysid;

            $nom = $request->request->get("nom");
            $session->set('nom', $nom);
            $_SESSION['nom'] = $nom;

            $ville = $request->request->get("ville");
            $session->set('ville', $ville);
            $_SESSION['ville'] = $ville;

            $trie = $request->request->get("trie");
            $session->set('trie', $trie);
            $_SESSION['trie'] = $trie;


            $price_min = $request->request->get("price_min");
            $session->set('price_min', $price_min);
            $_SESSION['price_min'] = $price_min;


            $price_max = $request->request->get("price_max");
            $session->set('price_max', $price_max);
            $_SESSION['price_max'] = $price_max;


            $star = $request->request->get("star");
            if($star==0)
            {
                $star =0;
            }
            $session->set('star', $star);
            $_SESSION['star'] = $star;

            $dated = $request->request->get("dated");
            $session->set('dated', $dated);
            $_SESSION['dated'] = $dated;

            $datef = $request->request->get("datef");
            $session->set('datef', $datef);
            $_SESSION['datef'] = $datef;

            $ad = $request->request->get("ad");
            $session->set('ad', $ad);
            $_SESSION['ad'] = $ad;

            $enf = $request->request->get("enf");
            $session->set('enf', $enf);
            $_SESSION['enf'] = $enf;

            $children = $request->request->get("children");
            $session->set('children', $children);
            $_SESSION['children'] = $children;

            $arr = $request->request->get("arr");
            $try = $request->request->get("testinput");

            $session->set('testinput', $try);
            $_SESSION['testinput'] = $try;

            $session->set('arr', $arr);
            $_SESSION['arr'] = $arr;

            //Tools::dump($request->request, true);
            // recupération de hotel price
            $tab = explode("/", $dated);

            $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);
            $tab2 = explode("/", $datef);
            $d2 = new \DateTime($tab2[2] . "-" . $tab2[1] . "-" . $tab2[0]);
            $d1->modify('+0 day');
            $datedd = $d1->format("Y-m-d");




            $nbjour = $d2->diff($d1);
            $nbjour = $nbjour->days;
            $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
            $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydate($d1, $user);
            $i = 0;

            foreach ($price as $key => $value) {

                if ($value->getHotel()->getAct()) {

                    $surdemande = false;
                    if ($value->getNbnuit() > $nbjour) {
                        $surdemande = true;
                    }
                    //$filterhotel[] = $value->getHotel();
                    $ArrFinal[$i]["hotel"] = $value->getHotel();
                    $ArrFinal[$i]['hotelpriceid'] = $value->getId();
                    $ArrFinal[$i]['majminstay'] = $value->getMajminstay();
                    $ArrFinal[$i]['redminstay'] = $value->getRedminstay();
                    $ArrFinal[$i]['persmajminstay'] = $value->getPersmajminstay();
                    $ArrFinal[$i]['persredminstay'] = $value->getPersredminstay();
                    $ArrFinal[$i]['retro'] = $value->getRetro();
                    $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($value->getHotel(),$datedd);
                    if($promotion)
                    {
                        $ArrFinal[$i]['promo'] = $promotion->getName();
                        $ArrFinal[$i]['datedpromo'] = $promotion->getDated();
                        $ArrFinal[$i]['datefpromo'] = $promotion->getDates();
                        $ArrFinal[$i]['valp'] = $promotion->getVal();
                        $ArrFinal[$i]['persp'] = $promotion->getPers();
                    }
                    else{
                        $ArrFinal[$i]['promo'] ="";
                        $ArrFinal[$i]['datedpromo'] ="";
                        $ArrFinal[$i]['datefpromo'] ="";
                        $ArrFinal[$i]['valp'] = "";
                        $ArrFinal[$i]['persp'] = "";
                    }

                    // calcule variante de marge








                    $ArrFinal[$i]["hotelname"] = trim($value->getHotel()->getName());
                    $ArrFinal[$i]["hotelid"] =intval($value->getHotel()->getId());


                    $ArrFinal[$i]["star"] = $value->getHotel()->getStar();
                    $ArrFinal[$i]["pays"] = $value->getHotel()->getPays()->getId();
                    $ArrFinal[$i]["ville"] = $value->getHotel()->getVille()->getId();
                    // gestion des images
                    $dataimg = $value->getHotel()->getHotelimg();
                    $img = "/back/img/dummy_150x150.gif";
                    $j = 0;
                    foreach ($dataimg as $keyimg => $valimg) {
                        if ($j == 0)
                            $img = $valimg->getFile();
                        if ($valimg->getPriori())
                            $img = $valimg->getFile();
                        ++$j;
                    }
                    $ArrFinal[$i]["image"] = $img;
                    /*l
                     * gestion des prix
                     */
                    // minimaume d'arrangement
                    $min = 0;
                    $j = 0;
                    $tab = array();
                    $name = "";
                    $pers = false;
                    $etat = false;
                    foreach ($value->getPricearr() as $keyarr => $valarr) {
                        if ($valarr->getEtat() == 1) {
                            if ($valarr->getMinstay() > $nbjour) {
                                $surdemande = true;
                            }
                            if ($j == 0) {
                                $min = $valarr->getPrice();

                                $pers = $valarr->getPers();
                                $etat = $valarr->getEtat();
                                $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                $margeprice = $valarr->getMarge();
                                $persmprice = $valarr->getPersm();
                            } else {
                                if ($min > $valarr->getPrice()) {
                                    $min = $valarr->getPrice();
                                    $pers = $valarr->getPers();
                                    $etat = $valarr->getEtat();
                                    $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                    $margeprice = $valarr->getMarge();
                                    $persmprice = $valarr->getPersm();
                                }
                            }
                            ++$j;
                        }
                    }
                    if ($pers) {
                        $ArrFinal[$i]["price"] = $value->getPrice() + (($value->getPrice() * $min) / 100);

                    } else {
                        $ArrFinal[$i]["price"] = $min + $value->getPrice();
                    }
                    // calcule de la marge
                    $ArrFinal[$i]["price"] = $ArrFinal[$i]["price"];
                    $ArrFinal[$i]["name"] = $name;
                    $ArrFinal[$i]["margeprice"] = $margeprice;
                    $ArrFinal[$i]["persmprice"] = $persmprice;
                    // gestion des chambre
                    $k = 0;
                    //Tools::dump($ad);
                    $iter =0;
                    foreach ($ad as $kad => $valad) {
                        $int= $iter;
                        $nbpersonne = $valad + $enf[$kad];
                        //if ($nbpersonne > 4) $nbpersonne = 4;
                        $room = null;
                        // test dispo room
                        $hotelroom = $value->getHotel()->getHotelroom();
                        $typepersonne = false;
                        foreach ($hotelroom as $khr => $vhr) {
                            if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                                $room = $vhr->getRoom();
                                $typepersonne = $vhr->getPersonne();
                            }
                        }
                        //test dispo arrangement
                        $arrang = null;
                        $hotelarrangement = $value->getHotel()->getHotelarrangement();

                        foreach  ($value->getPricearr() as $keyarr => $valarr) {

                            if ($valarr->getHotelarrangement()->getArrangement()->getId() == $arr[$kad]) {
                                if($valarr->getEtat()==1)
                                {
                                    $arrang =$valarr->getHotelarrangement()->getArrangement();
                                    $msg="";
                                }


                            }

                        }
                        if($arrang==null)
                        {
                            $pricearrs = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotelprice")->find(intval($ArrFinal[$i]['hotelpriceid']));
                            $pricecopc = $this->getDoctrine()->getRepository("BtobHotelBundle:Pricearr")->findFirstArrang($pricearrs);

                            foreach  ($pricecopc as $pricecopcar => $valpricecopc) {

                                $arrang =$valpricecopc->getHotelarrangement()->getArrangement();
                                $msg="*";

                            }
                        }
                        if ($room != null && $arrang != null) {
                            foreach ($hotelroom as $khr => $vhr) {
                                if ($room->getId() == $vhr->getRoom()->getId()) {
                                    $idform = $vhr->getId();
                                }
                            }
                            $ArrFinal[$i]["allprice"][$k]["nbpersonne"] = $nbpersonne;
                            $ArrFinal[$i]["allprice"][$k]["roomname"] = $room->getName();
                            $ArrFinal[$i]["allprice"][$k]["arrangement"] = $arrang->getName();
                            $ArrFinal[$i]["allprice"][$k]["msg"] = $msg;
                            foreach ($value->getPricearr() as $kk => $vv) {
                                if ($vv->getHotelarrangement()->getArrangement()->getId() == $arrang->getId()) {
                                    $ArrFinal[$i]["allprice"][$k]["arrangementid"] = $vv->getId();
                                    $ArrFinal[$i]["allprice"][$k]["minstay"] = $vv->getMinstay();
                                    $ArrFinal[$i]["allprice"][$k]["etat"] = $vv->getEtat();
                                }
                            }

                            $ArrFinal[$i]["allprice"][$k]["ad"] = $valad;
                            $ArrFinal[$i]["allprice"][$k]["enf"] = $enf[$kad];
                            $ArrFinal[$i]["allprice"][$k]["idform"] = $idform;
                            //$ArrFinal[$i]["allprice"][$k]["enf"] = $enf[$kad];
                            //$ArrFinal[$i]["allprice"][$k]["children"] = $children;
                            $ArrFinal[$i]["allprice"][$k]["enf"] = [$enf[$kad],$children];

                            //$calc=new Calculate();
                            $dtest = null;
                            $dtest = $d1;
                            // initialiser l'age des enfant a 6 s'il existe
                            // il faut les changer s'il y a un autre age d'enfant
                            $tabenf = array();
                            $tab = array();

                            // for ($xd = 0; $xd < $enf[$kad]; $xd++) {
                            $tab[] = $ArrFinal[$i]["allprice"][$k]["enf"];
                            // }
                            $iter+= $tab[0][0];

                            $a1= array();


                            for ($xv = $int; $xv < $iter; $xv++) {
                                array_push($a1, $tab[0][1][$xv]);
                            }
                            $ArrFinal[$i]["allprice"][$k]["age"] = $a1;
                            $tabenf = $a1;


                            $valp = $ArrFinal[$i]['valp'];
                            $vals =   $ArrFinal[$i]['persp'] ;







                            $ArrFinal[$i]["allprice"][$k]["dispo"]="0";
                            $disroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findByPriceRoom($value->getId(),$room->getId());

                            $valres = $value->getRetro();




                            if($nbjour>=$ArrFinal[$i]["allprice"][$k]["minstay"] && $disroom[0]->getQte() > 0 )
                            {
                                $ArrFinal[$i]["allprice"][$k]["dispo"]="1";
                            }
                            else{
                                $ArrFinal[$i]["allprice"][$k]["dispo"]="0";
                            }

                            //fin
                            $prix = $this->getDoctrine()
                                ->getRepository('BtobHotelBundle:Hotelprice')
                                ->calcultate($valp,$vals,$user, $dtest->format('Y-m-d'), $value->getHotel()->getId(), $nbjour, $arrang, $valad, $tabenf, $room, $typepersonne);

                            $prix = $prix; // calcule de marge

                            $ArrFinal[$i]["allprice"][$k]["price"] = $prix;

                            if ($prix == 0) {
                                $surdemande = true;
                            }
                            ++$k;
                        }

                    }
                    // Stop Sales
                    $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);
                    $ArrFinal[$i]["stopsales"] = count($stopsales);
                    if ($surdemande) {
                        $ArrFinal[$i]["stopsales"] = 1;
                    }
                    //Tools::dump($request, true);
                    $ArrFinal[$i]["bprix"] =$prix;
                    ++$i;
                }
            }

            if ($try == "nom") {
                $ArrFinal = Tools::array_sort($ArrFinal, "hotelname", SORT_ASC);
            } else if ($try == "prix") {
                $ArrFinal = Tools::array_sort($ArrFinal, "bprix", SORT_ASC);
            } else if ($try == "etoil") {
                $ArrFinal = Tools::array_sort($ArrFinal, "star", SORT_ASC);
            }
            else if ($try == "") {
                $ArrFinal = Tools::array_sort($ArrFinal, "hotelname", SORT_ASC);
            }
            /*
             * Appliquer les filtre de recheche
             */

            // pas de chambre disponible
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            foreach ($ArrFinal1 as $key => $value) {
                if (isset($value["allprice"])) {
                    $ArrFinal[] = $value;
                }
            }
            // filtre de prix
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            foreach ($ArrFinal1 as $key => $value) {
                if ($value["price"] >= $price_min && $value["price"] <= $price_max) {
                    $ArrFinal[] = $value;
                }
            }
            // filter $star
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            if($request->request->get("star")==0)
            {
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["star"] >= $star) {
                        $ArrFinal[] = $value;
                    }
                }
            }else{
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["star"] == $star) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            // filter pays
            if ($paysid > 0) {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["pays"] == $paysid) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            // filter ville
            if ($ville > 0) {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["ville"] == $ville) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            // filter name hotel
            if ($nom != "") {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if(strpos(htmlspecialchars($value["hotelname"]), $nom) !== false)
                    {
                        $ArrFinal[] = $value;
                    }
                }
            }


            $d1 = new \DateTime(Tools::explodedate($dated, '/'));
            $d2 = new \DateTime(Tools::explodedate($datef, '/'));

            $userid = $user->getId();


            $array = array(
                'pays' => $pays,
                'paysid' => $paysid,
                'ville' => $ville,
                'convert' => $convert,
                'convertd' => $convertd,
                'trie' => $trie,
                'try' => $try,
                'user' => $user,
                'price_min' => $price_min,
                'price_max' => $price_max,
                'star' => $star,
                'dated' => $dated,
                'datef' => $datef,
                'nom' => $nom,
                'price' => $ArrFinal,
                'filterhotel' => $filterhotel,
                'arr' => $arrangement,
                'nbjour' => $nbjour,
                'frmad' => $ad,
                'frmenf' => $enf,
                'frmchild' => $children,
                'frmarr' => $arr,
                'd1' => $d1,
                'd2' => $d2
            );


            $response = new JsonResponse($array);
            return $response;

        }else{

            $array = array(
                'method' => 'GET',
            );
            $response = new JsonResponse($array);
            return $response;

        }

    }



    public function detailAction($hotelid, $dated, $datef)

    {

        try{
           header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
        }catch (Exception $e){

        }
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');

        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

        $request = $this->get('request');
        $session = $this->getRequest()->getSession();


        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findActiv();
        // list des pays

        $dt = new \DateTime();
        $dt->modify('+1 day');
        $datedd = $dt->format("d/m/Y");
        $dt->modify('+1 day');
        $dateff = $dt->format("d/m/Y");




        $tab = explode("/", $datedd);

        $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);

        $tab2 = explode("/", $dateff);

        $d2 = new \DateTime($tab2[2] . "-" . $tab2[1] . "-" . $tab2[0]);

        //calculer la deference entre dated et dates
        $nbjour = $d2->diff($d1);


        $nbjour = $nbjour->days;

        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

        $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydate($d1, $user);

        $prices = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $user);
        $pricarres = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->findOneBy(
            array('hotelprice' => $prices[0]->getId(),'etat' => 1)


        );


        $id = $pricarres->getHotelarrangement()->getId();
        $arrg= $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelarrangement')
            ->find($id);

        $ark=$arrg->getArrangement()->getId();
        $ad = array(0 => 2);
        $enf = array(0 => 0);
        $arr = array(0 => $ark);


        $ad = $session->get("ad");
        $ad = $_SESSION['ad'];
        if($ad==NULL)
        {
            $ad = array(0 => 2);
        }

        $enf = $session->get("enf");
        $enf = $_SESSION['enf'];
        if($enf==NULL)
        {
            $enf = array(0 => 0);
        }


        $arr=$session->get("arr");
        $arr = $_SESSION['arr'];

        if($arr==NULL)
        {
            $arr = array(0 => $ark);
        }
        $d1 = new \DateTime(Tools::explodedate($datedd, '/'));
        $d2 = new \DateTime(Tools::explodedate($dateff, '/'));

        $userid = $user->getId();



        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $dataimg = $hotel->getHotelimg();



        $images = array();
        foreach ($dataimg as $keyimg => $valimg) {

            array_push($images, $valimg->getFile());

        }

        $session->set('ad', $ad);
        $_SESSION['ad'] = $ad;
        $session->set('enf', $enf);
        $_SESSION['enf']  = $enf;
        $session->set('arr', $arr);
        $_SESSION['arr'] = $arr;

        $array = array(

            "hotelid" => $hotelid,

            "dated" => $dated,

            "star" => $hotel->getStar(),

            "datef" => $datef,

            "name" => $hotel->getName(),

            "longdesc" => $hotel->getLongdesc(),

            "img" => $images,

        );


        $response = new JsonResponse($array);
        return $response;
    }



    public function selectionAction($hotelid, $dated, $datef)

    {


        try{
           header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
        }catch (Exception $e){

        }
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');

        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

        $hotels = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');


        $d1 = new \DateTime($dated);


        $d1->modify('+0 day');
        $datedd = $d1->format("Y-m-d");

        $d2 = new \DateTime($datef);

        $marge=$User->getHotelmarge();

        /*$marge = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotelmarge')

            ->getMargeByUser(,$d1);*/

        $usersite = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');


        $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriodeHotel($usersite,$hotels,$datedd);

        $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($hotels,$datedd);

        if($promotion)
        {
            $promo = $promotion->getName();
            $datedpromo = $promotion->getDated();
            $datefpromo = $promotion->getDates();
            $valp = $promotion->getVal();
            $persp = $promotion->getPers();
        }
        else{
            $promo ="";
            $datedpromo ="";
            $datefpromo ="";
            $valp = "0";
            $persp = false;
        }







        $request = $this->get('request');

        $session = $this->getRequest()->getSession();

        //$hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $ds = new \DateTime($request->get('dated'));


        $nbjour = $d2->diff($d1);

        $nbjour = $nbjour->days - 1;

        $ad = $session->get("ad");
        $ad = $_SESSION['ad'];

        $enf = $session->get("enf");
        $enf = $_SESSION['enf'];

        $arr = $session->get("arr");
        $arr = $_SESSION['arr'];

        $child = $session->get("children");
        $child = $_SESSION['children'];


        $user = $User;

        $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $user);


        $i = 0;

        $tabactualroom = array();

        foreach ($price as $key => $value) {


            $ArrFinal["majminstay"] = $value->getMajminstay();
            $ArrFinal["redminstay"] = $value->getRedminstay();
            $ArrFinal["persmajminstay"] = $value->getPersmajminstay();
            $ArrFinal["persredminstay"]  = $value->getPersredminstay();



            // calcule variante de marge





            //$filterhotel[] = $value->getHotel();

            $ArrFinal["hotel"] = $value->getHotel();

            $ArrFinal["hotelname"] = trim($value->getHotel()->getName());

            $ArrFinal["star"] = $value->getHotel()->getStar();

            $ArrFinal["pays"] = $value->getHotel()->getPays()->getId();

            $ArrFinal["ville"] = $value->getHotel()->getVille()->getId();

            // gestion des images

            $dataimg = $value->getHotel()->getHotelimg();

            $img = "/back/img/dummy_150x150.gif";

            $j = 0;

            foreach ($dataimg as $keyimg => $valimg) {

                if ($j == 0)

                    $img = $valimg->getFile();

                if ($valimg->getPriori())

                    $img = $valimg->getFile();

                ++$j;

            }

            $ArrFinal["image"] = $img;


            $min = 0;

            $j = 0;

            $tab = array();

            $name = "";

            $pers = false;

            $pricearr = array();

            foreach ($value->getPricearr() as $keyarr => $valarr) {

                if($valarr->getEtat()==1)
                {

                    if ($j == 0) {

                        $min = $valarr->getPrice();

                        $pers = $valarr->getPers();

                        $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                        $minstay = $valarr->getMinstay();

                        $margeprice = $valarr->getMarge();
                        $persmprice = $valarr->getPersm();
                    } else {

                        if ($min > $valarr->getPrice()) {

                            $min = $valarr->getPrice();

                            $pers = $valarr->getPers();

                            $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                            $margeprice = $valarr->getMarge();
                            $persmprice = $valarr->getPersm();

                        }

                    }

                    $pricearr[$j]["margeprice"] = $margeprice;
                    $pricearr[$j]["persmprice"] = $persmprice;
                    $pricearr[$j]["name"] = $valarr->getHotelarrangement()->getArrangement()->getName();
                    $pricearr[$j]["minstay"] = $valarr->getMinstay();

                    $pricearr[$j]["price"] = $valarr->getPrice();
                    $pricearr[$j]["pers"] = $valarr->getPers();
                    $pricearr[$j]["persm"] = $valarr->getPersm();
                    $pricearr[$j]["marge"] = $valarr->getMarge();

                    $pricearr[$j]["price"] = $pricearr[$j]["price"]; // calcule de marge

                    $pricearr[$j]["id"] = $valarr->getId();

                    $pricearr[$j]["idarr"] = $valarr->getHotelarrangement()->getArrangement()->getId();


                    ++$j;
                }
            }

            $pricearr = Tools::array_sort($pricearr, 'price', SORT_ASC);

            $ArrFinal["pricearr"] = $pricearr;


            if ($pers) {

                if($persp)
                {
                    $ArrFinal["price"] = ($value->getPrice()-($value->getPrice()*$valp/100)) + ((($value->getPrice()-($value->getPrice()*$valp/100)) * $min) / 100);

                }else{

                    $ArrFinal["price"] = ($value->getPrice()-$valp) + ((($value->getPrice()-$valp) * $min) / 100);
                }


            } else {

                if($persp)
                {
                    $ArrFinal["price"] = $min + ($value->getPrice()-($value->getPrice()*$valp/100));
                }else{
                    $ArrFinal["price"] = $min + $value->getPrice()-$valp;
                }

            }

            $ArrFinal["price"] = ($ArrFinal["price"]); // calcule de marge

            $ArrFinal["name"] = $name;

            $ArrFinal["sperssupsingle"] = $value->getPerssupsingle();
            $ArrFinal["ssupsingle"] = $value->getSupsingle();
            $ArrFinal["smargess"] = $value->getMargess();
            $ArrFinal["spersss"] = $value->getPersss();


            // gestion des chambre

            $k = 0;

            //Tools::dump($ad);

            foreach ($ad as $kad => $valad) {

                $nbpersonne = $valad + $enf[$kad];

                //if ($nbpersonne > 4) $nbpersonne = 4;

                $room = null;

                // test dispo room

                $hotelroom = $value->getHotel()->getHotelroom();

                $typepersonne = false;

                foreach ($hotelroom as $khr => $vhr) {

                    if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

                        $room = $vhr->getRoom();

                        $tabactualroom[] = $room->getId();

                        $typepersonne = $vhr->getPersonne();



                    }

                }






                //test dispo arrangement

                $arrang = null;

                $hotelarrangement = $value->getHotel()->getHotelarrangement();
                $arr = $session->get("arr");
                $arr = $_SESSION['arr'];

                $zz=array();
                foreach  ($value->getPricearr() as $keyarr => $valarr) {


                    if ($valarr->getHotelarrangement()->getArrangement()->getId() == $arr[$kad]) {
                        if($valarr->getEtat()==1)
                        {
                            $arrang =$valarr->getHotelarrangement()->getArrangement();

                        }


                    }

                }

                if($arrang==null)
                {
                    $pricearrs = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotelprice")->find($valarr->getHotelprice()->getId());
                    $pricecopc = $this->getDoctrine()->getRepository("BtobHotelBundle:Pricearr")->findFirstArrang($pricearrs);

                    foreach  ($pricecopc as $pricecopcar => $valpricecopc) {

                        $arrang =$valpricecopc->getHotelarrangement()->getArrangement();
                        $msg="*";

                    }
                }
                $taboneitem = array();

                if ($room != null && $arrang != null) {

                    $taboneitem = array(

                        "nbpersonne" => $nbpersonne,

                        "roomname" => $room->getName(),

                        "arrangement" => $arrang->getName(),

                        "ad" => $valad,

                        "enf" => $enf[$kad],

                        "arr" => $arr[$kad]

                    );



                    foreach ($hotelroom as $khr => $vhr) {

                        if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

                            $taboneitem["id"] = $vhr->getId();

                            $taboneitem["nbad"] = $vhr->getAd();

                            $taboneitem["nbenf"] = $vhr->getEnf();

                            $taboneitem["typepersonne"] = $vhr->getPersonne();

                        }

                    }

                    // get supplement



                    $suppl = $value->getHotel()->getHotelroomsup();

                    $supplement = $value->getPricesupplement();



                    foreach ($suppl as $ksup => $vsup) {

                        if ($vsup->getRoom()->getId() == $room->getId()) {

                            $xtab = array();// ce tableau pour le croisement de price sup et hotel sup

                            foreach ($supplement as $valsupplement) {

                                if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {

                                    if ($valsupplement->getPers()) {

                                        if($persp)
                                        {



                                            if($valsupplement->getPersm())
                                            {
                                                $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+
                                                    (((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);
                                            }else{
                                                $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ $valsupplement->getMarge();
                                            }
                                        }
                                        else{
                                            if($valsupplement->getPersm())
                                            {
                                                $xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+
                                                    (((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);
                                            }else{
                                                $xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+$valsupplement->getMarge();
                                            }
                                        }

                                        $xtab[0] = $xtab[0]; // calcule de marge

                                    }
                                    else{


                                        if($valsupplement->getPersm())
                                        {
                                            $xtab[0] = $valsupplement->getPrice()+($valsupplement->getPrice()*$valsupplement->getMarge()/100);
                                        }else{
                                            $xtab[0] = $valsupplement->getPrice()+$valsupplement->getMarge();
                                        }
                                    }

                                    $xtab[1] = $valsupplement->getId();

                                }

                            }

                            $taboneitem["supplement"][] = array($vsup, $xtab);

                        } else {

                            $taboneitem["supplement"][] = array();

                        }

                    }

                    // get events

                    $events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);

                    $tabev = array();

                    foreach ($events as $kev => $valev) {

                        $tabev[] = $valev;

                    }

                    $taboneitem['events'] = $tabev;

                    $ArrFinal["allprice"][$room->getId()][] = $taboneitem;

                    ++$k;



                }

                // affichage des autres chambres



                foreach ($value->getHotel()->getHotelroom() as $vrestroom) {

                    if (!in_array($vrestroom->getRoom()->getId(), $tabactualroom)) {

                        $roomother = $vrestroom->getRoom();

                        $nbpersonne = $roomother->getCapacity();

                        if ($roomother != null && $arrang != null) {

                            $taboneitem = array(

                                "nbpersonne" => $nbpersonne,

                                "roomname" => $roomother->getName(),

                                "arrangement" => $arrang->getName(),

                                "ad" => $valad,

                                "enf" => $enf[$kad],

                                "arr" => $arr[$kad]

                            );



                            foreach ($hotelroom as $khr => $vhr) {

                                if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

                                    $taboneitem["id"] = $vhr->getId();

                                    $taboneitem["nbad"] = $vhr->getAd();

                                    $taboneitem["nbenf"] = $vhr->getEnf();

                                }

                            }

                            // get supplement

                            $suppl = $value->getHotel()->getHotelroomsup();

                            $supplement = $value->getPricesupplement();

                            foreach ($suppl as $ksup => $vsup) {

                                if ($vsup->getRoom()->getId() == $roomother->getId()) {

                                    $xtab = array();// ce tableau pour le croisement de price sup et hotel sup

                                    foreach ($supplement as $valsupplement) {

                                        if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {

                                            if ($valsupplement->getPers()) {

                                                if($persp)
                                                {



                                                    if($valsupplement->getPersm())
                                                    {
                                                        $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+
                                                            (((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);
                                                    }else{
                                                        $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ $valsupplement->getMarge();
                                                    }
                                                }
                                                else{
                                                    if($valsupplement->getPersm())
                                                    {
                                                        $xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+
                                                            (((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);
                                                    }else{
                                                        $xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+$valsupplement->getMarge();
                                                    }
                                                }

                                                $xtab[0] = $xtab[0]; // calcule de marge

                                            }
                                            else{


                                                if($valsupplement->getPersm())
                                                {
                                                    $xtab[0] = $valsupplement->getPrice()+($valsupplement->getPrice()*$valsupplement->getMarge()/100);
                                                }else{
                                                    $xtab[0] = $valsupplement->getPrice()+$valsupplement->getMarge();
                                                }
                                            }

                                            $xtab[1] = $valsupplement->getId();

                                        }

                                    }

                                    $taboneitem["supplement"][] = array($vsup, $xtab);

                                } else {

                                    $taboneitem["supplement"][] = array();

                                }

                            }

                            // get events

                            $events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);

                            $tabev = array();

                            foreach ($events as $kev => $valev) {

                                $tabev[] = $valev;

                            }

                            $taboneitem['events'] = $tabev;

                            $ArrFinal["allotherprice"][$roomother->getId()][] = $taboneitem;

                            ++$k;



                        }

                    }

                }

                //Tools::dump($tabactualroom,true);

            }

            // Stop Sales

            $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);

            $ArrFinal["stopsales"] = count($stopsales);

            //Tools::dump($ArrFinal,true);



        }



        $idhotel=$ArrFinal['hotel']->getId();



        //$value = $request->get('_route_params');
        //$hotelname= $value["hotelname"];
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($ArrFinal['hotel']->getSeotitle())
            ->addMeta('name', 'description', $ArrFinal['hotel']->getSeodescreption())
            ->addMeta( 'name', 'keywords',$ArrFinal['hotel']->getSeokeyword())
        ;

        $dataimg = $hotels->getHotelimg();



        $images = array();
        foreach ($dataimg as $keyimg => $valimg) {

            array_push($images, $valimg->getFile());

        }


        return $this->render('FrontBtobBundle:List:selectionn.html.twig', array(

                   "hotelid" => $hotelid,

                "dated" => $d1,
                "convert" => $convert,
                "convertd" => $convertd,
                "hotelname" => $hotels->getName(),
                "star" => $hotels->getStar(),
                "images" => $images,
                "datef" => $d2,

                'price' => $ArrFinal,
                'allprice' => $ArrFinal["allprice"],

                'idhotel' => $idhotel,

                'nbjour' => $nbjour,

                'ad' => $ad,

                'enf' => $enf,

                'arr' => $arr,
                'valp' => $valp,

                'persp' => $persp,

                'child' => $child

                

            )

        );


    }




    public function recapAction(){

        try{
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
        }catch (Exception $e){

        }
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');

        ///Tools::dump($this->get('request'));
        $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');

        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

        $marge=$User->getHotelmarge();

        $request = $this->get('request');

        $request = $request->request;

        $session = $this->getRequest()->getSession();

        $d1 = new \DateTime($request->get('dated'));

        $d1->modify('+0 day');
        $datedd = $d1->format("Y-m-d");

        $d2 = new \DateTime($request->get('datef'));

        $hotelid = $request->get('hotelid');
        $idhot = intval($hotelid);
        $hotels =$this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($idhot);

        $user = $User;

        $hotelPrice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $user);
        $majminstay = $hotelPrice[0]->getMajminstay();
        $redminstay = $hotelPrice[0]->getRedminstay();
        $persmajminstay = $hotelPrice[0]->getPersmajminstay();
        $persredminstay = $hotelPrice[0]->getPersredminstay();

        $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($hotels,$datedd);

        if($promotion)
        {
            $promo = $promotion->getName();
            $datedpromo = $promotion->getDated();
            $datefpromo = $promotion->getDates();
            $valp = $promotion->getVal();
            $persp = $promotion->getPers();
        }
        else{
            $promo ="";
            $datedpromo ="";
            $datefpromo ="";
            $valp = "";
            $persp = "";
        }
        $hotelPrice0=$hotelPrice[0];

        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);


        $surdemande = false;

        $msgsurdemande = "";

        // test stopsales

        $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($hotel, $d1, $user);

        /*if (count($stopsales) > 0) {

            $surdemande = true;

            $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";

        }*/

        $price = $hotelPrice[0];

        $dataimg = $price->getHotel()->getHotelimg();

        $img = "/back/img/dummy_150x150.gif";

        $j = 0;

        foreach ($dataimg as $keyimg => $valimg) {

            if ($j == 0)

                $img = $valimg->getFile();

            if ($valimg->getPriori())

                $img = $valimg->getFile();

            ++$j;

        }


        $nbjour = $d2->diff($d1);

        $nbjour = $nbjour->days;

        $adulte = $request->get("adulte");

        $enfant = $request->get("enfant");


        $children = $request->get("children");

        $arrangement = $request->get("arrangement");




        $supp = $request->get("supp");



        $event = $request->get("event");//Tools::dump($supp,true);

        // set all recap to session

        /*foreach($enfant as $kk=>$vv){

            Tools::dump($vv);

        }exit;*/

        $recap["enfant"] = $enfant;

        $recap["adulte"] = $adulte;

        $recap["dated"] = $request->get('dated');

        $recap["datef"] = $request->get('datef');

        $recap["hotelid"] = $hotelid;

        //Tools::dump($request,true);

        $recap["children"] = $children;

        // Tools::dump($children,true);





        // exit;

        $recap["arrangement"] = $arrangement;


        $recap["supp"] = $supp;

        $recap["event"] = $event;

        $_SESSION['recap'] = $recap;
        $_SESSION['supp'] = $supp;
        $_SESSION['event'] = $event;

        $session->set('recap', $recap);

        $session->set('supp', $supp);

        $session->set('event', $event);


        // end set recap

        // test sur demande pour le dépassement de minstay général



        foreach ($hotelPrice as $vv) {

            if ($vv->getNbnuit() > $nbjour) {

                $surdemande = true;

                $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Nombre de Nuit&eacute;es minimum est à " . $vv->getNbnuit() . " nuit&eacute;es ";

            }

        }

        // test de l'existance de toute la période

        if ($this->getDoctrine()->getRepository('BtobHotelBundle:HotelPrice')->Testallperiode($hotel, $d2, $user)) {

            $surdemande = true;

            $msgsurdemande = "Cet hôtel est disponible pendant la période sélectionnée.<br>Vous pouvez néanmoins envoyer une demande en cliquant sur le bouton réserver.";

        }

        $ArrFinal = array();

        $totalchambre=array();

        foreach ($adulte as $key => $value) {

            $dbroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);



            $typepersonne = $dbroom->getPersonne();

            $typepersonne=0;

            $room = $dbroom->getRoom();



            foreach ($value as $k1 => $val1) {



                foreach ($val1 as $k2 => $val2) {

                    $ArrFinal[$room->getId()][$k1]['roomname'] = $room->getName();

                    $ArrFinal[$room->getId()][$k1]['adulte'] = $val2;

                    $ArrFinal[$room->getId()][$k1]['enfant'] = $enfant[$key][$k1][$k2];



                    $dbarr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->find($arrangement[$key][$k1][$k2]);




                    // calcule de sur demande de ministay



                    if ($dbarr->getMinstay() > $nbjour) {

                        $surdemande = true;

                        $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Nombre de Nuit&eacute;es minimum est à " . $dbarr->getMinstay() . " nuit&eacute;es ";



                    }

                    $ArrFinal[$room->getId()][$k1]['arrangement'] = $dbarr->getHotelarrangement()->getArrangement()->getName();


                    $ArrFinal[$room->getId()][$k1]['minstay'] = $dbarr->getMinstay();

                    $dtest = null;

                    $dtest = $d1;

                    $tabchild = array();

                    if ($k2 == 0) $kx = 1; else $kx = $k2;

                    $tabsupp = array();

                    // supplement



                    if (isset($supp[$key][$k1])) {

                        foreach ($supp[$key][$k1] as $ks => $vs) {
                            $tabsupps = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->find($vs);

                            $ArrFinal[$room->getId()][$k1]['arrangement'] .= "<br>- " . $tabsupps->getHotelsupplement()->getSupplement()->getName();
                            $tabsupp[] = $tabsupps;
                        }
                    }



                    $priceroom = $price->getPriceroom();

                    foreach ($priceroom as $kpr => $vpr) {

                        if ($room->getId() == $vpr->getRoom()->getId()) {

                            if ($vpr->getQte() == 0) {

                                $surdemande = true;

                                $msgsurdemande = "Cet hôtel est disponible sur demande.<br>La chambre " . $room->getName() . " n'est pas disponible.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";

                            }

                            if(!isset($totalchambre[$room->getName()]["qte"])){

                                $totalchambre[$room->getName()]["qte"]=1;



                            }else{

                                $totalchambre[$room->getName()]["qte"]=$totalchambre[$room->getName()]["qte"]+1;

                            }

                            $totalchambre[$room->getName()]["total"]=$vpr->getQte();

                        }

                    }

                    // enfant

                    if (isset($children[$key][$k1])) {

                        $tabchild = $children[$key][$k1];


                    }//echo "$k1-";


                    if (!isset($event[$key][$k1][$k2]) || count($event[$key][$k1][$k2]) == 0) {

                        $testeve = $hotel->getEvents();

                        foreach ($testeve as $vevtest) {

                            if($vevtest->getDated()->format('Ymd') >= $d1->format('Ymd')

                                && $vevtest->getDated()->format('Ymd')<= $d2->format('Ymd')  && $vevtest->getOblig())

                                $event[$key][$k1][$k2] = $vevtest->getId();

                        }



                        $_SESSION['event'] = $event;
                        $session->set('event', $event);

                    }

                    $dbev = array();

                    if (isset($event[$key][$k1])) {

                        foreach ($event[$key][$k1] as $kevv => $valevv) {

                            $xevent = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->find($valevv);

                            $dbev[] = $xevent;

                            $ArrFinal[$room->getId()][$k1]['arrangement'] .= "<br>- " . $xevent->getName();

                        }

                    }





                    $ArrFinal[$room->getId()][$k1]['price'] = $this->getDoctrine()

                        ->getRepository('BtobHotelBundle:Hotelprice')

                        ->calcultate($valp,$persp,$user,$dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne,  $tabsupp, $dbev);


                    $ArrFinal[$room->getId()][$k1]['price'] = ($ArrFinal[$room->getId()][$k1]['price']); // calcule de marge


                    // stop sales tarif =0

                    if ($ArrFinal[$room->getId()][$k1]['price'] == 0) {

                        $surdemande = true;

                        $msgsurdemande = "Cet hôtel est disponible pendant la période sélectionnée.<br>Vous pouvez néanmoins envoyer une demande en cliquant sur le bouton réserver.";

                    }

                }

            }

        }

        foreach($totalchambre as $key=>$value){

            if($value["total"]<$value["qte"]){

                $surdemande = true;

                $msgsurdemande = "Cet hôtel est disponible pendant la période sélectionnée.<br>Vous pouvez néanmoins envoyer une demande en cliquant sur le bouton réserver.";

            }

        }

        //Tools::dump($totalchambre,true);



        $session->set('ArrFinal', $ArrFinal);
        $_SESSION['ArrFinal'] = $ArrFinal;

        $session->set('surdemande', $surdemande);
        $_SESSION['surdemande'] = $surdemande;

        $session->set('msgsurdemande', $msgsurdemande);
        $_SESSION['msgsurdemande'] = $msgsurdemande;


        $array = array(
            'result' => "success",
        );
        $response = new JsonResponse($array);
        return $response;

        /*return $this->render('BtobHotelBundle:List:recap.html.twig', array(

                "hotelid" => $hotelid,

                "dated" => $d1,

                "datef" => $d2,

                'price' => $hotelPrice,

                'nbjour' => $nbjour,

                'entities' => $ArrFinal,

                'img' => $img,

                'entities' => $ArrFinal,

                'surdemande' => $surdemande,

                'msgsurdemande' => $msgsurdemande

            )

        );*/

    }


    public function inscriptionAction()
    {
        try{
           header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
        }catch (Exception $e){

        }
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');

        $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');

        /**
         * $marge = $this->getDoctrine()
         * ->getRepository('BtobHotelBundle:Hotelmarge')
         * ->findByUser($userid);*/
        $agence = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $agenceact = $agence->getId();


        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        $paiement =0;
        $request = $this->get('request');

        $request = $request->request;

        $recap = $session->get('recap');
        $recap = $_SESSION['recap'];

        $session->set('paiement', 0);
        $_SESSION['paiement'] = 0;

        $session->set('id_res_paiement', 0);
        $_SESSION['id_res_paiement'] = 0;

        $d1 = new \DateTime($recap['dated']);

        $d2 = new \DateTime($recap['datef']);

        $d1->modify('+0 day');
        $datedd = $d1->format("Y-m-d");

        $supp = $session->get('supp');
        $supp = $_SESSION['supp'];

        $hotelid = $recap["hotelid"];
        $idhotel = intval($hotelid);

        $zz= $session->get('ArrFinal');
        $zz= $_SESSION['ArrFinal'];
        //$child =$session->get('children');
        $ages = $session->get('children');
        $ages = $_SESSION['children'];

        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($idhotel);

        $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriodeHotel($agence,$hotel,$datedd);

        $margess = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByHotelAgence($hotel,$agence);


        $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($hotel,$datedd);

        if($promotion)
        {
            $promo = $promotion->getName();
            $datedpromo = $promotion->getDated();
            $datefpromo = $promotion->getDates();
            $valp = $promotion->getVal();
            $persp = $promotion->getPers();
        }
        else{
            $promo ="";
            $datedpromo ="";
            $datefpromo ="";
            $valp = "";
            $persp = "";
        }






        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);



        $hotelPrice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $agence);

        $majminstay = $hotelPrice[0]->getMajminstay();
        $redminstay = $hotelPrice[0]->getRedminstay();
        $persmajminstay = $hotelPrice[0]->getPersmajminstay();
        $persredminstay = $hotelPrice[0]->getPersredminstay();


        $nbjour = $d2->diff($d1);
        $nbjour = $nbjour->days;
        $ArrFinal = array();
        $ArrBase = array();
        $dispo = false ;
        $total = 0;
        $exist = true;

        foreach ($recap["adulte"] as $key => $value) {

            $dbroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);
            $typepersonne = $dbroom->getPersonne();
            $room = $dbroom->getRoom();
            foreach ($value as $k1 => $val1) {
                foreach ($val1 as $k2 => $val2) {
                    $ArrFinal[$room->getId()][$k1]['roomname'] = $room->getName();

                    $ArrFinal[$room->getId()][$k1]['adulte'] = $val2;
                    $ArrFinal[$room->getId()][$k1]['enfant'] = $recap['enfant'][$key][$k1][$k2];
                    $dbarr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->find($recap['arrangement'][$key][$k1][$k2]);

                    $ArrFinal[$room->getId()][$k1]['arrangement'] = $dbarr->getHotelarrangement()->getArrangement()->getName();
                    $ArrFinal[$room->getId()][$k1]['minstay'] = $dbarr->getMinstay();




                    //Disponiblité


                    $disroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findByPriceRoom($hotelPrice[0]->getId(),$room->getId());

                    $valres = $hotelPrice[0]->getRetro();




                    if($nbjour>=$ArrFinal[$room->getId()][$k1]['minstay'] && $disroom[0]->getQte() > 0  )
                    {
                        $dispo =true ;

                    }
                    else{
                        $dispo = false ;
                        $exist = false ;

                    }


                    //fin





                    $dtest = null;
                    $dtest = $d1;
                    $tabchild = array();

                    // enfant
                    if (isset($children[$key][$k1])) {
                        $tabchild = $children[$key][$k1];
                    }

                    $dbev = array();
                    $ArrFinal[$room->getId()][$k1]['events'] = array();
                    if (isset($event[$key][$k1])) {
                        foreach ($event[$key][$k1] as $kevv => $valevv) {
                            $xx = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->find($valevv);
                            $ArrFinal[$room->getId()][$k1]['events'] = $xx->getName();
                            $dbev[] = $xx;
                        }
                    }


                    $tabsupp = array();

                    $ArrFinal[$room->getId()][$k1]['supplement'] = array();
                    if (isset($supp[$key][$k1])) {
                        foreach ($supp[$key][$k1] as $ksupp => $valsupp) {
                            $yy = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->find($valsupp);
                            $ArrFinal[$room->getId()][$k1]['supplement'] = $yy->getHotelsupplement()->getSupplement()->getName();

                            $tabsupp[] = $yy;
                        }
                    }

                    //event
                    $ArrFinal[$room->getId()][$k1]['price'] = $this->getDoctrine()
                        ->getRepository('BtobHotelBundle:Hotelprice')
                        ->calcultate($valp,$persp,$agenceact,$dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne,  $tabsupp, $dbev);

                    $total += $ArrFinal[$room->getId()][$k1]['price'];

                    $prixfin=$ArrFinal[$room->getId()][$k1]['price'];
                }


            }

        }



        $d1 = new \DateTime($recap['dated']);
        $d2 = new \DateTime($recap['datef']);
        $dataimg = $hotel->getHotelimg();
        $img = "/back/img/dummy_150x150.gif";
        $j = 0;
        $images = array();
        foreach ($dataimg as $keyimg => $valimg) {

            array_push($images, $valimg->getFile());

        }
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            //Tools::dump($request);
            //die;

            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if (true) {

                $em->persist($client);
                $em->flush();


                $_SESSION['client'] = $client;
                $_SESSION['nameadulte'] = $request->request->get('childnamead');
                $_SESSION['nbageenfant'] = $request->request->get('nbageenfant');
                $_SESSION['nbageadult'] = $request->request->get('nbageadult');
                $_SESSION['namechildreen'] = $request->request->get('childnameenf');
                $_SESSION['options'] = $request->request->get('options');
                $_SESSION['paiement'] = $request->request->get('paiement');


                $session->set('client', $client);
                $session->set('nameadulte', $request->request->get('childnamead'));
                $session->set('nbageenfant', $request->request->get('nbageenfant'));
                $session->set('nbageadult', $request->request->get('nbageadult'));
                $session->set('namechildreen', $request->request->get('childnameenf'));
                $session->set('options', $request->request->get('options'));
                $session->set('paiement', $request->request->get('paiement'));







                $marge = $this->getDoctrine()
                    ->getRepository('BtobHotelBundle:Hotelmarge');

                // ->getMargeByUser($this->get('security.context')->getToken()->getUser());
                $em = $this->getDoctrine()->getManager();
                $session = $this->getRequest()->getSession();
                $caddy = array();
                if (!$session->has('caddy')) {
                    $session->set('caddy', $caddy);
                    $_SESSION['caddy'] = $caddy;
                } else {
                    $caddy = $session->get('caddy');
                    $caddy = $_SESSION['caddy'];
                }

                $recap = $session->get('recap');
                $recap = $_SESSION['recap'];

                $client = $session->get('client');
                $client = $_SESSION['client'];

                $supp = $session->get('supp');
                $supp = $_SESSION['supp'];

                $event = $session->get('event');
                $event = $_SESSION['event'];

                $children = $recap['children'];
                $idclient = $client->getId();
                $client = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->find($idclient);
                $hotelid = $recap["hotelid"];
                $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

                $nameadulte = $session->get('nameadulte');
                $nameadulte = $_SESSION['nameadulte'];

                $nbageenfant = $session->get('nbageenfant');
                $nbageenfant = $_SESSION['nbageenfant'];

                $nbageadult = $session->get('nbageadult');
                $nbageadult = $_SESSION['nbageadult'];

                $namechildreen = $session->get('namechildreen');
                $namechildreen = $_SESSION['namechildreen'];
                // get variante de marge


                $d1 = new \DateTime($recap['dated']);
                $d2 = new \DateTime($recap['datef']);
                $nbjour = $d2->diff($d1);
                $nbjour = $nbjour->days;
                $reservation = new Reservation();
                $reservation->setClient($client);
                $reservation->setHotel($hotel);
                $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
                $reservation->setUser($User);
                $reservation->setRemarque(""); // a terminer ajouter le champs dans le formulaires
                $reservation->setDated($d1);
                $reservation->setDatef($d2);
                $session->set('paiement', $request->request->get('paiement'));
                $_SESSION['paiement'] = $request->request->get('paiement');

                if ($session->get('surdemande')) {
                    $reservation->setEtat(5);
                } else {
                    $reservation->setEtat(1);
                }

                if ($_SESSION['surdemande']) {
                    $reservation->setEtat(5);
                } else {
                    $reservation->setEtat(1);
                }

                $reservation->setNbnuit($nbjour);
                $reservation->setNamead(json_encode($nameadulte));
                $reservation->setAgeenfant(json_encode($nbageenfant));
                $reservation->setAgeadult(json_encode($nbageadult));
                $reservation->setNameenf(json_encode($namechildreen));

                // calcule et génération de tablea
                // Tools::dump($recap,true);
                $ArrFinal = array();

                $total = 0;
                $totalbase = 0;
                foreach ($recap["adulte"] as $key => $value) {

                    $dbroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);
                    $typepersonne = $dbroom->getPersonne();
                    $room = $dbroom->getRoom();
                    foreach ($value as $k1 => $val1) {
                        foreach ($val1 as $k2 => $val2) {
                            $ArrFinal[$room->getId()][$k1]['roomname'] = $room->getName();

                            $ArrFinal[$room->getId()][$k1]['adulte'] = $val2;
                            $ArrFinal[$room->getId()][$k1]['enfant'] = $recap['enfant'][$key][$k1][$k2];
                            $dbarr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->find($recap['arrangement'][$key][$k1][$k2]);
                            $ArrFinal[$room->getId()][$k1]['arrangement'] = $dbarr->getHotelarrangement()->getArrangement()->getName();
                            $ArrFinal[$room->getId()][$k1]['minstay'] = $dbarr->getMinstay();

                            $dtest = null;
                            $dtest = $d1;
                            $tabchild = array();




                            // enfant
                            if (isset($children[$key][$k1])) {
                                $tabchild = $children[$key][$k1];
                            }

                            $dbev = array();
                            $ArrFinal[$room->getId()][$k1]['events'] = array();
                            if (isset($event[$key][$k1])) {
                                foreach ($event[$key][$k1] as $kevv => $valevv) {
                                    $xx = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->find($valevv);
                                    //$ArrFinal[$room->getId()][$k1]['events'] = $xx->getName();
                                    array_push($ArrFinal[$room->getId()][$k1]['events'], $xx->getName());

                                    $dbev[] = $xx;
                                }
                            }



                            $tabsupp = array();
                            $ArrFinal[$room->getId()][$k1]['supplement'] = array();
                            if (isset($supp[$key][$k1])) {
                                foreach ($supp[$key][$k1] as $ksup => $valsup) {
                                    $yy = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->find($valsup);
                                    //$ArrFinal[$room->getId()][$k1]['supplement'] = $yy->getHotelsupplement()->getSupplement()->getName();
                                    array_push($ArrFinal[$room->getId()][$k1]['supplement'], $yy->getHotelsupplement()->getSupplement()->getName());

                                    $tabsupp[] = $yy;
                                }
                            }


                            //event
                            $ArrFinal[$room->getId()][$k1]['price'] = $this->getDoctrine()
                                ->getRepository('BtobHotelBundle:Hotelprice')
                                ->calcultate($valp, $persp, $User, $dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne,  $tabsupp, $dbev);

                            $total += $ArrFinal[$room->getId()][$k1]['price'];

                            $ArrBase[$room->getId()][$k1]['price'] = $this->getDoctrine()
                                ->getRepository('BtobHotelBundle:Hotelprice')
                                ->calcultate($valp,$persp,$agenceact,$dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne, $tabsupp, $dbev);

                            $totalbase += $ArrBase[$room->getId()][$k1]['price'];

                        }


                    }
                }

                $reservation->setTotalint($total);
                $reservation->setTotalbase($totalbase);
                $total2 = $total;


                $reservation->setTotal($total2);

                $em->persist($reservation);
                $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRoles("ROLE_SUPER_ADMIN");


                foreach ($users as $kuser => $valuser) {
                    //if (in_array('ROLE_SUPER_ADMIN', $valuser->getRoles()) || in_array('SALES', $valuser->getRoles())) {
                    $notif = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findAll();
                    $tst = false;
                    $not = null;
                    foreach ($notif as $value) {
                        if ($valuser->getId() == $value->getUser()->getId()) {
                            $not = $value;
                            $tst = true;
                        }
                    }
                    if ($tst) {
                        $note = $not->getNotif() + 1;
                        $not->setNotif($note);
                    } else {
                        $not = new Notification();
                        $not->setNotif(1);
                        $not->setUser($valuser);
                    }
                    $caddy['notif'][] = $not;
                    $em->persist($not);
                    $em->flush();
                    //}
                }
                $em->flush();
                // génération de tableau pour l'enregistrer dans la bdd



                foreach ($ArrFinal as $kx => $vx) {


                    foreach ($vx as $key => $value) {
                        //Tools::dump($vx,true);
                        $detail = new Reservationdetail();
                        $detail->setPrice($value['price']);
                        $detail->setReservation($reservation);
                        $detail->setRoomname($value['roomname']);
                        $detail->setAd($value['adulte']);
                        $detail->setEnf($value['enfant']);
                        $detail->setArrangement($value['arrangement']);

                        $html = "";

                        if (is_array($vx[$key]["supplement"])) {
                            if (isset($vx[$key]["supplement"])) {
                                foreach ($vx[$key]["supplement"] as $ks => $vs) {
                                    $html .= $vs . "<br />";
                                }
                            }
                        } else {
                            $html .= $vx[$key]["supplement"] . "<br />";
                        }
                        $detail->setSupp($html);
                        $html = "";
                        //Tools::dump($vx[$key]["events"], true);

                        if (is_array($vx[$key]["events"])) {
                            if (isset($vx[$key]["events"])) {
                                foreach ($vx[$key]["events"] as $kev => $vev) {
                                    $html .= $vev . "<br />";
                                }
                            }
                        } else {
                            $html .= $vx[$key]["events"] . "<br />";
                        }

                        $detail->setEvent($html);
                        $reservation->addReservationdetail($detail);

                        $em->persist($detail);
                        $em->flush();
                    }
                }

                $caddy['resahotel'] = $reservation;
                $session->set('caddy', $caddy);
                $_SESSION['caddy'] = $caddy;

                $detailres = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($reservation->getId());

                setlocale(LC_TIME, 'fr_FR', 'fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%d %B %Y ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%A ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));


                $to = $reservation->getClient()->getEmail();
                //$to = "afef.tuninfo@gmail.com";

                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = "First Class Travel: Réservation Hôtel";
                $headers = "From: First-Class-Travel shamid-fct@menara.ma\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">                 
                <tbody>               <tr>                 <td>               
                <a href="http://www.firstclasstravel.ma/">
                <img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
                </a>                   
                </td>                  <td align="right">                   
                <img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>                  </td>            
                </tr>                </tbody>               
                </table>
                <br /> <b>Cher Madame/Monsieur,</b><br />
                 Merci pour votre réservation. Votre demande a bien été envoyée, nous traiterons votre demande dans les plus brefs délais.<br />
                 <table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre commande :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getUser()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
                $message1 .= '<td width="20%" height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getId() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getHotel()->getName() . ' ' . $detailres->getHotel()->getVille()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getDated()->format('Y-m-d H:i:s') . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getDatef()->format('Y-m-d H:i:s') . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Total</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getTotal() . ' MAD</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getCiv() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getPname() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getEmail() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getTel() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getCin() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getAdresse() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getPays()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message2 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed 5 et Av. Hassen 1er N° 7 Bloc B - Imm. SIBAM2 4000 Marrakech Maroc 
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                 <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
                </td>';
                $message1 .= '</tr>';
                $message1 .= '</table>';
                $message1 .= '</body>';

                mail($to, $subject, $message1, $headers);
                mail($detailres->getClient()->getEmail(), $subject, $message1, $headers);
                mail("infos.firstclass@gmail.com", $subject, $message1, $headers);
                mail('crazynames1@gmail.com', $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too = $admin->getEmail();
                //$too = "afef.tuninfo@gmail.com";          
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects = "Réservation  hôtel";
                $header = "From: First-Class-Travel shamid-fct@menara.ma\n";
                $header .= "Reply-To:shamid-fct@menara.ma\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">                 
                <tbody>               <tr>                 <td>               
                <a href="http://www.firstclasstravel.ma/">
                <img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
                </a>                   
                </td>                  <td align="right">                   
                <img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>                  </td>            
                </tr>                </tbody>       
                </table>';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Sélection :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getUser()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
                $message2 .= '<td width="20%" height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getId() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getHotel()->getName() . ' ' . $detailres->getHotel()->getVille()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getDated()->format('Y-m-d H:i:s') . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getDatef()->format('Y-m-d H:i:s') . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Total</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getTotal() . ' MAD</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Coordonnées :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getCiv() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getPname() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getEmail() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getTel() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getCin() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getAdresse() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getPays()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed 5 et Av. Hassen 1er N° 7 Bloc B - Imm. SIBAM2 4000 Marrakech Maroc 
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                 <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
                </td>';
                $message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';

                mail($too, $subject, $message2, $header);
                mail("infos.firstclass@gmail.com", $subject, $message2, $headers);
                mail($detailres->getClient()->getEmail(), $subject, $message2, $headers);
                mail('crazynames1@gmail.com', $subject, $message2, $headers);

                //mail responsable
                $namead = json_decode($detailres->getNamead(), true);
                $nameenf = json_decode($detailres->getNameenf(), true);
                $ageenfant = json_decode($detailres->getAgeenfant(), true);


                $responsable = $this->getDoctrine()->getRepository('BtobHotelBundle:Responsablehotel')->responsableHotel($hotel);


                if ($responsable) {

                    $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                    $toor = $responsable->getMail();
                    //$toor = "afef.tuninfo@gmail.com";

                    $mime_boundary = "----MSA Shipping----" . md5(time());
                    $subjectr = "Réservation Hôtel: " . $admin->getEmail();
                    $headerr = "From:First-Class-Travel shamid-fct@menara.ma \n";
                    $headerr .= "Reply-To:" . $reservation->getUser()->getName() . " " . $admin->getEmail() . "\n";
                    $headerr .= "MIME-Version: 1.0\n";
                    $headerr .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                    $message3 = "--$mime_boundary\n";
                    $message3 .= "Content-Type: text/html; charset=UTF-8\n";
                    $message3 .= "Content-Transfer-Encoding: 8bit\n\n";
                    $message3 .= "<html>\n";
                    $message3 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                    $message3 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">                 
                        <tbody>               <tr>                 <td>               
                            <a href="http://www.firstclasstravel.ma/">
                            <img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
                            </a>                   
                            </td>                  <td align="right">                   
                            <img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>                  </td>            
                            </tr>                </tbody>       
                        </table>';
                    $message3 .= '<table width="90%"  cellspacing="1" border="0">';
                    $message3 .= '<tr>';
                    $message3 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Sélection :</b></td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getUser()->getName() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
                    $message3 .= '<td width="20%" height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getId() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getHotel()->getName() . ' ' . $detailres->getHotel()->getVille()->getName() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getDated()->format('Y-m-d H:i:s') . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getDatef()->format('Y-m-d H:i:s') . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Total</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getTotal() . ' MAD</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Coordonnées :</b></td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getCiv() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getPname() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getName() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getEmail() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getTel() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getCin() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getAdresse() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getCp() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getPays()->getName() . '</td>';
                    $message3 .= '</tr>';


                    $message3 .= '</table>';


                    $message3 .= '<br><table style="border:1px solid #ccc" width="90%"  cellspacing="1" border="0">';
                    $cc = 0;
                    $message3 .= '<tr>';
                    $message3 .= '<td height="35" valign="middle" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Num chambre :</b></td>';
                    $message3 .= '<td height="35" valign="middle" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Noms des occupants :</b></td>';
                    $message3 .= '</tr>';
                    foreach ($namead as $keyn => $valn) {
                        $cc++;
                        $message3 .= '<tr>';
                        $message3 .= '<td height="30" style="border-right:1px solid #ccc; border-bottom:1px solid #ccc;" >' . $cc . '</td>';
                        $message3 .= '<td height="30" style="border-bottom:1px solid #ccc;" >';
                        foreach ($valn as $adv => $adx) {
                            $message3 .="<strong>Nom Adulte : </strong> ". $adx . '<br> ';
                        }
                        $message3 .= '</td>';


                        $message3 .= '</tr>';
                    }
                    $message3 .= '</table>';


                    $message3 .= '<br><table width="90%" style="border:1px solid #ccc"  cellspacing="1" border="0">';
                    $message3 .= '<tr>';
                    $message3 .= '<td height="35" valign="middle" colspan="7" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Chambres :</b></td>';
                    $message3 .= '</tr>';
                    foreach ($detailres->getReservationdetail() as $keyy => $vals) {
                        $message3 .= '<tr>';
                        $message3 .= '<td style="border-bottom:1px solid #ccc" height="30"> <strong> ' . $vals->getRoomname() . '</strong></td>';
                        $message3 .= '<td style="border-bottom:1px solid #ccc" height="30"><strong>Adulte(s):</strong> ' . $vals->getAd() . '</td>';
                        $message3 .= '<td style="border-bottom:1px solid #ccc" height="30"><strong>Enfant(s):</strong> ' . $vals->getEnf() . '</td>';
                        $message3 .= '<td style="border-bottom:1px solid #ccc" height="30"><strong>Arrangement:</strong> ' . $vals->getArrangement() . '</td>';
                        $message3 .= '<td style="border-bottom:1px solid #ccc" height="30"><strong>Suppléments:</strong> ' . $vals->getSupp() . '</td>';
                        $message3 .= '<td style="border-bottom:1px solid #ccc" height="30"><strong>Evenement:</strong> ' . $vals->getEvent() . '</td>';
                        $message3 .= '<td style="border-bottom:1px solid #ccc" height="30"><strong>Prix:</strong> ' . $vals->getPrice() . 'MAD</td>';
                        $message3 .= '</tr>';
                    }
                    $message3 .= '</table>';

                    $message3 .= '</body>';

                    mail($toor, $subject, $message3, $headers);
                    mail("infos.firstclass@gmail.com", $subject, $message3, $headers);
                    mail($detailres->getClient()->getEmail(), $subject, $message3, $headers);
                    mail('crazynames1@gmail.com', $subject, $message3, $headers);

                }




            } else {
                echo $form->getErrors();
            }
            $request->getSession()->getFlashBag()->add('notifomrafront', 'Votre réservation a été bien prise en compte. Notre conseiller vous contactera dans les plus bref délais.');
            
            $_SESSION['recap'] = $recap;
            $_SESSION['nameadulte'] = $request->request->get('childnamead');
            $_SESSION['nbageenfant'] = $request->request->get('nbageenfant');
            $_SESSION['nbageadult'] = $request->request->get('nbageadult');
            $_SESSION['namechildreen'] = $request->request->get('childnameenf');
            $_SESSION['options'] = $request->request->get('options');
            $_SESSION['paiement'] = $request->request->get('paiement');
            $_SESSION['img'] = $img;
            $_SESSION['total'] = $total2;
            $_SESSION['id_res_paiement'] = $reservation->getId();


            $session->set('recap', $recap);
            $session->set('nameadulte', $request->request->get('childnamead'));
            $session->set('nbageenfant', $request->request->get('nbageenfant'));
            $session->set('nbageadult', $request->request->get('nbageadult'));
            $session->set('namechildreen', $request->request->get('childnameenf'));
            $session->set('options', $request->request->get('options'));
            $session->set('paiement', $request->request->get('paiement'));
            $session->set('img', $img);
            $session->set('total', $total2);
            $session->set('id_res_paiement', $reservation->getId());

            $array = array(
                'result' => "success",

            );
            $response = new JsonResponse($array);
            return $response;


        }
        // traitement pour l'affichage de formulaire de rooming list
        $tabform = array();
        foreach ($recap['adulte'] as $key => $value) {
            foreach ($value as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    $tabform[$key][$k2]["adulte"] = $v2;
                    $tabform[$key][$k2]["enfant"] = $recap['enfant'][$key][$k1][$k2];
                }
            }
        }
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        //Tools::dump($recap,true);
        $ctr = count($recap['adulte']);

        $mode = $this->getDoctrine()->getRepository('BtobHotelBundle:Payement')->find(1);

        $ax= array();

        if(isset($recap['children']))
        {

            foreach ($recap['children'] as $keyss => $valuess) {
                foreach ($valuess as $keysss => $valuesss) {
                    foreach ($valuesss as $keyssss => $valuessss) {
                        array_push($ax, $valuessss);
                    }
                }
            }
        }


        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($hotel->getSeotitle())
            ->addMeta('name', 'description', $hotel->getSeodescreption())
            ->addMeta( 'name', 'keywords',$hotel->getSeokeyword());



        $arrays = array(

            'arrfinal' =>  $zz,

            "hotelid" => $hotelid,
            "name" => $hotel->getName(),
            "star" => $hotel->getStar(),
            "convert" => $convert,
            "convertd" => $convertd,
            "dated" => $d1,
            "datef" => $d2,
            "ax" => $ax,
            "prixfin" => $prixfin,
            'entities' => $hotel,
            'img' => $img,
            'images' => $images,
            'valres' => $valres,
            'margess' => $margess,
            'recap' => $recap,
            //'form' => $form->createView(),
            'tabform' => $tabform,
            // affichage de recap de l'action recap
            'exist' => $exist,
            'user' => $user,
            'mode' => $mode,
            'surdemande' => $_SESSION['surdemande'],
            'msgsurdemande' => $_SESSION['msgsurdemande']
        );



        $response = new JsonResponse($arrays);
        return $response;
    }



}

