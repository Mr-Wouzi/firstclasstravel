<?php

namespace Btob\DashBundle\Twig;


use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;

class PageExtension extends \Twig_Extension
{
    private $doctrine;
    private $context;

    public function __construct(\Doctrine\ORM\EntityManager $em, $context)
    {
        $this->doctrine = $em;
        $this->context = $context;
    }

    public function getFilters()
    {
        return array(
            'string2url' => new \Twig_Filter_Method($this, 'string2urlFilter'),
            'getshort' => new \Twig_Filter_Method($this, 'getshortFilter'),
            'getfinper' => new \Twig_Filter_Method($this, 'getfinperFilter'),
            'getcoderef' => new \Twig_Filter_Method($this, 'getcoderefFilter'),
            'getnumberformat' => new \Twig_Filter_Method($this, 'getnumberformatFilter'),
            'addTo' => new \Twig_Filter_Method($this, 'addToFilter'),
            'tobdd' => new \Twig_Filter_Method($this, 'tobddFilter'),
            'RefResa' => new \Twig_Filter_Method($this, 'RefResaFilter'),
            'RefOmra' => new \Twig_Filter_Method($this, 'RefOmraFilter'),
            'changedevise' => new \Twig_Filter_Method($this, 'changedeviseFilter'),
            'changedevisefront' => new \Twig_Filter_Method($this, 'changedevisefrontFilter'),
            'calculemargefrontFilter' => new \Twig_Filter_Method($this, 'calculemargefrontFilter'),
            'calculemarge' => new \Twig_Filter_Method($this, 'calculemargeFilter'),
            'Marqueblanche' => new \Twig_Filter_Method($this, 'MarqueblancheFilter'),
        );
    }
    public function MarqueblancheFilter($price){
        $marque=$this->context->getToken()->getUser()->getMarque();
        $perst=$this->context->getToken()->getUser()->getPrstmarque();
        if($perst){
            $price=$price+($price*$marque/100);
        }else{
            $price=$price+$marque;
        }
        return $price;
    }
    public function calculemargeFilter($price)
    {
        $margepers= $this->context->getToken()->getUser()->getPrst();
        $margeval= $this->context->getToken()->getUser()->getMarge();
        if ($margepers == 1) {
            $price += ($price * $margeval) / 100;
        } else {
            $price = $price + $margeval;
        }
        return $price;
    }
    public function calculemargefrontFilter($price)
    {
        $user = $this->doctrine->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $margepers= $this->$user->getPrst();
        $margeval= $this->$user->getMarge();
        if ($margepers == 1) {
            $price += ($price * $margeval) / 100;
        } else {
            $price = $price + $margeval;
        }
        return $price;
    }

    public function changedeviseFilter($price)
    {
        $user = $this->context->getToken()->getUser();
        $symb=$user->getDevise()->getSymb();
        $currency=$user->getDevise()->getCurrency();
        $chiffre=$user->getDevise()->getchiffre();
        $price=$price*$currency;
        return number_format($price,$chiffre,"."," ") ." ".$symb;
    }
    public function changedevisefrontFilter($price)
    {
        $user = $this->doctrine->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
//echo $user['0']->getDevise()->getCurrency();exit;
        //  $user = $this->context->getToken()->getUser();
        $symb=$user->getDevise()->getSymb();
        $currency=$user->getDevise()->getCurrency();
        $chiffre=$user->getDevise()->getchiffre();
        $price=$price*$currency;
        return number_format($price,$chiffre,"."," ") ." ".$symb;
    }



    public function RefResaFilter($ref, $id)
    {
        if ($ref == "") {
            return substr_replace("00000000", $id, -strlen($id));
        } else {
            return $ref;
        }
    }
    public function RefOmraFilter($ref, $id)
    {
        if ($ref == "") {
            return substr_replace("OMRA000000", $id, -strlen($id));
        } else {
            return $ref;
        }
    }

    public function tobddFilter($dt)
    {
        $tab = explode("/", $dt);
        return $tab[2] . "-" . $tab[1] . "-" . $tab[0];
    }

    public function addToFilter($string)
    {
        $string = addslashes($string);
        return $string;
    }

    public function getcoderefFilter($id)
    {
        $nb = strlen($id);
        $str = "";
        for ($i = $nb; $i <= 4; $i++) {
            $str .= "0";
        }
        return $str = date("Y") . $str . $id;
    }

    public function getfinperFilter($p)
    {
        return ceil($p);
    }

    public function getnumberformatFilter($p)
    {
        return number_format($p, 3, ".", "");
    }

    public function string2urlFilter($str)
    {
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);
        return $str;
    }

    public function getshortFilter($html)
    {
        $nb = 200; //return $html;
        $html = strip_tags($html);
        if (strlen($html) > $nb) {
            return substr($html, 0, strpos($html, ' ', $nb)) . "...";
        } else {
            return $html;
        }
        return $html;
    }


    public function getName()
    {
        return 'page_extension';
    }

}
