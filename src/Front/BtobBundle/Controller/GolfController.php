<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Common\Tools;
use Btob\OmraBundle\Entity\Omra;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\User;
use Btob\GolfBundle\Entity\Golf;
use Btob\GolfBundle\Entity\Reservationgolf;
class GolfController extends Controller
{

    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobGolfBundle:Golf")->findby(array('act' => 1));
        $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        return $this->render('FrontBtobBundle:Golf:index.html.twig', array(
            'entities' => $entities,			'convert' => $convert,
            'banner' => $banner,
            ));
    }

    public function detailAction(Golf $Golf)
    {
        return $this->render('FrontBtobBundle:Golf:detail.html.twig', array('entry' => $Golf));
    }


    public function reservationgolfAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobGolfBundle:Golf")->find($id);
        $User =  $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $datea = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));


                $nbrad = $request->request->get('nbrad');
                $nbrenf = $request->request->get('nbrenf');
                $remarque = $request->request->get('remarque');

                $reservation = new Reservationgolf();
                $reservation->setClient($client);
                $reservation->setSejour($entities);
                $reservation->setDated($datea);
                $reservation->setAgent($User);
                $reservation->setNbradult($nbrad);
                $reservation->setNbrenf($nbrenf);
                $reservation->setRemarque($remarque);
                $em->persist($reservation);
                $em->flush();


                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);


 date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $reservation->getClient()->getEmail();
                //$to = "afef.tuninfo@gmail.com";
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = "MENA TOURS: R�servation Outgoing" ;
                $headers = "From:MENATOURS - R�servation Outgoing <contact@menatours.com>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='



				' .'
                      <img id="site-logo" src="http://www.menatours.com/front/images/logo.png" alt="Mena Tours">
                    <br /><br />
				 Cher Madame/Monsieur,<br /><br />
				 Merci pour la confiance, nous notons votre r�servation qui reste en instance de confirmation jusqu�au payement.<br />
				 Moyens de payement soit par :<br />
				- Payement directement � notre agence MENA TOURS.<br />
				 Adresse : 21 Avenue USA - 1er �tage immeuble les Jasmins Belv�d�re - 1002 Tunis - Tunisie.<br />
				- Virement bancaire avec l�envoi d�un justificatif de payement ( virement scann� et envoy� par email au contact@menatours.com ou par fax au (+ 216) 71 844 335).<br />
					
					Un Bon Voucher vous serait remis sit�t payement effectu�.<br /><br />
					<b>Contact</b><br />
<b>Adresse:</b> 21 Avenue USA - 1er �tage immeuble les Jasmins Belv�d�re - 1002 Tunis - Tunisie<br />
<b>Tel:</b> (+216) 71 844 335 / (+216) 31 332 332<br />
<b>Mobile:</b> (+216) 53 605 073 / (+216) 58 558 502<br />
<b>Fax:</b> (+ 216) 71 844 335<br />
<b>E-mail:</b> contact@menatours.com<br />
<b>Website:</b> www.menatours.com



  </body>';

$message1 .= 
              
             "C.I.N / Passeport ".getCin()." "
             ."Civilit� ".$client->getCiv()." "
             ."pr�nom".$client->getPname()." "
             ."Nom  ".$client->getName()." "
			 ."Tel ".$client->getTel()." "
             ."Pays".$client->getPays()." "
             ."Adresse  ".$client->getAdresse()." "
			 ."Sejour".$reservation->getSejour()." "
			 ."Date de D�part".$reservation->getDated." "
             ."Adult".$reservation->getNbradult()." "
             ."Enfant".$reservation->getNbrenf()." "
             ."Remarque".$reservation->getRemarque()." ";
                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                //$too = "afef.tuninfo@gmail.com";
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "R�servation  Outgoing" ;
                $header = "From:MENATOURS - R�servation Outgoing <".$to.">\n";
                $header .= "Reply-To:" .$reservation->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='
                      <img id="site-logo" src="http://www.menatours.com/front/images/logo.png" alt="Mena Tours">
                    <br /><br />
				 '  . $reservation->getAgent()->getName() .',<br>
				 vous avez re�u une r�servation Outgoing  , merci de consulter votre backoffice .


  </body>';

$message2 .= 
              
             "C.I.N / Passeport ".getCin()." "
             ."Civilit� ".$client->getCiv()." "
             ."pr�nom".$client->getPname()." "
             ."Nom  ".$client->getName()." "
			 ."Tel ".$client->getTel()." "
             ."Pays".$client->getPays()." "
             ."Adresse  ".$client->getAdresse()." "
			 ."Sejour".$reservation->getSejour()." "
			 ."Date de D�part".$reservation->getDated()." "
             ."Adult".$reservation->getNbradult()." "
             ."Enfant".$reservation->getNbrenf()." "
             ."Remarque".$reservation->getRemarque()." ";
                mail($too, $subjects, $message2, $header);




                $request->getSession()->getFlashBag()->add('notigolf', 'Votre message a bien �t� envoy�. Merci.');
                return $this->redirect($this->generateUrl('front_golf_homepage'));


            }
        }

        return $this->render('FrontBtobBundle:Golf:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }


}
