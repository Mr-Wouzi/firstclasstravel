<?php



use Symfony\Component\HttpKernel\Kernel;

use Symfony\Component\Config\Loader\LoaderInterface;



class AppKernel extends Kernel

{

    public function registerBundles()

    {

        $bundles = array(

            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),

            new Symfony\Bundle\SecurityBundle\SecurityBundle(),

            new Symfony\Bundle\TwigBundle\TwigBundle(),

            new Symfony\Bundle\MonologBundle\MonologBundle(),

            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),

            new Symfony\Bundle\AsseticBundle\AsseticBundle(),

            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),

            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            // vendors

            new Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle(),

            new Knp\Bundle\MenuBundle\KnpMenuBundle(),

            new FOS\UserBundle\FOSUserBundle(),

            new WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),

            

            

            new Btob\HotelBundle\BtobHotelBundle(),

            new Btob\DashBundle\BtobDashBundle(),

            new User\UserBundle\UserUserBundle(),

            new Btob\OmraBundle\BtobOmraBundle(),

            new Btob\CuircuitBundle\BtobCuircuitBundle(),

            new Btob\SiminaireBundle\BtobSiminaireBundle(),

            new Btob\CroissiereBundle\BtobCroissiereBundle(),

            new Btob\TransfertBundle\BtobTransfertBundle(),

	    new Btob\BienetreBundle\BtobBienetreBundle(),

            new Btob\CaddyBundle\BtobCaddyBundle(),

            new Btob\SejourBundle\BtobSejourBundle(),
			new Btob\EvenementBundle\BtobEvenementBundle(),

            new Btob\GolfBundle\BtobGolfBundle(),

            new Btob\TeamBundle\BtobTeamBundle(),

            new Btob\VoleBundle\BtobVoleBundle(),

            new Btob\BannaireBundle\BtobBannaireBundle(),

            new Btob\ActiviteBundle\BtobActiviteBundle(),

	    new Btob\VoitureBundle\BtobVoitureBundle(),

	    new Btob\ClasseBundle\BtobClasseBundle(),

            new Btob\OptionvoitureBundle\BtobOptionvoitureBundle(),

	    new Btob\JeuxBundle\BtobJeuxBundle(),

	    new Front\BtobBundle\FrontBtobBundle(),
            new Sonata\SeoBundle\SonataSeoBundle(),

            

            // new Btob\HotelBundle\BtobHotelBundle(),

            // new Btob\DashBundle\BtobDashBundle(),

            // new User\UserBundle\UserUserBundle(),

            // new Btob\OmraBundle\BtobOmraBundle(),

            // new Btob\CuircuitBundle\BtobCuircuitBundle(),

            // new Btob\SiminaireBundle\BtobSiminaireBundle(),

            // new Btob\CroissiereBundle\BtobCroissiereBundle(),

            // new Btob\TransfertBundle\BtobTransfertBundle(),

			// new Btob\BienetreBundle\BtobBienetreBundle(),

            // new Btob\CaddyBundle\BtobCaddyBundle(),

            // new Front\BtobBundle\FrontBtobBundle(),

			// new Btob\SejourBundle\BtobSejourBundle(),

			// new Btob\GolfBundle\BtobGolfBundle(),

			// new Btob\TeamBundle\BtobTeamBundle(),

            // new Btob\VoleBundle\BtobVoleBundle(),

            // new Btob\BannaireBundle\BtobBannaireBundle(),


		

            new Api\ApiBundle\ApiBundle(),


        );



        if (in_array($this->getEnvironment(), array('dev', 'test'))) {

            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();

            //$bundles[] = new Acme\DemoBundle\AcmeDemoBundle();

            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();

            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();

            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();

        }



        return $bundles;

    }



    public function registerContainerConfiguration(LoaderInterface $loader)

    {

        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');

    }

}

