<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotelmarge
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelmargeRepository")
 */
class Hotelmarge {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float" , nullable=true)
     */
    private $marge;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prst", type="boolean" , nullable=true)
     */
    private $prst;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="hotelmarge")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="hotelmarge")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;



    public function __construct() {
        $this->prst=false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marge
     *
     * @param float $marge
     * @return Hotelmarge
     */
    public function setMarge($marge)
    {
        $this->marge = $marge;

        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */
    public function getMarge()
    {
        return $this->marge;
    }

    /**
     * Set prst
     *
     * @param boolean $prst
     * @return Hotelmarge
     */
    public function setPrst($prst)
    {
        $this->prst = $prst;

        return $this;
    }

    /**
     * Get prst
     *
     * @return boolean 
     */
    public function getPrst()
    {
        return $this->prst;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Hotelmarge
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }


    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Hotelmarge
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }



    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
