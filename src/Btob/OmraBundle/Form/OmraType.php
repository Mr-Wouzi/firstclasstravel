<?php



namespace Btob\OmraBundle\Form;



use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Validator\Constraints\Null;



class OmraType extends AbstractType

{

    /**

     * @param FormBuilderInterface $builder

     * @param array $options

     */

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder

            ->add('libelle', 'text', array('label' => "Libelle", 'required' => true))

            ->add('prix', NULL, array('label' => 'Prix', 'required' => true))

          //  ->add('description')

            ->add('description', null, array('label' => "Description longue", 'required' => false))
            ->add('seotitle', Null, array('label' => "Seo Title", 'required' => true))
            ->add('seodescreption', Null, array('label' => "Seo Description ", 'required' => true))
            ->add('seokeyword', Null, array('label' => "Seo Keyword", 'required' => true))


        ;

    }

    

    /**

     * @param OptionsResolverInterface $resolver

     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => 'Btob\OmraBundle\Entity\Omra'

        ));

    }



    /**

     * @return string

     */

    public function getName()

    {

        return 'btob_omrabundle_omra';

    }

}

