<?php 
/**
 * This file serves as an example
 * 
 * 
 * PHP dependencies:
 * PHP >= 5.2.0
 * PHP CURL module
 * PHP Mcrypt module
 * 
 * @package Connect2PayClient
 * @version 2.0.3
 * @author Mehdi ATRAIMCHE <mehdi.atraimche@vpscorp.ma>
 **/
?>

<!DOCTYPE html>
<html>
<head>
	<title>Example of product</title>
	<style type="text/css">
	.product{
		width: 900px;
		margin:0 auto;
	}
	</style>
</head>
<body>
	<div class="product">
		<h1>Sample Product</h1>
		<img src="https://cdn0.iconfinder.com/data/icons/huge-business-vector-icons/60/box-128.png">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, eum neque magni ipsa unde optio rem 
		molestias explicabo alias animi dicta repudiandae beatae sit molestiae assumenda! Neque dolores necessitatibus 
		numquam.</p>
		<a href="buy.php">Buy Now</a>
	</div>
</body>
</html>