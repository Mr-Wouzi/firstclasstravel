<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PayementType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('num', 'text', array('label' => "RIB", 'required' => true))
            ->add('actrib', null, array('label' => "Virement bancaire?", 'required' => false))
            ->add('actline', null, array('label' => "Paiement en ligne?", 'required' => false))
            ->add('actagence', null, array('label' => "Paiement à l'agence?", 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Payement'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_payement';
    }
}
