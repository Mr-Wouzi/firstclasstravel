<?php

namespace Btob\HotelBundle\Entity;

use Btob\HotelBundle\Utils\Hotels as Hotels;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelRepository")
 */
class Hotel
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="star", type="integer" , nullable=true)
     */
    private $star;
    /**
     * @var integer
     *
     * @ORM\Column(name="maxenfant", type="integer",nullable=true)
     */
    private $maxenfant;
    /**
     * @var integer
     *
     * @ORM\Column(name="minenfant", type="integer",nullable=true)
     */
    private $minenfant;

    /**
     * @var integer
     *
     * @ORM\Column(name="contrat", type="integer" , nullable=true)
     */
    private $contrat;

    /**
     * @var string
     *
     * @ORM\Column(name="shortdesc", type="text" , nullable=true)
     */
    private $shortdesc;

    /**
     * @var string
     *
     * @ORM\Column(name="visite", type="string", length=255,nullable=true)
     */
    private $visite;

    /**
     * @var string
     *
     * @ORM\Column(name="longdesc", type="text" , nullable=true)
     */
    private $longdesc;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean" , nullable=true)
     */
    private $act;

    /**
     * @ORM\ManyToOne(targetEntity="Pays", inversedBy="hotel")
     * @ORM\JoinColumn(name="pays_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $pays;

    /**
     * @ORM\ManyToOne(targetEntity="Ville", inversedBy="hotel")
     * @ORM\JoinColumn(name="ville_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $ville;

    /**
     * @ORM\OneToMany(targetEntity="Hotelchild", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelchild;

    /**
     * @ORM\OneToMany(targetEntity="Hotelamg", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelamg;

    /**
     * @ORM\OneToMany(targetEntity="Hotelarrangement", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelarrangement;

    /**
     * @ORM\OneToMany(targetEntity="Hotellocalisation", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotellocalisation;

    /**
     * @ORM\OneToMany(targetEntity="Hoteloptions", mappedBy="hotel", cascade={"remove"})
     */
    protected $hoteloptions;

    /**
     * @ORM\OneToMany(targetEntity="Hotelthemes", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelthemes;
    /**
     * @ORM\OneToMany(targetEntity="Hotelroom", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelroom;
    /**
     * @ORM\OneToMany(targetEntity="Hotelimg", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelimg;
    /**
     * @ORM\OneToMany(targetEntity="Week", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelweek;
    /**
     * @ORM\OneToMany(targetEntity="Hotelprice", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelprice;
    /**
     * @ORM\OneToMany(targetEntity="Hotelsupplement", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelsupplement;
    /**
     * @ORM\OneToMany(targetEntity="Promotion", mappedBy="hotel", cascade={"remove"})
     */
    protected $promotion;

    /**
     * @ORM\OneToMany(targetEntity="Responsablehotel", mappedBy="hotel", cascade={"remove"})
     */
    protected $responsablehotel;
    /**
     * @ORM\OneToMany(targetEntity="Stopsales", mappedBy="hotel", cascade={"remove"})
     */
    protected $stopsales;
    /**
     * @ORM\OneToMany(targetEntity="Hotelmarge", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelmarge;
    /**
     * @ORM\OneToMany(targetEntity="Hotelroomsup", mappedBy="hotel", cascade={"remove"})
     */
    protected $hotelroomsup;
    /**
     * @ORM\OneToMany(targetEntity="Events", mappedBy="hotel", cascade={"remove"})
     */
    protected $events;
    /**
     * @ORM\OneToMany(targetEntity="Reservation", mappedBy="hotel", cascade={"remove"})
     */
    protected $reservation;
    /**
     * @ORM\ManyToMany(targetEntity="Baseroom", inversedBy="hotel")
     * @ORM\JoinTable(name="Hotelbaseroom")
     */
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="seotitle", type="string", length=255 , nullable=true)
     */

    private $seotitle;

    /**
     * @var string
     *
     * @ORM\Column(name="seodescreption", type="string", length=255 , nullable=true)
     */

    private $seodescreption;

    /**
     * @var string
     *
     * @ORM\Column(name="seokeyword", type="string", length=255 , nullable=true)
     */

    private $seokeyword;
    
    
    
    private $baseroom;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hotelchild = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelamg = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelarrangement = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotellocalisation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hoteloptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelthemes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelroom = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelimg = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelweek = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelprice = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelsupplement = new \Doctrine\Common\Collections\ArrayCollection();
        $this->promotion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->responsablehotel = new \Doctrine\Common\Collections\ArrayCollection();
        $this->stopsales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelmarge = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hotelroomsup = new \Doctrine\Common\Collections\ArrayCollection();
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reservation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->baseroom = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Hotel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set star
     *
     * @param integer $star
     * @return Hotel
     */
    public function setStar($star)
    {
        $this->star = $star;

        return $this;
    }

    /**
     * Get star
     *
     * @return integer 
     */
    public function getStar()
    {
        return $this->star;
    }

    /**
     * Set maxenfant
     *
     * @param integer $maxenfant
     * @return Hotel
     */
    public function setMaxenfant($maxenfant)
    {
        $this->maxenfant = $maxenfant;

        return $this;
    }

    /**
     * Get maxenfant
     *
     * @return integer 
     */
    public function getMaxenfant()
    {
        return $this->maxenfant;
    }

    /**
     * Set minenfant
     *
     * @param integer $minenfant
     * @return Hotel
     */
    public function setMinenfant($minenfant)
    {
        $this->minenfant = $minenfant;

        return $this;
    }

    /**
     * Get minenfant
     *
     * @return integer 
     */
    public function getMinenfant()
    {
        return $this->minenfant;
    }

    /**
     * Set contrat
     *
     * @param integer $contrat
     * @return Hotel
     */
    public function setContrat($contrat)
    {
        $this->contrat = $contrat;

        return $this;
    }

    /**
     * Get contrat
     *
     * @return integer 
     */
    public function getContrat()
    {
        return $this->contrat;
    }

    /**
     * Set shortdesc
     *
     * @param string $shortdesc
     * @return Hotel
     */
    public function setShortdesc($shortdesc)
    {
        $this->shortdesc = $shortdesc;

        return $this;
    }

    /**
     * Get shortdesc
     *
     * @return string 
     */
    public function getShortdesc()
    {
        return $this->shortdesc;
    }

    /**
     * Set visite
     *
     * @param string $visite
     * @return Hotel
     */
    public function setVisite($visite)
    {
        $this->visite = $visite;

        return $this;
    }

    /**
     * Get visite
     *
     * @return string 
     */
    public function getVisite()
    {
        return $this->visite;
    }

    /**
     * Set longdesc
     *
     * @param string $longdesc
     * @return Hotel
     */
    public function setLongdesc($longdesc)
    {
        $this->longdesc = $longdesc;

        return $this;
    }

    /**
     * Get longdesc
     *
     * @return string 
     */
    public function getLongdesc()
    {
        return $this->longdesc;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Hotel
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set pays
     *
     * @param \Btob\HotelBundle\Entity\Pays $pays
     * @return Hotel
     */
    public function setPays(\Btob\HotelBundle\Entity\Pays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \Btob\HotelBundle\Entity\Pays 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param \Btob\HotelBundle\Entity\Ville $ville
     * @return Hotel
     */
    public function setVille(\Btob\HotelBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \Btob\HotelBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Add hotelchild
     *
     * @param \Btob\HotelBundle\Entity\Hotelchild $hotelchild
     * @return Hotel
     */
    public function addHotelchild(\Btob\HotelBundle\Entity\Hotelchild $hotelchild)
    {
        $this->hotelchild[] = $hotelchild;

        return $this;
    }

    /**
     * Remove hotelchild
     *
     * @param \Btob\HotelBundle\Entity\Hotelchild $hotelchild
     */
    public function removeHotelchild(\Btob\HotelBundle\Entity\Hotelchild $hotelchild)
    {
        $this->hotelchild->removeElement($hotelchild);
    }

    /**
     * Get hotelchild
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelchild()
    {
        return $this->hotelchild;
    }

    /**
     * Add hotelamg
     *
     * @param \Btob\HotelBundle\Entity\Hotelamg $hotelamg
     * @return Hotel
     */
    public function addHotelamg(\Btob\HotelBundle\Entity\Hotelamg $hotelamg)
    {
        $this->hotelamg[] = $hotelamg;

        return $this;
    }

    /**
     * Remove hotelamg
     *
     * @param \Btob\HotelBundle\Entity\Hotelamg $hotelamg
     */
    public function removeHotelamg(\Btob\HotelBundle\Entity\Hotelamg $hotelamg)
    {
        $this->hotelamg->removeElement($hotelamg);
    }

    /**
     * Get hotelamg
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelamg()
    {
        return $this->hotelamg;
    }

    /**
     * Add hotelarrangement
     *
     * @param \Btob\HotelBundle\Entity\Hotelarrangement $hotelarrangement
     * @return Hotel
     */
    public function addHotelarrangement(\Btob\HotelBundle\Entity\Hotelarrangement $hotelarrangement)
    {
        $this->hotelarrangement[] = $hotelarrangement;

        return $this;
    }

    /**
     * Remove hotelarrangement
     *
     * @param \Btob\HotelBundle\Entity\Hotelarrangement $hotelarrangement
     */
    public function removeHotelarrangement(\Btob\HotelBundle\Entity\Hotelarrangement $hotelarrangement)
    {
        $this->hotelarrangement->removeElement($hotelarrangement);
    }

    /**
     * Get hotelarrangement
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelarrangement()
    {
        return $this->hotelarrangement;
    }

    /**
     * Add hotellocalisation
     *
     * @param \Btob\HotelBundle\Entity\Hotellocalisation $hotellocalisation
     * @return Hotel
     */
    public function addHotellocalisation(\Btob\HotelBundle\Entity\Hotellocalisation $hotellocalisation)
    {
        $this->hotellocalisation[] = $hotellocalisation;

        return $this;
    }

    /**
     * Remove hotellocalisation
     *
     * @param \Btob\HotelBundle\Entity\Hotellocalisation $hotellocalisation
     */
    public function removeHotellocalisation(\Btob\HotelBundle\Entity\Hotellocalisation $hotellocalisation)
    {
        $this->hotellocalisation->removeElement($hotellocalisation);
    }

    /**
     * Get hotellocalisation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotellocalisation()
    {
        return $this->hotellocalisation;
    }

    /**
     * Add hoteloptions
     *
     * @param \Btob\HotelBundle\Entity\Hoteloptions $hoteloptions
     * @return Hotel
     */
    public function addHoteloption(\Btob\HotelBundle\Entity\Hoteloptions $hoteloptions)
    {
        $this->hoteloptions[] = $hoteloptions;

        return $this;
    }

    /**
     * Remove hoteloptions
     *
     * @param \Btob\HotelBundle\Entity\Hoteloptions $hoteloptions
     */
    public function removeHoteloption(\Btob\HotelBundle\Entity\Hoteloptions $hoteloptions)
    {
        $this->hoteloptions->removeElement($hoteloptions);
    }

    /**
     * Get hoteloptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHoteloptions()
    {
        return $this->hoteloptions;
    }

    /**
     * Add hotelthemes
     *
     * @param \Btob\HotelBundle\Entity\Hotelthemes $hotelthemes
     * @return Hotel
     */
    public function addHoteltheme(\Btob\HotelBundle\Entity\Hotelthemes $hotelthemes)
    {
        $this->hotelthemes[] = $hotelthemes;

        return $this;
    }

    /**
     * Remove hotelthemes
     *
     * @param \Btob\HotelBundle\Entity\Hotelthemes $hotelthemes
     */
    public function removeHoteltheme(\Btob\HotelBundle\Entity\Hotelthemes $hotelthemes)
    {
        $this->hotelthemes->removeElement($hotelthemes);
    }

    /**
     * Get hotelthemes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelthemes()
    {
        return $this->hotelthemes;
    }

    /**
     * Add hotelroom
     *
     * @param \Btob\HotelBundle\Entity\Hotelroom $hotelroom
     * @return Hotel
     */
    public function addHotelroom(\Btob\HotelBundle\Entity\Hotelroom $hotelroom)
    {
        $this->hotelroom[] = $hotelroom;

        return $this;
    }

    /**
     * Remove hotelroom
     *
     * @param \Btob\HotelBundle\Entity\Hotelroom $hotelroom
     */
    public function removeHotelroom(\Btob\HotelBundle\Entity\Hotelroom $hotelroom)
    {
        $this->hotelroom->removeElement($hotelroom);
    }

    /**
     * Get hotelroom
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelroom()
    {
        return $this->hotelroom;
    }

    /**
     * Add hotelimg
     *
     * @param \Btob\HotelBundle\Entity\Hotelimg $hotelimg
     * @return Hotel
     */
    public function addHotelimg(\Btob\HotelBundle\Entity\Hotelimg $hotelimg)
    {
        $this->hotelimg[] = $hotelimg;

        return $this;
    }

    /**
     * Remove hotelimg
     *
     * @param \Btob\HotelBundle\Entity\Hotelimg $hotelimg
     */
    public function removeHotelimg(\Btob\HotelBundle\Entity\Hotelimg $hotelimg)
    {
        $this->hotelimg->removeElement($hotelimg);
    }

    /**
     * Get hotelimg
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelimg()
    {
        return $this->hotelimg;
    }

    /**
     * Add hotelweek
     *
     * @param \Btob\HotelBundle\Entity\Week $hotelweek
     * @return Hotel
     */
    public function addHotelweek(\Btob\HotelBundle\Entity\Week $hotelweek)
    {
        $this->hotelweek[] = $hotelweek;

        return $this;
    }

    /**
     * Remove hotelweek
     *
     * @param \Btob\HotelBundle\Entity\Week $hotelweek
     */
    public function removeHotelweek(\Btob\HotelBundle\Entity\Week $hotelweek)
    {
        $this->hotelweek->removeElement($hotelweek);
    }

    /**
     * Get hotelweek
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelweek()
    {
        return $this->hotelweek;
    }

    /**
     * Add hotelprice
     *
     * @param \Btob\HotelBundle\Entity\Hotelprice $hotelprice
     * @return Hotel
     */
    public function addHotelprice(\Btob\HotelBundle\Entity\Hotelprice $hotelprice)
    {
        $this->hotelprice[] = $hotelprice;

        return $this;
    }

    /**
     * Remove hotelprice
     *
     * @param \Btob\HotelBundle\Entity\Hotelprice $hotelprice
     */
    public function removeHotelprice(\Btob\HotelBundle\Entity\Hotelprice $hotelprice)
    {
        $this->hotelprice->removeElement($hotelprice);
    }

    /**
     * Get hotelprice
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelprice()
    {
        return $this->hotelprice;
    }

    /**
     * Add hotelsupplement
     *
     * @param \Btob\HotelBundle\Entity\Hotelsupplement $hotelsupplement
     * @return Hotel
     */
    public function addHotelsupplement(\Btob\HotelBundle\Entity\Hotelsupplement $hotelsupplement)
    {
        $this->hotelsupplement[] = $hotelsupplement;

        return $this;
    }

    /**
     * Remove hotelsupplement
     *
     * @param \Btob\HotelBundle\Entity\Hotelsupplement $hotelsupplement
     */
    public function removeHotelsupplement(\Btob\HotelBundle\Entity\Hotelsupplement $hotelsupplement)
    {
        $this->hotelsupplement->removeElement($hotelsupplement);
    }

    /**
     * Get hotelsupplement
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelsupplement()
    {
        return $this->hotelsupplement;
    }

    /**
     * Add promotion
     *
     * @param \Btob\HotelBundle\Entity\Promotion $promotion
     * @return Hotel
     */
    public function addPromotion(\Btob\HotelBundle\Entity\Promotion $promotion)
    {
        $this->promotion[] = $promotion;

        return $this;
    }

    /**
     * Remove promotion
     *
     * @param \Btob\HotelBundle\Entity\Promotion $promotion
     */
    public function removePromotion(\Btob\HotelBundle\Entity\Promotion $promotion)
    {
        $this->promotion->removeElement($promotion);
    }

    /**
     * Get promotion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Add stopsales
     *
     * @param \Btob\HotelBundle\Entity\Stopsales $stopsales
     * @return Hotel
     */
    public function addStopsale(\Btob\HotelBundle\Entity\Stopsales $stopsales)
    {
        $this->stopsales[] = $stopsales;

        return $this;
    }

    /**
     * Remove stopsales
     *
     * @param \Btob\HotelBundle\Entity\Stopsales $stopsales
     */
    public function removeStopsale(\Btob\HotelBundle\Entity\Stopsales $stopsales)
    {
        $this->stopsales->removeElement($stopsales);
    }

    /**
     * Get stopsales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStopsales()
    {
        return $this->stopsales;
    }

    /**
     * Add hotelmarge
     *
     * @param \Btob\HotelBundle\Entity\Hotelmarge $hotelmarge
     * @return Hotel
     */
    public function addHotelmarge(\Btob\HotelBundle\Entity\Hotelmarge $hotelmarge)
    {
        $this->hotelmarge[] = $hotelmarge;

        return $this;
    }

    /**
     * Remove hotelmarge
     *
     * @param \Btob\HotelBundle\Entity\Hotelmarge $hotelmarge
     */
    public function removeHotelmarge(\Btob\HotelBundle\Entity\Hotelmarge $hotelmarge)
    {
        $this->hotelmarge->removeElement($hotelmarge);
    }

    /**
     * Get hotelmarge
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelmarge()
    {
        return $this->hotelmarge;
    }

    /**
     * Add hotelroomsup
     *
     * @param \Btob\HotelBundle\Entity\Hotelroomsup $hotelroomsup
     * @return Hotel
     */
    public function addHotelroomsup(\Btob\HotelBundle\Entity\Hotelroomsup $hotelroomsup)
    {
        $this->hotelroomsup[] = $hotelroomsup;

        return $this;
    }

    /**
     * Remove hotelroomsup
     *
     * @param \Btob\HotelBundle\Entity\Hotelroomsup $hotelroomsup
     */
    public function removeHotelroomsup(\Btob\HotelBundle\Entity\Hotelroomsup $hotelroomsup)
    {
        $this->hotelroomsup->removeElement($hotelroomsup);
    }

    /**
     * Get hotelroomsup
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotelroomsup()
    {
        return $this->hotelroomsup;
    }

    /**
     * Add events
     *
     * @param \Btob\HotelBundle\Entity\Events $events
     * @return Hotel
     */
    public function addEvent(\Btob\HotelBundle\Entity\Events $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param \Btob\HotelBundle\Entity\Events $events
     */
    public function removeEvent(\Btob\HotelBundle\Entity\Events $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add reservation
     *
     * @param \Btob\HotelBundle\Entity\Reservation $reservation
     * @return Hotel
     */
    public function addReservation(\Btob\HotelBundle\Entity\Reservation $reservation)
    {
        $this->reservation[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \Btob\HotelBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\Btob\HotelBundle\Entity\Reservation $reservation)
    {
        $this->reservation->removeElement($reservation);
    }

    /**
     * Get reservation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * Add baseroom
     *
     * @param \Btob\HotelBundle\Entity\Baseroom $baseroom
     * @return Hotel
     */
    public function addBaseroom(\Btob\HotelBundle\Entity\Baseroom $baseroom)
    {
        $this->baseroom[] = $baseroom;

        return $this;
    }

    /**
     * Remove baseroom
     *
     * @param \Btob\HotelBundle\Entity\Baseroom $baseroom
     */
    public function removeBaseroom(\Btob\HotelBundle\Entity\Baseroom $baseroom)
    {
        $this->baseroom->removeElement($baseroom);
    }

    /**
     * Get baseroom
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBaseroom()
    {
        return $this->baseroom;
    }




    /**
     * Add responsablehotel
     *
     * @param \Btob\HotelBundle\Entity\Responsablehotel $responsablehotel
     * @return Hotel
     */
    public function addResponsablehotel(\Btob\HotelBundle\Entity\Responsablehotel $responsablehotel)
    {
        $this->responsablehotel[] = $responsablehotel;

        return $this;
    }

    /**
     * Remove responsablehotel
     *
     * @param \Btob\HotelBundle\Entity\Responsablehotel $responsablehotel
     */
    public function removeResponsablehotel(\Btob\HotelBundle\Entity\Responsablehotel $responsablehotel)
    {
        $this->responsablehotel->removeElement($responsablehotel);
    }

    /**
     * Get responsablehotel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponsablehotel()
    {
        return $this->responsablehotel;
    }

    
    
    /**

     * Set seotitle

     *

     * @param string $seotitle

     * @return Hotel

     */

    public function setSeotitle($seotitle)

    {

        $this->seotitle = $seotitle;



        return $this;

    }



    /**

     * Get seotitle

     *

     * @return string

     */

    public function getSeotitle()

    {

        return $this->seotitle;

    }


    /**

     * Set seodescreption

     *

     * @param string $seodescreption

     * @return Hotel

     */

    public function setSeodescreption($seodescreption)

    {

        $this->seodescreption = $seodescreption;



        return $this;

    }



    /**

     * Get seodescreption

     *

     * @return string

     */

    public function getSeodescreption()

    {

        return $this->seodescreption;

    }


    /**

     * Set seokeyword
     *

     * @param string $seokeyword

     * @return Hotel

     */

    public function setSeokeyword($seokeyword)

    {

        $this->seokeyword = $seokeyword;



        return $this;

    }



    /**

     * Get seodkeyword

     *

     * @return string

     */

    public function getSeokeyword()

    {

        return $this->seokeyword;

    }
    
    public function getNameSlug()
    {
        return Hotels::slugify($this->getName());
    }    
    
}
