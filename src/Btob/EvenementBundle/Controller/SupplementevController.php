<?php

namespace Btob\EvenementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\EvenementBundle\Entity\Supplementev;
use Symfony\Component\HttpFoundation\Request;
use Btob\EvenementBundle\Form\SupplementevType;
use Symfony\Component\HttpFoundation\JsonResponse;

class SupplementevController extends Controller {

    public function indexAction($evenementid) {
        $evenement = $this->getDoctrine()
                ->getRepository('BtobEvenementBundle:Evenement')
                ->find($evenementid);
        return $this->render('BtobEvenementBundle:Supplementev:index.html.twig', array('entities' => $evenement->getSupplementev(), "evenementid" => $evenementid));
    }

    public function addAction($evenementid) {
        $evenement = $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenement')->find($evenementid);
        $Supplementev = new Supplementev();
        $form = $this->createForm(new SupplementevType(), $Supplementev);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Supplementev->setEvenement($evenement);
                $em->persist($Supplementev);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplementev_homepage', array("evenementid" => $evenementid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobEvenementBundle:Supplementev:form.html.twig', array('form' => $form->createView(), "evenementid" => $evenementid));
    }

    public function editAction($id, $evenementid) {
        $request = $this->get('request');
        $Supplementev = $this->getDoctrine()
                ->getRepository('BtobEvenementBundle:Supplementev')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementevType(), $Supplementev);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplementev_homepage', array("evenementid" => $evenementid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobEvenementBundle:Supplementev:form.html.twig', array('form' => $form->createView(), 'id' => $id, "evenementid" => $evenementid)
        );
    }

    public function deleteAction(Supplementev $Supplementev, $evenementid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Supplementev) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
        $em->remove($Supplementev);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplementev_homepage', array("evenementid" => $evenementid)));
    }

}
