<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Ville;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\VilleType;
use Symfony\Component\HttpFoundation\JsonResponse;

class VilleController extends Controller {

    public function indexAction() {
        $ville = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Ville')
                ->findAll();
        return $this->render('BtobHotelBundle:Ville:index.html.twig', array('entities' => $ville));
    }

    public function addAction() {
        $ville = new Ville();
        $form = $this->createForm(new VilleType(), $ville);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($ville);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_ville_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Ville:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $ville = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Ville')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new VilleType(), $ville);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_ville_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Ville:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Ville $ville) {
        $em = $this->getDoctrine()->getManager();

        if (!$ville) {
            throw new NotFoundHttpException("Ville non trouvée");
        }
        $em->remove($ville);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_ville_homepage'));
    }

    public function ajxPaysAction() {
        $request = $this->get('request');
        $id=$request->request->get("id");
        $ville = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Ville')
                ->findByPays($id);
        
        return $this->render('BtobHotelBundle:Ville:option.html.twig', array('entity'=>$ville)
        );
    }

}
