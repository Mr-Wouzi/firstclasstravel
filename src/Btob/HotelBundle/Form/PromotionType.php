<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PromotionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date debut",'format' => 'dd/MM/yyyy'))
            ->add('dates', 'date', array(
                    'widget' => 'single_text',
                    'label' => "Date fin",
                    'format' => 'dd/MM/yyyy',
                   ))
            ->add('name', 'text', array('required' => true, 'label' => "Titre de la promotion"))
            ->add('shortdesc',null, array('required' => true, 'label' => "Description de la promotion"))
            ->add('val',null, array('required' => true, 'label' => "Valeur de la promotion"))
            ->add('pers', null, array('label' => "Porcentage", 'required' => false))
            ->add('act', null, array('label' => "Active?", 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Promotion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_promotion';
    }
}
