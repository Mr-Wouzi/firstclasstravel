<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pricesupplement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PricesupplementRepository")
 */
class Pricesupplement
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float")
     */
    private $marge;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pers", type="boolean")
     */
    private $pers;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="persm", type="boolean")
     */
    private $persm;

    /**
     * @ORM\ManyToOne(targetEntity="Hotelprice", inversedBy="Pricesupplement", cascade={"persist"})
     * @ORM\JoinColumn(name="price_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotelprice;

    /**
     * @ORM\ManyToOne(targetEntity="Hotelsupplement", inversedBy="Pricesupplement", cascade={"persist"})
     * @ORM\JoinColumn(name="hotsupp_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotelsupplement;

    public function __construct()
    {
        $this->pers = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Pricesupplement
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set pers
     *
     * @param boolean $pers
     * @return Pricesupplement
     */
    public function setPers($pers)
    {
        $this->pers = $pers;

        return $this;
    }

    /**
     * Get pers
     *
     * @return boolean
     */
    public function getPers()
    {
        return $this->pers;
    }

    /**
     * Set hotelprice
     *
     * @param \Btob\HotelBundle\Entity\Hotelprice $hotelprice
     * @return Pricesupplement
     */
    public function setHotelprice(\Btob\HotelBundle\Entity\Hotelprice $hotelprice = null)
    {
        $this->hotelprice = $hotelprice;

        return $this;
    }

    /**
     * Get hotelprice
     *
     * @return \Btob\HotelBundle\Entity\Hotelprice
     */
    public function getHotelprice()
    {
        return $this->hotelprice;
    }

    /**
     * Set hotelsupplement
     *
     * @param \Btob\HotelBundle\Entity\Hotelsupplement $hotelsupplement
     * @return Pricesupplement
     */
    public function setHotelsupplement(\Btob\HotelBundle\Entity\Hotelsupplement $hotelsupplement = null)
    {
        $this->hotelsupplement = $hotelsupplement;

        return $this;
    }

    /**
     * Get hotelsupplement
     *
     * @return \Btob\HotelBundle\Entity\Hotelsupplement
     */
    public function getHotelsupplement()
    {
        return $this->hotelsupplement;
    }
    
    
    
    /**
     * Set marge
     *
     * @param float $marge
     * @return Pricesupplement
     */
    public function setMarge($marge)
    {
        $this->marge = $marge;

        return $this;
    }

    /**
     * Get marge
     *
     * @return float
     */
    public function getMarge()
    {
        return $this->marge;
    }
    
    
    /**
     * Set persm
     *
     * @param boolean $persm
     * @return Pricesupplement
     */
    public function setPersm($persm)
    {
        $this->persm = $persm;

        return $this;
    }

    /**
     * Get persm
     *
     * @return boolean
     */
    public function getPersm()
    {
        return $this->persm;
    }

}
