<?php

namespace User\UserBundle\Form\Type;

use Btob\HotelBundle\Common\Tools;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilder;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $entity = $builder->getData();
        $builder
            ->add('name', 'text', array('label' => "Nom de l'agence"))
            //->add('pname', 'text', array('label' => "Prénom"))
            ->add('email', 'email', array('label' => "Email", 'translation_domain' => 'FOSUserBundle'))
            ->add('username', null, array('label' => "Login", 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => "Mot de passe",
                    'attr' => array('class' => 'form-control')),
                'second_options' => array('label' => 'Retaper le mot de passe',
                    'attr' => array('class' => 'form-control')),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('enabled', null, array('label' => "Active?", 'required' => false))
            ->add('roles', 'choice', array('label' => "Type d'utilisateur",
                    'choices' => array('CONTRACTINGID' => 'CONTRACTING', 'SALES' => 'SALES',
                        'ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN',
                    ), 'required' => true, 'multiple' => true,
                )
            );
        $builder
            ->add('devise', 'entity', array(
                'class' => 'BtobDashBundle:Devise',
                'empty_value' => 'Choisissez une devise',
                'required' => false,
                'label' => "Devise",
            ))
            ->add('marcher', 'entity', array(
                'class' => 'BtobHotelBundle:Marcher',
                'empty_value' => 'Choisissez un marché',
                'required' => false,
                'label' => "Marché",
            ))
            ->add('responsable', 'text', array('label' => "Responsable", "required" => false))
            ->add('adresse', 'text', array('label' => "Adresse", "required" => false))
            ->add('cp', 'text', array('label' => "Code postal", "required" => false))
            ->add('tel', 'text', array('label' => "Téléphone", "required" => false))
            ->add('fax', 'text', array('label' => "Fax", "required" => false))
            ->add('matriculefiscal', 'text', array('label' => "Matricule Fiscal", "required" => false))
            ->add('observation', 'text', array('label' => "Observation", "required" => false))
            //->add('logo', 'text', array('label' => "logo", "required" => false))
            ->add('marge', 'text', array('label' => "Marge Hotel", "required" => false))
            ->add('sold', 'text', array('label' => "Sold de l'agence", "required" => true))
            ->add('prst', null, array('label' => "Pourcentage marge Hôtel", "required" => false))
            ->add('illimite', null, array('label' => "Sold illimité", "required" => false))
            ->add('margeomra', 'text', array('label' => "Marge Omra", "required" => false))
            ->add('actomra', null, array('label' => "Activication Omra", "required" => false))
            ->add('margecircuit', 'text', array('label' => "Marge Circuit", "required" => false))
            ->add('actcircuit', null, array('label' => "Activication Circuit", "required" => false))
            ->add('margesejour', 'text', array('label' => "Marge Sejour", "required" => false))
            ->add('actsejour', null, array('label' => "Activication Sejour", "required" => false))
                
                
            ->add('margeevenement', 'text', array('label' => "Marge Evenement", "required" => false))
            ->add('actevenement', null, array('label' => "Activication Eevenement", "required" => false))
              
                
            ->add('margevole', 'text', array('label' => "Marge Vole", "required" => false))
            ->add('actvole', null, array('label' => "Activication Vole", "required" => false))
            ->add('margecroissiere', 'text', array('label' => "Marge Croissiére", "required" => false))
            ->add('actcroissiere', null, array('label' => "Activication Croissiére", "required" => false))
            ->add('margevoiture', 'text', array('label' => "Marge Voiture", "required" => false))
            ->add('actvoiture', null, array('label' => "Activication Voiture", "required" => false))
            ->add('acttransfert', null, array('label' => "Activication Transfert", "required" => false))
            ->add('actbienetre', null, array('label' => "Activication Bien Etre", "required" => false))
            ->add('actseminaire', null, array('label' => "Activication Séminaire", "required" => false))
            ->add('actgrandjeu', null, array('label' => "Activication Le Grand Jeu", "required" => false))
            ->add('actactivite', null, array('label' => "Activication Activites", "required" => false));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'User\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_userbundle_registration';
    }

}
