<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletter_has_email
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\Newsletter_has_emailRepository")
 */
class Newsletter_has_email
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    

    /**
     * @ORM\ManyToOne(targetEntity="Newsletter", inversedBy="newsletter_has_email")
     * @ORM\JoinColumn(name="newsletter_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $newsletter;

    /**
     * @ORM\ManyToOne(targetEntity="Email", inversedBy="newsletter_has_email")
     * @ORM\JoinColumn(name="email_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $email;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    
    /**
     * Set newsletter
     *
     * @param \Btob\HotelBundle\Entity\Newsletter $newsletter
     * @return Newsletter_has_email
     */
    public function setNewsletter(\Btob\HotelBundle\Entity\Newsletter $newsletter = null)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return \Btob\HotelBundle\Entity\Newsletter 
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

   
    /**
     * Set email
     *
     * @param \Btob\HotelBundle\Entity\Email $email
     * @return Newsletter_has_email
     */
    public function setEmail(\Btob\HotelBundle\Entity\Email $email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return \Btob\HotelBundle\Entity\Email 
     */
    public function getEmail()
    {
        return $this->email;
    }

}
