<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotelroomsup
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelroomsupRepository")
 */
class Hotelroomsup {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="hotelroomsup")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;

    /**
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="hotelroomsup")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $room;

    /**
     * @ORM\ManyToOne(targetEntity="Supplement", inversedBy="hotelroomsup")
     * @ORM\JoinColumn(name="sup_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $supplement;

    public function __construct() {
        $this->act = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Hotelrommsup
     */
    public function setAct($act) {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct() {
        return $this->act;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Hotelrommsup
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null) {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel() {
        return $this->hotel;
    }

    /**
     * Set room
     *
     * @param \Btob\HotelBundle\Entity\Room $room
     * @return Hotelrommsup
     */
    public function setRoom(\Btob\HotelBundle\Entity\Room $room = null) {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \Btob\HotelBundle\Entity\Room 
     */
    public function getRoom() {
        return $this->room;
    }

    /**
     * Set supplement
     *
     * @param \Btob\HotelBundle\Entity\Supplement $supplement
     * @return Hotelrommsup
     */
    public function setSupplement(\Btob\HotelBundle\Entity\Supplement $supplement = null) {
        $this->supplement = $supplement;

        return $this;
    }

    /**
     * Get supplement
     *
     * @return \Btob\HotelBundle\Entity\Supplement 
     */
    public function getSupplement() {
        return $this->supplement;
    }

}
