<?php

namespace Btob\VoitureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgv
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoitureBundle\Entity\ImgvRepository")
 */
class Imgv
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="voiture", inversedBy="imgv")
     * @ORM\JoinColumn(name="voiture_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $voiture;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;



    public  function  __construct(){
        $this->dcr = new \DateTime();

    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgv
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgv
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;
    
        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set voiture
     *
     * @param \Btob\VoitureBundle\Entity\voiture $voiture
     * @return Imgv
     */
    public function setVoiture(\Btob\VoitureBundle\Entity\voiture $voiture = null)
    {
        $this->voiture = $voiture;
    
        return $this;
    }

    /**
     * Get voiture
     *
     * @return \Btob\VoitureBundle\Entity\voiture 
     */
    public function getVoiture()
    {
        return $this->voiture;
    }
}
