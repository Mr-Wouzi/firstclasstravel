<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Baseroom
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\BaseroomRepository")
 */
class Baseroom
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;
    /**
     * @var integer
     *
     * @ORM\Column(name="ad", type="integer" , nullable=true)
     */
    private $ad;
    /**
     * @var integer
     *
     * @ORM\Column(name="enf", type="integer" , nullable=true)
     */
    private $enf;


    /**
     * @ORM\ManyToMany(targetEntity="Hotel", mappedBy="baseroom")
     */
    private $hotel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hotel = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Baseroom
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ad
     *
     * @param integer $ad
     * @return Baseroom
     */
    public function setAd($ad)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return integer 
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set enf
     *
     * @param integer $enf
     * @return Baseroom
     */
    public function setEnf($enf)
    {
        $this->enf = $enf;

        return $this;
    }

    /**
     * Get enf
     *
     * @return integer 
     */
    public function getEnf()
    {
        return $this->enf;
    }

    /**
     * Add hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Baseroom
     */
    public function addHotel(\Btob\HotelBundle\Entity\Hotel $hotel)
    {
        $this->hotel[] = $hotel;

        return $this;
    }

    /**
     * Remove hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     */
    public function removeHotel(\Btob\HotelBundle\Entity\Hotel $hotel)
    {
        $this->hotel->removeElement($hotel);
    }

    /**
     * Get hotel
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotel()
    {
        return $this->hotel;
    }
    public function __toString(){
        return $this->name;
    }
}
