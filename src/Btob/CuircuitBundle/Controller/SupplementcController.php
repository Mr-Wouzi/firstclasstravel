<?php

namespace Btob\CuircuitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Supplementc;
use Symfony\Component\HttpFoundation\Request;
use Btob\CuircuitBundle\Form\SupplementcType;
use Symfony\Component\HttpFoundation\JsonResponse;

class SupplementcController extends Controller {

    public function indexAction($cuircuitid) {
        $cuircuit = $this->getDoctrine()
                ->getRepository('BtobCuircuitBundle:Cuircuit')
                ->find($cuircuitid);
        return $this->render('BtobCuircuitBundle:Supplementc:index.html.twig', array('entities' => $cuircuit->getSupplementc(), "cuircuitid" => $cuircuitid));
    }

    public function addAction($cuircuitid) {
        $cuircuit = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuit')->find($cuircuitid);
        $Supplementc = new Supplementc();
        $form = $this->createForm(new SupplementcType(), $Supplementc);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Supplementc->setCuircuit($cuircuit);
                $em->persist($Supplementc);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplementc_homepage', array("cuircuitid" => $cuircuitid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCuircuitBundle:Supplementc:form.html.twig', array('form' => $form->createView(), "cuircuitid" => $cuircuitid));
    }

    public function editAction($id, $cuircuitid) {
        $request = $this->get('request');
        $Supplementc = $this->getDoctrine()
                ->getRepository('BtobCuircuitBundle:Supplementc')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementcType(), $Supplementc);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplementc_homepage', array("cuircuitid" => $cuircuitid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobCuircuitBundle:Supplementc:form.html.twig', array('form' => $form->createView(), 'id' => $id, "cuircuitid" => $cuircuitid)
        );
    }

    public function deleteAction(Supplementc $Supplementc, $cuircuitid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Supplementc) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
        $em->remove($Supplementc);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplementc_homepage', array("cuircuitid" => $cuircuitid)));
    }

}
