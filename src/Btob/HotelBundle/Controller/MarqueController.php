<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Reservation;
use Btob\HotelBundle\Entity\Reservationdetail;
use Btob\HotelBundle\Entity\Notification;

class MarqueController extends Controller
{
    public function validationAction()
    {
        $user=$this->get('security.context')->getToken()->getUser();
        $marge=$user->getUsermarge();
        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        $caddy=array();
        if(!$session->has('caddy')){
            $session->set('caddy',$caddy);
        }else{
            $caddy=$session->get('caddy');
        }
        $recap = $session->get('recap');
        $client = $session->get('client');
        $supp = $session->get('supp');
        $event = $session->get('event');
        $children = $recap['children'];
        $idclient = $client->getId();
        $client = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->find($idclient);
        $hotelid = $recap["hotelid"];
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $nameadulte = $session->get('nameadulte');
        $nbageenfant = $session->get('nbageenfant');
        $namechildreen = $session->get('namechildreen');
        $d1 = new \DateTime($recap['dated']);
        $d2 = new \DateTime($recap['datef']);
        // get variante de marge
        $margeval = 0;
        $margepers = 1;
        foreach ($marge as $valm) {
            foreach($valm->getHotelmarge() as $valhm){
                if($d1->format('Ymd')>=$valm->getDated()->format('Ymd')
                    && $d1->format('Ymd')<=$valm->getDatef()->format('Ymd')
                    && $valhm->getHotel()->getId()==$hotel->getId()){
                    $margeval = $valhm->getMarge();
                    $margepers = $valhm->getPrst();
                }
            }

        }

        $nbjour = $d2->diff($d1);
        $nbjour = $nbjour->days;
        $reservation = new Reservation();
        $reservation->setClient($client);
        $reservation->setHotel($hotel);
        $reservation->setUser($this->get('security.context')->getToken()->getUser());
        $reservation->setRemarque(""); // a terminer ajouter le champs dans le formulaires
        $reservation->setDated($d1);
        $reservation->setDatef($d2);
        if ($session->get('surdemande')) {
            $reservation->setEtat(5);
        } else {
            $reservation->setEtat(1);
        }

        $reservation->setNbnuit($nbjour);
        $reservation->setNamead(json_encode($nameadulte));
        $reservation->setAgeenfant(json_encode($nbageenfant));
        $reservation->setNameenf(json_encode($namechildreen));

        // calcule et génération de tablea
        // Tools::dump($recap,true);
        $ArrFinal = array();
        $total = 0;
        foreach ($recap["adulte"] as $key => $value) {
            $dbroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);
            $typepersonne = $dbroom->getPersonne();
            $room = $dbroom->getRoom();
            foreach ($value as $k1 => $val1) {
                foreach ($val1 as $k2 => $val2) {
                    $ArrFinal[$room->getId()][$k1]['roomname'] = $room->getName();
                    $ArrFinal[$room->getId()][$k1]['adulte'] = $val2;
                    $ArrFinal[$room->getId()][$k1]['enfant'] = $recap['enfant'][$key][$k1][$k2];
                    $dbarr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->find($recap['arrangement'][$key][$k1][$k2]);
                    $ArrFinal[$room->getId()][$k1]['arrangement'] = $dbarr->getHotelarrangement()->getArrangement()->getName();
                    $dtest = null;
                    $dtest = $d1;
                    $tabchild = array();
                    if ($k2 == 0) $kx = 1; else $kx = $k2;
                    $tabsupp = null;
                    // supplement
                    $ArrFinal[$room->getId()][$k1]['supplement'] = array();
                    if (isset($supp[$key][$kx])) {
                        //Tools::dump($supp,true);
                        $tabsupp = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->find($supp[$key][$kx][0]);
                        $ArrFinal[$room->getId()][$k1]['supplement'][] = $tabsupp->getHotelsupplement()->getSupplement()->getName();

                    }
                    // enfant
                    if (isset($children[$key][$k1])) {
                        $tabchild = $children[$key][$k1];
                    }

                    $dbev = array();
                    $ArrFinal[$room->getId()][$k1]['events'] = array();
                    if (isset($event[$key][$k1])) {
                        foreach ($event[$key][$k1] as $kevv => $valevv) {
                            $xx = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->find($valevv);
                            $ArrFinal[$room->getId()][$k1]['events'] = $xx->getName();
                            $dbev[] = $xx;
                        }
                    }

                    //event
                    $ArrFinal[$room->getId()][$k1]['price'] = $this->getDoctrine()
                        ->getRepository('BtobHotelBundle:Hotelprice')
                        ->calcultate($user,$dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne, $margeval, $margepers, $tabsupp, $dbev);
                    $total += $ArrFinal[$room->getId()][$k1]['price'];
                }
            }
        }
        $reservation->setTotalint($total);
        $total2 = Tools::calculemarge($total, $margepers, $margeval); // calcule de marge
        $reservation->setTotal($total2);

        //$em->persist($reservation);
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findBy(array('enabled' => 1));
        foreach ($users as $kuser => $valuser) {
            if (in_array('ROLE_SUPER_ADMIN', $valuser->getRoles()) || in_array('SALES', $valuser->getRoles())) {
                $notif = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findAll();
                $tst = false;
                $not = null;
                foreach ($notif as $value) {
                    if ($valuser->getId() == $value->getUser()->getId()) {
                        $not = $value;
                        $tst = true;
                    }
                }
                if ($tst) {
                    $note = $not->getNotif() + 1;
                    $not->setNotif($note);
                } else {
                    $not = new Notification();
                    $not->setNotif(1);
                    $not->setUser($valuser);
                }
                //$caddy['notif'][]=$not;
                $em->persist($not);
                $em->flush();
            }
        }
        //$em->flush();

        // génération de tableau pour l'enregistrer dans la bdd
        foreach ($ArrFinal as $kx => $vx) {
            foreach ($vx as $key => $value) {
                //Tools::dump($vx,true);
                $detail = new Reservationdetail();
                $detail->setReservation($reservation);
                $detail->setPrice(Tools::calculemarge($value['price'], $margepers, $margeval));
                $detail->setRoomname($value['roomname']);
                $detail->setAd($value['adulte']);
                $detail->setEnf($value['enfant']);
                $detail->setArrangement($value['arrangement']);
                $html = "";
                foreach ($value["supplement"] as $ks => $vs) {
                    $html .= $vs . "<br />";
                }
                $detail->setSupp($html);
                $html = "";
                //Tools::dump($vx[$key]["events"], true);

                if (is_array($vx[$key]["events"])) {
                    if (isset($vx[$key]["events"])) {
                        foreach ($vx[$key]["events"] as $kev => $vev) {
                            $html .= $vev . "<br />";
                        }
                    }
                } else {
                    $html .= $vx[$key]["events"] . "<br />";
                }

                $detail->setEvent($html);
                //$reservation->addReservationdetail($detail);
                $em->persist($detail);
                $em->flush();
            }
        }
        //$caddy['resahotel'][]=$reservation;
        //$session->set('caddy',$caddy);
        $reservation->setRecap($html);


        return $this->render('BtobHotelBundle:Marque:validation.html.twig', array(
                "hotelid" => $hotelid,
                "dated" => $d1,
                "datef" => $d2,
                'entities' => $hotel,
                'recap' => $recap,
                'reservation' => $reservation
            )
        );
    }

    public function inscriptionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        $recap = $session->get('recap');
        $hotelid = $recap["hotelid"];
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $d1 = new \DateTime($recap['dated']);
        $d2 = new \DateTime($recap['datef']);
        $dataimg = $hotel->getHotelimg();
        $img = "/back/img/dummy_150x150.gif";
        $j = 0;
        foreach ($dataimg as $keyimg => $valimg) {
            if ($j == 0)
                $img = $valimg->getFile();
            if ($valimg->getPriori())
                $img = $valimg->getFile();
            ++$j;
        }
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);

            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $session->set('client', $client);
                $session->set('nameadulte', $request->request->get('childnamead'));
                $session->set('nbageenfant', $request->request->get('nbageenfant'));
                $session->set('namechildreen', $request->request->get('childnameenf'));
                $session->set('options', $request->request->get('options'));
                return $this->redirect($this->generateUrl('btob_marque_validation_reservation'));
                // redirection
            } else {
                echo $form->getErrors();
            }
        }
        // traitement pour l'affichage de formulaire de rooming list
        $tabform = array();
        foreach ($recap['adulte'] as $key => $value) {
            foreach ($value as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    $tabform[$key][$k2]["adulte"] = $v2;
                    $tabform[$key][$k2]["enfant"] = $recap['enfant'][$key][$k1][$k2];
                }
            }
        }


        //Tools::dump($recap,true);
        return $this->render('BtobHotelBundle:Marque:inscription.html.twig', array(
                "hotelid" => $hotelid,
                "dated" => $d1,
                "datef" => $d2,
                'entities' => $hotel,
                'img' => $img,
                'recap' => $recap,
                'form' => $form->createView(),
                'tabform' => $tabform,
                // affichage de recap de l'action recap
                'ArrFinal' => $session->get('ArrFinal'),
                'surdemande' => $session->get('surdemande'),
                'msgsurdemande' => $session->get('msgsurdemande')
            )
        );
    }

    public function recapAction()
    {
        $marge=$this->get('security.context')->getToken()->getUser()->getUsermarge();
        $request = $this->get('request');
        $request = $request->request;
        $session = $this->getRequest()->getSession();
        $d1 = new \DateTime($request->get('dated'));
        $d2 = new \DateTime($request->get('datef'));
        $hotelid = $request->get('hotelid');
        $user = $this->get('security.context')->getToken()->getUser();
        $hotelPrice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $user);
        $hotelPrice0=$hotelPrice[0];
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $surdemande = false;
        $msgsurdemande = "";
        // test stopsales
        $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($hotel, $d1, $user);
        if (count($stopsales) > 0) {
            $surdemande = true;
            $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";
        }

        $margeval = 0;
        $margepers = 1;
        foreach ($marge as $valm) {
            foreach($valm->getHotelmarge() as $valhm){
                if($d1->format('Ymd')>=$valm->getDated()->format('Ymd')
                    && $d1->format('Ymd')<=$valm->getDatef()->format('Ymd')
                    && $valhm->getHotel()->getId()==$hotel->getId()){
                    $margeval = $valhm->getMarge();
                    $margepers = $valhm->getPrst();
                }
            }

        }
        $price = $hotelPrice[0];
        $dataimg = $price->getHotel()->getHotelimg();
        $img = "/back/img/dummy_150x150.gif";
        $j = 0;
        foreach ($dataimg as $keyimg => $valimg) {
            if ($j == 0)
                $img = $valimg->getFile();
            if ($valimg->getPriori())
                $img = $valimg->getFile();
            ++$j;
        }
        $nbjour = $d2->diff($d1);
        $nbjour = $nbjour->days;
        $adulte = $request->get("adulte");
        $enfant = $request->get("enfant");
        $children = $request->get("children");
        $arrangement = $request->get("arrangement");
        $supp = $request->get("supp");
        $event = $request->get("event");
        // set all recap to session
        $recap["enfant"] = $enfant;
        $recap["adulte"] = $adulte;
        $recap["dated"] = $request->get('dated');
        $recap["datef"] = $request->get('datef');
        $recap["hotelid"] = $hotelid;
        //Tools::dump($request,true);
        $recap["children"] = $children;
        // Tools::dump($children,true);


        // exit;
        $recap["arrangement"] = $arrangement;
        $recap["supp"] = $supp;
        $recap["event"] = $event;

        $session->set('recap', $recap);
        $session->set('supp', $supp);
        $session->set('event', $event);
        $session->set('event', $event);
        // end set recap
        // test sur demande pour le dépassement de minstay général

        foreach ($hotelPrice as $vv) {
            if ($vv->getNbnuit() > $nbjour) {
                $surdemande = true;
                $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Nombre de Nuit&eacute;es minimum est à " . $vv->getNbnuit() . " nuit&eacute;es ";
            }
        }
        // test de l'existance de toute la période
        if ($this->getDoctrine()->getRepository('BtobHotelBundle:HotelPrice')->Testallperiode($hotel, $d2, $user)) {
            $surdemande = true;
            $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";
        }
        $ArrFinal = array();
        $totalchambre=array();
        foreach ($adulte as $key => $value) {
            $dbroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);
            $typepersonne = $dbroom->getPersonne();
            $room = $dbroom->getRoom();
            foreach ($value as $k1 => $val1) {
                foreach ($val1 as $k2 => $val2) {
                    $ArrFinal[$room->getId()][$k1]['roomname'] = $room->getName();
                    $ArrFinal[$room->getId()][$k1]['adulte'] = $val2;
                    $ArrFinal[$room->getId()][$k1]['enfant'] = $enfant[$key][$k1][$k2];

                    $dbarr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->find($arrangement[$key][$k1][$k2]);

                    // calcule de sur demande de ministay

                    if ($dbarr->getMinstay() > $nbjour) {
                        $surdemande = true;
                        $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Nombre de Nuit&eacute;es minimum est à " . $dbarr->getMinstay() . " nuit&eacute;es ";

                    }
                    $ArrFinal[$room->getId()][$k1]['arrangement'] = $dbarr->getHotelarrangement()->getArrangement()->getName();
                    $dtest = null;
                    $dtest = $d1;
                    $tabchild = array();
                    if ($k2 == 0) $kx = 1; else $kx = $k2;
                    $tabsupp = null;
                    // supplement

                    if (isset($supp[$key][$k1])) {
                        $tabsupp = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->find($supp[$key][$k1][0]);
                        $ArrFinal[$room->getId()][$k1]['arrangement'] .= "<br>- " . $tabsupp->getHotelsupplement()->getSupplement()->getName();
                    }
                    $priceroom = $price->getPriceroom();
                    foreach ($priceroom as $kpr => $vpr) {
                        if ($room->getId() == $vpr->getRoom()->getId()) {
                            if ($vpr->getQte() == 0) {
                                $surdemande = true;
                                $msgsurdemande = "Cet hôtel est disponible sur demande.<br>La chambre " . $room->getName() . " n'est pas disponible.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";
                            }
                            if(!isset($totalchambre[$room->getName()]["qte"])){
                                $totalchambre[$room->getName()]["qte"]=1;

                            }else{
                                $totalchambre[$room->getName()]["qte"]=$totalchambre[$room->getName()]["qte"]+1;
                            }
                            $totalchambre[$room->getName()]["total"]=$vpr->getQte();
                        }
                    }
                    // enfant
                    if (isset($children[$key][$k1])) {
                        $tabchild = $children[$key][$k1];
                    }//echo "$k1-";
                    if (!isset($event[$key][$k1][$k2]) || count($event[$key][$k1][$k2]) == 0) {
                        $testeve = $hotel->getEvents();
                        foreach ($testeve as $vevtest) {
                            if($vevtest->getDated()->format('Ymd') >= $d1->format('Ymd')
                                && $vevtest->getDated()->format('Ymd')<= $d2->format('Ymd')  && $vevtest->getOblig())
                                $event[$key][$k1][$k2] = $vevtest->getId();
                        }

                        $session->set('event', $event);
                    }
                    $dbev = array();
                    if (isset($event[$key][$k1])) {
                        foreach ($event[$key][$k1] as $kevv => $valevv) {
                            $xevent = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->find($valevv);
                            $dbev[] = $xevent;
                            $ArrFinal[$room->getId()][$k1]['arrangement'] .= "<br>- " . $xevent->getName();
                        }
                    }


                    $ArrFinal[$room->getId()][$k1]['price'] = $this->getDoctrine()
                        ->getRepository('BtobHotelBundle:Hotelprice')
                        ->calcultate($user,$dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne, $margeval, $margepers, $tabsupp, $dbev);
                    $ArrFinal[$room->getId()][$k1]['price'] = Tools::calculemarge($ArrFinal[$room->getId()][$k1]['price'], $margepers, $margeval); // calcule de marge
                    // stop sales tarif =0
                    if ($ArrFinal[$room->getId()][$k1]['price'] == 0) {
                        $surdemande = true;
                        $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";
                    }
                }
            }
        }
        foreach($totalchambre as $key=>$value){
            if($value["total"]<$value["qte"]){
                $surdemande = true;
                $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";
            }
        }
        //Tools::dump($totalchambre,true);

        $session->set('ArrFinal', $ArrFinal);
        $session->set('surdemande', $surdemande);
        $session->set('msgsurdemande', $msgsurdemande);
        return $this->redirect($this->generateUrl('btob_marque_clientform_homepage'));
        /*return $this->render('BtobHotelBundle:Marque:recap.html.twig', array(
                "hotelid" => $hotelid,
                "dated" => $d1,
                "datef" => $d2,
                'price' => $hotelPrice,
                'nbjour' => $nbjour,
                'entities' => $ArrFinal,
                'img' => $img,
                'entities' => $ArrFinal,
                'surdemande' => $surdemande,
                'msgsurdemande' => $msgsurdemande
            )
        );*/
    }

    public function selectionAction($hotelid, $dated, $datef)
    {
        $d1 = new \DateTime($dated);
        $d2 = new \DateTime($datef);
        $marge=$this->get('security.context')->getToken()->getUser()->getUsermarge();
        /*$marge = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->getMargeByUser(,$d1);*/
        $margeval=0;
        $margepers=1;
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        //$hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $nbjour = $d2->diff($d1);
        $nbjour = $nbjour->days - 1;
        $ad = $session->get("ad");
        $enf = $session->get("enf");
        $arr = $session->get("arr");
        $user = $this->get('security.context')->getToken()->getUser();
        $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $user);
        $i = 0;
        $tabactualroom = array();
        foreach ($price as $key => $value) {
            // calcule variante de marge
            foreach ($marge as $valm) {
                foreach($valm->getHotelmarge() as $valhm){
                    if($d1->format('Ymd')>=$valm->getDated()->format('Ymd')
                        && $d1->format('Ymd')<=$valm->getDatef()->format('Ymd')
                        && $valhm->getHotel()->getId()==$value->getHotel()->getId()){
                        $margeval = $valhm->getMarge();
                        $margepers = $valhm->getPrst();
                    }
                }

            }
            //$filterhotel[] = $value->getHotel();
            $ArrFinal["hotel"] = $value->getHotel();
            $ArrFinal["hotelname"] = trim($value->getHotel()->getName());
            $ArrFinal["star"] = $value->getHotel()->getStar();
            $ArrFinal["pays"] = $value->getHotel()->getPays()->getId();
            $ArrFinal["ville"] = $value->getHotel()->getVille()->getId();
            // gestion des images
            $dataimg = $value->getHotel()->getHotelimg();
            $img = "/back/img/dummy_150x150.gif";
            $j = 0;
            foreach ($dataimg as $keyimg => $valimg) {
                if ($j == 0)
                    $img = $valimg->getFile();
                if ($valimg->getPriori())
                    $img = $valimg->getFile();
                ++$j;
            }
            $ArrFinal["image"] = $img;
            /*
             * gestion des prix
             */
            // minimaume d'arrangement
            $min = 0;
            $j = 0;
            $tab = array();
            $name = "";
            $pers = false;
            $pricearr = array();
            foreach ($value->getPricearr() as $keyarr => $valarr) {
                if ($j == 0) {
                    $min = $valarr->getPrice();
                    $pers = $valarr->getPers();
                    $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                } else {
                    if ($min > $valarr->getPrice()) {
                        $min = $valarr->getPrice();
                        $pers = $valarr->getPers();
                        $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                    }
                }
                $pricearr[$j]["name"] = $valarr->getHotelarrangement()->getArrangement()->getName();
                $pricearr[$j]["price"] = $valarr->getPrice();
                $pricearr[$j]["price"] = Tools::calculemarge($pricearr[$j]["price"], $margepers, $margeval); // calcule de marge
                $pricearr[$j]["id"] = $valarr->getId();
                $pricearr[$j]["idarr"] = $valarr->getHotelarrangement()->getArrangement()->getId();
                ++$j;
            }
            $pricearr = Tools::array_sort($pricearr, 'price', SORT_ASC);
            $ArrFinal["pricearr"] = $pricearr;
            if ($pers) {
                $ArrFinal["price"] = $value->getPrice() + (($value->getPrice() * $min) / 100);
            } else {
                $ArrFinal["price"] = $min + $value->getPrice();
            }
            $ArrFinal["price"] = Tools::calculemarge($ArrFinal["price"], $margepers, $margeval); // calcule de marge
            $ArrFinal["name"] = $name;

            // gestion des chambre
            $k = 0;
            //Tools::dump($ad);
            foreach ($ad as $kad => $valad) {
                $nbpersonne = $valad + $enf[$kad];
                //if ($nbpersonne > 4) $nbpersonne = 4;
                $room = null;
                // test dispo room
                $hotelroom = $value->getHotel()->getHotelroom();
                $typepersonne = false;
                foreach ($hotelroom as $khr => $vhr) {
                    if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                        $room = $vhr->getRoom();
                        $tabactualroom[] = $room->getId();
                        $typepersonne = $vhr->getPersonne();

                    }
                }
                //test dispo arrangement
                $arrang = null;
                $hotelarrangement = $value->getHotel()->getHotelarrangement();

                foreach ($hotelarrangement as $karr => $varr) {
                    if ($varr->getArrangement()->getId() == $arr[$kad]) {
                        $arrang = $varr->getArrangement();
                    }
                }
                $taboneitem = array();
                if ($room != null && $arrang != null) {
                    $taboneitem = array(
                        "nbpersonne" => $nbpersonne,
                        "roomname" => $room->getName(),
                        "arrangement" => $arrang->getName(),
                        "ad" => $valad,
                        "enf" => $enf[$kad],
                        "arr" => $arr[$kad]
                    );

                    foreach ($hotelroom as $khr => $vhr) {
                        if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                            $taboneitem["id"] = $vhr->getId();
                            $taboneitem["nbad"] = $vhr->getAd();
                            $taboneitem["nbenf"] = $vhr->getEnf();
                            $taboneitem["typepersonne"] = $vhr->getPersonne();
                        }
                    }
                    // get supplement

                    $suppl = $value->getHotel()->getHotelroomsup();
                    $supplement = $value->getPricesupplement();

                    foreach ($suppl as $ksup => $vsup) {
                        if ($vsup->getRoom()->getId() == $room->getId()) {
                            $xtab = array();// ce tableau pour le croisement de price sup et hotel sup
                            foreach ($supplement as $valsupplement) {
                                if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {
                                    $xtab[0] = $valsupplement->getPrice();
                                    if ($valsupplement->getPers()) {
                                        $xtab[0] = ($value->getPrice() * $valsupplement->getPrice()) / 100;
                                        $xtab[0] = Tools::calculemarge($xtab[0], $margepers, $margeval); // calcule de marge
                                    }
                                    $xtab[1] = $valsupplement->getId();
                                }
                            }
                            $taboneitem["supplement"][] = array($vsup, $xtab);
                        } else {
                            $taboneitem["supplement"][] = array();
                        }
                    }
                    // get events
                    $events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);
                    $tabev = array();
                    foreach ($events as $kev => $valev) {
                        $tabev[] = $valev;
                    }
                    $taboneitem['events'] = $tabev;
                    $ArrFinal["allprice"][$room->getId()][] = $taboneitem;
                    ++$k;

                }
                // affichage des autres chambres

                foreach ($value->getHotel()->getHotelroom() as $vrestroom) {
                    if (!in_array($vrestroom->getRoom()->getId(), $tabactualroom)) {
                        $roomother = $vrestroom->getRoom();
                        $nbpersonne = $roomother->getCapacity();
                        if ($roomother != null && $arrang != null) {
                            $taboneitem = array(
                                "nbpersonne" => $nbpersonne,
                                "roomname" => $roomother->getName(),
                                "arrangement" => $arrang->getName(),
                                "ad" => $valad,
                                "enf" => $enf[$kad],
                                "arr" => $arr[$kad]
                            );

                            foreach ($hotelroom as $khr => $vhr) {
                                if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                                    $taboneitem["id"] = $vhr->getId();
                                    $taboneitem["nbad"] = $vhr->getAd();
                                    $taboneitem["nbenf"] = $vhr->getEnf();
                                }
                            }
                            // get supplement
                            $suppl = $value->getHotel()->getHotelroomsup();
                            $supplement = $value->getPricesupplement();
                            foreach ($suppl as $ksup => $vsup) {
                                if ($vsup->getRoom()->getId() == $roomother->getId()) {
                                    $xtab = 0;// ce tableau pour le croisement de price sup et hotel sup
                                    foreach ($supplement as $valsupplement) {
                                        if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {
                                            $xtab = $valsupplement->getPrice();
                                            if ($valsupplement->getPers()) {
                                                $xtab = ($value->getPrice() * $valsupplement->getPrice()) / 100;
                                            }
                                            $xtab = Tools::calculemarge($xtab, $margepers, $margeval); // calcule de marge
                                        }
                                    }
                                    $taboneitem["supplement"][] = array($vsup, $xtab);
                                }
                            }
                            // get events
                            $events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);
                            $tabev = array();
                            foreach ($events as $kev => $valev) {
                                $tabev[] = $valev;
                            }
                            $taboneitem['events'] = $tabev;
                            $ArrFinal["allotherprice"][$roomother->getId()][] = $taboneitem;
                            ++$k;

                        }
                    }
                }
                //Tools::dump($tabactualroom,true);
            }
            // Stop Sales
            $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);
            $ArrFinal["stopsales"] = count($stopsales);
            //Tools::dump($ArrFinal,true);
        }
        return $this->render('BtobHotelBundle:Marque:selection.html.twig', array(
                "hotelid" => $hotelid,
                "dated" => $d1,
                "datef" => $d2,
                'price' => $ArrFinal,
                'nbjour' => $nbjour,
                'ad' => $ad,
                'enf' => $enf,
                'arr' => $arr,
                'margeval' => $margeval,
                'margepers' => $margepers
            )
        );
    }

    public function indexAction()
    {
        $marge=$this->get('security.context')->getToken()->getUser()->getUsermarge();
        $margeval = 0;
        $margepers = 1;
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        /*
         *  donner de formulaire
         */
        // list des arrangement
        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findActiv();
        // list des pays
        $pays = $this->getDoctrine()->getRepository('BtobHotelBundle:Pays')->findActiv();

        // valeur de submit
        $nbpage = 5;
        $nbjour = 1;
        $paysid = "";
        $ville = "";
        $nom = "";
        $trie = 1;
        $price_min = 0;
        $price_max = 3000;
        $star = 5;
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
        $dt->modify('+1 day');
        $datef = $dt->format("d/m/Y");
        $page = 1;
        $ArrFinal = array();
        $filterhotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findActiv();
        $totpage = 1;
        $ad = array(0 => 2);
        $enf = array(0 => 0);
        $arr = array(0 => 3);

        if ($request->getMethod() == 'POST') {
            //Tools::dump($request->request);
            $paysid = $request->request->get("pays");
            $session->set('paysid', $paysid);
            $nom = $request->request->get("nom");
            $session->set('nom', $nom);
            $ville = $request->request->get("ville");
            $session->set('ville', $ville);
            $trie = $request->request->get("trie");
            $session->set('trie', $trie);
            $price_min = $request->request->get("price_min");
            $session->set('price_min', $price_min);
            $price_max = $request->request->get("price_max");
            $session->set('price_max', $price_max);
            $star = $request->request->get("star");
            $session->set('star', $star);
            $dated = $request->request->get("dated");
            $session->set('dated', $dated);
            $datef = $request->request->get("datef");
            $session->set('datef', $datef);
            $page = $request->request->get("page");
            $session->set('page', $page);
            $ad = $request->request->get("ad");
            $session->set('ad', $ad);
            $enf = $request->request->get("enf");
            $session->set('enf', $enf);
            $arr = $request->request->get("arr");
            $session->set('arr', $arr);
            //Tools::dump($request->request, true);
            // recupération de hotel price
            $tab = explode("/", $dated);
            $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);
            $tab2 = explode("/", $datef);
            $d2 = new \DateTime($tab2[2] . "-" . $tab2[1] . "-" . $tab2[0]);
            // calculer la deference entre dated et dates
            $nbjour = $d2->diff($d1);
            $nbjour = $nbjour->days;
            $user = $this->get('security.context')->getToken()->getUser();
            $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydate($d1, $user);
            $i = 0;

            foreach ($price as $key => $value) {
                if ($value->getHotel()->getAct()) {
                    $surdemande = false;
                    if ($value->getNbnuit() > $nbjour) {
                        $surdemande = true;
                    }
                    //$filterhotel[] = $value->getHotel();
                    $ArrFinal[$i]["hotel"] = $value->getHotel();
                    $ArrFinal[$i]['hotelpriceid'] = $value->getId();
                    // calcule variante de marge
                    foreach ($marge as $valm) {
                        foreach($valm->getHotelmarge() as $valhm){
                            if($d1->format('Ymd')>=$valm->getDated()->format('Ymd')
                                && $d1->format('Ymd')<=$valm->getDatef()->format('Ymd')
                                && $valhm->getHotel()->getId()==$value->getHotel()->getId()){
                                $margeval = $valhm->getMarge();
                                $margepers = $valhm->getPrst();
                            }
                        }

                    }



                    $ArrFinal[$i]["hotelname"] = trim($value->getHotel()->getName());
                    $ArrFinal[$i]["star"] = $value->getHotel()->getStar();
                    $ArrFinal[$i]["pays"] = $value->getHotel()->getPays()->getId();
                    $ArrFinal[$i]["ville"] = $value->getHotel()->getVille()->getId();
                    // gestion des images
                    $dataimg = $value->getHotel()->getHotelimg();
                    $img = "/back/img/dummy_150x150.gif";
                    $j = 0;
                    foreach ($dataimg as $keyimg => $valimg) {
                        if ($j == 0)
                            $img = $valimg->getFile();
                        if ($valimg->getPriori())
                            $img = $valimg->getFile();
                        ++$j;
                    }
                    $ArrFinal[$i]["image"] = $img;
                    /*
                     * gestion des prix
                     */
                    // minimaume d'arrangement
                    $min = 0;
                    $j = 0;
                    $tab = array();
                    $name = "";
                    $pers = false;
                    foreach ($value->getPricearr() as $keyarr => $valarr) {
                        if ($valarr->getMinstay() > $nbjour) {
                            $surdemande = true;
                        }
                        if ($j == 0) {
                            $min = $valarr->getPrice();
                            $pers = $valarr->getPers();
                            $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                        } else {
                            if ($min > $valarr->getPrice()) {
                                $min = $valarr->getPrice();
                                $pers = $valarr->getPers();
                                $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                            }
                        }
                        ++$j;
                    }
                    if ($pers) {
                        $ArrFinal[$i]["price"] = $value->getPrice() + (($value->getPrice() * $min) / 100);
                    } else {
                        $ArrFinal[$i]["price"] = $min + $value->getPrice();
                    }
                    // calcule de la marge
                    $ArrFinal[$i]["price"] = Tools::calculemarge($ArrFinal[$i]["price"], $margepers, $margeval);
                    $ArrFinal[$i]["name"] = $name;

                    // gestion des chambre
                    $k = 0;
                    //Tools::dump($ad);
                    foreach ($ad as $kad => $valad) {
                        $nbpersonne = $valad + $enf[$kad];
                        //if ($nbpersonne > 4) $nbpersonne = 4;
                        $room = null;
                        // test dispo room
                        $hotelroom = $value->getHotel()->getHotelroom();
                        $typepersonne = false;
                        foreach ($hotelroom as $khr => $vhr) {
                            if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                                $room = $vhr->getRoom();
                                $typepersonne = $vhr->getPersonne();
                            }
                        }
                        //test dispo arrangement
                        $arrang = null;
                        $hotelarrangement = $value->getHotel()->getHotelarrangement();

                        foreach ($hotelarrangement as $karr => $varr) {
                            if ($varr->getArrangement()->getId() == $arr[$kad]) {
                                $arrang = $varr->getArrangement();
                            }
                        }
                        if ($room != null && $arrang != null) {
                            foreach ($hotelroom as $khr => $vhr) {
                                if ($room->getId() == $vhr->getRoom()->getId()) {
                                    $idform = $vhr->getId();
                                }
                            }
                            $ArrFinal[$i]["allprice"][$k]["nbpersonne"] = $nbpersonne;
                            $ArrFinal[$i]["allprice"][$k]["roomname"] = $room->getName();
                            $ArrFinal[$i]["allprice"][$k]["arrangement"] = $arrang->getName();
                            foreach ($value->getPricearr() as $kk => $vv) {
                                if ($vv->getHotelarrangement()->getArrangement()->getId() == $arrang->getId()) {
                                    $ArrFinal[$i]["allprice"][$k]["arrangementid"] = $vv->getId();
                                }
                            }

                            $ArrFinal[$i]["allprice"][$k]["ad"] = $valad;
                            $ArrFinal[$i]["allprice"][$k]["enf"] = $enf[$kad];
                            $ArrFinal[$i]["allprice"][$k]["idform"] = $idform;
                            //$calc=new Calculate();
                            $dtest = null;
                            $dtest = $d1;
                            // initialiser l'age des enfant a 6 s'il existe
                            // il faut les changer s'il y a un autre age d'enfant
                            $tabenf = array();
                            for ($xd = 0; $xd < $enf[$kad]; $xd++) {
                                $tabenf[] = $value->getHotel()->getMaxenfant();
                            }
                            $prix = $this->getDoctrine()
                                ->getRepository('BtobHotelBundle:Hotelprice')
                                ->calcultate($user,$dtest->format('Y-m-d'), $value->getHotel()->getId(), $nbjour, $arrang, $valad, $tabenf, $room, $typepersonne, $margeval, $margepers);
                            $prix = Tools::calculemarge($prix, $margepers, $margeval); // calcule de marge
                            $ArrFinal[$i]["allprice"][$k]["price"] = $prix;
                            if ($prix == 0) {
                                $surdemande = true;
                            }
                            ++$k;
                        }
                    }
                    // Stop Sales
                    $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);
                    $ArrFinal[$i]["stopsales"] = count($stopsales);
                    if ($surdemande) {
                        $ArrFinal[$i]["stopsales"] = 1;
                    }
                    //Tools::dump($request, true);
                    ++$i;
                }
            }


            /*
             * Appliquer les filtre de recheche
             */

            // pas de chambre disponible
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            foreach ($ArrFinal1 as $key => $value) {
                if (isset($value["allprice"])) {
                    $ArrFinal[] = $value;
                }
            }
            // filtre de prix
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            foreach ($ArrFinal1 as $key => $value) {
                if ($value["price"] >= $price_min && $value["price"] <= $price_max) {
                    $ArrFinal[] = $value;
                }
            }
            // filter $star
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            foreach ($ArrFinal1 as $key => $value) {
                if ($value["star"] <= $star) {
                    $ArrFinal[] = $value;
                }
            }
            // filter pays
            if ($paysid > 0) {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["pays"] == $paysid) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            // filter ville
            if ($ville > 0) {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["ville"] == $ville) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            // filter name hotel
            if ($nom != "") {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["hotelname"] == $nom) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            //Filter trie
            if ($trie == 1) {
                $ArrFinal = Tools::array_sort($ArrFinal, "hotelname", SORT_ASC);
            } else if ($trie == 2) {
                $ArrFinal = Tools::array_sort($ArrFinal, "price", SORT_ASC);
            } else if ($trie == 3) {
                $ArrFinal = Tools::array_sort($ArrFinal, "price", SORT_DESC);
            }
            /*
             * pagination
             */
            $totpage = ceil(count($ArrFinal) / $nbpage);
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            $i = 0;
            $start = $nbpage * ($page - 1);
            $end = $nbpage * $page;
            if ($start > count($ArrFinal1)) {
                $start = 0;
                $page = 1;
                $end = $nbpage;
            }
            foreach ($ArrFinal1 as $key => $value) {
                if ($i >= $start && $i < $end) {
                    $ArrFinal[] = $value;
                }
                ++$i;
            }
        }
        $d1 = new \DateTime(Tools::explodedate($dated, '/'));
        $d2 = new \DateTime(Tools::explodedate($datef, '/'));
        return $this->render('BtobHotelBundle:Marque:index.html.twig', array(
                'pays' => $pays,
                'paysid' => $paysid,
                'ville' => $ville,
                'trie' => $trie,
                'price_min' => $price_min,
                'price_max' => $price_max,
                'star' => $star,
                'dated' => $dated,
                'datef' => $datef,
                'nom' => $nom,
                'page' => $page,
                'price' => $ArrFinal,
                'filterhotel' => $filterhotel,
                'totpage' => $totpage,
                'arr' => $arrangement,
                'nbjour' => $nbjour,
                'frmad' => $ad,
                'frmenf' => $enf,
                'frmarr' => $arr,
                'd1' => $d1,
                'd2' => $d2,
                'margeval' => $margeval,
                'margepers' => $margepers,
            )
        );
    }

    public function detailAction($hotelid, $dated, $datef)
    {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $dataimg = $hotel->getHotelimg();
        $img = "/back/img/dummy_150x150.gif";
        $j = 0;
        foreach ($dataimg as $keyimg => $valimg) {
            if ($j == 0)
                $img = $valimg->getFile();
            if ($valimg->getPriori())
                $img = $valimg->getFile();
            ++$j;
        }
        return $this->render('BtobHotelBundle:Marque:detail.html.twig', array(
                "hotelid" => $hotelid,
                "dated" => $dated,
                "datef" => $datef,
                "hotel" => $hotel,
                "img" => $img
            )
        );
    }

}
