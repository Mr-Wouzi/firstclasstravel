<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\VoleBundle\Entity\Vol;
use Btob\HotelBundle\Common\Tools;
class VoleController extends Controller
{
    public function indexAction()
    {
        $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Billetterie'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->find(10);
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $type = $request->request->get('type');
                //    echo 'test'.$type ; exit;
                $depart = $request->request->get('depart');
                $arrive = $request->request->get('arrive');
                $adultjeune = $request->request->get('adult_jeune');
                $adultetud = $request->request->get('adult_etud');
                $adult = $request->request->get('adult');
                $enfant = $request->request->get('enfant');
                $bebe = $request->request->get('bebe');
                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
                $dater = new \Datetime(Tools::explodedate($request->request->get('dater'),'/'));
                $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

                $vol = new Vol();
                $vol->setClient($client);
                $vol->setAgent($User);
                $vol->setType($type);
                $vol->setDepart($depart);
                $vol->setArrive($arrive);
                $vol->setJeunes($adultjeune);
                $vol->setEtudiant($adultetud);
                $vol->setAdulte($adult);
                $vol->setEnfant($enfant);
                $vol->setAgent($User);
                $vol->setBebe($bebe);
                $vol->setDated($dated);
                $vol->setDater($dater);
                $em->persist($vol);
                $em->flush();
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $vol->getClient()->getEmail();
                //$to = "afef.tuninfo@gmail.com";
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = "MENA TOURS: R�servation Vol" ;
                $headers = "From:MENATOURS - R�servation Vol <contact@menatours.com>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message1 .='



				' .'
                      <img id="site-logo" src="http://www.menatours.com/front/images/logo.png" alt="Mena Tours">
                    <br /><br />
				 Cher Madame/Monsieur,<br /><br />
				 Merci pour la confiance, nous notons votre r�servation qui reste en instance de confirmation jusqu�au payement.<br />
				 Moyens de payement soit par :<br />
				- Payement directement � notre agence MENA TOURS.<br />
				 Adresse : 21 Avenue USA - 1er �tage immeuble les Jasmins Belv�d�re - 1002 Tunis - Tunisie.<br />
				- Virement bancaire avec l�envoi d�un justificatif de payement ( virement scann� et envoy� par email au contact@menatours.com ou par fax au (+ 216) 71 844 335).<br />
					
					Un Bon Voucher vous serait remis sit�t payement effectu�.<br /><br />
					<b>Contact</b><br />
<b>Adresse:</b> 21 Avenue USA - 1er �tage immeuble les Jasmins Belv�d�re - 1002 Tunis - Tunisie<br />
<b>Tel:</b> (+216) 71 844 335 / (+216) 31 332 332<br />
<b>Mobile:</b> (+216) 53 605 073 / (+216) 58 558 502<br />
<b>Fax:</b> (+ 216) 71 844 335<br />
<b>E-mail:</b> contact@menatours.com<br />
<b>Website:</b> www.menatours.com



  </body>';


                mail($to, $subject, $message1, $headers);


                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                //$too = "afef.tuninfo@gmail.com";
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "R�servation Vol" ;
                $header = "From:MENATOURS - R�servation Vol <".$to.">\n";
                $header .= "Reply-To:" .$vol->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='

                      <img id="site-logo" src="http://www.menatours.com/front/images/logo.png" alt="Mena Tours">
                    <br /><br />
				 '  . $vol->getAgent()->getName() .',<br>
				 vous avez re�u une r�servation vol  , merci de consulter votre backoffice .

  </body>';


                mail($too, $subjects, $message2, $header);

            } else {
                echo $form->getErrors();
            }
                $request->getSession()->getFlashBag()->add('noticvol', 'Votre message a bien ete envoye. Merci.');
                return $this->redirect($this->generateUrl('front_vole_homepage'));

        }
        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();
        return $this->render('FrontBtobBundle:Vole:index.html.twig', array(
            'form' => $form->createView(),
            'banner' => $banner,
            ));
    }

}
