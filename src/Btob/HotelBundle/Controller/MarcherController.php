<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Marcher;
use Btob\HotelBundle\Form\MarcherType;

class MarcherController extends Controller {

    public function indexAction() {
        $pays = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Marcher')
                ->findAll();
        return $this->render('BtobHotelBundle:Marcher:index.html.twig', array('entities' => $pays));
    }

    public function addAction() {
        $pays = new Marcher();
        $form = $this->createForm(new MarcherType(), $pays);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($pays);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_marcher_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Marcher:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $pays = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Marcher')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new MarcherType(), $pays);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_marcher_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Marcher:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Marcher $marcher) {
        $em = $this->getDoctrine()->getManager();

        if (!$marcher) {
            throw new NotFoundHttpException("Marcher non trouvée");
        }
        $em->remove($marcher);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_marcher_homepage'));
    }

}
