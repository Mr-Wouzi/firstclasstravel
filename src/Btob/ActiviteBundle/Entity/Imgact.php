<?php

namespace Btob\ActiviteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgact
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ActiviteBundle\Entity\ImgactRepository")
 */
class Imgact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="Activite", inversedBy="imgact")
     * @ORM\JoinColumn(name="activite_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $activite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgact
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgact
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set activite
     *
     * @param \Btob\ActiviteBundle\Entity\Activite $activite
     * @return Imgact
     */
    public function setActivite(\Btob\ActiviteBundle\Entity\Activite $activite = null)
    {
        $this->activite = $activite;

        return $this;
    }

    /**
     * Get activite
     *
     * @return \Btob\ActiviteBundle\Entity\Activite 
     */
    public function getActivite()
    {
        return $this->activite;
    }
}
