<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Payement;
use Btob\HotelBundle\Form\PayementType;

class PayementController extends Controller {

    public function indexAction() {
        $payement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Payement')
                ->findAll();
        return $this->render('BtobHotelBundle:Payement:index.html.twig', array('entities' => $payement));
    }



    public function editAction($id) {
        $request = $this->get('request');
        $payement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Payement')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PayementType(), $payement);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_payement_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Payement:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }


}
