<?php

namespace Btob\EvenementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\EvenementBundle\Entity\Evenementprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\EvenementBundle\Form\EvenementpriceType;
use Symfony\Component\HttpFoundation\JsonResponse;



class EvenementpriceController extends Controller
{

    public function indexAction($evenementid)
    {
        $evenement = $this->getDoctrine()
            ->getRepository('BtobEvenementBundle:Evenement')
            ->find($evenementid);
	    $name =  $evenement->getTitre();
        $price = array();
        foreach ($evenement->getEvenementprice() as $value) {
           
                $price[] = $value;
           
        }
        return $this->render('BtobEvenementBundle:Evenementprice:index.html.twig', array(
            "entities" => $price,
            "evenementid" => $evenementid,
	    "name" => $name,
        ));
    }


    public function addAction($evenementid)
    {
        $evenement = $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenement')->find($evenementid);
      
        $allevenementprice = $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenementprice')->findBy(array('evenement' => $evenement));
      
        

        $evenementprice = new Evenementprice();
        $form = $this->createForm(new EvenementpriceType(), $evenementprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $evenementprice->setEvenement($evenement);
                $em->persist($evenementprice);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_evenementprice_homepage', array("evenementid" => $evenementid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobEvenementBundle:Evenementprice:form.html.twig', array(
            'form' => $form->createView(),
            "evenement" => $evenement

        ));
    }



    public function editAction($id, $evenementid)
    {
        $evenement = $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenement')->find($evenementid);
        $request = $this->get('request');
        $evenementprice = $this->getDoctrine()
            ->getRepository('BtobEvenementBundle:Evenementprice')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new EvenementpriceType(), $evenementprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $allevenementprice = $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenementprice')->findBy(array('evenement' => $evenement));
      
    
       
        if ($form->isValid()) {

            $evenementprice->setDmj(new \DateTime());
            $em->flush();


            return $this->redirect($this->generateUrl('btob_evenementprice_homepage', array("evenementid" => $evenementid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobEvenementBundle:Evenementprice:edit.html.twig', array('form' => $form->createView(), 'price' => $evenementprice, 'id' => $id, "evenement" => $evenement)
        );
    }

  
    public function deleteAction(Evenementprice $evenementprice, $evenementid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$evenementprice) {
            throw new NotFoundHttpException("Evenementprice non trouvée");
        }
        $em->remove($evenementprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_evenementprice_homepage', array("evenementid" => $evenementid)));
    }



}












