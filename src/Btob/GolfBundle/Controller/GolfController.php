<?php

namespace Btob\GolfBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Common\Tools;
use Btob\GolfBundle\Entity\Golf;
use Btob\GolfBundle\Entity\Imgegolf;
use Btob\GolfBundle\Form\GolfType;

/**
 * Golf controller.
 *
 */
class GolfController extends Controller
{

    /**
     * Lists all Golf entities.
     *
     */

    public function indexAction()
    {
      //  $entities = $this->getDoctrine()->getRepository("BtobGolfBundle:Golf")->findby(array('act' => 1));
        $entities = $this->getDoctrine()->getRepository("BtobGolfBundle:Golf")->findAll();

        return $this->render('BtobGolfBundle:Golf:index.html.twig', array('entities' => $entities));
    }

    /**
     * Creates a new Golf entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Golf();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $request = $this->get('request');
        if ($form->isValid()) {
            $dated = Tools::explodedate($request->request->get("dated"),'/');
            $datef = Tools::explodedate($request->request->get("datef"),'/');

            $em = $this->getDoctrine()->getManager();
            $entity->setDated(new \DateTime($dated));
            $entity->setDatef(new \DateTime($datef));
            $em->persist($entity);
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgegolf();
                        $img->setGolf($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('golf'));
        }

        return $this->render('BtobGolfBundle:Golf:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Golf entity.
     *
     * @param Golf $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Golf $entity)
    {
        $form = $this->createForm(new GolfType(), $entity, array(
            'action' => $this->generateUrl('golf_create'),
            'method' => 'POST',
        ));

       // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Golf entity.
     *
     */
    public function newAction()
    {
        $entity = new Golf();
        $form = $this->createCreateForm($entity);

        return $this->render('BtobGolfBundle:Golf:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Golf entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobGolfBundle:Golf')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Golf entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobGolfBundle:Golf:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Golf entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobGolfBundle:Golf')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Golf entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobGolfBundle:Golf:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Golf entity.
     *
     * @param Golf $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Golf $entity)
    {
        $form = $this->createForm(new GolfType(), $entity, array(
            'action' => $this->generateUrl('golf_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

      //  $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Golf entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobGolfBundle:Golf')->find($id);
        foreach ($entity->getImgegolf() as $key => $value) {
            $em->remove($value);
            $em->flush();
        }
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Golf entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgegolf();
                        $img->setGolf($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('golf'));
        }

        return $this->render('BtobGolfBundle:Golf:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Golf entity.
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BtobGolfBundle:Golf')->find($id);
        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('Golf'));
    }

    /**
     * Creates a form to delete a Golf entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('golf_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
}
