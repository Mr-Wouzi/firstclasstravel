<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotelamg
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelamgRepository")
 */
class Hotelamg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="hotelamg")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;
    /**
     * @ORM\ManyToOne(targetEntity="Amg", inversedBy="hotelamg")
     * @ORM\JoinColumn(name="amg_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $amg;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Hotelamg
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set amg
     *
     * @param \Btob\HotelBundle\Entity\Amg $amg
     * @return Hotelamg
     */
    public function setAmg(\Btob\HotelBundle\Entity\Amg $amg = null)
    {
        $this->amg = $amg;

        return $this;
    }

    /**
     * Get amg
     *
     * @return \Btob\HotelBundle\Entity\Amg 
     */
    public function getAmg()
    {
        return $this->amg;
    }
}
