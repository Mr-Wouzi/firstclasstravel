<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Modepaiement;
use Btob\HotelBundle\Form\ModepaiementType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ModepaiementController extends Controller {

    public function indexAction() {
        $modepaiement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Modepaiement')
                ->findAll();
        return $this->render('BtobHotelBundle:Modepaiement:index.html.twig', array('modepaiement' => $modepaiement));
    }


    public function addAction() {
        $modepaiement = new Modepaiement();
        $form = $this->createForm(new ModepaiementType(), $modepaiement);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($modepaiement);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_modepaiement_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Modepaiement:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $modepaiement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Modepaiement')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ModepaiementType(), $modepaiement);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_modepaiement_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Modepaiement:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Modepaiement $modepaiement) {
        $em = $this->getDoctrine()->getManager();

        if (!$modepaiement) {
            throw new NotFoundHttpException("Mode non trouvée");
        }
        $em->remove($modepaiement);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_modepaiement_homepage'));
    }
    
    public function activateAction(Modepaiement $modepaiement) {

        $em = $this->getDoctrine()->getManager();
        if (!$modepaiement) {
            throw new NotFoundHttpException("Mode non trouvée");
        }else{
	        if ($modepaiement->getAct()==true) {
	            $modepaiement->setAct(false);
	            $em->flush();
	        }else{
	            $modepaiement->setAct(true);
	            $em->flush();
	        }
        }
        
        
        
        return $this->redirect($this->generateUrl('btob_modepaiement_homepage'));
    }
    

}