<?php

namespace Btob\DashBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        // calcule des résa par interval de date
        $dt=new \DateTime();
        $dtday=$dt->modify('-1 day');
        $dt=new \DateTime();
        $dtweek=$dt->modify('-7 day');
        $dt=new \DateTime();
        $dtmonth=$dt->modify('-1 month');
        $dt=new \DateTime();
        $user = $this->get('security.context')->getToken()->getUser();
        $resaday=$this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->getResaDash($dt,$dtday);
        if (in_array('AGENCEID', $user->getRoles()) || in_array('SALES', $user->getRoles())) {
            $tabx = array();
            foreach ($resaday as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabx[] = $value;
                }
            }
            $resaday = $tabx;
        }
        $totalday=0;
        foreach($resaday as $value){
            $totalday += $value->getTotal();
        }
        $resaweek=$this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->getResaDash($dt,$dtweek);
        if (in_array('AGENCEID', $user->getRoles()) || in_array('SALES', $user->getRoles())) {
            $tabx = array();
            foreach ($resaweek as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabx[] = $value;
                }
            }
            $resaweek = $tabx;
        }
        $totalweek=0;
        foreach($resaweek as $value){
            $totalweek += $value->getTotal();
        }
        $resamonth=$this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->getResaDash($dt,$dtmonth);
        if (in_array('AGENCEID', $user->getRoles()) || in_array('SALES', $user->getRoles())) {
            $tabx = array();
            foreach ($resamonth as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabx[] = $value;
                }
            }
            $resamonth = $tabx;
        }
        $totalmonth=0;
        foreach($resamonth as $value){
            $totalmonth += $value->getTotal();
        }
        return $this->render('BtobDashBundle:Default:index.html.twig', array(
            'totalday' => $totalday,
            'totalweek' => $totalweek,
            'totalmonth' => $totalmonth,
        ));
    }
}
