<?php





namespace Btob\HotelBundle\Entity;





use Doctrine\ORM\EntityRepository;





/**


 * MargeperiodRepository


 *


 * This class was generated by the Doctrine ORM. Add your own custom


 * repository methods below.


 */


class MargeperiodRepository extends EntityRepository


{
    public function ValidateInterval($id, $dated, $dates, $margeid = "")
    {
        $qb = $this->createQueryBuilder('r')
            ->select('COUNT(r)')
            ->where("r.dated <= '" . $dated->format("Y-m-d") . "' and  r.dates>= '" . $dated->format("Y-m-d") . "'")
            ->orWhere("r.dates >= '" . $dates->format("Y-m-d") . "' and r.dated <= '" . $dates->format("Y-m-d") . "'")
           ;
        //echo $qb->getQuery()->getSQL();exit;
        if ($margeid > 0) {
            $qb->andWhere("r.id != $margeid");
        }
        $rs = $qb->getQuery()->getSingleScalarResult();

        if ($rs == 0)
            return "true";
        else
            return "false";
    }


    public function findFirstPeriod($dated)
    {
        $qb = $this->createQueryBuilder('r')


            ->where('r.dates >= :datecourant')
            ->andWhere('r.dated < :datecourants')
            ->setParameter('datecourants', $dated)
            ->setParameter('datecourant', $dated);

        return $qb->getQuery()
            ->getOneOrNullResult();
    }

    public function findFirstArrang($userid)
    {
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->andWhere('u.id =:id')
            ->andWhere('a.etat =:etat')
            ->andWhere('a.price =:price')
            ->setParameter('etat', 1)
            ->setParameter('price', 0)
            ->setParameter('id', $userid);
        return $query->getQuery()->getResult();
    }
}


