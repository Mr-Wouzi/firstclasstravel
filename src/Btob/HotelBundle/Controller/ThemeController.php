<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Themes;
use Btob\HotelBundle\Form\ThemesType;

class ThemeController extends Controller {

    public function indexAction() {
        $theme = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Themes')
                ->findAll();
        return $this->render('BtobHotelBundle:Theme:index.html.twig', array('entities' => $theme));
    }

    public function addAction() {
        $theme = new Themes();
        $form = $this->createForm(new ThemesType(), $theme);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($theme);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_theme_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Theme:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $theme = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Themes')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ThemesType(), $theme);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_theme_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Theme:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Themes $theme) {
        $em = $this->getDoctrine()->getManager();

        if (!$theme) {
            throw new NotFoundHttpException("Theme non trouvée");
        }
        $em->remove($theme);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_theme_homepage'));
    }

}
