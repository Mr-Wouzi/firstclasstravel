<?php

namespace Btob\HotelBundle\Common;

class PriceCurr {

    private $priceC;
    private $from;
    private $to;

    function PriceCurr() {
        
    }

    function getPrice($price, $from, $to) {
        $priceCurr = 0;

        $url = ("http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s=" . $from . $to . '=X');
        $handle = fopen($url, 'r');

        if ($handle) {
            $result = fgetcsv($handle);
            fclose($handle);
        }

        $priceCurr = ($result[0] * $price);
        return round($priceCurr, 3);
    }

    function getPriceCurr($price) {
        $devise = 'TND';
        $vart = "";
        if (isset($_SESSION['s_currency']) && $_SESSION['s_currency'] == "DA") {
            $devise = 'DZD';
            $prixT = $this->getPrice($price, $devise, 'TND');
            $vart = "<tr><td><img src='images/prixT.png' /></td><td align='left' height='20px' width='60px' style='padding-left:6px;'>" . $prixT . "</td><td>Tunisien (TND)</td></tr>";
        }

        $prixE = $this->getPrice($price, $devise, 'EUR');
        $prixU = $this->getPrice($price, $devise, 'USD');
        $prixC = $this->getPrice($price, $devise, 'CAD');
        $prixCH = $this->getPrice($price, $devise, 'CHF');
        $var = '';

        $var = $var . "<table>";
        $var .= $vart;
        $var = $var . "<tr><td><img src='images/prixE.gif' /></td><td align='left' height='20px' width='60px' style='padding-left:6px;'>" . $prixE . "</td><td>Euro (EUR)</td></tr>";
        $var = $var . "<tr><td><img src='images/prixD.gif' /></td><td align='left' height='20px' style='padding-left:6px;'>" . $prixU . "</td><td>Dollar Americain (USD)</td></tr>";
        $var = $var . "<tr><td><img src='images/prixC.gif' /></td><td align='left' height='20px' style='padding-left:6px;'>" . $prixC . "</td><td>Dollar Canadian (CAD)</td></tr>";
        $var = $var . "<tr><td><img src='images/prixCH.jpg' /></td><td align='left' height='20px' style='padding-left:6px;'>" . $prixCH . "</td><td>Franc Suisse (CHF)</td></tr>";
        $var = $var . "</table>";
        return $var;
    }

    function getPriceCurrAff($price, $from, $to) {
        $prix = 'Non Dispo';
        $prix = $this->getPrice($price, $from, $to);
        return $prix;
    }

    function getPriceCurrAfff($price, $from, $to) {
        return $price;
    }

}

?>