<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Arrangement;
use Btob\HotelBundle\Form\ArrangementType;

class ArrangementController extends Controller {

    public function indexAction() {
        $arrangement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Arrangement')
                ->findAll();
        return $this->render('BtobHotelBundle:Arrangement:index.html.twig', array('entities' => $arrangement));
    }

    public function addAction() {
        $arrangement = new Arrangement();
        $form = $this->createForm(new ArrangementType(), $arrangement);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($arrangement);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_arangement_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Arrangement:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $arrangement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Arrangement')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ArrangementType(), $arrangement);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_arangement_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Arrangement:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Arrangement $arrangement) {
        $em = $this->getDoctrine()->getManager();

        if (!$arrangement) {
            throw new NotFoundHttpException("Arrangement non trouvée");
        }
        $em->remove($arrangement);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_arangement_homepage'));
    }

}
