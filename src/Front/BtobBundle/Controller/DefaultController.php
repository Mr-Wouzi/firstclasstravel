<?php



namespace Front\BtobBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\CuircuitBundle\Entity\Cuircuit;

use Btob\HotelBundle\Common\Tools;

use Btob\OmraBundle\Entity\Omra;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use User\UserBundle\Entity\User;

use Btob\HotelBundle\Entity\Hotel;

use Btob\BienetreBundle\Entity\Reservationbienetre;

use Btob\CuircuitBundle\Entity\Resacircui;

use Btob\BienetreBundle\Entity\Bienetre;

use Btob\HotelBundle\Entity\Newsletter;

use Btob\HotelBundle\Entity\Email;



class DefaultController extends Controller

{



    public function indexAction()

    {

        $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Acceuil'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;
        $convert = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('EUR');
        $convertd = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findBySymb('USD');
        $outgoing1 = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->listPriceIndex();
        $sejact = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findby( array('act' => 1));		  
        $countsej =count($sejact);		   		   
        $ceract = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuit")->findby( array('active' => 1));		  
        $countcer =count($ceract);


        /*

         * Activite

         */

        $activites = $this->getDoctrine()->getRepository("BtobActiviteBundle:Activite")->findby(array(

            'act'=>1,

            ),

            array('dcr' => 'desc'),        // Tri

            1                             // Limite

			);


        /*

         * bien

         */

        $bien = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->findby(array(

            'act'=>1,

            'pindex'=>1,

            ),

            array('dcr' => 'desc'),        // Tri

            9                             // Limite

			);

        /*

         * Circuit

         */

        $circuits = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuit")->findby(array(

            'active'=>1,

            'pindex'=>1,

            ),

            array('dcr' => 'desc'),        // Tri

            9                             // Limite

			);

        //dump($circuits);  exit;
        $listomra = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findAll(

        // Limite

        );


            $sejourBottom = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findby(

                array(

            'act'=>1,

            'pindex'=>1,

                ),

            array('prix' => 'asc '),        // Tri

            6                          // Limite

                    );


            /*

             * Reservation index

             */
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $agence = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $agenceact =$agence->getId();

      
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        /*
         *  donner de formulaire
         */
        // list des arrangement
        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findActiv();
        // list des pays
        $pays = $this->getDoctrine()->getRepository('BtobHotelBundle:Pays')->findActiv();

        // valeur de submit
        $nbpage = 5;
        $nbjour = 1;
        $paysid = "3";
        $ville = "";
        $nom = "";
        $trie = 1;

        $price_min = 0;
        $price_max = 9000;
        $star = 5;
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
        $dt->modify('+1 day');
        $datef = $dt->format("d/m/Y");
        $page = 1;
        $ArrFinal = array();
        $filterhotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findActiv();
        $totpage = 1;
        $ad = array(0 => 2);
        $enf = array(0 => 0);
        $arr = array(0 => 3);


        //Tools::dump($request->request);
        $paysid = $request->request->get("pays");
        $arg = 100;
        $session->set('arg', $arg);
        $session->set('paysid', $paysid);
        $nom = $request->request->get("nom");
        $session->set('nom', $nom);
        $ville = $request->request->get("ville");
        $session->set('ville', $ville);

        $price_min = 0;
        $session->set('price_min', $price_min);
        $price_max = 9000;
        $session->set('price_max', $price_max);
        $star = 0;
        $session->set('star', $star);
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
        $datedd = $dt->format("Y-m-d");
        $session->set('dated', $dated);
        $dt->modify('+1 day');
        $datef = $dt->format("d/m/Y");
        $session->set('datef', $datef);
        $page = 1;
        $session->set('page', $page);
        $ad = array(0 => 2);
        $session->set('ad', $ad);

        $enf = array(0 => 0);
        $session->set('enf', $enf);
        $arr = array(0 => 3);
        $session->set('arr', $arr);
        //Tools::dump($request->request, true);
        // recupération de hotel price
        $tab = explode("/", $dated);

        $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);

        $tab2 = explode("/", $datef);
        $d2 = new \DateTime($tab2[2] . "-" . $tab2[1] . "-" . $tab2[0]);

        // calculer la deference entre dated et dates
        $nbjour = $d2->diff($d1);
        $nbjour = $nbjour->days;
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydate($d1, $user);
        $i = 0;
        $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriode($user,$datedd);

        foreach ($price as $key => $value) {
            $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($value->getHotel(), $datedd);

                  if ($value->getHotel()->getAct()) {
                 $ath= array();
                   $hoteltheme = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelthemes')->listThemeHotel($value->getHotel());
                 foreach ($hoteltheme as $kth=>$valth)
               {
                   array_push($ath, $valth->getThemes()->getName());
               }
               
                $surdemande = false;
                if ($value->getNbnuit() > $nbjour) {
                    $surdemande = true;
                }
                
                
               if(in_array("Maisons d'hôte", $ath))
               {
                  $ArrFinal[$i]["themesmaison"] = "Maisons d'hôte";     
               }
               else{
                  $ArrFinal[$i]["themesmaison"] = ""; 
               }
                //$filterhotel[] = $value->getHotel();
                $ArrFinal[$i]["hotel"] = $value->getHotel();
                $ArrFinal[$i]['hotelpriceid'] = $value->getId();
                $ArrFinal[$i]['majminstay'] = $value->getMajminstay();
                $ArrFinal[$i]['redminstay'] = $value->getRedminstay();
                $ArrFinal[$i]['persmajminstay'] = $value->getPersmajminstay();
                $ArrFinal[$i]['persredminstay'] = $value->getPersredminstay();

                $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($value->getHotel(),$datedd);
                if($promotion)
                {
                    $ArrFinal[$i]['promo'] = $promotion->getName();
                    $ArrFinal[$i]['datedpromo'] = $promotion->getDated();
                    $ArrFinal[$i]['datefpromo'] = $promotion->getDates();
                    $ArrFinal[$i]['valp'] = $promotion->getVal();
                    $ArrFinal[$i]['persp'] = $promotion->getPers();
                    $ArrFinal[$i]['promoid'] = $promotion->getId();
                }
                else{
                    $ArrFinal[$i]['promo'] ="";
                    $ArrFinal[$i]['datedpromo'] ="";
                    $ArrFinal[$i]['datefpromo'] ="";
                    $ArrFinal[$i]['valp'] = "";
                    $ArrFinal[$i]['persp'] = "";
                    $ArrFinal[$i]['promoid'] = "";
                }
                // calcule variante de marge
                
              
                $ArrFinal[$i]["hotelname"] = trim($value->getHotel()->getName());
                $ArrFinal[$i]["hotelid"] =intval($value->getHotel()->getId());

               


                $ArrFinal[$i]["star"] = $value->getHotel()->getStar();
                $ArrFinal[$i]["pays"] = $value->getHotel()->getPays()->getId();
                $ArrFinal[$i]["ville"] = $value->getHotel()->getVille()->getId();
                // gestion des images
                $dataimg = $value->getHotel()->getHotelimg();
                $img = "/back/img/dummy_150x150.gif";
                $j = 0;
                foreach ($dataimg as $keyimg => $valimg) {
                    if ($j == 0)
                        $img = $valimg->getFile();
                    if ($valimg->getPriori())
                        $img = $valimg->getFile();
                    ++$j;
                }
                $ArrFinal[$i]["image"] = $img;
                /*
                 * gestion des prix
                 */
                // minimaume d'arrangement
                $min = 0;
                $j = 0;
                $tab = array();
                $name = "";
                $pers = false;
                $etat = false;
                foreach ($value->getPricearr() as $keyarr => $valarr) {
                    if ($valarr->getEtat() == 1) {
                        if ($valarr->getMinstay() > $nbjour) {
                            $surdemande = true;
                        }
                        if ($j == 0) {
                            $min = $valarr->getPrice();
                            $pers = $valarr->getPers();
                            $etat = $valarr->getEtat();
                            $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                            $margeprice = $valarr->getMarge();
                        $persmprice = $valarr->getPersm();
                        } else {
                            if ($min > $valarr->getPrice()) {
                                $min = $valarr->getPrice();
                                $pers = $valarr->getPers();
                                $etat = $valarr->getEtat();
                                $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                $margeprice = $valarr->getMarge();
                                $persmprice = $valarr->getPersm();
                        
                            }
                        }
                        ++$j;
                    }

                }
                if ($pers) {
                    $ArrFinal[$i]["price"] = $value->getPrice() + (($value->getPrice() * $min) / 100);

                } else {
                    $ArrFinal[$i]["price"] = $min + $value->getPrice();
                }
                // calcule de la marge
                $ArrFinal[$i]["price"] = $ArrFinal[$i]["price"];
                $ArrFinal[$i]["name"] = $name;
                $ArrFinal[$i]["margeprice"] = $margeprice;
                $ArrFinal[$i]["persmprice"] = $persmprice;
               
                // gestion des chambre
                $k = 0;
                //Tools::dump($ad);
                foreach ($ad as $kad => $valad) {
                    $nbpersonne = $valad + $enf[$kad];
                    //if ($nbpersonne > 4) $nbpersonne = 4;
                    $room = null;
                    // test dispo room
                    $hotelroom = $value->getHotel()->getHotelroom();
                    $typepersonne = false;
                    foreach ($hotelroom as $khr => $vhr) {
                        if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                            $room = $vhr->getRoom();
                            $typepersonne = $vhr->getPersonne();
                        }
                    }
                    //test dispo arrangement
                    $arrang = null;
                    $hotelarrangement = $value->getHotel()->getHotelarrangement();

                    foreach  ($value->getPricearr() as $keyarr => $valarr) {


                        if($valarr->getEtat()==1 && $valarr->getPrice()==0)
                        {
                            $arrang =$valarr->getHotelarrangement()->getArrangement();
                        }




                    }
                    if ($room != null && $arrang != null) {
                        foreach ($hotelroom as $khr => $vhr) {
                            if ($room->getId() == $vhr->getRoom()->getId()) {
                                $idform = $vhr->getId();
                            }
                        }
                        $ArrFinal[$i]["allprice"][$k]["nbpersonne"] = $nbpersonne;
                        $ArrFinal[$i]["allprice"][$k]["roomname"] = $room->getName();
                        $ArrFinal[$i]["allprice"][$k]["arrangement"] = $arrang->getName();
                        foreach ($value->getPricearr() as $kk => $vv) {
                            if ($vv->getHotelarrangement()->getArrangement()->getId() == $arrang->getId()) {
                                $ArrFinal[$i]["allprice"][$k]["arrangementid"] = $vv->getId();
                                $ArrFinal[$i]["allprice"][$k]["minstay"] = $vv->getMinstay();
                                $ArrFinal[$i]["allprice"][$k]["etat"] = $vv->getEtat();
                            }
                        }

                        //Disponiblité
                        $ArrFinal[$i]["allprice"][$k]["dispo"]="0";
                        $disroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findByPriceRoom($value->getId(),$room->getId());

                        $valres = $value->getRetro()-1;
                        $now =new \DateTime();
                        $res =$now->modify('+1 day');
                        $dres = $res->modify('+'.$valres. 'day');
                        $nows =new \DateTime();
                        $ress =$nows->modify('+1 day');

                        if($nbjour>=$ArrFinal[$i]["allprice"][$k]["minstay"] && $disroom[0]->getQte() > 0 && $dres->format("Ymd") <= $ress->format("Ymd") )
                        {
                            $ArrFinal[$i]["allprice"][$k]["dispo"]="1";

                        }
                        else{
                            $ArrFinal[$i]["allprice"][$k]["dispo"]="0";

                        }

                        //fin

                        $ArrFinal[$i]["allprice"][$k]["ad"] = $valad;
                        $ArrFinal[$i]["allprice"][$k]["enf"] = $enf[$kad];
                        $ArrFinal[$i]["allprice"][$k]["idform"] = $idform;

                        //$calc=new Calculate();
                        $dtest = null;
                        $dtest = $d1;
                        // initialiser l'age des enfant a 6 s'il existe
                        // il faut les changer s'il y a un autre age d'enfant
                        $tabenf = array();
                        for ($xd = 0; $xd < $enf[$kad]; $xd++) {
                            $tabenf[] = $value->getHotel()->getMaxenfant();
                        }
                        $valp = $ArrFinal[$i]['valp'];
                        $vals =   $ArrFinal[$i]['persp'] ;

                        $prix = $this->getDoctrine()
                            ->getRepository('BtobHotelBundle:Hotelprice')
                            ->calcultate($valp,$vals,$user, $dtest->format('Y-m-d'), $value->getHotel()->getId(), $nbjour, $arrang, $valad, $tabenf, $room, $typepersonne);

                        $prix = $prix; // calcule de marge

                        $ArrFinal[$i]["allprice"][$k]["price"] = $prix;

                        if ($prix == 0) {
                            $surdemande = true;
                        }
                        ++$k;
                    }
                }
                // Stop Sales
                $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);
                $ArrFinal[$i]["stopsales"] = count($stopsales);
                if ($surdemande) {
                    $ArrFinal[$i]["stopsales"] = 1;
                }
                //Tools::dump($request, true);
                ++$i;
                
            }
        }


        $ArrFinal = Tools::array_sort($ArrFinal, "hotelid", SORT_DESC);

        // filtre de prix


        // filter $star
        $ArrFinal1 = $ArrFinal;


        // filter pays

        // filter ville

        // filter name hotel

        //Filter trie

        /*
         * pagination
         */
        $totpage = ceil(count($ArrFinal) / $nbpage);
        $ArrFinal1 = $ArrFinal;
        $ArrFinal = array();
        $i = 0;
        $start = $nbpage * ($page - 1);
        $end = $nbpage * $page;
        if ($start > count($ArrFinal1)) {
            $start = 0;
            $page = 1;
            $end = $nbpage;
        }
        foreach ($ArrFinal1 as $key => $value) {
            if ($i >= $start && $i < $end) {
                $ArrFinal[] = $value;
            }
            ++$i;
        }





        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $ArrFinal1,
            $request->query->get('page', 1)/*page number*/,
            100/*limit per page*/
        );


        $d1 = new \DateTime(Tools::explodedate($dated, '/'));
        $d2 = new \DateTime(Tools::explodedate($datef, '/'));

        $userid = $user->getId();

       
        $pricecopc = $this->getDoctrine()->getRepository("BtobHotelBundle:Pricearr")->testPrice();

/*

 * Bannier !  !!

 */

		$familles = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->findby(

            array('type' => 'séjour de noces','act' => 1  )

        );

		$mieux = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->findby(

            array('type' => 'mieux notes','act' => 1  )

        );

		$luxe = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->findby(

            array('type' => 'luxe & prestige','act' => 1  )

        );

		$petits = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->findby(

            array('type' => 'petits budgets','act' => 1  )

        );

		$all = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->findby(

            array('type' => 'all in','act' => 1  )

        );

		$gratuite = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->findby(

            array('type' => 'gratuite enfants','act' => 1  )

        );

		

		

		// top promo !!

		$hoetl=array();

		$idhotel=array();

		$hotelprice=array();

		$hoteldate=array();

		$ii=1;

        $coupc11 = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotelthemes")->findAll();

		foreach($coupc11 as $value){

			if($value->getThemes()->getName()=='PROMO'){

				$idhotel[$ii]=$value->getHotel()->getId();

				$ii++;

			}

		}

		

		$con=1;	

		

		foreach($idhotel as $value){

			$hoetl[$con] = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotel")->find($idhotel[$con]);

			$con++;

		}

		// dump($hoetl);exit;

		$conn=1;		

		foreach($hoetl as $value){		 

			//récupérer id des hotels tel leur prix s'il existe dans hoetl de mêmes pour les dates 

			$hotelprice[$conn] = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotelprice")->find($hoetl[$conn]);	

			//$hotelprice[$conn]->getPrice();

			//$hoteldate[$conn] = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotelprice")->find($hoetl[$conn])->getDated();



		$conn++;	

		 

		}

		

		// top promo !!

		

		

        $em = $this->getDoctrine()->getManager();



        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        $hotelmarges= $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotelmarge')

            ->findByUser($User, array('id' => 'DESC'));



			  $pricearrs = $this->getDoctrine()->getRepository("BtobHotelBundle:Pricearr")->findby(

            array('price' => 0, 'etat' => 1  )

        );


        $hotact = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotel")->findby(array('act' => 1));        $counthot =count($hotact);
        $themes = $this->getDoctrine()->getRepository("BtobHotelBundle:Themes")->findActiv();
        
        
       
        return $this->render('FrontBtobBundle:Default:index.html.twig', array(

            'couphotel' => $hoetl,
            'themes' => $themes,             			
            'convert' => $convert,
            'convertd' => $convertd,
            'familles' => $familles,

            'mieux' => $mieux,            

	    'counthot' => $counthot,           

	   'countsej' => $countsej,           

	  'countcer' => $countcer,

            'luxe' => $luxe,

            'petits' => $petits,

            'all' => $all,

            'gratuite' => $gratuite,

            'outgoing1' => $outgoing1,
            'listomra'    =>$listomra,

            'sejourBottom' => $sejourBottom,

	   'pricearrs' => $pricearrs,

	  'hotelmarges'=>$hotelmarges,

            'circuit' => $circuits,

            'pays' => $pays,

            'paysid' => $paysid,

            'ville' => $ville,

            'trie' => $trie,

            'price_min' => $price_min,

            'price_max' => $price_max,

            'star' => $star,

            'dated' => $dated,

            'datef' => $datef,

            'nom' => $nom,

            'page' => $page,

            'price' => $ArrFinal,

            'filterhotel' => $filterhotel,

            'totpage' => $totpage,

            'arr' => $arrangement,


            'nbjour' => $nbjour,

            'frmad' => $ad,

            'frmenf' => $enf,


            'frmarr' => $arr,

            'd1' => $d1,

            'd2' => $d2,



            'banner'    =>$banner,

            'activite' =>$activites,
            'pagination' => $pagination,

            'bien' =>$bien

        ));

    }





    public function bannerAction(){

        $em = $this->getDoctrine()->getManager();


        $listhotelprice = $this->getDoctrine()->getRepository("BtobHotelBundle:hotelprice")->Testperiode();
        $counth = count($listhotelprice);
        $circuit = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuit")->findby(array('active'=>1));
        $countcir = count($circuit);
        $croisière = $this->getDoctrine()->getRepository("BtobCroissiereBundle:Croissiere")->findby(array('act' => 1));
        $countcroi = count($croisière);
        $sejour = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findby(array('act' => 1));
        $countsej = count($sejour);
        $omra = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findAll();
        $countomra = count($omra);
        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->listBannaire();

        return $this->render('FrontBtobBundle:Default:banner.html.twig', array(

            'banner'=>$banner,'counth'=>$counth, 'countcir'=>$countcir, 'countcroi'=>$countcroi, 'countsej'=>$countsej, 'countomra'=>$countomra

        ));

    }

	public function sendNewsletterAction(){

        $alertexist = false;

		//dump("test");exit;

        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');

        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

        if ($request->getMethod() == 'GET') {

			$emailnews = $request->query->get('email_news');

			

			//dump("result");

			//dump($formPost);exit;

            $oneemail = null;

            if ($emailnews !=null){

                $oneemail = $this->getDoctrine()->getRepository("BtobHotelBundle:Email")->findby(array('email_adress'=>$emailnews));

            }

			

			// to set the date in french

			

			/* Configure le script en français */

			setlocale (LC_TIME, 'fr_FR','fra');

			//Définit le décalage horaire par défaut de toutes les fonctions date/heure  

			date_default_timezone_set("Europe/Paris");

			//Definit l'encodage interne

			mb_internal_encoding("UTF-8");

			//Convertir une date US vers une date en français affichant le jour de la semaine

            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

			$dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

			

			

			

            if ($oneemail == null){

                            

                if (! empty($emailnews)) {

                    $email = new Email();

                    $email->setEmail_adress($emailnews);

                    $em->persist($email);

                    $em->flush();

					 $to      = $emailnews;

					

					# -=-=-=- MIME BOUNDARY

					$mime_boundary = "----MSA Shipping----" . md5(time());

					# -=-=-=- MAIL HEADERS


					$subject = "Newsletter First Class Travel : Merci de votre inscription... " ;
					$headers = "From: FirstClassTravel \n";
					$headers .= "Reply-To: FirstClassTravel \n";
					$headers .= "MIME-Version: 1.0\n";
					$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
					$message1 = "--$mime_boundary\n";
					$message1 .= "Content-Type: text/html; charset=UTF-8\n";
					$message1 .= "Content-Transfer-Encoding: 8bit\n\n";
					$message1 .= "<html>\n";
					$message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:24px; text-align:justify;"  leftmargin="0">';
                    $message1 .= '<table width="50%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				  <tr>				   <td>				    <img src="http://bbc-weather.net/first/public_html/front/images/logo.png"/>				   </td>				   <td align="right">				    <img src="http://bbc-weather.net/first/public_html/front/images/telmail.png"/>				   </td>				  </tr>				 </tbody>				</table>
                    <br /> <b>Cher Madame/Monsieur,</b><br />
				    Vous venez de vous inscrire à la newsletter de First Class Travel et nous vous en remercions. <br>
					Ainsi serez-vous informés des nouveautés et des offres que nous les mettons à votre disposition.. <br>
					Vous recevrez désormais toutes les deux semaines nos actualités. <br>
					Ce rendez-vous régulier s\'accompagnera de la découverte des détails de chaque offre.';
                    $message1 .= '</body>';

# -=-=-=- FINAL BOUNDARY

					

					

					

					

					mail($to, $subject, $message1, $headers);
					//mail('afef.tuninfo@gmail.com', $subject, $message1, $headers);

					 

                } 

            }else {

                $alertexist = true;



            }

        }

        return $this->redirect($this->generateUrl('front_btob_homepage'));

    }

	

	public function loginAction(){    

        return $this->render('FrontBtobBundle:Default:login.html.twig');

    }

	

	public function passeAction(){    

        return $this->render('FrontBtobBundle:Default:passe.html.twig');

    }
	
	  public function maintenanceAction()
    {
        
         return $this->render('FrontBtobBundle:Default:maintenance.html.twig');
    }

}

