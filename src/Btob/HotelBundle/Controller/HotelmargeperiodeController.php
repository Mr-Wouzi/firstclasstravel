<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Promotion;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\PromotionType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Hotelmargeperiode;
use Btob\HotelBundle\Entity\HotelmargeperiodeRepository;
use Btob\HotelBundle\Form\HotelmargeperiodeType;
use Btob\HotelBundle\Entity\Hotelmarge;
use Btob\HotelBundle\Entity\HotelmargeRepository;

class HotelmargeperiodeController extends Controller {


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function indexAction($hotelid, $userid)
    {


        $hotel = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotel')

            ->find($hotelid);
        $user = $this->getDoctrine()

            ->getRepository('UserUserBundle:User')

            ->find($userid);

        $entities = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotelmargeperiode')

            ->listUserHotel($userid,$hotelid);

        return $this->render('BtobHotelBundle:Hotelmargeperiode:index.html.twig', array(
            'entities' => $entities,
            'userid' => $userid,
            'hotel' => $hotel,
            'user' => $user,
            'hotelid' => $hotelid

        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction($hotelid, $userid)

    {

        $margeperiod = new Hotelmargeperiode();

        $form = $this->createForm(new HotelmargeperiodeType(), $margeperiod);

        $request = $this->get('request');



        $hotel = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotel')

            ->find($hotelid);
        $user = $this->getDoctrine()

            ->getRepository('UserUserBundle:User')

            ->find($userid);



        if ($request->getMethod() == 'POST') {



            $form->bind($request);





            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

                $user = $em->getRepository('UserUserBundle:User')->find($userid);

                $margeperiod->setUser($user);
                $margeperiod->setHotel($hotel);

                $em->persist($margeperiod);

                $em->flush();


                return $this->redirect($this->generateUrl('btob_hotelmargeperiode_homepage', array('userid' => $userid,'hotelid' => $hotelid)));

            } else {

                echo $form->getErrors();

            }

        }

        return $this->render('BtobHotelBundle:Hotelmargeperiode:form.html.twig', array(

            'form' => $form->createView(),
            'hotelid' => $hotelid,
            'hotel' => $hotel,
            'user' => $user,
            'userid' => $userid,


        ));

    }


    public function editAction($id)
    {

        $request = $this->get('request');

        $request = $this->get('request');
        $value = $request->get('_route_params');
        $hotelid = intval($value["hotelid"]);
        $userid = intval($value["userid"]);

        $margeperiod = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmargeperiode')
            ->UpdateUserHotel($id,$userid,$hotelid);

        $hotel = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotel')

            ->find($hotelid);
        $user = $this->getDoctrine()

            ->getRepository('UserUserBundle:User')

            ->find($userid);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new HotelmargeperiodeType(), $margeperiod);

        $form->handleRequest($request);



        if ($form->isValid()) {


            $em->flush();




            return $this->redirect($this->generateUrl('btob_hotelmargeperiode_homepage', array('userid' => $userid,'hotelid' => $hotelid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Hotelmargeperiode:edit.html.twig', array('form' => $form->createView(),  'id' => $id,'hotelid' => $hotelid,
                'userid' => $userid,
                'hotel' => $hotel,
                'user' => $user
            )
        );
    }

    public function deleteAction(Hotelmargeperiode $margeperiod,$userid,$hotelid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$margeperiod) {
            throw new NotFoundHttpException("Hotelprice non trouvée");
        }
        $em->remove($margeperiod);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_hotelmargeperiode_homepage', array("userid" => $userid, 'hotelid' => $hotelid)));
    }


    public function validdateaddAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {
            $id = $request->request->get("id");
            $dated = explode("/", $request->request->get("dated"));
            $dates = explode("/", $request->request->get("dates"));
            $userid = $request->request->get("userid");
            $dt = new \DateTime();
            $dt->setDate($dated[2], $dated[1], $dated[0]);
            $dt2 = new \DateTime();
            $dt2->setDate($dates[2], $dates[1], $dates[0]);
            $nbjour = $dt->diff($dt2)->days;
            if($nbjour<1){
                echo "false";
                exit;
            }

            $priceid = $request->request->get("priceid");

            //$marche=$this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find($marcheid);
            //$hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->find($id);
            if (!is_numeric($priceid)) {
                echo $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->ValidateInterval($id, $dt, $dt2, $userid);
            } else {
                echo $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->ValidateInterval($id, $dt, $dt2, $userid, $priceid);
            }
            exit;

        }
        echo "false";
        exit;
    }
}
