<?php

namespace Btob\CroissiereBundle\Controller;

use Btob\CroissiereBundle\Entity\Reservationcroi;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobCroissiereBundle:Croissiere")->findby(array('act' => 1));
          $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('BtobCroissiereBundle:Default:index.html.twig', array('entities' => $entities,'user' => $user));

    }

    public function detailAction($id)
    {
        $entities = $this->getDoctrine()->getRepository("BtobCroissiereBundle:Croissiere")->find($id);

        return $this->render('BtobCroissiereBundle:Default:detail.html.twig', array('entry' => $entities));
    }

    public function reservationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobCroissiereBundle:Croissiere")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();

                $Reservationcroi = new Reservationcroi();
                $adulte = $request->request->get('adulte');
                $enfant = $request->request->get('enfant');
                $depart = $request->request->get('depart');
                $voyage = $request->request->get('voyage');
                $body = $request->request->get('body');
                $Comment = $request->request->get('Comment');
                $vmariage = $request->request->get('vmariage');
                $contacte = $request->request->get('contacte');

                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
                if ($request->request->get('mariage') != null){
                $mariage = new \Datetime(Tools::explodedate($request->request->get('mariage'),'/'));
                    $Reservationcroi->setDatem($mariage);
                }
                $Reservationcroi->setClient($client);
                $Reservationcroi->setNbradultes($adulte);
                $Reservationcroi->setNbrenf($enfant);
                $Reservationcroi->setDated($dated);
                $Reservationcroi->setVilled($depart);
                $Reservationcroi->setClassev($voyage);
                $Reservationcroi->setDemande($body);
                $Reservationcroi->setComment($Comment);
                $Reservationcroi->setVoyage($vmariage);
                $Reservationcroi->setAgent($this->get('security.context')->getToken()->getUser());

                $Reservationcroi->setContacte($contacte);
                $Reservationcroi->setCroissiere($entities);
                $em->persist($Reservationcroi);
                $em->flush();
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('noticereserv', 'Votre message a bien été envoyé. Merci.');

                return $this->render('BtobCroissiereBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('BtobCroissiereBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }
}
