<?php

namespace Btob\BannaireBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BtobBannaireBundle:Default:index.html.twig', array('name' => $name));
    }
}
