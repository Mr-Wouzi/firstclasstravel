<?php



namespace User\UserBundle\Controller;



use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\RedirectResponse;

use FOS\UserBundle\Model\UserInterface;

use FOS\UserBundle\FOSUserEvents;

use FOS\UserBundle\Event\FormEvent;

use FOS\UserBundle\Event\FilterUserResponseEvent;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use User\UserBundle\Form\ResettingFormType;

use User\UserBundle\Entity\User;

use Btob\HotelBundle\Entity\Hotelmarge;

use Btob\HotelBundle\Common\Tools;

use User\UserBundle\Form\Type\RegistrationFormType2;



class RegistrationController extends BaseController

{



    public function deleteAction($id)

    {

        $userManager = $this->container->get('fos_user.user_manager');

        $om = $this->container->get('doctrine.orm.entity_manager');

        $user = $userManager->findUserBy(array('id' => $id));

        $om->remove($user);

        $om->flush();

        $url = $this->container->get('router')->generate('show_users');

        $response = new RedirectResponse($url);

        return $response;

    }



    public function editAction(Request $request, $id)

    {

        //Tools::dump($request->request, true);

        $userManager = $this->container->get('fos_user.user_manager');

//$user = new User();

        $om = $this->container->get('doctrine.orm.entity_manager');

        $user = $userManager->findUserBy(array('id' => $id));
        $roles= $user->getRoles()[0];
//echo  Tools::dump($user->getRoles(), true); ;exit;

        $formFactory = $this->container->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        $form->setData($user);


        if ('POST' === $request->getMethod()) {

            $form->bind($request);



            if ($form->isValid()) {

                $user->setLogo($request->request->get('files'));
                if($roles == "ROLE_SUPER_ADMIN")
                {
                    $devise = $this->getDoctrine()
                        ->getRepository('BtobDashBundle:Devise')
                        ->find(1);
                    $user->setName("Admin");
                    $user->setDevise($devise);
                    $user->setIllimite(0);
                    
                    $user->setMarge(0);
                    $user->setPrst(0);
                    $user->setMargeomra(0);
                    $user->setActomra(0);
                    $user->setMargecircuit(0);
                    $user->setActcircuit(0);
                    $user->setMargesejour(0);
                    $user->setActsejour(0);
                    $user->setMargevole(0);
                    $user->setActvole(0);
                    $user->setMargecroissiere(0);
                    $user->setActcroissiere(0);
                    $user->setMargevoiture(0);
                    $user->setActvoiture(0);
                    $user->setActtransfert(0);
                    $user->setActbienetre(0);
                    $user->setActseminaire(0);
                    $user->setActgrandjeu(0);
                    $user->setActactivite(0);
                    $user->setMargeevenement(0);
                    $user->setActevenement(0);
                }


                $userManager->updateUser($user);



                $om->flush();



                if (in_array("AGENCEID", $user->getRoles()) || in_array("SALES", $user->getRoles())|| in_array("MONSITEWEB", $user->getRoles())) {

                    $em = $this->getDoctrine()->getManager();

                    foreach ($user->getHotelmarge() as $key => $value) {

                        $em->remove($value);

                        $em->flush();

                    }

                    $hotels = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();

                    foreach ($hotels as $key => $value) {

                    }

                }



                $url = $this->container->get('router')->generate('show_users');

                $response = new RedirectResponse($url);

                return $response;

            }

        }



        return $this->container->get('templating')->renderResponse(

            'UserUserBundle:Registration:edit.html.twig', array('form' => $form->createView(), 'id' => $id, "user" => $user,"roles" => $roles)

        );

    }



    public function edit2Action(Request $request, $id)

    {

        $userManager = $this->container->get('fos_user.user_manager');

//$user = new User();

        $om = $this->container->get('doctrine.orm.entity_manager');

        $user = $userManager->findUserBy(array('id' => $id));



        $formFactory = $this->container->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        $form->setData($user);

        if ('POST' === $request->getMethod()) {

            $form->bind($request);



            if ($form->isValid()) {



                $userManager->updateUser($user);



                $om->persist($user);

                $om->flush();

                return $this->redirect($this->generateUrl('btob_dash_homepage'));

            }

        }



        return $this->container->get('templating')->renderResponse(

            'UserUserBundle:Registration:edit2.html.twig', array('form' => $form->createView(), 'id' => $id)

        );

    }



    public function registerAction(Request $request)

    {

        $user = new User();
        $roles ="";

        $formFactory = $this->container->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        $form->setData($user);
            $request = $this->get('request');
        if ('POST' === $request->getMethod()) {

            
            $form->bind($request);

            //Tools::dump($form,true);
          
            $om = $this->container->get('doctrine.orm.entity_manager');



            if ($form->isValid()) {

                $userManager = $this->container->get('fos_user.user_manager');
               
                $event = new FormEvent($form, $request);


                // get default devise

                $datadevise = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findAll();

                $devise = null;

                foreach ($datadevise as $value) {

                    if ($value->getDef()) {

                        $devise = $value;

                    }

                }

                //$user->setDevise($devise);

             $userManager->createUser($user);
               $login=$user->getUsername();
              $password= $user->getPlainPassword();
               

                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $user->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "FirtsClassTravel - ".$user->getUsername().": votre compte à été créer " ;
                $headers = "From:FirtsClassTravel \n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				  <tr>				   <td>				    <img src="http://bbc-weather.net/first/public_html/front/images/logo.png"/>				   </td>				   <td align="right">				    <img src="http://bbc-weather.net/first/public_html/front/images/telmail.png"/>				   </td>				  </tr>				 </tbody>				</table>
				<br /> <b>Cher Madame/Monsieur,</b><br />
				 Accédez à votre compte avec votre accès personnel.<br />
				 <table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre accès personnel :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $user->getEmail() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Login</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $login . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Password</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $password . '</td>';
                $message1 .= '</tr>';
				
				$message1 .= '<tr>';                
				$message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"></td>';                
				$message1 .= '</tr>';
                $message1 .= '</table>';
                $message1 .= '</body>';
				

                mail($to, $subject, $message1, $headers);

                
                
                
                
                
                

                $om->persist($user);

                $om->flush();

                

                

                

                $url = $this->container->get('router')->generate('show_users');

                $response = new RedirectResponse($url);

                return $response;

            }

        }



        return $this->container->get('templating')->renderResponse(

            'UserUserBundle:Registration:edit.html.twig', array('form' => $form->createView(),"roles" => $roles)

        );

    }



}

