<?php

namespace Btob\OmraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgomra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\ImgomraRepository")
 */
class Imgomra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="omra", inversedBy="imgv")
     * @ORM\JoinColumn(name="omra_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $omra;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;



    public  function  __construct(){
        $this->dcr = new \DateTime();

    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgv
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgv
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;
    
        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set omra
     *
     * @param \Btob\OmraBundle\Entity\omra $omra
     * @return Imgomra
     */
    public function setOmra(\Btob\OmraBundle\Entity\omra $omra = null)
    {
        $this->omra = $omra;
    
        return $this;
    }

    /**
     * Get omra
     *
     * @return \Btob\OmraBundle\Entity\omra 
     */
    public function getOmra()
    {
        return $this->omra;
    }
}
