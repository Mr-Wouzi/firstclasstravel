<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modepaiement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\ModepaiementRepository")
 */
class Modepaiement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=false)
     */
    private $name;
	
	/**
     * @var string
     *
     * @ORM\Column(name="terminal", type="string", length=255 , nullable=false)
     */
    private $terminal;
	
	/**
     * @var string
     *
     * @ORM\Column(name="rib", type="string", length=255 , nullable=false)
     */
    private $rib;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;


    public  function  __construct(){
        
        
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


        /**
     * Set name
     *
     * @param string $name
     * @return Modepaiement
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

     /**
     * Set terminal
     *
     * @param string $terminal
     * @return Modepaiement
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;

        return $this;
    }

    /**
     * Get terminal
     *
     * @return string 
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

     /**
     * Set rib
     *
     * @param string $rib
     * @return Modepaiement
     */
    public function setRib($rib)
    {
        $this->rib = $rib;

        return $this;
    }

    /**
     * Get rib
     *
     * @return string 
     */
    public function getRib()
    {
        return $this->rib;
    }

    /**
     * Set act
     *
     * @param boolean$act
     * @return Modepaiement
     */
    public function setAct($act)
    {
        $this->act= $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean
     */
    public function getAct()
    {
        return $this->act;
    }
}