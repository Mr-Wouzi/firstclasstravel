<?php
/*
 * This file shows how to handle the payment callback issued by the payment page
 * system. This script must be hosted under the URL provided during the payment
 * creation in the ctrlCallbackURL parameter
 */
 

require_once(dirname(__FILE__) . "/Connect2PayClient.php");
$connect2pay = "https://paiement.payzone.ma";
$merchant = "104227";
$password = "rs{k-64(558_6UdV";

// Setup the connection and handle Callback Status
$c2pClient = new Connect2PayClient($connect2pay, $merchant, $password);

 
if ($c2pClient->handleCallbackStatus()) {
  // Get the Error code
  $status = $c2pClient->getStatus();

  // The payment status code
  $errorCode = $status->getErrorCode();
  // Custom data that could have been provided in ctrlCustomData when creating
  // the payment
  $merchantData = $status->getCtrlCustomData();
  // The transaction ID generated for this payment
  $transactionId = $status->getTransactionID();
  // The unique token, known only by the payment page and the merchant
  $merchantToken = $status->getMerchantToken();

  // /!\ /!\
  // The received callback *must* be authenticated by checking that the merchant
  // token matches with a previous known transaction. If this check is not done,
  // anyone can manipulate the payment status by providing fake data to this
  // script.
  // For example the merchant token can be stored with the application order
  // system (ctrlCustomData could also be used to authenticate in other ways)

    // errorCode = 000 => payment transaction is successful
    if ($errorCode == '000') {
        $url = 'http://www.firstclasstravel.ma/reservationnnn-hotel/success';
$fields = array(
	'transactionid' => $c2pClient->getStatus()->getOrderID(),
         'ip' => $_SERVER['REMOTE_ADDR'],
	
);


foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);
      // If we reach this part of the code the payment succeeded
      // Do the required stuff to validate the payment in the application
    } else {
      // Add here the code in case the payment is denied
    }

    // Some debug statement example
    $log = "Received a new transaction status from " . $_SERVER["REMOTE_ADDR"] . ". Merchant token: " . $merchantToken . ", Status: " . $status->getStatus() .
         ", Error code: " . $errorCode;
    if ($errorCode >= 0) {
      $log .= ", Error message: " . $status->getErrorMessage();
      $log .= ", Transaction ID: " . $transactionId;
    }
    syslog(LOG_INFO, $log);

    // Send back a response to mark this transaction as notified on the payment
    // page
    $response = array("status" => "OK", "message" => "Status recorded");
    header("Content-type: application/json");
    echo json_encode($response);
} else {
  syslog(LOG_ERR, "Error. Received an incorrect status from " . $_SERVER["REMOTE_ADDR"] . ".");
}

// Send back a default error response
$response = array("status" => "KO", "message" => "Error handling the callback");
header("Content-type: application/json");
echo json_encode($response);
?>