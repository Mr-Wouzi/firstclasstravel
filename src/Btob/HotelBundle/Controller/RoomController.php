<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Room;
use Btob\HotelBundle\Form\RoomType;

class RoomController extends Controller {

    public function indexAction() {
        $room = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Room')
                ->findAll();
        return $this->render('BtobHotelBundle:Room:index.html.twig', array('entities' => $room));
    }

    public function addAction() {
        $room = new Room();
        $form = $this->createForm(new RoomType(), $room);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($room);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_room_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Room:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $room = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Room')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new RoomType(), $room);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_room_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Room:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Room $room) {
        $em = $this->getDoctrine()->getManager();

        if (!$room) {
            throw new NotFoundHttpException("Chambre non trouvée");
        }
        $em->remove($room);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_room_homepage'));
    }

}
