<?php

namespace Btob\TeamBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgteam
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TeamBundle\Entity\ImgteamRepository")
 */
class Imgteam
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="imgteam")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $team;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgteam
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgteam
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Set team
     *
     * @param \Btob\TeamBundle\Entity\Team $team
     * @return Imgteam
     */
    public function setTeam(\Btob\TeamBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \Btob\TeamBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }
}
