<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Listpays;
use Btob\HotelBundle\Form\ListpaysType;

class ListpaysController extends Controller {

    public function indexAction() {
        $pays = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Listpays')
                ->findAll();
        return $this->render('BtobHotelBundle:Listpays:index.html.twig', array('entities' => $pays));
    }

    public function addAction() {
        $pays = new Listpays();
        $form = $this->createForm(new ListpaysType(), $pays);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($pays);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_listpays_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Listpays:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $pays = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Listpays')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ListpaysType(), $pays);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_listpays_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Listpays:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Listpays $pays) {
        $em = $this->getDoctrine()->getManager();

        if (!$pays) {
            throw new NotFoundHttpException("Pays non trouvée");
        }
        $em->remove($pays);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_listpays_homepage'));
    }

}
