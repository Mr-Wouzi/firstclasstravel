<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Btob\HotelBundle\Entity\Ville;

class HotelType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => "Nom de l'hôtel", 'required' => true))
            ->add('star', 'choice', array(
                'choices' => array('3' => '3', '4' => '4', '5' => '5', '6' => 'Charme'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label' => 'Nombre d\'étoile',
            ))
            ->add('minenfant', 'choice', array(
                'choices' => array('2' => '2','3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label' => 'Age minimal d\'enfant',
            ))
            ->add('maxenfant', 'choice', array(
                'choices' => array('2' => '2','3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label' => 'Age maximal d\'enfant',
            ))
            ->add('contrat', 'choice', array(
                'choices' => array('1' => 'Réduction par âge d\'enfant', '2' => 'Réduction par nombre d\'enfant'),
                'required' => true,
            ))
            ->add('shortdesc', Null, array('label' => "Description courte", 'required' => true))
            ->add('visite', 'text', array('label' => "Viste Virtuelle", 'required' => false))
            ->add('longdesc', null, array('label' => "Description longue", 'required' => false))
            ->add('act', Null, array('label' => "Active?", 'required' => false))
            ->add('pays', 'entity', array(
                'class' => 'BtobHotelBundle:Pays',
                'empty_value' => 'Choisissez un pays',
                'required' => true,
                'label' => "Pays",
            ))
            ->add('baseroom', 'entity', array(
                'class' => 'BtobHotelBundle:Baseroom',
                'empty_value' => 'Choisissez un pays',
                'required' => false,
                'label' => "Base de calcule pour les chambres",
                'multiple'=>true
            ))
        ->add('seotitle', Null, array('label' => "Seo Title", 'required' => true))
        ->add('seodescreption', Null, array('label' => "Seo Description ", 'required' => true))
        ->add('seokeyword', Null, array('label' => "Seo Keyword", 'required' => true))
          ;      
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Hotel'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_hotel';
    }

}
