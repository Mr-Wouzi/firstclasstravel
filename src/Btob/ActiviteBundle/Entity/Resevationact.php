<?php

namespace Btob\ActiviteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resevationact
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ActiviteBundle\Entity\ResevationactRepository")
 */
class Resevationact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date", nullable=true)
     */
    private $dated;

    /**
     * @var integer
     *
     * @ORM\Column(name="adulte", type="integer", nullable=true)
     */
    private $adulte;

    /**
     * @var integer
     *
     * @ORM\Column(name="enfant", type="integer", nullable=true)
     */
    private $enfant;

    /**
     * @var integer
     *
     * @ORM\Column(name="bebe", type="integer", nullable=true)
     */
    private $bebe;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="resevationact")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Activite", inversedBy="resevationact")
     * @ORM\JoinColumn(name="activite_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $activite;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="resevationact")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date", nullable=true)
     */
    private $dcr;



    public  function  __construct(){
        $this->dcr = new \DateTime();

    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Resevationact
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set adulte
     *
     * @param integer $adulte
     * @return Resevationact
     */
    public function setAdulte($adulte)
    {
        $this->adulte = $adulte;

        return $this;
    }

    /**
     * Get adulte
     *
     * @return integer 
     */
    public function getAdulte()
    {
        return $this->adulte;
    }

    /**
     * Set enfant
     *
     * @param integer $enfant
     * @return Resevationact
     */
    public function setEnfant($enfant)
    {
        $this->enfant = $enfant;

        return $this;
    }

    /**
     * Get enfant
     *
     * @return integer 
     */
    public function getEnfant()
    {
        return $this->enfant;
    }

    /**
     * Set bebe
     *
     * @param integer $bebe
     * @return Resevationact
     */
    public function setBebe($bebe)
    {
        $this->bebe = $bebe;

        return $this;
    }

    /**
     * Get bebe
     *
     * @return integer 
     */
    public function getBebe()
    {
        return $this->bebe;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     * @return Resevationact
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Resevationact
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Resevationact
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set activite
     *
     * @param \Btob\ActiviteBundle\Entity\Activite $activite
     * @return Resevationact
     */
    public function setActivite(\Btob\ActiviteBundle\Entity\Activite $activite = null)
    {
        $this->activite = $activite;

        return $this;
    }

    /**
     * Get activite
     *
     * @return \Btob\ActiviteBundle\Entity\Activite 
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Resevationact
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }
}
