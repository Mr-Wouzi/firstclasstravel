<?php

namespace Btob\CuircuitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgcuir
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\ImgcuirRepository")
 */
class Imgcuir
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Cuircuit", inversedBy="imgcuir")
     * @ORM\JoinColumn(name="cuir_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $cuircuit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgcuir
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgcuir
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Set cuircuit
     *
     * @param \Btob\CuircuitBundle\Entity\Cuircuit $cuircuit
     * @return Imgcuir
     */
    public function setCuircuit(\Btob\CuircuitBundle\Entity\Cuircuit $cuircuit = null)
    {
        $this->cuircuit = $cuircuit;

        return $this;
    }

    /**
     * Get cuircuit
     *
     * @return \Btob\CuircuitBundle\Entity\Cuircuit 
     */
    public function getCuircuit()
    {
        return $this->cuircuit;
    }
}
