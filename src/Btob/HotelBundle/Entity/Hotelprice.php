<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotelprice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelpriceRepository")
 */
class Hotelprice {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable=true)
     */
    private $price;

    
    
   
    
    /**
     * @var float
     *
     * @ORM\Column(name="pricew", type="float" , nullable=true)
     */
    private $pricew;
    
  
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dates", type="date" , nullable=true)
     */
    private $dates;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbnuit", type="integer" , nullable=true)
     */
    private $nbnuit;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbperso", type="integer" , nullable=true)
     */
    private $nbperso;

    /**
     * @var integer
     *
     * @ORM\Column(name="delaiannul", type="integer" , nullable=true)
     */
    private $delaiannul;

    /**
     * @var integer
     *
     * @ORM\Column(name="retro", type="integer" , nullable=true)
     */
    private $retro;

    /**
     * @var float
     *
     * @ORM\Column(name="red3lit", type="float" , nullable=true)
     */
    private $red3lit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pers3lit", type="boolean" , nullable=true)
     */
    private $pers3lit;

    /**
     * @var float
     *
     * @ORM\Column(name="red4lit", type="float" , nullable=true)
     */
    private $red4lit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pers4lit", type="boolean" , nullable=true)
     */
    private $pers4lit;

    /**
     * @var float
     *
     * @ORM\Column(name="supsingle", type="float" , nullable=true)
     */
    private $supsingle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="perssupsingle", type="boolean" , nullable=true)
     */

    private $perssupsingle;

    
    /**
     * @var float
     *
     * @ORM\Column(name="margess", type="float")
     */
    private $margess;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="persss", type="boolean")
     */
    private $persss;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="promotion")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;

    /**
     * @ORM\OneToMany(targetEntity="Pricearr", mappedBy="hotelprice", cascade={"remove"})
     */
    protected $pricearr;

    /**
     * @ORM\OneToMany(targetEntity="Pricechild", mappedBy="hotelprice", cascade={"remove"})
     */
    protected $pricechild;

    /**
     * @ORM\OneToMany(targetEntity="Pricesupplement", mappedBy="hotelprice", cascade={"remove"})
     */
    protected $pricesupplement;
    /**
     * @ORM\OneToMany(targetEntity="Priceroom", mappedBy="hotelprice", cascade={"remove"})
     */
    protected $priceroom;
    /**
     * @ORM\ManyToOne(targetEntity="Marcher", inversedBy="User")
     * @ORM\JoinColumn(name="marcher_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $marcher;

    /**
     * @var float
     *
     * @ORM\Column(name="majminstay", type="float" , nullable=true)
     */
    private $majminstay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persmajminstay", type="boolean" , nullable=true)
     */
    private $persmajminstay;


    /**
     * @var float
     *
     * @ORM\Column(name="redminstay", type="float" , nullable=true)
     */
    private $redminstay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persredminstay", type="boolean" , nullable=true)
     */
    private $persredminstay;


    /**
     * @var float
     *
     * @ORM\Column(name="enf12ad", type="float" , nullable=true)
     */
    private $enf12ad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persenf12ad", type="boolean" , nullable=true)
     */

    private $persenf12ad;

    /**
     * @var float
     *
     * @ORM\Column(name="enf11ad", type="float" , nullable=true)
     */
    private $enf11ad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persenf11ad", type="boolean" , nullable=true)
     */

    private $persenf11ad;

    /**
     * @var float
     *
     * @ORM\Column(name="enf21ad", type="float" , nullable=true)
     */
    private $enf21ad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persenf21ad", type="boolean" , nullable=true)
     */

    private $persenf21ad;

    /**
     * @var float
     *
     * @ORM\Column(name="minenf2", type="float" , nullable=true)
     */
    private $minenf2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persminenf2", type="boolean" , nullable=true)
     */

    private $persminenf2;

    /**
     * @var float
     *
     * @ORM\Column(name="enf22ad", type="float" , nullable=true)
     */
    private $enf22ad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persenf22ad", type="boolean" , nullable=true)
     */

    private $persenf22ad;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pricearr = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pricechild = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pricesupplement = new \Doctrine\Common\Collections\ArrayCollection();
        $this->priceroom = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Hotelprice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set pricew
     *
     * @param float $pricew
     * @return Hotelprice
     */
    public function setPricew($pricew)
    {
        $this->pricew = $pricew;

        return $this;
    }

    /**
     * Get pricew
     *
     * @return float 
     */
    public function getPricew()
    {
        return $this->pricew;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Hotelprice
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dates
     *
     * @param \DateTime $dates
     * @return Hotelprice
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * Get dates
     *
     * @return \DateTime 
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * Set nbnuit
     *
     * @param integer $nbnuit
     * @return Hotelprice
     */
    public function setNbnuit($nbnuit)
    {
        $this->nbnuit = $nbnuit;

        return $this;
    }

    /**
     * Get nbnuit
     *
     * @return integer 
     */
    public function getNbnuit()
    {
        return $this->nbnuit;
    }

    /**
     * Set nbperso
     *
     * @param integer $nbperso
     * @return Hotelprice
     */
    public function setNbperso($nbperso)
    {
        $this->nbperso = $nbperso;

        return $this;
    }

    /**
     * Get nbperso
     *
     * @return integer 
     */
    public function getNbperso()
    {
        return $this->nbperso;
    }

    /**
     * Set delaiannul
     *
     * @param integer $delaiannul
     * @return Hotelprice
     */
    public function setDelaiannul($delaiannul)
    {
        $this->delaiannul = $delaiannul;

        return $this;
    }

    /**
     * Get delaiannul
     *
     * @return integer 
     */
    public function getDelaiannul()
    {
        return $this->delaiannul;
    }

    /**
     * Set retro
     *
     * @param integer $retro
     * @return Hotelprice
     */
    public function setRetro($retro)
    {
        $this->retro = $retro;

        return $this;
    }

    /**
     * Get retro
     *
     * @return integer 
     */
    public function getRetro()
    {
        return $this->retro;
    }

    /**
     * Set red3lit
     *
     * @param float $red3lit
     * @return Hotelprice
     */
    public function setRed3lit($red3lit)
    {
        $this->red3lit = $red3lit;

        return $this;
    }

    /**
     * Get red3lit
     *
     * @return float 
     */
    public function getRed3lit()
    {
        return $this->red3lit;
    }

    /**
     * Set pers3lit
     *
     * @param boolean $pers3lit
     * @return Hotelprice
     */
    public function setPers3lit($pers3lit)
    {
        $this->pers3lit = $pers3lit;

        return $this;
    }

    /**
     * Get pers3lit
     *
     * @return boolean 
     */
    public function getPers3lit()
    {
        return $this->pers3lit;
    }

    /**
     * Set red4lit
     *
     * @param float $red4lit
     * @return Hotelprice
     */
    public function setRed4lit($red4lit)
    {
        $this->red4lit = $red4lit;

        return $this;
    }

    /**
     * Get red4lit
     *
     * @return float 
     */
    public function getRed4lit()
    {
        return $this->red4lit;
    }

    /**
     * Set pers4lit
     *
     * @param boolean $pers4lit
     * @return Hotelprice
     */
    public function setPers4lit($pers4lit)
    {
        $this->pers4lit = $pers4lit;

        return $this;
    }

    /**
     * Get pers4lit
     *
     * @return boolean 
     */
    public function getPers4lit()
    {
        return $this->pers4lit;
    }

    /**
     * Set supsingle
     *
     * @param float $supsingle
     * @return Hotelprice
     */
    public function setSupsingle($supsingle)
    {
        $this->supsingle = $supsingle;

        return $this;
    }

    /**
     * Get supsingle
     *
     * @return float 
     */
    public function getSupsingle()
    {
        return $this->supsingle;
    }

    /**
     * Set perssupsingle
     *
     * @param boolean $perssupsingle
     * @return Hotelprice
     */
    public function setPerssupsingle($perssupsingle)
    {
        $this->perssupsingle = $perssupsingle;

        return $this;

    }

    /**
     * Get perssupsingle
     *
     * @return boolean
     */
    public function getPerssupsingle()
    {
        return $this->perssupsingle;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Hotelprice
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Hotelprice
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Hotelprice
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Add pricearr
     *
     * @param \Btob\HotelBundle\Entity\Pricearr $pricearr
     * @return Hotelprice
     */
    public function addPricearr(\Btob\HotelBundle\Entity\Pricearr $pricearr)
    {
        $this->pricearr[] = $pricearr;

        return $this;
    }

    /**
     * Remove pricearr
     *
     * @param \Btob\HotelBundle\Entity\Pricearr $pricearr
     */
    public function removePricearr(\Btob\HotelBundle\Entity\Pricearr $pricearr)
    {
        $this->pricearr->removeElement($pricearr);
    }

    /**
     * Get pricearr
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPricearr()
    {
        return $this->pricearr;
    }

    /**
     * Add pricechild
     *
     * @param \Btob\HotelBundle\Entity\Pricechild $pricechild
     * @return Hotelprice
     */
    public function addPricechild(\Btob\HotelBundle\Entity\Pricechild $pricechild)
    {
        $this->pricechild[] = $pricechild;

        return $this;
    }

    /**
     * Remove pricechild
     *
     * @param \Btob\HotelBundle\Entity\Pricechild $pricechild
     */
    public function removePricechild(\Btob\HotelBundle\Entity\Pricechild $pricechild)
    {
        $this->pricechild->removeElement($pricechild);
    }

    /**
     * Get pricechild
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPricechild()
    {
        return $this->pricechild;
    }

    /**
     * Add pricesupplement
     *
     * @param \Btob\HotelBundle\Entity\Pricesupplement $pricesupplement
     * @return Hotelprice
     */
    public function addPricesupplement(\Btob\HotelBundle\Entity\Pricesupplement $pricesupplement)
    {
        $this->pricesupplement[] = $pricesupplement;

        return $this;
    }

    /**
     * Remove pricesupplement
     *
     * @param \Btob\HotelBundle\Entity\Pricesupplement $pricesupplement
     */
    public function removePricesupplement(\Btob\HotelBundle\Entity\Pricesupplement $pricesupplement)
    {
        $this->pricesupplement->removeElement($pricesupplement);
    }

    /**
     * Get pricesupplement
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPricesupplement()
    {
        return $this->pricesupplement;
    }

    /**
     * Add priceroom
     *
     * @param \Btob\HotelBundle\Entity\Priceroom $priceroom
     * @return Hotelprice
     */
    public function addPriceroom(\Btob\HotelBundle\Entity\Priceroom $priceroom)
    {
        $this->priceroom[] = $priceroom;

        return $this;
    }

    /**
     * Remove priceroom
     *
     * @param \Btob\HotelBundle\Entity\Priceroom $priceroom
     */
    public function removePriceroom(\Btob\HotelBundle\Entity\Priceroom $priceroom)
    {
        $this->priceroom->removeElement($priceroom);
    }

    /**
     * Get priceroom
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPriceroom()
    {
        return $this->priceroom;
    }

    /**
     * Set marcher
     *
     * @param \Btob\HotelBundle\Entity\Marcher $marcher
     * @return Hotelprice
     */
    public function setMarcher(\Btob\HotelBundle\Entity\Marcher $marcher = null)
    {
        $this->marcher = $marcher;

        return $this;
    }

    /**
     * Get marcher
     *
     * @return \Btob\HotelBundle\Entity\Marcher 
     */
    public function getMarcher()
    {
        return $this->marcher;
    }

    /**
     * Set majminstay
     *
     * @param float $majminstay
     * @return Hotelprice
     */
    public function setMajminstay($majminstay)
    {
        $this->majminstay = $majminstay;

        return $this;
    }

    /**
     * Get majminstay
     *
     * @return float
     */
    public function getMajminstay()
    {
        return $this->majminstay;
    }

    /**
     * Set persmajminstay
     *
     * @param boolean $persmajminstay
     * @return Hotelprice
     */
    public function setPersmajminstay($persmajminstay)
    {
        $this->persmajminstay = $persmajminstay;

        return $this;
    }

    /**
     * Get persmajminstay
     *
     * @return boolean
     */
    public function getPersmajminstay()
    {
        return $this->persmajminstay;
    }

    /**
     * Set redminstay
     *
     * @param float $redminstay
     * @return Hotelprice
     */
    public function setRedminstay($redminstay)
    {
        $this->redminstay = $redminstay;

        return $this;
    }

    /**
     * Get redminstay
     *
     * @return float
     */
    public function getRedminstay()
    {
        return $this->redminstay;
    }

    /**
     * Set persredminstay
     *
     * @param boolean $persredminstay
     * @return Hotelprice
     */
    public function setPersredminstay($persredminstay)
    {
        $this->persredminstay = $persredminstay;

        return $this;
    }

    /**
     * Get persredminstay
     *
     * @return boolean
     */
    public function getPersredminstay()
    {
        return $this->persredminstay;
    }

    /**
     * Set enf12ad
     *
     * @param float $enf12ad
     * @return Hotelprice
     */
    public function setEnf12ad($enf12ad)
    {
        $this->enf12ad = $enf12ad;

        return $this;
    }

    /**
     * Get enf12ad
     *
     * @return float
     */
    public function getEnf12ad()
    {
        return $this->enf12ad;
    }

    /**
     * Set persenf12ad
     *
     * @param boolean $persenf12ad
     * @return Hotelprice
     */
    public function setPersenf12ad($persenf12ad)
    {
        $this->persenf12ad = $persenf12ad;

        return $this;
    }

    /**
     * Get persenf12ad
     *
     * @return boolean
     */
    public function getPersenf12ad()
    {
        return $this->persenf12ad;
    }

    /**
     * Set enf11ad
     *
     * @param float $enf11ad
     * @return Hotelprice
     */
    public function setEnf11ad($enf11ad)
    {
        $this->enf11ad = $enf11ad;

        return $this;
    }

    /**
     * Get enf11ad
     *
     * @return float
     */
    public function getEnf11ad()
    {
        return $this->enf11ad;
    }

    /**
     * Set persenf11ad
     *
     * @param boolean $persenf11ad
     * @return Hotelprice
     */
    public function setPersenf11ad($persenf11ad)
    {
        $this->persenf11ad = $persenf11ad;

        return $this;
    }

    /**
     * Get persenf11ad
     *
     * @return boolean
     */
    public function getPersenf11ad()
    {
        return $this->persenf11ad;
    }

    /**
     * Set enf21ad
     *
     * @param float $enf21ad
     * @return Hotelprice
     */
    public function setEnf21ad($enf21ad)
    {
        $this->enf21ad = $enf21ad;

        return $this;
    }

    /**
     * Get enf21ad
     *
     * @return float
     */
    public function getEnf21ad()
    {
        return $this->enf21ad;
    }

    /**
     * Set persenf21ad
     *
     * @param boolean $persenf21ad
     * @return Hotelprice
     */
    public function setPersenf21ad($persenf21ad)
    {
        $this->persenf21ad = $persenf21ad;

        return $this;
    }

    /**
     * Get persenf21ad
     *
     * @return boolean
     */
    public function getPersenf21ad()
    {
        return $this->persenf21ad;
    }

    /**
     * Set enf22ad
     *
     * @param float $enf22ad
     * @return Hotelprice
     */
    public function setEnf22ad($enf22ad)
    {
        $this->enf22ad = $enf22ad;

        return $this;
    }

    /**
     * Get enf22ad
     *
     * @return float
     */
    public function getEnf22ad()
    {
        return $this->enf22ad;
    }

    /**
     * Set persenf22ad
     *
     * @param boolean $persenf22ad
     * @return Hotelprice
     */
    public function setPersenf22ad($persenf22ad)
    {
        $this->persenf22ad = $persenf22ad;

        return $this;
    }

    /**
     * Get persenf22ad
     *
     * @return boolean
     */
    public function getPersenf22ad()
    {
        return $this->persenf22ad;
    }

    /**
     * Set minenf2
     *
     * @param float $minenf2
     * @return Hotelprice
     */
    public function setMinenf2($minenf2)
    {
        $this->minenf2 = $minenf2;

        return $this;
    }

    /**
     * Get minenf2
     *
     * @return float
     */
    public function getMinenf2()
    {
        return $this->minenf2;
    }

    /**
     * Set persminenf2
     *
     * @param boolean $persminenf2
     * @return Hotelprice
     */
    public function setPersminenf2($persminenf2)
    {
        $this->persminenf2 = $persminenf2;

        return $this;
    }

    /**
     * Get persminenf2
     *
     * @return boolean
     */
    public function getPersminenf2()
    {
        return $this->persminenf2;
    }
    
    
    
     
    
    
    
         /**
     * Set margess
     *
     * @param float $margess
     * @return Hotelprice
     */
    public function setMargess($margess)
    {
        $this->margess = $margess;

        return $this;
    }

    /**
     * Get margess
     *
     * @return float
     */
    public function getMargess()
    {
        return $this->margess;
    }
    
    
    /**
     * Set persss
     *
     * @param boolean $persss
     * @return Hotelprice
     */
    public function setPersss($persss)
    {
        $this->persss = $persss;

        return $this;
    }

    /**
     * Get persss
     *
     * @return boolean
     */
    public function getPersss()
    {
        return $this->persss;
    }
}
