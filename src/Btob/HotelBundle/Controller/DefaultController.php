<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Hotel;
use Btob\HotelBundle\Form\HotelType;
use Btob\HotelBundle\Entity\Hotelamg;
use Btob\HotelBundle\Entity\Hotelarrangement;
use Btob\HotelBundle\Entity\Hotelchild;
use Btob\HotelBundle\Entity\Hotellocalisation;
use Btob\HotelBundle\Entity\Hoteloptions;
use Btob\HotelBundle\Entity\Hotelthemes;
use Btob\HotelBundle\Entity\Hotelsupplement;
use Btob\HotelBundle\Entity\Hotelroom;
use Btob\HotelBundle\Entity\Week;
use Btob\HotelBundle\Entity\Hotelimg;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Hotelroomsup;
use Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Entity\Hotelmarge;
use Btob\HotelBundle\Entity\HotelmargeRepository;

use Btob\HotelBundle\Entity\Hotelprice;
use Btob\HotelBundle\Entity\Pricearr;
use Btob\HotelBundle\Entity\Priceroom;
use Btob\HotelBundle\Entity\Pricechild;
use Btob\HotelBundle\Entity\Pricesupplement;

class DefaultController extends Controller
{
    public function dashstatAction()
    {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findBy(array('act' => 1));
        $tab = array();
        foreach ($hotel as $value) {
            $tab[$value->getVille()->getName()][] = 1;
        }
        $ArrFinal = array();
        foreach ($tab as $key => $value) {
            $ArrFinal[$key] = count($value);
        }
        //Tools::dump($ArrFinal);
        return new JsonResponse($ArrFinal);
    }

    public function indexAction()
    {
        $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->findBy(array('act' => 1));
        $hotels = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->findAll();
        return $this->render('BtobHotelBundle:Default:index.html.twig', array('entities' => $hotels, 'marcher' => $marcher));
    }

    public function addAction()
    {

        $hotel = new Hotel();
        $form = $this->createForm(new HotelType(), $hotel);
        $child = $this->getDoctrine()->getRepository('BtobHotelBundle:Child')->findAll();
        $option = $this->getDoctrine()->getRepository('BtobHotelBundle:Options')->findAll();
        $localisation = $this->getDoctrine()->getRepository('BtobHotelBundle:Localisation')->findAll();
        $themes = $this->getDoctrine()->getRepository('BtobHotelBundle:Themes')->findAll();
        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findAll();
        $amg = $this->getDoctrine()->getRepository('BtobHotelBundle:Amg')->findAll();
        $room = $this->getDoctrine()->getRepository('BtobHotelBundle:Room')->findAll();
        $supplement = $this->getDoctrine()->getRepository('BtobHotelBundle:Supplement')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $ville = $this->getDoctrine()->getRepository('BtobHotelBundle:Ville')->find($request->request->get("ville"));
                $hotel->setVille($ville);
                $em->persist($hotel);
                $em->flush();
                $agencied = $em->getRepository('UserUserBundle:User')->findRole();
                $sales= $em->getRepository('UserUserBundle:User')->findRoles();
                $entitiessite = $em->getRepository('UserUserBundle:User')->findRolem();
                $agencieds = array_merge($agencied, $entitiessite, $sales);
                $countagencied= count($agencieds);

                for ($i=0; $i<$countagencied; $i++) {
                    $margehotel = new Hotelmarge();
                    $margehotel->setUser($agencieds[$i]);
                    $margehotel->setHotel($hotel);
                    $margehotel->setMarge($agencieds[$i]->getMarge());
                    $margehotel->setPrst($agencieds[$i]->getPrst());
                    $em->persist($margehotel);

                    $em->flush();

                }

                //Tools::dump($request->request, true);
                // injection de chambre
                if (is_array($request->request->get("room")))
                    foreach ($request->request->get("room") as $key => $value) {
                        $hotelroom = new Hotelroom();
                        $hotelroom->setHotel($hotel);
                        $aaroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Room')->find($value);
                        $hotelroom->setRoom($aaroom);
                        $hotelroom->setAd($aaroom->getCapacity());
                        $hotelroom->setBb($aaroom->getCapacity());
                        $hotelroom->setEnf($aaroom->getCapacity());
                        $em->persist($hotelroom);
                        $em->flush();
                    }
                // injection de la combinaison room-supplement
                if (is_array($request->request->get("room")) && is_array($request->request->get("supplement"))) {
                    foreach ($request->request->get("room") as $key => $value) {
                        foreach ($request->request->get("supplement") as $k => $v) {
                            $hotelroomsup = new Hotelroomsup();
                            $hotelroomsup->setHotel($hotel);
                            $hotelroomsup->setRoom($this->getDoctrine()->getRepository('BtobHotelBundle:Room')->find($value));
                            $hotelroomsup->setSupplement($this->getDoctrine()->getRepository('BtobHotelBundle:Supplement')->find($v));
                            $em->persist($hotelroomsup);
                            $em->flush();
                        }
                    }
                }
                // injection de enfant
                if (is_array($request->request->get("child")))
                    foreach ($request->request->get("child") as $key => $value) {
                        $hotelchild = new Hotelchild();
                        $hotelchild->setHotel($hotel);
                        $hotelchild->setChild($this->getDoctrine()->getRepository('BtobHotelBundle:Child')->find($value));
                        $em->persist($hotelchild);
                        $em->flush();
                    }
                // injection de option
                if (is_array($request->request->get("option")))
                    foreach ($request->request->get("option") as $key => $value) {
                        $hoteloption = new Hoteloptions();
                        $hoteloption->setHotel($hotel);
                        $hoteloption->setOptions($this->getDoctrine()->getRepository('BtobHotelBundle:Options')->find($value));
                        $em->persist($hoteloption);
                        $em->flush();
                    }
                // injection de option
                if (is_array($request->request->get("localisation")))
                    foreach ($request->request->get("localisation") as $key => $value) {
                        $hotellocalisation = new Hotellocalisation();
                        $hotellocalisation->setHotel($hotel);
                        $hotellocalisation->setLocalisation($this->getDoctrine()->getRepository('BtobHotelBundle:Localisation')->find($value));
                        $em->persist($hotellocalisation);
                        $em->flush();
                    }
                // injection de themes
                if (is_array($request->request->get("themes")))
                    foreach ($request->request->get("themes") as $key => $value) {
                        $hoteltheme = new Hotelthemes();
                        $hoteltheme->setHotel($hotel);
                        $hoteltheme->setThemes($this->getDoctrine()->getRepository('BtobHotelBundle:Themes')->find($value));
                        $em->persist($hoteltheme);
                        $em->flush();
                    }
                // injection de supplement
                if (is_array($request->request->get("supplement")))
                    foreach ($request->request->get("supplement") as $key => $value) {
                        $hotelsup = new Hotelsupplement();
                        $hotelsup->setHotel($hotel);
                        $hotelsup->setSupplement($this->getDoctrine()->getRepository('BtobHotelBundle:Supplement')->find($value));
                        $em->persist($hotelsup);
                        $em->flush();
                    }
                // injection de arrangement
                if (is_array($request->request->get("arrangement")))
                    foreach ($request->request->get("arrangement") as $key => $value) {
                        $hotelarrangement = new Hotelarrangement();
                        $hotelarrangement->setHotel($hotel);
                        $hotelarrangement->setArrangement($this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->find($value));
                        $hotelarrangement->setEtat(0);
                        $em->persist($hotelarrangement);
                        $em->flush();
                    }
                // injection de arrangement
                if (is_array($request->request->get("amg")))
                    foreach ($request->request->get("amg") as $key => $value) {
                        $hotelamg = new Hotelamg();
                        $hotelamg->setHotel($hotel);
                        $hotelamg->setAmg($this->getDoctrine()->getRepository('BtobHotelBundle:Amg')->find($value));
                        $em->persist($hotelamg);
                        $em->flush();
                    }
                // injection de weekend
                if (is_array($request->request->get("weekend")))
                    foreach ($request->request->get("weekend") as $key => $value) {
                        $week = new Week();
                        $week->setHotel($hotel);
                        $week->setName($value);
                        $em->persist($week);
                        $em->flush();
                    }
                // injection de image
                $i = 0;
                if (is_array($request->request->get("files")))
                    foreach ($request->request->get("files") as $key => $value) {
                        if ($value != "") {
                            $hotelimg = new Hotelimg();
                            $hotelimg->setHotel($hotel);
                            $hotelimg->setFile($value);
                            if ($i == 0) {
                                $hotelimg->setPriori(true);
                            } else {
                                $hotelimg->setPriori(false);
                            }
                            $em->persist($hotelimg);
                            $em->flush();
                            $i++;
                        }
                    }
                return $this->redirect($this->generateUrl('btob_hotel_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Default:form.html.twig', array(
            'form' => $form->createView(),
            'child' => $child,
            'options' => $option,
            'localisation' => $localisation,
            'themes' => $themes,
            'arrangement' => $arrangement,
            'room' => $room,
            'amg' => $amg,
            'hotel' => $hotel,
            'supplement' => $supplement
        ));
    }

    public function editAction($id)
    {

        $request = $this->get('request');
        $hotel = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->find($id);
        $child = $this->getDoctrine()->getRepository('BtobHotelBundle:Child')->findAll();
        $option = $this->getDoctrine()->getRepository('BtobHotelBundle:Options')->findAll();
        $localisation = $this->getDoctrine()->getRepository('BtobHotelBundle:Localisation')->findAll();
        $themes = $this->getDoctrine()->getRepository('BtobHotelBundle:Themes')->findAll();
        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findAll();
        $amg = $this->getDoctrine()->getRepository('BtobHotelBundle:Amg')->findAll();
        $room = $this->getDoctrine()->getRepository('BtobHotelBundle:Room')->findAll();
        $supplement = $this->getDoctrine()->getRepository('BtobHotelBundle:Supplement')->findAll();
//----------to not touch tarif page----------------
        $allhotelprice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findBy(array('hotel' => $hotel));
        /*$allpricearr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->findAll();
        $allpriceroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findAll();
        $allpricechild = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricechild')->findAll();
        $allpricesupplement = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->findAll();*/
        $oldlisthotelarrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelarrangement')->findBy(array('hotel' => $hotel));
        $oldlisthotelroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->findBy(array('hotel' => $hotel));
        $oldlisthotelchild = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelchild')->findBy(array('hotel' => $hotel));
        $oldlisthotelsupplement = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelsupplement')->findBy(array('hotel' => $hotel));
        
//dump( $oldlisthotelarrangement);
        //dump( $oldlisthotelroom);
        //dump( $oldlisthotelchild);
        //dump( $oldlisthotelsupplement);
        $i=0;
        $oldlistarrangement = array();
        $oldlistroom = array();
        $oldlistchild = array();
        $oldlistsupplement = array();
        if (!empty($oldlisthotelarrangement))
        foreach ($oldlisthotelarrangement as $olditem) {
            $oldlistarrangement[$i] = $olditem->getArrangement();
            $i = $i + 1;
        }
        $i=0;
        if (!empty($oldlisthotelroom))
        foreach ($oldlisthotelroom as $olditem) {
            $oldlistroom[$i] = $olditem->getRoom();
            $i = $i + 1;
        }
        $i=0;
        if (!empty($oldlisthotelchild))
        foreach ($oldlisthotelchild as $olditem) {
            $oldlistchild[$i] = $olditem->getChild();
            $i = $i + 1;
        }
        $i=0;
        if (!empty($oldlisthotelsupplement))
        foreach ($oldlisthotelsupplement as $olditem) {
            $oldlistsupplement[$i] = $olditem->getSupplement();
            $i = $i + 1;
        }
        $i=0;
        //dump( $oldlistarrangement);
        //dump( $oldlistroom);
        //dump( $oldlistchild);
        //dump( $oldlistsupplement);
        $allpricearrs = array();
        $allpricerooms = array();
        $allpricechilds = array();
        $allpricesupplements = array();
        //dump( $allhotelprice);
        if (!empty($allhotelprice))
        foreach ($allhotelprice as $hotelprice) {
                $allhotelprices[$i] = $hotelprice;
                $allpricearrs[$i] = $this->getDoctrine()->getRepository('BtobHotelBundle:pricearr')->findBy(array('hotelprice' => $hotelprice));
                $allpricerooms[$i] = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findBy(array('hotelprice' => $hotelprice));
                $allpricechilds[$i] = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricechild')->findBy(array('hotelprice' => $hotelprice));
                $allpricesupplements[$i] = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->findBy(array('hotelprice' => $hotelprice));
                $i = $i + 1;
        }
            //dump( $allhotelprices);
            //dump( $allpricearrs);
            //dump( $allpricerooms);
            //dump( $allpricechilds);
            //dump( $allpricesupplements);
            /*foreach ($allhotelprices as $key => $value) {
                    dump($key);
                    dump($value);
            }*/
            //exit;
        $curarrangements = array();
        $currooms = array();
        $curchilds = array();
        $cursupplements = array();
        $i=0;
            if (!empty($allpricearrs)){
                foreach ($allpricearrs[0] as $oneitem) {
                //to do
                    $curarrangements[$i] = $oneitem->getHotelarrangement()->getArrangement();
                    $i = $i + 1;
                }
            }
        $i=0;
            if (!empty($allpricerooms)){
                /*foreach ($allpricerooms[0] as $oneitem) {
                //to do
                    $currooms[$i] = $oneitem->getRoom();
                    $i = $i + 1;
                }
            } else {*/
                foreach ($oldlisthotelroom as $oneitem) {
                //to do
                    $currooms[$i] = $oneitem->getRoom();
                    $i = $i + 1;
                }
            }
        $i=0;
            if (!empty($allpricechilds)){
                foreach ($allpricechilds[0] as $oneitem) {
                //to do
                    $curchilds[$i] = $oneitem->getHotelchild()->getChild();
                    $i = $i + 1;
                }
            }
        $i=0;
            if (!empty($allpricesupplements)){
                foreach ($allpricesupplements[0] as $oneitem) {
                //to do
                    $cursupplements[$i] = $oneitem->getHotelsupplement()->getSupplement();
                    $i = $i + 1;
                }
            }
             $interestoldlisthotelroom = array();
            if(!empty($oldlisthotelroom))
            {
                
        foreach ($oldlisthotelroom as $oldhotelroomss) {
            array_push($interestoldlisthotelroom, $oldhotelroomss);
        } 
            }
          
            /*dump('current values');
        dump( $curarrangements);
        dump( $currooms);
        dump( $curchilds);
        dump( $cursupplements);*/
        //exit;
//----------end to not touch tarif page----------------
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new HotelType(), $hotel);
        $form->handleRequest($request);

        if ($form->isValid()) {
           
           $hotsupprooms= $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroomsup')->findBy(array('hotel' => $hotel));

           $array_hotsupproom = array();
           foreach ($hotsupprooms as $hotsupproom) {
               
             $hotsuppproom=  $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroomsup')->find($hotsupproom->getId());
            array_push($array_hotsupproom, $hotsuppproom);
            
        }

            //dump ('old list hotel arrangement');
            if (!empty($oldlisthotelarrangement))
                foreach ($oldlisthotelarrangement as $olditem) {
                    //dump ($olditem);
                }
            //dump ('new list hotel arrangement');
                 if(!empty($request->request->get("arrangement")))
                foreach ($request->request->get("arrangement") as $olditem) {
                    //dump ($olditem);
                }
                
            $ville = $this->getDoctrine()->getRepository('BtobHotelBundle:Ville')->find($request->request->get("ville"));
            $hotel->setVille($ville);
            $em->flush();
            // supprimer toues les combinaison
            foreach ($hotel->getHotelarrangement() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHotelamg() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHotelsupplement() as $key => $value) {
                $em->remove($value);
                //echo $value->getId();
                $em->flush();
            }
            
            foreach ($hotel->getHotelchild() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHotelimg() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHotellocalisation() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHoteloptions() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHotelroom() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHotelthemes() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHotelweek() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotel->getHotelroomsup() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            if (!empty($allhotelprice))
                foreach ($allhotelprice as $hotelprice) {
                    foreach ($hotelprice->getPriceroom() as $key => $value) {
                        $em->remove($value);
                        $em->flush();
                    }
					foreach ($hotelprice->getPricechild() as $key => $value) {
                        $em->remove($value);
                        $em->flush();
                    }
                }



            // injection de chambre
            if (is_array($request->request->get("room")))
                    foreach ($request->request->get("room") as $key => $value) {
                            $hotelroom = new Hotelroom();
                            $hotelroom->setHotel($hotel);
                            $aaroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Room')->find($value);
                            $hotelroom->setRoom($aaroom);
                            $hotelroom->setAd($aaroom->getCapacity());
                            $hotelroom->setBb($aaroom->getCapacity());
                            $hotelroom->setEnf($aaroom->getCapacity());
                            $em->persist($hotelroom);
                            $em->flush();
                            if (!empty($allhotelprices)){
                            foreach ($allhotelprices as $key => $value) {
                               
                                $newpriceroom = new Priceroom();
                                $newpriceroom->setHotelprice($value);
                                $newpriceroom->setRoom($aaroom);
                                if (!empty($oldlistroom)){
                                    if (! in_array($hotelroom->getRoom(), $oldlistroom)) {
                                       
                                        $newpriceroom->setQte(0);
                                        
                                    }
                                    else{
                                        if (!empty($allpricerooms[$key])){
                                            foreach ($allpricerooms[$key] as $pkey => $pvalue) {
                                                if ($pvalue->getRoom() == $hotelroom->getRoom()) {
                                                    
                                                    $newpriceroom->setQte($pvalue->getQte());
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                                $em->persist($newpriceroom);
                                $em->flush();
                            }
                        }
                    }
            
                    $newshotelrooms = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->findBy(array('hotel' => $hotel));
                foreach ($newshotelrooms as $keyss=>$valuee)
                {
                    $newshotelroom =$this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($valuee);
                    
                   if (!empty($interestoldlisthotelroom))
                   {
                       foreach ($interestoldlisthotelroom as $keysss=>$valuees)
                       {
           
                           if($newshotelroom->getRoom()->getId()==$valuees->getRoom()->getId())
                           {
                               $newshotelroom->setAd($valuees->getAd());
                            $newshotelroom->setBb($valuees->getBb());
                            $newshotelroom->setEnf($valuees->getEnf());
                            $em->persist($newshotelroom);
                            $em->flush();
                           }
                       }
                   }
                }
                    
                    //
            // injection de supplement
            if (is_array($request->request->get("supplement")))
                foreach ($request->request->get("supplement") as $key => $value) {
                    $hotelsupplement = new Hotelsupplement();
                    
                    $hotelsupplement->setHotel($hotel);
                    $hotelsupplement->setSupplement($this->getDoctrine()->getRepository('BtobHotelBundle:Supplement')->find($value));
                    $em->persist($hotelsupplement);
                    $em->flush();
                    if (!empty($allhotelprices)){
                        foreach ($allhotelprices as $key => $value) {
                           //dump('create new pricearr'); 
                            $newpricesupplement = new Pricesupplement();
                            $newpricesupplement->setPrice(0);
                            $newpricesupplement->setHotelprice($value);
                            $newpricesupplement->setPersm(0);
                            $newpricesupplement->setMarge(0);
                            $newpricesupplement->setHotelsupplement($hotelsupplement);
                            if (!empty($oldlistsupplement)){
                                if (! in_array($hotelsupplement->getSupplement(), $oldlistsupplement)) {
                                   
                                    $newpricesupplement->setPrice(0);
                                    $newpricesupplement->setPers(0);
                                    $newpricesupplement->setPersm(0);
                                    $newpricesupplement->setMarge(0);
                                    
                                }
                                else{
                                    if (!empty($allpricesupplements[$key])){
                                        foreach ($allpricesupplements[$key] as $pkey => $pvalue) {
                                            if ($pvalue->getHotelsupplement()->getSupplement() == $hotelsupplement->getSupplement()) {
                                                $newpricesupplement->setPrice($pvalue->getPrice());
                                                $newpricesupplement->setPers($pvalue->getPers());
                                                $newpricesupplement->setPersm($pvalue->getPersm());
                                                $newpricesupplement->setMarge($pvalue->getMarge());
                                                
                                            }
                                        }
                                    }
                                }
                            }
                            $em->persist($newpricesupplement);
                            $em->flush();
                        }
                    }
                }

//Tools::dump($request->request, true);
            // injection de la combinaison room-supplement
            if (is_array($request->request->get("room")) && is_array($request->request->get("supplement"))) {
                foreach ($request->request->get("room") as $key => $value) {
                   // var_dump(count($request->request->get("supplement")));
                    //die();
                    foreach ($request->request->get("supplement") as $k => $v) {
                        $hotelroomsup = new Hotelroomsup();
                        $hotelroomsup->setHotel($hotel);
                        $hotelroomsup->setRoom($this->getDoctrine()->getRepository('BtobHotelBundle:Room')->find($value));
                        $hotelroomsup->setSupplement($this->getDoctrine()->getRepository('BtobHotelBundle:Supplement')->find($v));
                        $em->persist($hotelroomsup);
                        $em->flush();
                     }   
                }
            }

            
            $hotsupproomsss= $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroomsup')->findBy(array('hotel' => $hotel));
            // var_dump(count($hotsupproomsss));
            // die();
            
            foreach ($hotsupproomsss as $hotsupproms) {
                foreach ($array_hotsupproom as $arrayhotsupproom) {
                    if(($hotsupproms->getSupplement()==$arrayhotsupproom->getSupplement())&&($hotsupproms->getRoom()==$arrayhotsupproom->getRoom()))
                    {
                        $hotsupproms->setAct($arrayhotsupproom->getAct());
                        $em->persist($hotsupproms);
                        $em->flush();
                    }
                }
                
                
            }
            
           
            // injection de enfant
            if (is_array($request->request->get("child")))
                foreach ($request->request->get("child") as $key => $value) {
                    $hotelchild = new Hotelchild();
                    $hotelchild->setHotel($hotel);
                    $hotelchild->setChild($this->getDoctrine()->getRepository('BtobHotelBundle:Child')->find($value));
                    $em->persist($hotelchild);
                    $em->flush();
                    if (!empty($allhotelprices)){
                        foreach ($allhotelprices as $key => $value) {
                           //dump('create new pricearr'); 
                            
                            if (!empty($oldlistchild)){
							//nouveau
                                if (! in_array($hotelchild->getChild(), $oldlistchild)) {
									$newpricechild1 = new Pricechild();
									$newpricechild1->setHotelprice($value);
									$newpricechild1->setHotelchild($hotelchild);
                                                                        $newpricechild1->setPrice(0);
                                                                       $newpricechild1->setPers(0);
                                                                       $newpricechild1->setType(1);
									$newpricechild2 = new Pricechild();
									$newpricechild2->setHotelprice($value);
									$newpricechild2->setHotelchild($hotelchild);
                                                                        $newpricechild2->setPrice(0);
                                                                        $newpricechild2->setPers(0);
                                                                          $newpricechild2->setType(2);
									$newpricechild3 = new Pricechild();
									$newpricechild3->setHotelprice($value);
									$newpricechild3->setHotelchild($hotelchild);
                                                                        $newpricechild3->setPrice(0);
                                                                        $newpricechild3->setPers(0);
                                                                         $newpricechild3->setType(3);
                                                                        $newpricechild4 = new Pricechild();
									$newpricechild4->setHotelprice($value);
									$newpricechild4->setHotelchild($hotelchild);
                                                                        $newpricechild4->setPrice(0);
                                                                        $newpricechild4->setPers(0);
                                                                         $newpricechild4->setType(4);
                                                                        $newpricechild5 = new Pricechild();
									$newpricechild5->setHotelprice($value);
									$newpricechild5->setHotelchild($hotelchild);
                                                                        $newpricechild5->setPrice(0);
                                                                        $newpricechild5->setPers(0);
                                                                         $newpricechild5->setType(5);
                                                                        
                                                                        
									
									
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
									$em->persist($newpricechild1);
									$em->flush();
									$em->persist($newpricechild2);
									$em->flush();
									$em->persist($newpricechild3);
									$em->flush();
                                                                        
                                                                        $em->persist($newpricechild4);
									$em->flush();
                                                                        $em->persist($newpricechild5);
									$em->flush();
                                }
							//ancien
                                else{
                                    if (!empty($allpricechilds[$key])){
                                        foreach ($allpricechilds[$key] as $pkey => $pvalue) {
                                            if ($pvalue->getHotelchild()->getChild() == $hotelchild->getChild()) {
													$newpricechild1 = new Pricechild();
													$newpricechild1->setHotelprice($value);
													$newpricechild1->setHotelchild($hotelchild);
													/* $newpricechild2 = new Pricechild();
													$newpricechild2->setHotelprice($value);
													$newpricechild2->setHotelchild($hotelchild);
													$newpricechild3 = new Pricechild();
													$newpricechild3->setHotelprice($value);
													$newpricechild3->setHotelchild($hotelchild); */
													$newpricechild1->setPrice($pvalue->getPrice());
													$newpricechild1->setPers($pvalue->getPers());
													$newpricechild1->setType($pvalue->getType());
												
													/* $newpricechild2->setPrice($pvalue->getPrice());
													$newpricechild2->setPers($pvalue->getPers());
													$newpricechild2->setType($pvalue->getType());
												
													$newpricechild3->setPrice($pvalue->getPrice());
													$newpricechild3->setPers($pvalue->getPers());
													$newpricechild3->setType($pvalue->getType()); */
													$em->persist($newpricechild1);
													$em->flush();
													/* $em->persist($newpricechild2);
													$em->flush();
													$em->persist($newpricechild3);
													$em->flush(); */
                                            }
                                        }
                                    }
                                }
                            }else {
									$newpricechild1 = new Pricechild();
									$newpricechild1->setHotelprice($value);
									$newpricechild1->setHotelchild($hotelchild);
                                                                         $newpricechild1->setPrice(0);
                                                                         $newpricechild1->setPers(0);
                                                                        $newpricechild1->setType(1);
									$newpricechild2 = new Pricechild();
									$newpricechild2->setHotelprice($value);
									$newpricechild2->setHotelchild($hotelchild);
                                                                        $newpricechild2->setPrice(0);
                                                                       $newpricechild2->setPers(0);
                                                                        $newpricechild2->setType(2);
									$newpricechild3 = new Pricechild();
									$newpricechild3->setHotelprice($value);
									$newpricechild3->setHotelchild($hotelchild);
                                                                        $newpricechild3->setPrice(0);
                                                                        $newpricechild3->setPers(0);
                                                                        $newpricechild3->setType(3);
									$newpricechild4 = new Pricechild();
									$newpricechild4->setHotelprice($value);
									$newpricechild4->setHotelchild($hotelchild);
                                                                        $newpricechild4->setPrice(0);
                                                                        $newpricechild4->setPers(0);
                                                                        $newpricechild4->setType(4);
									$newpricechild5 = new Pricechild();
									$newpricechild5->setHotelprice($value);
									$newpricechild5->setHotelchild($hotelchild);
                                                                        $newpricechild5->setPrice(0);
                                                                        $newpricechild5->setPers(0);
                                                                        $newpricechild5->setType(5);
									$newpricechild6 = new Pricechild();
									$newpricechild6->setHotelprice($value);
									$newpricechild6->setHotelchild($hotelchild);
                                                                        $newpricechild6->setPrice(0);
                                                                        $newpricechild6->setPers(0);
                                                                        $newpricechild6->setType(6);
									$newpricechild7 = new Pricechild();
									$newpricechild7->setHotelprice($value);
									$newpricechild7->setHotelchild($hotelchild);
                                   
									$newpricechild7->setPrice(0);
                                                                         $newpricechild7->setPers(0);
                                                                         $newpricechild7->setType(7);
									
									
									
									
									
									$em->persist($newpricechild1);
									$em->flush();
									$em->persist($newpricechild2);
									$em->flush();
									$em->persist($newpricechild3);
									$em->flush();
									$em->persist($newpricechild4);
									$em->flush();
									$em->persist($newpricechild5);
									$em->flush();
									$em->persist($newpricechild6);
									$em->flush();
									$em->persist($newpricechild7);
									$em->flush();
							}
						}
					}
				}
                         
            // injection de arrangement
                /*if (is_array($request->request->get("room")))
                foreach ($request->request->get("room") as $key => $value) {
                    $hotelroom = new Hotelroom();
                    $hotelroom->setHotel($hotel);
                    $aaroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Room')->find($value);
                    $hotelroom->setRoom($aaroom);
                    $hotelroom->setAd($aaroom->getCapacity());
                    $hotelroom->setBb($aaroom->getCapacity());
                    $hotelroom->setEnf($aaroom->getCapacity());
                    $em->persist($hotelroom);
                    $em->flush();
                    foreach ($allhotelprices as $key => $value) {
                       
                        $newpriceroom = new Priceroom();
                        $newpriceroom->setHotelprice($value);
                        $newpriceroom->setRoom($aaroom);
                        if (! in_array($aaroom, $oldlistroom)) {
                           
                            $newpriceroom->setQte(0);
                        }
                        else{
                            foreach ($allpricerooms[$key] as $pkey => $pvalue) {
                                if ($pvalue->getRoom() == $aaroom) {
                                  
                                    $newpriceroom->setQte($pvalue->getQte());
                                }
                            }
                        }
                        $em->persist($newpriceroom);
                        $em->flush();
                    }
                }
*/
            if (is_array($request->request->get("arrangement")))
                foreach ($request->request->get("arrangement") as $key => $value) {
                    $hotelarrangement = new Hotelarrangement();
                    $hotelarrangement->setHotel($hotel);
                    $hotelarrangement->setArrangement($this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->find($value));
                   $hotelarrangement->setEtat(0);
                    $em->persist($hotelarrangement);
                    $em->flush();
                    //dump('create new arrangement');
                    if (!empty($allhotelprices)){
                        foreach ($allhotelprices as $key => $value) {
                           //dump('create new pricearr'); 
                            $newpricearr = new Pricearr();
                            $newpricearr->setPrice(0);
                            $newpricearr->setHotelprice($value);
                            $newpricearr->setMinstay(0);
                            $newpricearr->setPersm(0);
                            $newpricearr->setMarge(0);
                            $newpricearr->setEtat(0);
                            $newpricearr->setHotelarrangement($hotelarrangement);
                            if (!empty($oldlistarrangement)){
                                if (! in_array($hotelarrangement->getArrangement(), $oldlistarrangement)) {
                                   
                                    $newpricearr->setPrice(0);
                                    $newpricearr->setMinstay(0);
                                    $newpricearr->setPers(0);
                                    $newpricearr->setEtat(0);
                                    $newpricearr->setPersm(0);
                                    $newpricearr->setMarge(0);
                                }
                                else{
                                    if (!empty($allpricearrs[$key])){
                                        foreach ($allpricearrs[$key] as $pkey => $pvalue) {
                                            if ($pvalue->getHotelarrangement()->getArrangement() == $hotelarrangement->getArrangement()) {
                                                $newpricearr->setPrice($pvalue->getPrice());
                                                $newpricearr->setMinstay($pvalue->getMinstay());
                                                $newpricearr->setPers($pvalue->getPers());
                                                 $newpricearr->setPersm($pvalue->getPersm());
                                                  $newpricearr->setMarge($pvalue->getMarge());
                                                $newpricearr->setEtat($pvalue->getEtat());
                                                
                                            }
                                        }
                                    }
                                }
                            }
                            $em->persist($newpricearr);
                            $em->flush();
                        }
                    }
                }
            
            // injection de option
            if (is_array($request->request->get("option")))
                foreach ($request->request->get("option") as $key => $value) {
                    $hoteloption = new Hoteloptions();
                    $hoteloption->setHotel($hotel);
                    $hoteloption->setOptions($this->getDoctrine()->getRepository('BtobHotelBundle:Options')->find($value));
                    $em->persist($hoteloption);
                    $em->flush();
                }

            // injection de localisation
            if (is_array($request->request->get("localisation")))
                foreach ($request->request->get("localisation") as $key => $value) {
                    $hotellocalisation = new Hotellocalisation();
                    $hotellocalisation->setHotel($hotel);
                    $hotellocalisation->setLocalisation($this->getDoctrine()->getRepository('BtobHotelBundle:Localisation')->find($value));
                    $em->persist($hotellocalisation);
                    $em->flush();
                }
            // injection de themes
            if (is_array($request->request->get("themes")))
                foreach ($request->request->get("themes") as $key => $value) {
                    $hoteltheme = new Hotelthemes();
                    $hoteltheme->setHotel($hotel);
                    $hoteltheme->setThemes($this->getDoctrine()->getRepository('BtobHotelBundle:Themes')->find($value));
                    $em->persist($hoteltheme);
                    $em->flush();
                }

            // injection de amenagement
            if (is_array($request->request->get("amg")))
                foreach ($request->request->get("amg") as $key => $value) {
                    $hotelamg = new Hotelamg();
                    $hotelamg->setHotel($hotel);
                    $hotelamg->setAmg($this->getDoctrine()->getRepository('BtobHotelBundle:Amg')->find($value));
                    $em->persist($hotelamg);
                    $em->flush();
                }
            // injection de weekend
            if (is_array($request->request->get("weekend")))
                foreach ($request->request->get("weekend") as $key => $value) {
                    $week = new Week();
                    $week->setHotel($hotel);
                    $week->setName($value);
                    $em->persist($week);
                    $em->flush();
                }
            // injection de image
            $i = 0;
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $hotelimg = new Hotelimg();
                        $hotelimg->setHotel($hotel);
                        $hotelimg->setFile($value);
                        if ($i == 0) {
                            $hotelimg->setPriori(true);
                        } else {
                            $hotelimg->setPriori(false);
                        }
                        $em->persist($hotelimg);
                        $em->flush();
                        $i++;
                    }
                }
                //exit;
            return $this->redirect($this->generateUrl('btob_hotel_homepage'));
        } else {
            echo $form->getErrors();
        }
        //dump($hotel); exit;
        return $this->render('BtobHotelBundle:Default:edit.html.twig', array(
                'form' => $form->createView(),
                'id' => $id,
                'child' => $child,
                'options' => $option,
                'localisation' => $localisation,
                'themes' => $themes,
                'arrangement' => $arrangement,
                'room' => $room,
                'amg' => $amg,
                'hotel' => $hotel,
                'supplement' => $supplement,
                'curarrangements' => $curarrangements,
                'currooms' => $currooms,
                'curchilds' => $curchilds,
                'cursupplements' => $cursupplements
            )
        );
    }

    public function deleteAction(hotel $hotel)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$hotel) {
            throw new NotFoundHttpException("hotel non trouvée");
        }
        $em->remove($hotel);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_hotel_homepage'));
    }

    public function roomoptionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $hotels = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            //Tools::dump($request->request, true);
            $ad = $request->request->get("ad");
            $enf = $request->request->get("enf");
            $bb = $request->request->get("bb");
            $personne = $request->request->get("personne");
            foreach ($ad as $key => $value) {
                $hotelroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);
                $hotelroom->setAd($value);
                $hotelroom->setBb($bb[$key]);
                $hotelroom->setEnf($enf[$key]);
                if (isset($personne[$key])) {
                    $hotelroom->setPersonne(TRUE);
                } else {
                    $hotelroom->setPersonne(FALSE);
                }
                $em->persist($hotelroom);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('btob_hotel_homepage', array("id" => $id)));
        }
        return $this->render('BtobHotelBundle:Default:roomoption.html.twig', array('hotel' => $hotels));
    }

    public function roomsupAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $hotels = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            
            $hotelsupplement = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelsupplement')->findAll();
            $hotelsroomsupp = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroomsup')->findAll();
            $rr = $request->request->get("rr");
            foreach ($hotelsroomsupp as $key => $value) {
                if (isset($rr[$value->getId()])) {
                    $value->setAct(true);
                } else {
                    $value->setAct(false);
                }
                $em->persist($value);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('btob_hotel_homepage', array("id" => $id)));
        }
        return $this->render('BtobHotelBundle:Default:roomsup.html.twig', array('hotel' => $hotels));
    }

}
