<?php

namespace Btob\SejourBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplements
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SejourBundle\Entity\SupplementsRepository")
 */
class Supplements
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable=true)
     */
    private $price;

    
    /**
     * @ORM\ManyToOne(targetEntity="Sejour", inversedBy="supplements")
     * @ORM\JoinColumn(name="sejour_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $sejour;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set name
     *
     * @param string $name
     * @return Supplements
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Supplements
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
   

    /**
     * Set cuircuit
     *
     * @param \Btob\SejourBundle\Entity\Sejour $sejour
     * @return Supplements
     */
    public function setSejour(\Btob\SejourBundle\Entity\Sejour $sejour = null)
    {
        $this->sejour = $sejour;

        return $this;
    }

    /**
     * Get sejour
     *
     * @return \Btob\SejourBundle\Entity\Sejour 
     */
    public function getSejour()
    {
        return $this->sejour;
    }
}
