<?php

namespace Btob\TransfertBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\TransfertBundle\Entity\Transfert;
use Btob\TransfertBundle\Form\TransfertType;

/**
 * Transfert controller.
 *
 */
class TransfertController extends Controller
{

    /**
     * Lists all Transfert entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobTransfertBundle:Transfert')->findAll();

        return $this->render('BtobTransfertBundle:Transfert:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Transfert entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Transfert();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('transfert_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobTransfertBundle:Transfert:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Transfert entity.
     *
     * @param Transfert $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Transfert $entity)
    {
        $form = $this->createForm(new TransfertType(), $entity, array(
            'action' => $this->generateUrl('transfert_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Transfert entity.
     *
     */
    public function newAction()
    {
        $entity = new Transfert();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobTransfertBundle:Transfert:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Transfert entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobTransfertBundle:Transfert')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transfert entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobTransfertBundle:Transfert:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Transfert entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobTransfertBundle:Transfert')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transfert entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobTransfertBundle:Transfert:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Transfert entity.
    *
    * @param Transfert $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Transfert $entity)
    {
        $form = $this->createForm(new TransfertType(), $entity, array(
            'action' => $this->generateUrl('transfert_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Transfert entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobTransfertBundle:Transfert')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transfert entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('transfert_edit', array('id' => $id)));
        }

        return $this->render('BtobTransfertBundle:Transfert:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Transfert entity.
     *
     */
    public function deleteAction( $id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobTransfertBundle:Transfert')->find($id);


            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('transfert'));
    }

    /**
     * Creates a form to delete a Transfert entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('transfert_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
