<?php

namespace Btob\VoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vol
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoleBundle\Entity\VolRepository")
 */
class Vol
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="vol")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="vol")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="depart", type="string", length=255, nullable=true)
     */
    private $depart;

    /**
     * @var string
     *
     * @ORM\Column(name="arrive", type="string", length=255, nullable=true)
     */
    private $arrive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date", nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dater", type="date", nullable=true)
     */
    private $dater;

    /**
     * @var integer
     *
     * @ORM\Column(name="jeunes", type="integer", nullable=true)
     */
    private $jeunes;

    /**
     * @var integer
     *
     * @ORM\Column(name="etudiant", type="integer", nullable=true)
     */
    private $etudiant;

    /**
     * @var integer
     *
     * @ORM\Column(name="adulte", type="integer", nullable=true)
     */
    private $adulte;

    /**
     * @var integer
     *
     * @ORM\Column(name="enfant", type="integer", nullable=true)
     */
    private $enfant;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;

    /**
     * @var integer
     *
     * @ORM\Column(name="bebe", type="integer")
     */
    private $bebe;

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Vol
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set depart
     *
     * @param string $depart
     * @return Vol
     */
    public function setDepart($depart)
    {
        $this->depart = $depart;

        return $this;
    }

    /**
     * Get depart
     *
     * @return string 
     */
    public function getDepart()
    {
        return $this->depart;
    }

    /**
     * Set arrive
     *
     * @param string $arrive
     * @return Vol
     */
    public function setArrive($arrive)
    {
        $this->arrive = $arrive;

        return $this;
    }

    /**
     * Get arrive
     *
     * @return string 
     */
    public function getArrive()
    {
        return $this->arrive;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Vol
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dater
     *
     * @param \DateTime $dater
     * @return Vol
     */
    public function setDater($dater)
    {
        $this->dater = $dater;

        return $this;
    }

    /**
     * Get dater
     *
     * @return \DateTime 
     */
    public function getDater()
    {
        return $this->dater;
    }

    /**
     * Set jeunes
     *
     * @param integer $jeunes
     * @return Vol
     */
    public function setJeunes($jeunes)
    {
        $this->jeunes = $jeunes;

        return $this;
    }

    /**
     * Get jeunes
     *
     * @return integer 
     */
    public function getJeunes()
    {
        return $this->jeunes;
    }

    /**
     * Set etudiant
     *
     * @param integer $etudiant
     * @return Vol
     */
    public function setEtudiant($etudiant)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return integer 
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set adulte
     *
     * @param integer $adulte
     * @return Vol
     */
    public function setAdulte($adulte)
    {
        $this->adulte = $adulte;

        return $this;
    }

    /**
     * Get adulte
     *
     * @return integer 
     */
    public function getAdulte()
    {
        return $this->adulte;
    }

    /**
     * Set enfant
     *
     * @param integer $enfant
     * @return Vol
     */
    public function setEnfant($enfant)
    {
        $this->enfant = $enfant;

        return $this;
    }

    /**
     * Get enfant
     *
     * @return integer 
     */
    public function getEnfant()
    {
        return $this->enfant;
    }

    /**
     * Set bebe
     *
     * @param integer $bebe
     * @return Vol
     */
    public function setBebe($bebe)
    {
        $this->bebe = $bebe;

        return $this;
    }

    /**
     * Get bebe
     *
     * @return integer 
     */
    public function getBebe()
    {
        return $this->bebe;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Vol
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Vol
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Vol
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }
}
