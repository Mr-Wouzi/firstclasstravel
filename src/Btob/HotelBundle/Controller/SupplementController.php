<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Supplement;
use Btob\HotelBundle\Form\SupplementType;

class SupplementController extends Controller {

    public function indexAction() {
        $supplement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Supplement')
                ->findAll();
        return $this->render('BtobHotelBundle:Supplement:index.html.twig', array('entities' => $supplement));
    }

    public function addAction() {
        $supplement = new Supplement();
        $form = $this->createForm(new SupplementType(), $supplement);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($supplement);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplement_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Supplement:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $supplement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Supplement')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementType(), $supplement);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplement_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Supplement:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Supplement $supplement) {
        $em = $this->getDoctrine()->getManager();

        if (!$supplement) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
        $em->remove($supplement);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplement_homepage'));
    }

}
