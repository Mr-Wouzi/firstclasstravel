<?php

namespace Btob\VoitureBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Btob\VoitureBundle\Entity\Voiture;
use Btob\VoitureBundle\Entity\Imgv;
use Btob\VoitureBundle\Form\voitureType;

/**
 * voiture controller.
 *
 * @Route("/voiture")
 */
class VoitureController extends Controller
{

    /**
     * Lists all voiture entities.
     *
     * @Route("/", name="voiture")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobVoitureBundle:Voiture')->findAll();

        return $this->render('BtobVoitureBundle:Voiture:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new voiture entity.
     *
     * @Route("/", name="voiture_create")
     * @Method("POST")
     * @Template("BtobVoitureBundle:voiture:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new voiture();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
       // $entity->upload();


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('voiture_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a voiture entity.
     *
     * @param voiture $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(voiture $entity)
    {
        $form = $this->createForm(new voitureType(), $entity, array(
            'action' => $this->generateUrl('voiture_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new voiture entity.
     *
     * @Route("/new", name="voiture_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new voiture();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a voiture entity.
     *
     * @Route("/{id}", name="voiture_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find voiture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing voiture entity.
     *
     * @Route("/{id}/edit", name="voiture_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find voiture entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a voiture entity.
    *
    * @param voiture $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(voiture $entity)
    {
        $form = $this->createForm(new voitureType(), $entity, array(
            'action' => $this->generateUrl('voiture_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing voiture entity.
     *
     * @Route("/{id}", name="voiture_update")
     * @Method("PUT")
     * @Template("BtobVoitureBundle:voiture:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find voiture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('voiture_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a voiture entity.
     *
     * @Route("/{id}", name="voiture_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Voiture $voiture) {
        $em = $this->getDoctrine()->getManager();

        if (!$voiture) {
            throw new NotFoundHttpException("voiture non trouvée");
        }
        $em->remove($voiture);
        $em->flush();
        return $this->redirect($this->generateUrl('voiture'));
    }


    /**
     * Creates a form to delete a voiture entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('voiture_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    public function delete1Action(Voiture $voiture) {
        $em = $this->getDoctrine()->getManager();

        if (!$voiture) {
            throw new NotFoundHttpException("voiture non trouvée");
        }
        $em->remove($voiture);
        $em->flush();
        return $this->redirect($this->generateUrl('voiture'));
    }

    public function adAction() {
        $voiture2 = new Voiture();
        $form = $this->createForm(new voitureType(), $voiture2);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
                $em = $this->getDoctrine()->getManager();
                $em->persist($voiture2);
                $em->flush();
                if (is_array($request->request->get("files")))
                    foreach ($request->request->get("files") as $key => $value) {
                        if ($value != "") {
                            $img = new Imgv();
                            $img->setvoiture($voiture2);
                            $img->setImage($value);
                            $em->persist($img);
                            $em->flush();

                        }
                    }
                return $this->redirect($this->generateUrl('voiture'));

        }
        return $this->render('BtobVoitureBundle:Voiture:form.html.twig', array('form' => $form->createView() , 'entity'=>$voiture2));
    }

    public function edit1Action($id) {/*
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $voiture = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new voitureType(), $voiture);
        $form->handleRequest($request);

        if ($form->isValid()) {
            foreach ($voiture->getImgv() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgv();
                        $img->setvoiture($voiture);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('voiture'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobVoitureBundle:Voiture:form.html.twig', array('form' => $form->createView(), 'id' => $id,'entity'=>$voiture)
        );
    }
*/



        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $voiture = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new VoitureType(), $voiture);
        $form->handleRequest($request);

        if ($form->isValid()) {
            foreach ($voiture->getImgv() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgv();
                        $img->setVoiture($voiture);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('voiture'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobVoitureBundle:Voiture:form.html.twig', array('form' => $form->createView(), 'id' => $id,'entity'=>$voiture)
        );

}
}
