<?php

namespace Front\BtobBundle\Controller;

use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\TransfertBundle\Entity\Transfert;
use Btob\TransfertBundle\Entity\Tcategories;
use Btob\TransfertBundle\Form\TransfertType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;


class TransfertController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
		
		$seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Transfert'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $client = new Clients();
        $transfert = new Transfert();
        $form = $this->createForm(new ClientsType(), $client);
        $formTrans = $this->createForm(new TransfertType(), $transfert);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
        

            $dated = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
            $datea = new \Datetime(Tools::explodedate($request->request->get('datea'),'/'));

            $defaultd = $request->request->get('tp-defaultd');
            $defaulta = $request->request->get('tp-defaulta');
            $organisme = $request->request->get('organisme');
            $pointd = $request->request->get('pointd');
            $villed = $request->request->get('villed');
            $villea = $request->request->get('pointd');
            $cat = $request->request->get('cat');
		
            $nb = $request->request->get('nb');


            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();


                $transfert->setClient($client);
                $transfert->setHeurd($defaultd);
                $transfert->setHeura($defaulta);
                $transfert->setOrganisme($organisme);
                $transfert->setDated($dated);
                $transfert->setDatea($datea);
                $transfert->setVillea($villed);
                $transfert->setVilled($villea);
                $transfert->setPointd($pointd);

                $transfert->setAgent($User);

                $em->persist($transfert);
                $em->flush();

                foreach( $cat as $key=>$value){
                    $catg = new Tcategories();
                    $catg->setTransfert($transfert);
                    $catg->setNbr($nb[$key]);
                    $catg->setCategories($cat[$key]);

                    $em->persist($catg);
                    $em->flush();

                }		

             $categories= $this->getDoctrine()->getRepository("BtobTransfertBundle:Tcategories")->findByTransfert($transfert->getId());

				setlocale (LC_TIME, 'fr_FR','fra');               
				date_default_timezone_set("Europe/Paris");                
				mb_internal_encoding("UTF-8");                
				$daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));                
				$dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $to = $transfert->getClient()->getEmail();                                
				$mime_boundary = "----MSA Shipping----" . md5(time());                
				$subject =  "First Class Travel: Réservation Transfert " ;                
				$headers = "From:First-Class-Travel infos.firstclass@gmail.com \n";                
				$headers .= "MIME-Version: 1.0\n";                
				$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";                
				$message1 = "--$mime_boundary\n";                
				$message1 .= "Content-Type: text/html; charset=UTF-8\n";                
				$message1 .= "Content-Transfer-Encoding: 8bit\n\n";                
				$message1 .= "<html>\n";                
				$message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';                
				$message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>				 				<tr>
				<td>				    			
				<a href="http://www.firstclasstravel.ma/">
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
				</a>		
				</td>		
				<td align="right">				    <img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>		
				</td>				  </tr>				 </tbody>		
				</table>';
				$message1 .='				
				' .'			 
				<br /> Cher Madame/Monsieur,<br /><br />				 
				 Merci pour votre réservation. Votre demande a bien été envoyée, nous traiterons votre demande dans les plus brefs délais.<br />
				<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre commande :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Organisme</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $transfert->getOrganisme() . '</td>';
                $message1 .= '</tr>';
                $message1 .= '</tr>';

				 foreach( $categories as $kc=>$valuc){
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Categorie</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $valuc->getCategories() . '</td>';
                $message1 .= '</tr>';
				
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                 $message1 .= '<td height="30">' . $valuc->getNbr() . '</td>';
                $message1 .= '</tr>';
                  }

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Ville de départ</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $transfert->getVilled() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Heure de départ</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $transfert->getHeurd() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Date de départ</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $transfert->getDated()->format('Y-m-d') . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Ville d\'arrivée</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $transfert->getVillea() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Heure d\'arrivée</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $transfert->getHeura() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Date d\'arrivée</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $transfert->getDatea()->format('Y-m-d') . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Point de départ et d\'arrivée</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $transfert->getPointd() . '</td>';
				
				
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">C.I.N / Passeport</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $client->getCin() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Civilité</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' .$client->getCiv() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $client->getPname() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $client->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $client->getTel() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $client->getPays() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $client->getAdresse() . '</td>';
                $message1 .= '</tr>';
				
				$message1 .= '<tr>';                
				$message1 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed V et Av. Hassen I Bloc B - Imm. SIBAM2 40020 - Marrakech
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                        <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
 </td>';                
				$message1 .= '</tr>';
                $message1 .= '</table>';
                $message1 .= '</body>';
				
				mail($to, $subject, $message1, $headers);  
				//mail('afef.tuninfo@gmail.com', $subject, $message1, $headers);              
				
				$admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');                
				$too      = $admin->getEmail();                
				$mime_boundary = "----MSA Shipping----" . md5(time());                
				$subjects =" Réservation  Transfert " ;                
				$header = "From:First-Class-Travel infos.firstclass@gmail.com \n";                
				$header .= "MIME-Version: 1.0\n";                
				$header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";                
				$message2 = "--$mime_boundary\n";                
				$message2 .= "Content-Type: text/html; charset=UTF-8\n";                
				$message2 .= "Content-Transfer-Encoding: 8bit\n\n";                
				$message2 .= "<html>\n";                
				$message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';             
				$message2 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">				 <tbody>		
				<tr>						<td>				    									    			
				<a href="http://www.firstclasstravel.ma/">
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>
				</a>				
				</td>				   <td align="right">				
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>				   </td>				  </tr>				 </tbody>				</table>';
				$message2 .='				
				' .'			 
				
				<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre commande :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Organisme</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $transfert->getOrganisme() . '</td>';
                $message2 .= '</tr>';

				 foreach( $categories as $kc=>$valuc){
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Categorie</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $valuc->getCategories() . '</td>';
                $message1 .= '</tr>';
				
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                 $message1 .= '<td height="30">' . $valuc->getNbr() . '</td>';
                $message1 .= '</tr>';
                  }

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Ville de départ</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $transfert->getVilled() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Heure de départ</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $transfert->getHeurd() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date de départ</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $transfert->getDated()->format('Y-m-d') . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Ville d\'arrivée</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $transfert->getVillea() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Heure d\'arrivée</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $transfert->getHeura() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date d\'arrivée</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $transfert->getDatea()->format('Y-m-d') . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Point de départ et d\'arrivée</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $transfert->getPointd() . '</td>';
                $message2 .= '</tr>';
				
				
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">C.I.N / Passeport</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getCin() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civilité</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' .$client->getCiv() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getPname() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getTel() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getPays() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getAdresse() . '</td>';
                $message2 .= '</tr>';
				
				$message2 .= '<tr>';                
				$message2 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed V et Av. Hassen I Bloc B - Imm. SIBAM2 40020 - Marrakech
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                        <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
 </td>';                
				$message2 .= '</tr>';
                $message2 .= '</table>';
                $message2 .= '</body>';             
				
				mail($too, $subjects, $message2, $header);
                mail('infos.firstclass@gmail.com', $subjects, $message2, $header);
                mail('shamid-fct@menara.ma', $subjects, $message2, $header);
                mail('hmahi-fct@menara.ma', $subjects, $message2, $header);
                mail('firas.benkharrat@gmail.com', $subjects, $message2, $header);
				//mail("afef.tuninfo@gmail.com", $subjects, $message2, $header);
				
				$client = new Clients();
                $transfert = new Transfert();
                $form = $this->createForm(new ClientsType(), $client);
                $formTrans = $this->createForm(new TransfertType(), $transfert);

                // return $this->redirect($this->generateUrl('btob_omra_validation'));
                $request->getSession()->getFlashBag()->add('notice', 'Votre réservation a été bien prise en compte. Notre conseiller vous contactera dans les plus bref délais.');
				

                return $this->render('FrontBtobBundle:Transfert:index.html.twig', array('client' => $form->createView(), 'transfert' => $formTrans->createView(),));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('FrontBtobBundle:Transfert:index.html.twig', array('client' => $form->createView(), 'transfert' => $formTrans->createView(),));
    }
}
