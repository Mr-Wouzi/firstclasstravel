<?php

namespace Btob\CuircuitBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\CuircuitBundle\Entity\Resacircui;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuit")->findby(array('active'=>1));
         $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('BtobCuircuitBundle:Default:index.html.twig', array('entities' => $entities,'user' => $user));

    }

    public function detailAction(Cuircuit $circuit)
    {
        return $this->render('BtobCuircuitBundle:Default:detail.html.twig', array('entry' => $circuit));
    }

    public function reservationAction(Cuircuit $circuit)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $resa=new Resacircui();

                $date = Tools::explodedate($request->request->get("dated"),'/');
                $resa->setDated(new \DateTime($date));
                $resa->setUser($this->get('security.context')->getToken()->getUser());
                $resa->setClient($client);
                $resa->setBebe($request->request->get("bebe"));
                $resa->setAdulte($request->request->get("adulte"));
                $resa->setEnfant($request->request->get("enfant"));
                $resa->setComment($request->request->get("body"));
                $resa->setCircuit($circuit);
                $em->persist($resa);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_circuit_validation'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCuircuitBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$circuit));
    }
    public function validationAction(){
        return $this->render('BtobCuircuitBundle:Default:validation.html.twig', array());
    }
}
