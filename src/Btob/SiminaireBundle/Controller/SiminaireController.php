<?php

namespace Btob\SiminaireBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\SiminaireBundle\Entity\Siminaire;
use Btob\SiminaireBundle\Entity\Imgs;
use Btob\SiminaireBundle\Form\SiminaireType;

/**
 * Siminaire controller.
 *
 */
class SiminaireController extends Controller
{

    /**
     * Lists all Siminaire entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobSiminaireBundle:Siminaire')->findAll();

        return $this->render('BtobSiminaireBundle:Siminaire:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Siminaire entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Siminaire();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgs();
                        $img->setSiminaire($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
         //   return $this->redirect($this->generateUrl('siminaire_show', array('id' => $entity->getId())));
         return $this->redirect($this->generateUrl('siminaire'));
        }

        return $this->render('BtobSiminaireBundle:Siminaire:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Siminaire entity.
     *
     * @param Siminaire $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Siminaire $entity)
    {
        $form = $this->createForm(new SiminaireType(), $entity, array(
            'action' => $this->generateUrl('siminaire_create'),
            'method' => 'POST',
        ));

     //   $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Siminaire entity.
     *
     */
    public function newAction()
    {
        $entity = new Siminaire();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobSiminaireBundle:Siminaire:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Siminaire entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSiminaireBundle:Siminaire')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Siminaire entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobSiminaireBundle:Siminaire:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Siminaire entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSiminaireBundle:Siminaire')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Siminaire entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobSiminaireBundle:Siminaire:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Siminaire entity.
    *
    * @param Siminaire $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Siminaire $entity)
    {
        $form = $this->createForm(new SiminaireType(), $entity, array(
            'action' => $this->generateUrl('siminaire_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

      //  $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Siminaire entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSiminaireBundle:Siminaire')->find($id);
        foreach ($entity->getImgsim() as $key => $value) {
            $em->remove($value);
            $em->flush();
        }
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Siminaire entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgs();
                        $img->setSiminaire($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
           // return $this->redirect($this->generateUrl('siminaire_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('siminaire'));
        }

        return $this->render('BtobSiminaireBundle:Siminaire:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Siminaire entity.
     *
     */
    public function deleteAction( $id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobSiminaireBundle:Siminaire')->find($id);


            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('siminaire'));
    }

    /**
     * Creates a form to delete a Siminaire entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('siminaire_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
