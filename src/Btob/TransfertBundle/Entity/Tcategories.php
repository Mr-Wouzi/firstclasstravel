<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tcategories
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\TcategoriesRepository")
 */
class Tcategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Transfert", inversedBy="tcategories")
     * @ORM\JoinColumn(name="transfert_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $transfert;
    /**
     * @var string
     *
     * @ORM\Column(name="categories", type="string", length=255)
     */
    private $categories;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr", type="integer")
     */
    private $nbr;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categories
     *
     * @param string $categories
     * @return Tcategories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get categories
     *
     * @return string 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set nbr
     *
     * @param integer $nbr
     * @return Tcategories
     */
    public function setNbr($nbr)
    {
        $this->nbr = $nbr;

        return $this;
    }

    /**
     * Get nbr
     *
     * @return integer 
     */
    public function getNbr()
    {
        return $this->nbr;
    }

    /**
     * Set transfert
     *
     * @param \Btob\TransfertBundle\Entity\Transfert $transfert
     * @return Tcategories
     */
    public function setTransfert(\Btob\TransfertBundle\Entity\Transfert $transfert = null)
    {
        $this->transfert = $transfert;

        return $this;
    }

    /**
     * Get transfert
     *
     * @return \Btob\TransfertBundle\Entity\Transfert 
     */
    public function getTransfert()
    {
        return $this->transfert;
    }
}
