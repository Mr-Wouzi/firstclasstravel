<?php

namespace Btob\EvenementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SupplementevType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', array('label' => "Nom de Supplement", 'required' => true))
                ->add('price', null, array('label' => "Prix Adult" , 'required' => true))
                ->add('priceenf', null, array('label' => "Prix Enfant" , 'required' => true))
                ->add('priceb', null, array('label' => "Prix Bébé" , 'required' => true))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\EvenementBundle\Entity\Supplementev'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'btob_evenementbundle_supplementev';
    }

}
