<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * HotelRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class HotelRepository extends EntityRepository
{

    public function findActiv()
    {
        $qb = $this->createQueryBuilder('r')
            ->where("r.act= 1 ")
            ->orderBy('r.name', 'ASC')
            ->getQuery()
            ->getResult();
        return $qb;
    }

}
