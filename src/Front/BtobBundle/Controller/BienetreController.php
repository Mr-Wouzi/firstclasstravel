<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Common\Tools;
use Btob\OmraBundle\Entity\Omra;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\User;
use Btob\BienetreBundle\Entity\Reservationbienetre;
use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\BienetreBundle\Entity\Bienetre;
class BienetreController extends Controller
{

    public function bienetreAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->findby(array('act' => 1));

        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();
        return $this->render('FrontBtobBundle:Bien:index.html.twig', array(
            'entities' => $entities,
            'banner' => $banner,

            ));
    }

    public function detailbienetreAction(Bienetre $Bienetre)
    {
        return $this->render('FrontBtobBundle:Bien:detail.html.twig', array('entry' => $Bienetre));
    }


    public function reservationbienbienetreAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->find($id);
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $reservation = new Reservationbienetre();
                $datea = new \Datetime(Tools::explodedate($request->request->get('datea'),'/'));
                $defaulta = $request->request->get('defaulta');
                $nbrpr = $request->request->get('nbrpr');
                $nbrbien = $request->request->get('nbrbien');

                $reservation->setAgent($User);
                $reservation->setClient($client);
                $reservation->setBienetre($entities);
                $reservation->setDatear($datea);
                $reservation->setNbrbien($nbrbien);
                $reservation->setNbrpr($nbrpr);
                $reservation->setHeur($defaulta);
                $em->persist($reservation);
                $em->flush();
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);

                $request->getSession()->getFlashBag()->add('notiBienetre', 'Votre message a bien été envoyé. Merci.');
                return $this->render('FrontBtobBundle:Bien:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('FrontBtobBundle:Bien:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }




}
