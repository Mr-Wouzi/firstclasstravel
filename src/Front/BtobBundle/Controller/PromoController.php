<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Common\Tools;
use Btob\OmraBundle\Entity\Omra;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\User;
use Btob\SejourBundle\Entity\Sejour;
use Btob\SejourBundle\Entity\Hotel;
use Btob\SejourBundle\Entity\Reservationsejour;
class PromoController extends Controller
{

    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotel")->findby(array('act' => 1));


        return $this->render('FrontBtobBundle:Promo:index.html.twig', array(
            'entities' => $entities,
            ));
    }


}
