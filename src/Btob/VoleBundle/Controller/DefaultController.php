<?php

namespace Btob\VoleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\VoleBundle\Entity\Vol;
use Btob\HotelBundle\Common\Tools;
class DefaultController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $type = $request->request->get('type');
            //    echo 'test'.$type ; exit;
                $depart = $request->request->get('depart');
                $arrive = $request->request->get('arrive');
                $adultjeune = $request->request->get('adult_jeune');
                $adultetud = $request->request->get('adult_etud');
                $adult = $request->request->get('adult');
                $enfant = $request->request->get('enfant');
                $bebe = $request->request->get('bebe');
                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
                $dater = new \Datetime(Tools::explodedate($request->request->get('dater'),'/'));


                $vol = new Vol();
                $vol->setClient($client);
                $vol->setAgent($this->get('security.context')->getToken()->getUser());
                $vol->setType($type);
                $vol->setDepart($depart);
                $vol->setArrive($arrive);
                $vol->setJeunes($adultjeune);
                $vol->setEtudiant($adultetud);
                $vol->setAdulte($adult);
                $vol->setEnfant($enfant);
                $vol->setBebe($bebe);
                $vol->setDated($dated);
                $vol->setDater($dater);
                $em->persist($vol);
                $em->flush();


                $request->getSession()->getFlashBag()->add('noticvol', 'Votre message a bien été envoyé. Merci.');

                return $this->redirect($this->generateUrl('btob_vole_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobVoleBundle:Default:index.html.twig', array('form' => $form->createView(),));
    }


}
