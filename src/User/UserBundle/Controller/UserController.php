<?php



namespace User\UserBundle\Controller;



use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use User\UserBundle\Entity\User;

use User\UserBundle\Form\UserType;

use Btob\HotelBundle\Entity\Hotelmarge;

use Btob\HotelBundle\Entity\HotelmargeRepository;



/**

 * User controller.

 *

 */

class UserController extends Controller

{



    /**

     * Lists all User entities.

     *

     */

    public function indexAction()

    {

        $em = $this->getDoctrine()->getManager();



        $entities = $em->getRepository('UserUserBundle:User')->findAll();



        return $this->render('UserUserBundle:User:index.html.twig', array(

            'entities' => $entities,

        ));

    }



    /**

     * Creates a new User entity.

     *

     */

    public function createAction(Request $request)

    {

        $entity = new User();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);



        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);

            $em->flush();



            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));

        }



        return $this->render('UserUserBundle:User:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Creates a form to create a User entity.

     *

     * @param User $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createCreateForm(User $entity)

    {

        $form = $this->createForm(new UserType(), $entity, array(

            'action' => $this->generateUrl('user_create'),

            'method' => 'POST',

        ));



        $form->add('submit', 'submit', array('label' => 'Create'));



        return $form;

    }



    /**

     * Displays a form to create a new User entity.

     *

     */

    public function newAction()

    {

        $entity = new User();

        $form = $this->createCreateForm($entity);



        return $this->render('UserUserBundle:User:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Finds and displays a User entity.

     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('UserUserBundle:User')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find User entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



        return $this->render('UserUserBundle:User:show.html.twig', array(

            'entity' => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Displays a form to edit an existing User entity.

     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('UserUserBundle:User')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find User entity.');

        }



        $editForm = $this->createEditForm($entity);

        $deleteForm = $this->createDeleteForm($id);





        return $this->render('UserUserBundle:User:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Creates a form to edit a User entity.

     *

     * @param User $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createEditForm(User $entity)

    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new UserType(), $entity, array(

            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));

        $margehotel = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotelmarge')

            ->findByUser($entity->getId());





        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();



                foreach ($hotel as $value) {


                    $hotelmarge = new Hotelmarge();

                    $hotelmarge->setUser($entity);

                    $hotelmarge->setHotel($value);


                    $hotelmarge->setMarge($entity->getMarge());

                    $hotelmarge->setPrst($entity->getPrst());

                    $em->persist($hotelmarge);

                    $em->flush();


                }






        //    $form->add('submit', 'submit', array('label' => 'Update'));



        return $form;

    }

    /**

    $hotel = $this->getDoctrine()

    ->getRepository('BtobHotelBundle:Hotelmarge')

    ->findByAgence($id);

    var_dump($hotel);

    die();

     */

    /**

     * Edits an existing User entity.

     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('UserUserBundle:User')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find User entity.');

        }



        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);

        $margehotel = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotelmarge')

            ->findByAgence($id);

        $count = count($margehotel);

        for ($i=0; $i<$count; $i++) {

            if ($margehotel[$i] == null) {

                $margehotel[$i] = new Hotelmarge();

                $margehotel[$i]->setUser($id);

                $margehotel[$i]->setHotel(8);

                $margehotel[$i]->setMarge($editForm->getData()->getMarge());

                $em->persist($margehotel[1]);



                $em->flush();

            } else {

                $em = $this->getDoctrine()->getManager();

                $update = $margehotel[$i];



                $update->setMarge($editForm->getData()->getMarge());

                $update->setPrst($editForm->getData()->getPrst());

                $em->flush();

            }

        }



        if ($editForm->isValid()) {

            $em->flush();



            return $this->redirect($this->generateUrl('show_users'));

        }



        return $this->render('UserUserBundle:User:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Deletes a User entity.

     *

     */

    public function deleteAction(Request $request, $id)

    {

        $form = $this->createDeleteForm($id);

        $form->handleRequest($request);



        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('UserUserBundle:User')->find($id);



            if (!$entity) {

                throw $this->createNotFoundException('Unable to find User entity.');

            }



            $em->remove($entity);

            $em->flush();

        }



        return $this->redirect($this->generateUrl('user'));

    }



    /**

     * Creates a form to delete a User entity by id.

     *

     * @param mixed $id The entity id

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()

            ->setAction($this->generateUrl('user_delete', array('id' => $id)))

            ->setMethod('DELETE')

            ->add('submit', 'submit', array('label' => 'Delete'))

            ->getForm();

    }

}

