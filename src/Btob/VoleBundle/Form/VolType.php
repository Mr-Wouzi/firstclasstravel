<?php

namespace Btob\VoleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VolType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type')
            ->add('depart')
            ->add('arrive')
            ->add('dated')
            ->add('dater')
            ->add('jeunes')
            ->add('etudiant')
            ->add('adulte')
            ->add('enfant')
            ->add('dcr')
            ->add('bebe')
            ->add('client')
            ->add('agent')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\VoleBundle\Entity\Vol'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_volebundle_vol';
    }
}
