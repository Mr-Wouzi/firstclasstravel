<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Baseroom;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\BaseroomType;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseroomController extends Controller {

    public function indexAction() {
        $Baseroom = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Baseroom')
                ->findAll();
        return $this->render('BtobHotelBundle:Baseroom:index.html.twig', array('entities' => $Baseroom));
    }

    public function addAction() {
        $Baseroom = new Baseroom();
        $form = $this->createForm(new BaseroomType(), $Baseroom);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($Baseroom);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_baseroom_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Baseroom:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $Baseroom = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Baseroom')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new BaseroomType(), $Baseroom);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_baseroom_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Baseroom:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Baseroom $Baseroom) {
        $em = $this->getDoctrine()->getManager();

        if (!$Baseroom) {
            throw new NotFoundHttpException("enregistrement non trouvée");
        }
        $em->remove($Baseroom);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_baseroom_homepage'));
    }



}
