<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\ReservationRepository")
 */
class Reservation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="datetime")
     */
    private $dated;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datef", type="datetime")
     */
    private $datef;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;
    /**
     * @var float
     *
     * @ORM\Column(name="totalint", type="float")
     */
    private $totalint;

    /**
     * @var float
     *
     * @ORM\Column(name="totalbase", type="float")
     */
    private $totalbase;
    /**
     * @var integer
     *
     * @ORM\Column(name="nbnuit", type="integer")
     */
    private $nbnuit;

    /**
     * @var array
     *
     * @ORM\Column(name="namead", type="text",nullable=true)
     */
    private $namead;

    /**
     * @var array
     *
     * @ORM\Column(name="ageenfant", type="text",nullable=true)
     */
    private $ageenfant;

    /**
     * @var array
     *
     * @ORM\Column(name="ageadult", type="text",nullable=true)
     */
    private $ageadult;

    /**
     * @var array
     *
     * @ORM\Column(name="nameenf", type="array",nullable=true)
     */
    private $nameenf;

    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="text",nullable=true)
     */
    private $remarque;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;
    /**
     * @var boolean
     *
     * @ORM\Column(name="del", type="boolean")
     */
    private $del;

    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=50,nullable=true)
     */
    private $ref;
    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="reservation")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;
    /**
     * @ORM\ManyToOne(targetEntity="Clients", inversedBy="reservation")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservation")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Resacomment", mappedBy="reservation", cascade={"remove"})
     */
    protected $resacomment;
    /**
     * @ORM\OneToMany(targetEntity="Reservationdetail", mappedBy="reservation", cascade={"remove"})
     */
    protected $reservationdetail;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resacomment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dcr = new \DateTime();
        $this->nameenf=array();
        $this->namead=array();
        $this->del=true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservation
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Reservation
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     * @return Reservation
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime 
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Reservation
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set totalbase
     *
     * @param float $totalbase
     * @return Reservation
     */
    public function setTotalbase($totalbase)
    {
        $this->totalbase = $totalbase;

        return $this;
    }

    /**
     * Get totalbase
     *
     * @return float 
     */
    public function getTotalbase()
    {
        return $this->totalbase;
    }
    

    /**
     * Set totalint
     *
     * @param float $totalint
     * @return Reservation
     */
    public function setTotalint($totalint)
    {
        $this->totalint = $totalint;

        return $this;
    }

    /**
     * Get totalint
     *
     * @return float 
     */
    public function getTotalint()
    {
        return $this->totalint;
    }

    /**
     * Set nbnuit
     *
     * @param integer $nbnuit
     * @return Reservation
     */
    public function setNbnuit($nbnuit)
    {
        $this->nbnuit = $nbnuit;

        return $this;
    }

    /**
     * Get nbnuit
     *
     * @return integer 
     */
    public function getNbnuit()
    {
        return $this->nbnuit;
    }

    /**
     * Set namead
     *
     * @param string $namead
     * @return Reservation
     */
    public function setNamead($namead)
    {
        $this->namead = $namead;

        return $this;
    }

    /**
     * Get namead
     *
     * @return string 
     */
    public function getNamead()
    {
        return $this->namead;
    }

    /**
     * Set nameenf
     *
     * @param array $nameenf
     * @return Reservation
     */
    public function setNameenf($nameenf)
    {
        $this->nameenf = $nameenf;

        return $this;
    }

    /**
     * Get nameenf
     *
     * @return array 
     */
    public function getNameenf()
    {
        return $this->nameenf;
    }

    /**
     * Set remarque
     *
     * @param string $remarque
     * @return Reservation
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string 
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Reservation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set del
     *
     * @param boolean $del
     * @return Reservation
     */
    public function setDel($del)
    {
        $this->del = $del;

        return $this;
    }

    /**
     * Get del
     *
     * @return boolean 
     */
    public function getDel()
    {
        return $this->del;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Reservation
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Reservation
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservation
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Reservation
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add resacomment
     *
     * @param \Btob\HotelBundle\Entity\Resacomment $resacomment
     * @return Reservation
     */
    public function addResacomment(\Btob\HotelBundle\Entity\Resacomment $resacomment)
    {
        $this->resacomment[] = $resacomment;

        return $this;
    }

    /**
     * Remove resacomment
     *
     * @param \Btob\HotelBundle\Entity\Resacomment $resacomment
     */
    public function removeResacomment(\Btob\HotelBundle\Entity\Resacomment $resacomment)
    {
        $this->resacomment->removeElement($resacomment);
    }

    /**
     * Get resacomment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResacomment()
    {
        return $this->resacomment;
    }

    /**
     * Add reservationdetail
     *
     * @param \Btob\HotelBundle\Entity\Reservationdetail $reservationdetail
     * @return Reservation
     */
    public function addReservationdetail(\Btob\HotelBundle\Entity\Reservationdetail $reservationdetail)
    {
        $this->reservationdetail[] = $reservationdetail;

        return $this;
    }

    /**
     * Remove reservationdetail
     *
     * @param \Btob\HotelBundle\Entity\Reservationdetail $reservationdetail
     */
    public function removeReservationdetail(\Btob\HotelBundle\Entity\Reservationdetail $reservationdetail)
    {
        $this->reservationdetail->removeElement($reservationdetail);
    }

    /**
     * Get reservationdetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservationdetail()
    {
        return $this->reservationdetail;
    }

    /**
     * Set ageenfant
     *
     * @param string $ageenfant
     * @return Reservation
     */
    public function setAgeenfant($ageenfant)
    {
        $this->ageenfant = $ageenfant;

        return $this;
    }

    /**
     * Get ageenfant
     *
     * @return string 
     */
    public function getAgeenfant()
    {
        return $this->ageenfant;
    }

    /**
     * Set ageadult
     *
     * @param string $ageadult
     * @return Reservation
     */
    public function setAgeadult($ageadult)
    {
        $this->ageadult = $ageadult;
        return $this;
    }

    /**
     * Get ageadult
     *
     * @return string
     */
    public function getAgeadult()
    {
        return $this->ageadult;
    }
}
