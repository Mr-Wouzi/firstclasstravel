<?php

namespace Btob\SiminaireBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SiminaireType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pub', NULL, array('label' =>'activer', 'required' => false))
            //->add('region', 'text', array('label' => "Region", 'required' => true))
            ->add('titre', 'text', array('label' => "Titre", 'required' => true))
            ->add('prix', NULL, array('label' =>'Prix' , 'required' => true))
            ->add('nbrjr', NULL, array('label' =>'Nombre des jours' , 'required' => true))
            ->add('description', null, array('label' => "Description ", 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\SiminaireBundle\Entity\Siminaire'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_siminairebundle_siminaire';
    }
}
