<?php

namespace Btob\BannaireBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\BannaireBundle\Entity\Background;
use Btob\BannaireBundle\Form\BackgroundType;

/**
 * Background controller.
 *
 */
class BackgroundController extends Controller
{

    /**
     * Lists all Background entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobBannaireBundle:Background')->findAll();

        return $this->render('BtobBannaireBundle:Background:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Background entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Background();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $entity->upload();
            $image = $entity->getImage();
            if ($image == 1) {
               
                $em->merge($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('backgroundjouteradmin', " insert success");

                return $this->redirect($this->generateUrl('background'));
            } else {
                $em->remove($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('backgroundprbtailleadmin', " probléme de taille d'image il faut que le minimum de width est 1024 pixel et pour height est 348 pixel ");
                return $this->redirect($this->generateUrl('background_new'));
            }
        }

        return $this->render('BtobBannaireBundle:Background:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Background entity.
     *
     * @param Background $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Background $entity)
    {
        $form = $this->createForm(new BackgroundType(), $entity, array(
            'action' => $this->generateUrl('background_create'),
            'method' => 'POST',
        ));

       // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Background entity.
     *
     */
    public function newAction()
    {
        $entity = new Background();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobBannaireBundle:Background:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Background entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBannaireBundle:Background')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Background entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobBannaireBundle:Background:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Background entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBannaireBundle:Background')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Background entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobBannaireBundle:Background:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Background entity.
    *
    * @param Background $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Background $entity)
    {
        $form = $this->createForm(new BackgroundType(), $entity, array(
            'action' => $this->generateUrl('background_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }
    /**
     * Edits an existing Background entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBannaireBundle:Background')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Background entity.');
        }

     //   $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
           $file = $editForm->get("file")->getData();



            if($file != null){
                $entity->upload();

                $image = $entity->getImage();

                if ($image == 1) {
              
                $em->merge($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('backgroundjouteradmin', " insert success");

                return $this->redirect($this->generateUrl('background'));
            } else {
                $this->get('session')->getFlashBag()->add('backgroundprbtailleadminedit', " probléme de taille d'image il faut que le minimum de width est 1600 pixel et pour height est 348 pixel ");
                return $this->redirect($this->generateUrl('background_edit', array('id' => $id)));
            }

            }
        }
        return $this->redirect($this->generateUrl('background'));

      
    }
    /**
     * Deletes a Background entity.
     *
     */
    public function deleteAction( $id)
    {
          $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobBannaireBundle:Background')->find($id);


            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('background'));
    }

    /**
     * Creates a form to delete a Background entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('background_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
