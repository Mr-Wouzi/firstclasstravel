<?php







namespace Btob\ActiviteBundle\Form;







use Symfony\Component\Form\AbstractType;



use Symfony\Component\Form\FormBuilderInterface;



use Symfony\Component\OptionsResolver\OptionsResolverInterface;







class ActiviteType extends AbstractType



{



    /**



     * @param FormBuilderInterface $builder



     * @param array $options



     */



    public function buildForm(FormBuilderInterface $builder, array $options)



    {



        $builder



            ->add('region', NULL, array('label' =>'Région', 'required' => false))



            ->add('title', NULL, array('label' =>'Titre', 'required' => true))



            ->add('libelle', NULL, array('label' =>'Libellé', 'required' => true))



            ->add('prix', NULL, array('label' =>'Prix', 'required' => false))



            ->add('nbj', NULL, array('label' =>'Nombre des jours', 'required' => false))



            ->add('description', NULL, array('label' =>'Description', 'required' => false))



            ->add('act', NULL, array('label' =>'active', 'required' => false))



            ->add('arrangement', NULL, array('label' =>'Arrangement', 'required' => false))



            //->add('type', NULL, array('label' =>'Type', 'required' => false))







        ;



    }



    



    /**



     * @param OptionsResolverInterface $resolver



     */



    public function setDefaultOptions(OptionsResolverInterface $resolver)



    {



        $resolver->setDefaults(array(



            'data_class' => 'Btob\ActiviteBundle\Entity\Activite'



        ));



    }







    /**



     * @return string



     */



    public function getName()



    {



        return 'btob_activitebundle_activite';



    }



}



