<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Email
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\EmailRepository")
 */
class Email
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    

    /**
     * @var string
     *
     * @ORM\Column(name="email_adress", type="string", length=100 , nullable=false)
     */
    private $email_adress;

    /**
     * @var date
     *
     * @ORM\Column(name="dateinsc", type="date")
     */
    private $dateinsc;


     /**
     * @var boolean
     *
     * @ORM\Column(name="statue", type="boolean" , nullable=true)
     */
    private $statue;
	
	
    public  function  __construct(){
        $this->dateinsc = new \DateTime();
        
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set email_adress
     *
     * @param string $email_adress
     * @return Email
     */
    public function setEmail_adress($email_adress)
    {
        $this->email_adress = $email_adress;

        return $this;
    }

    /**
     * Get email_adress
     *
     * @return string 
     */
    public function getEmail_adress()
    {
        return $this->email_adress;
    }

   /**
     * Set dateinsc
     *
     * @param \DateTime $dateinsc
     * @return Email
     */
    public function setDateinsc($dateinsc)
    {
        $this->dateinsc = $dateinsc;

        return $this;
    }

    /**
     * Get dateinsc
     *
     * @return \DateTime 
     */
    public function getDateinsc()
    {
        return $this->dateinsc;
    }
	
	/**
     * Set statue
     *
     * @param boolean $statue
     * @return Email
     */
    public function setStatue($statue)
    {
        $this->statue = $statue;

        return $this;
    }

    /**
     * Get statue
     *
     * @return boolean 
     */
    public function getStatue()
    {
        return $this->statue;
    }
	
}
