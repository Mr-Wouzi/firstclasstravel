<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Stopsales;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\StopsalesType;
use Symfony\Component\HttpFoundation\JsonResponse;

class StopsalesController extends Controller {

    public function indexAction($hotelid) {
        /*$user=$this->getDoctrine()->getRepository('UserUserBundle:User')->getListAgence();
        foreach($user as $value){
            echo $value->getId();
        }*/
        $hotel = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Hotel')
                ->find($hotelid);
        return $this->render('BtobHotelBundle:Stopsales:index.html.twig', array('entities' => $hotel->getStopsales(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function addAction($hotelid) {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $Stopsales = new Stopsales();
        $form = $this->createForm(new StopsalesType(), $Stopsales);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Stopsales->setHotel($hotel);
                $em->persist($Stopsales);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_stopsales_homepage', array("hotelid" => $hotelid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Stopsales:form.html.twig', array('form' => $form->createView(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function editAction($id, $hotelid) {
        $request = $this->get('request');
        $Stopsales = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Stopsales')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new StopsalesType(), $Stopsales);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_stopsales_homepage', array("hotelid" => $hotelid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Stopsales:form.html.twig', array('form' => $form->createView(), 'id' => $id, "hotelid" => $hotelid)
        );
    }

    public function deleteAction(Stopsales $Stopsales, $hotelid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Stopsales) {
            throw new NotFoundHttpException("Stopsales non trouvée");
        }
        $em->remove($Stopsales);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_stopsales_homepage', array("hotelid" => $hotelid)));
    }

}
