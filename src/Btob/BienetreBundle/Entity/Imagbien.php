<?php

namespace Btob\BienetreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imagbien
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\BienetreBundle\Entity\ImagbienRepository")
 */
class Imagbien
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="Bienetre", inversedBy="imagbien")
     * @ORM\JoinColumn(name="bienetre_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $bienetre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imagbien
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imagbien
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set bienetre
     *
     * @param \Btob\BienetreBundle\Entity\Bienetre $bienetre
     * @return Imagbien
     */
    public function setBienetre(\Btob\BienetreBundle\Entity\Bienetre $bienetre = null)
    {
        $this->bienetre = $bienetre;

        return $this;
    }

    /**
     * Get bienetre
     *
     * @return \Btob\BienetreBundle\Entity\Bienetre 
     */
    public function getBienetre()
    {
        return $this->bienetre;
    }
}
