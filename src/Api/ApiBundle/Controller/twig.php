<?php



function builderpage($array){
    $result = '';

    $result .= '<div class="clearfix"></div>';
    $result .= '<br>';

    $result .= '<div id="slideshow">';
    foreach ($array['images'] as $img) {
        $result .= '        <div>';
        $result .= '            <img src="http://www.firstclasstravel.ma'.$img.'">';
        $result .= '        </div>';
    }
    $result .= '</div>';


    $result .= '<form id="form-reserver-selection">';

    $result .= '<div class="col-md-12 col-xs-12 wow fadeInUpBig titre-detail" data-wow-delay="0.2s">';
    $result .= '        <div class="col-md-5 col-xs-5 " >';
    $result .= '            <div class="titre-hotel-detail">'.$array['hotelname'].'</div>';

    for ($i=0; $i < $array['star'] ; $i++) { 
        $result .= '                <i class="fa fa-star orange" aria-hidden="true"></i>';
    }

    $result .= '        </div>';
    $result .= '        <div class="col-md-7 col-xs-7 " align="right">';
    $result .= '            <input type="button" class="bt-reserver bt-reserver-selection" onclick="selectionmodif()" value="Réserver"/>';
    $result .= '        </div>';

    $result .= '    </div>';
    $result .= '    <div class="clearfix"></div>';
    $result .= '    <br>';

    $result .= '<input type="hidden" name="hotelid" value="'.$array['hotelid'].'"/>';
    $result .= '<input type="hidden" name="dated" value="'.$array["dated"]->format("Y-m-d").'"/>';
    $result .= '<input type="hidden" name="datef" value="'.$array['datef']->format("Y-m-d").'"/>';

    $result .= '<div class="col-sm-12" >';

    $entry  = $array['price'];
    $v      = 0;
    $hotel  = $entry['hotel'];
    $foo    = [];
    $idtable= 1;
    $k      = 1;

    foreach ($entry["allprice"] as $itemxy ) {
        $result .= '<table width="100%" id="tabroom'.$idtable.'">';
        $k = 1;
        foreach ($itemxy as $item ) {
            if($k==1){
                $result .= '<thead>';
                $result .= '<tr class="head-table" id="room">';
                array_push($foo, $item['roomname']);
                $result .= '<th colspan="3" class="span7 head-table" > '.$item['roomname'];
                $result .= '<select name="selector" id="selector" class="select-nbrech" onchange="Addrowtotable(\'tabroom'.$idtable.'\',this.value,'.$item["nbpersonne"].', '.$item["ad"].', '.$item["enf"].')">';
                for ($x=0; $x <= 20 ; $x++) { 
                    $result .= '<option ';
                    if(count($itemxy) == $x) 
                        $result .= 'selected="selected"';
                    $result .= ' value="'.$x.'">'.$x.'</option>';
                }
                                
                $result .= '</select>'   ;

                $result .= '<br>';
                $result .= '&Agrave; partir de'  ;   

                if( $entry['pricearr'][0]['persm']){
                            if($item['roomname']=="Chambre single"){

                                if( $entry['sperssupsingle']) {
                                    if($entry['spersss']) {
                                        $prix = $entry['price']+(($entry['price']*$entry['ssupsingle']/100))+((($entry['price']*$entry['ssupsingle']/100))*$entry['smargess']/100) ;
                                    } else {
                                        $prix = $entry['price']+($entry['price']*$entry['ssupsingle']/100)+ $entry['smargess'] ;
                                    }
                                    
                                } else {
                                    if($entry['spersss']){
                                        $prix = $entry['price']+($entry['price']*0/100) +$entry['ssupsingle']+($entry['ssupsingle']*$entry['smargess']/100) ;
                                    }else {
                                        $prix = $entry['price']+($entry['price']*0/100) +$entry['ssupsingle']+$entry['smargess'] ;
                                    }
                                }

                            }else{
                                $prix = $entry['price']+($entry['price']*0/100) ;
                            
                            }
                } else{
                            if($item['roomname']=="Chambre single") {


                                if($entry['sperssupsingle']) {
                                    if($entry['spersss']) {
                                        $prix = $entry['price']+$entry['pricearr'][0]['marge']+(($entry['price'])*$entry['ssupsingle']/100)+((($entry['price'])*$entry['ssupsingle']/100)*$entry['smargess']/100) ;
                                    } else{
                                        $prix = $entry['price']+$entry['pricearr'][0]['marge']+(($entry['price'])*$entry['ssupsingle']/100)+ $entry['smargess'] ;
                                    }
                                } else{

                                    if($entry['spersss']) {

                                        $prix = $entry['price']+$entry['pricearr'][0]['marge'] +$entry['ssupsingle']+($entry['ssupsingle']*$entry['smargess']/100) ;
                                    } else{
                                        $prix = $entry['price']+$entry['pricearr'][0]['marge'] +$entry['ssupsingle']+$entry['smargess'] ;
                                    }

                                }
                            } else{
                                $prix = $entry['price']+$entry['pricearr'][0]['marge'] ;
                            }
                    }

                    $result .='<span class=" tooltip2 bleu-tooltip"> '.$prix.' MAD </span>';

                    if ($itemxy[0]['typepersonne']==true) {
                        $result .= 'par personne';
                    }else {
                        $result .= 'par chambre';
                    }
                    $result .=  'en '.$entry['name'].'</th>';
                    $result .= '</tr> </thead>';
                    $result .= '<tbody id="tb'.$item['nbpersonne'].'" class="cham">';

            }


            $result .= '<tr>';
            $result .= '        <td class="span3 td1-res">';
            $result .= '            <div class="col-md-4 col-xs-6">';

            $result .= '                <label>Adultes :</label>';
            $result .= '                <select ';
            $result .= '                        class="selectmenuforchanging select-nbrech2" ';
            $result .= '                        name="adulte['.$item["id"].']['.$k.'][]" ';
            $result .= '                        id="adulte['.$item['id'].']['.$k.'][]" ';
            $result .= '                        onchange="AdulteCalcul(this,'.$item['nbad'].', '.$item["id"].' ,'.$k.')" > ';

            for ($xy=0; $xy <= $item['nbad']; $xy++) { 
                $result .= '                        <option ';
                if ($xy==$item["ad"])
                    $result .='selected="selected"';
                $result .= '                                value="'.$xy.'">'.$xy.'</option>';
            }

            $result .= '                </select>';
            $result .= '            </div>';



            $result .= '<div class="col-md-4 col-xs-6">';
            $result .= '                <label>Enfants :</label>';
            $result .= '                <select ';
            $result .= '                        class="selectmenuforchanging2 select-nbrech2" ';
            $result .= '                        name="enfant['.$item["id"].']['.$k.'][]" ';
            $result .= '                        id="enfant['.$item["id"].']['.$k.'][]" ';
            $result .= '                        onchange="ChildCalcul(this,'.$item["nbenf"].', '.$item["id"].','.$k.')"';
            $result .= '                            > ';
            for ($xy=0; $xy <= $item['nbenf'] ; $xy++) { 
                $result .= '                        <option ';
                if ($xy==$item["enf"])
                    $result .='selected="selected"';
                $result .= '                                value="'.$xy.'">'.$xy.'</option>';
            }
            $result .= '                </select> ';

                            if ($item['enf']>0 ){
                                $result .= '<div class="span12 listagetodelete">';
                                    for ($it=1; $it <= $item['enf']; $it++) { 
                                     
                                    
                                        $result .= '<label class="labeltochange"> Age Enfant '.$it.' </label>';
                                        $result .= '<select ';
                                        $result .= '        class="selectmenuforchanging3" ';

                                        $result .= '        name="children['.$item["id"].']['.$k.'][]" ';
                                        $result .= '        id="children['.$item["id"].']['.$k.'][]"> ';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="0">0 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="1">1 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="2">2 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="3">3 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="4">4 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="5">5 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="6">6 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="7">7 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="8">8 ';
                                         $result .= '   </option>';
                                        $result .= '    <option selected="selected" ';
                                        $result .= '            value="9">9 ';
                                         $result .= '   </option>';
                                         $result .= '   <option {selected="selected" ';
                                         $result .= '           value="10">10 ';
                                         $result .= '   </option>';
                                         $result .= '   <option {selected="selected" ';
                                         $result .= '           value="11">11 ';
                                         $result .= '   </option>';
                                         $result .= '   <option {selected="selected" ';
                                         $result .= '           value="12">12 ';
                                         $result .= '   </option>';


                                       $result .= ' </select><label class="labeltochangeend"> an(s)';
                                       $result .= ' </label>';
                                        $v++;
                                    }
                               $result .= ' </div>';
                            }
                        $result .= '</div>';

                 $result .= '<div class="col-md-4 col-xs-12">';
                  $result .= '           <label>avec :</label>';
                  
                             $result .= ' <select ';
                             $result .= '        class="selectmenuforchanging4 select-nbrech2" ';
                             $result .= '        name="arrangement['.$item["id"].']['.$k.'][]" ';
                             $result .= '        id="arrangement['.$item["id"].']['.$k.'][]" ';
                              $result .= '           > ';
                              $w = 0;

                              
                                foreach ($entry["pricearr"] as $xarritem ) {
                                  
                                    $result .= '<option value="'.$xarritem["id"].'" ';
                                     if ($item['arr']==$xarritem["idarr"] ) $result .= 'selected="selected" ';
                                     $result .= '>';
                                        $result .= $xarritem["name"] ;
                                        if ($w!=0) { 
                                            $result .= '(+';
                                            if($xarritem["pers"]) { 

                                                if($xarritem["persm"]) {  
                                                    $prixarr = ($entry["price"]*$xarritem["price"]/100)+(($entry["price"]*$xarritem["price"]/100)*$xarritem["marge"]/100) ;
                                                } else { 
                                                    $prixarr = ($entry["price"]*$xarritem["price"]/100)+$xarritem["marge"] ;

                                                }


                                            } else { 
                                                if($xarritem["persm"]) {
                                                    $prixarr = $xarritem["price"]+($xarritem["price"]*$xarritem["marge"]/100) ;
                                                
                                                }else {
                                                    $prixarr = $xarritem["price"]+$xarritem["marge"] ;
                                                }
                                                

                                            }
                                            $result .= $prixarr;
                                            $result .= ' MAD)';
                                    }
                                    $result .= '</option>';

                                    $w=$w+1;  
                                }


                                    
                            $result .= '</select>';
                        $result .= '</div>';
                    $result .= '</td>';
                $result .= '</tr>';
                /*
                <tr class="dontshow">
                    <td colspan="3">
                        <div class="col-md-4 col-xs-12">

                            {% if item['supplement'] is defined %}
                                {% if item['supplement']|length>0 %}
                                    <div class="span6"><label>Suppl&eacute;ment :</label>
                                        <div class="radiosupp">

                                            {% for itemsup in item["supplement"] %}
                                                {% if itemsup[0] is defined %}
                                                    {% if  (itemsup[0].act==1) and (itemsup[0].supplement.category == "Seul") %}


                                                        <input
                                                                class="radiosuppforchanging"
                                                                value="{{ itemsup[1][1] }}"
                                                                type="radio"
                                                                name="supp[{{ item['id'] }}][{{ k }}][]"/>&nbsp;&nbsp;{{ itemsup[0].supplement.name }}
                                                        (
                                                        {% if itemsup[1][0]>0 %}

                                                            {% set prixsupp = itemsup[1][0] %}

                                                        {% else %}
                                                            {% set prixsupp=itemsup[1][0] %}
                                                        {% endif %}
                                                        {{ prixsupp }}
                                                        )<br>
                                                    {% endif %}
                                                {% endif %}
                                            {% endfor %}
                                        </div>
                                    </div>
                                    <div class="span6"><h5>Autres Suppl&eacute;ment(s) :</h5>
                                        {% for itemsup in item["supplement"] %}

                                            {% if itemsup[0] is defined %}
                                                {% if  (itemsup[0].act==1) and (itemsup[0].supplement.category == "Pleusieur") %}
                                                    <div class="radiosupp"> <input
                                                                class="radiosuppforchanging"
                                                                value="{{ itemsup[1][1] }}"
                                                                type="checkbox"
                                                                name="supp[{{ item['id'] }}][{{ k }}][]"/>&nbsp;&nbsp;{{ itemsup[0].supplement.name }}
                                                        (
                                                        {% if itemsup[1][0]>0 %}

                                                            {% set prixsupp = itemsup[1][0] %}

                                                        {% else %}
                                                            {% set prixsupp=itemsup[1][0] %}
                                                        {% endif %}}
                                                        {{ prixsupp }}
                                                        )<br></div>
                                                {% endif %}
                                            {% endif %}

                                        {% endfor %}
                                    </div>
                                {% endif %}
                            {% endif %}
                            {% if item['events']|length>0 %}
                                <div class="span6">
                                    <h5>&Eacute;venements :</h5>
                                    {% for itemev in item['events'] %}
                                        <label class="radioevent">
                                            <input{% if itemev.oblig==1 %} checked="checked" onclick="return false" {% endif %}
                                                    class="radioeventforchanging"
                                                    type="checkbox"
                                                    name="event[{{ item['id'] }}][{{ k }}][]"
                                                    value="{{ itemev.id }}"/>
                                            &nbsp;&nbsp;{{ itemev.name }}
                                            (Adulte
                                            {% if itemev.pricead>0 %}

                                                {% if itemev.pers=="1" %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = entry['price']*itemev.pricead/100+((entry['price']*itemev.pricead/100)*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = (entry['price']*itemev.pricead/100)+itemev.marge %}
                                                    {% endif %}
                                                {% else %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = itemev.pricead+(itemev.pricead*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = itemev.pricead+itemev.marge %}
                                                    {% endif %}
                                                {% endif %}

                                            {% else %}
                                                {% if itemev.pers=="1" %}

                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = (entry['price']*itemev.pricead/100)+((entry['price']*itemev.pricead/100)*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = (entry['price']*itemev.pricead/100)+itemev.marge %}
                                                    {% endif %}
                                                {% else %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = itemev.pricead+(itemev.pricead*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = itemev.pricead+itemev.marge %}
                                                    {% endif %}
                                                {% endif %}
                                            {% endif %}

                                            /
                                            Enfant
                                            {% if itemev.pricech >0 %}
                                                {% if itemev.pers=="1" %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = (entry['price']*itemev.pricech/100)+((entry['price']*itemev.pricech/100)*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = (entry['price']*itemev.pricech/100 )+ itemev.marge %}

                                                    {% endif %}
                                                {% else %}

                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = itemev.pricech+(itemev.pricech*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = itemev.pricech+itemev.marge %}

                                                    {% endif %}


                                                {% endif %}

                                            {% else %}
                                                {% if itemev.pers=="1" %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = entry['price']*itemev.pricech/100+((entry['price']*itemev.pricech/100)*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = (entry['price']*itemev.pricech/100)+itemev.marge %}

                                                    {% endif %}
                                                {% else %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = itemev.pricech+(itemev.pricech*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = itemev.pricech+itemev.marge %}

                                                    {% endif %}
                                                {% endif %}
                                            {% endif %}

                                            )
                                        </label>
                                    {% endfor %}
                                </div>
                            {% endif %}

                        </div>
                    </td>

                </tr>
                {% set k= k+1 %}

                {% endfor %}


                </tbody>

                </table>{% set idtable= idtable +1 %}

            {% endfor %}

*/

        }//end



    }

    return $result;
}

/*
                            
                        <div class="col-md-4 col-xs-12">
                            <label>avec :</label>
                            <select
                                    class="selectmenuforchanging4 select-nbrech2"
                                    name="arrangement[{{ item['id'] }}][{{ k }}][]"
                                    id="arrangement[{{ item['id'] }}][{{ k }}][]"
                                        >
                                {% set w=0 %}
                                {% for xarritem in entry.pricearr %}
                                    <option value="{{ xarritem["id"] }}"{% if item['arr']==xarritem["idarr"] %} selected="selected"{% endif %}>
                                        {{ xarritem["name"] }}{% if w!=0 %} (+
                                            {% if(xarritem["pers"]) %}

                                                {% if(xarritem["persm"]) %}
                                                    {% set prixarr = (entry["price"]*xarritem["price"]/100)+((entry["price"]*xarritem["price"]/100)*xarritem["marge"]/100) %}
                                                {% else %}
                                                    {% set prixarr = (entry["price"]*xarritem["price"]/100)+xarritem["marge"] %}

                                                {% endif %}


                                            {% else %}
                                                {% if(xarritem["persm"]) %}

                                                    {% set prixarr = xarritem["price"]+(xarritem["price"]*xarritem["marge"]/100) %}
                                                {% else %}
                                                    {% set prixarr = xarritem["price"]+xarritem["marge"] %}                          {% endif %}

                                            {% endif %}
                                            {{ prixarr }}
                                            ){% endif %}
                                    </option>

                                    {% set w=w+1 %}   {% endfor %}
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="dontshow">
                    <td colspan="3">
                        <div class="col-md-4 col-xs-12">

                            {% if item['supplement'] is defined %}
                                {% if item['supplement']|length>0 %}
                                    <div class="span6"><label>Suppl&eacute;ment :</label>
                                        <div class="radiosupp">

                                            {% for itemsup in item["supplement"] %}
                                                {% if itemsup[0] is defined %}
                                                    {% if  (itemsup[0].act==1) and (itemsup[0].supplement.category == "Seul") %}


                                                        <input
                                                                class="radiosuppforchanging"
                                                                value="{{ itemsup[1][1] }}"
                                                                type="radio"
                                                                name="supp[{{ item['id'] }}][{{ k }}][]"/>&nbsp;&nbsp;{{ itemsup[0].supplement.name }}
                                                        (
                                                        {% if itemsup[1][0]>0 %}

                                                            {% set prixsupp = itemsup[1][0] %}

                                                        {% else %}
                                                            {% set prixsupp=itemsup[1][0] %}
                                                        {% endif %}
                                                        {{ prixsupp }}
                                                        )<br>
                                                    {% endif %}
                                                {% endif %}
                                            {% endfor %}
                                        </div>
                                    </div>
                                    <div class="span6"><h5>Autres Suppl&eacute;ment(s) :</h5>
                                        {% for itemsup in item["supplement"] %}

                                            {% if itemsup[0] is defined %}
                                                {% if  (itemsup[0].act==1) and (itemsup[0].supplement.category == "Pleusieur") %}
                                                    <div class="radiosupp"> <input
                                                                class="radiosuppforchanging"
                                                                value="{{ itemsup[1][1] }}"
                                                                type="checkbox"
                                                                name="supp[{{ item['id'] }}][{{ k }}][]"/>&nbsp;&nbsp;{{ itemsup[0].supplement.name }}
                                                        (
                                                        {% if itemsup[1][0]>0 %}

                                                            {% set prixsupp = itemsup[1][0] %}

                                                        {% else %}
                                                            {% set prixsupp=itemsup[1][0] %}
                                                        {% endif %}}
                                                        {{ prixsupp }}
                                                        )<br></div>
                                                {% endif %}
                                            {% endif %}

                                        {% endfor %}
                                    </div>
                                {% endif %}
                            {% endif %}
                            {% if item['events']|length>0 %}
                                <div class="span6">
                                    <h5>&Eacute;venements :</h5>
                                    {% for itemev in item['events'] %}
                                        <label class="radioevent">
                                            <input{% if itemev.oblig==1 %} checked="checked" onclick="return false" {% endif %}
                                                    class="radioeventforchanging"
                                                    type="checkbox"
                                                    name="event[{{ item['id'] }}][{{ k }}][]"
                                                    value="{{ itemev.id }}"/>
                                            &nbsp;&nbsp;{{ itemev.name }}
                                            (Adulte
                                            {% if itemev.pricead>0 %}

                                                {% if itemev.pers=="1" %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = entry['price']*itemev.pricead/100+((entry['price']*itemev.pricead/100)*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = (entry['price']*itemev.pricead/100)+itemev.marge %}
                                                    {% endif %}
                                                {% else %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = itemev.pricead+(itemev.pricead*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = itemev.pricead+itemev.marge %}
                                                    {% endif %}
                                                {% endif %}

                                            {% else %}
                                                {% if itemev.pers=="1" %}

                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = (entry['price']*itemev.pricead/100)+((entry['price']*itemev.pricead/100)*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = (entry['price']*itemev.pricead/100)+itemev.marge %}
                                                    {% endif %}
                                                {% else %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = itemev.pricead+(itemev.pricead*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = itemev.pricead+itemev.marge %}
                                                    {% endif %}
                                                {% endif %}
                                            {% endif %}

                                            /
                                            Enfant
                                            {% if itemev.pricech >0 %}
                                                {% if itemev.pers=="1" %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = (entry['price']*itemev.pricech/100)+((entry['price']*itemev.pricech/100)*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = (entry['price']*itemev.pricech/100 )+ itemev.marge %}

                                                    {% endif %}
                                                {% else %}

                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = itemev.pricech+(itemev.pricech*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = itemev.pricech+itemev.marge %}

                                                    {% endif %}


                                                {% endif %}

                                            {% else %}
                                                {% if itemev.pers=="1" %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = entry['price']*itemev.pricech/100+((entry['price']*itemev.pricech/100)*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = (entry['price']*itemev.pricech/100)+itemev.marge %}

                                                    {% endif %}
                                                {% else %}
                                                    {% if itemev.persm=="1" %}
                                                        {% set prixev = itemev.pricech+(itemev.pricech*itemev.marge/100) %}
                                                    {% else %}
                                                        {% set prixev = itemev.pricech+itemev.marge %}

                                                    {% endif %}
                                                {% endif %}
                                            {% endif %}

                                            )
                                        </label>
                                    {% endfor %}
                                </div>
                            {% endif %}

                        </div>
                    </td>

                </tr>
                {% set k= k+1 %}

                {% endfor %}


                </tbody>

                </table>{% set idtable= idtable +1 %}

            {% endfor %}





            <br>
            <section class="ac-container">
                <div>
                    <div class="span12" >
                        {% set idtable=1 %}


                        {% set showit=false %}



                        {% if entry['allotherprice'] is defined %}
                            {% for itemxy in entry["allotherprice"] %}
                                {% set tagvalide=true %}
                                {% set tagduplicated=true %}

                                {% set k=1 %}

                            <table width="100%"  id="roomother{{ idtable }}">

                                <thead>


                                {% for item in itemxy %}

                                {% if foo|length==3 %}
                                    {% for i in 1..2 %}
                                        {% if (item['roomname'] == foo[1]) or (item['roomname'] == foo[2]) %}
                                            {% set tagvalide =false %}
                                        {% endif %}
                                    {% endfor %}
                                {% endif %}

                                {% if foo|length==4 %}
                                    {% for i in 1..3 %}
                                        {% if (item['roomname']==foo[1]) or (item['roomname']==foo[2]) or (item['roomname']==foo[3]) %}
                                            {% set tagvalide=false %}
                                        {% endif %}
                                    {% endfor %}
                                {% endif %}

                                {% if foo|length==5 %}
                                    {% for i in 1..4 %}
                                        {% if (item['roomname']==foo[1]) or (item['roomname']==foo[2]) or (item['roomname']==foo[3]) or (item['roomname']==foo[4]) %}
                                            {% set tagvalide=false %}
                                        {% endif %}
                                    {% endfor %}
                                {% endif %}



                                {% if tagvalide==true and tagduplicated==true %}

                                {% if k==1 %}

                                <tr  class="{{ item['roomname'] }}  head-table"
                                     id="room">


                                    <th
                                            colspan="3" class="span7 head-table"> {{ item['roomname'] }}

                                        <select name="selectors"
                                                id="{{ item['roomname']|replace({' ': '_'}) }}"
                                                class="select-nbrech"
                                                onchange="Addrowtotabledeux('roomother{{ idtable }}',this.value,{{ item['nbpersonne'] }})">
                                            {% for x in 0..20 %}
                                                <option {% if x==0 %} selected="selected"{% endif %}
                                                value="{{ x }}">{{ x }}</option>{% endfor %}
                                        </select><br>

                                        &Agrave; partir de

                                        {% if(entry.pricearr[0]['persm']) %}
                                            {% if(item['roomname']=="Chambre single") %}


                                                {% if(entry['sperssupsingle']) %}
                                                    {% if(entry['spersss']) %}
                                                        {% set prix = entry['price']+((entry['price']*entry['ssupsingle']/100))+(((entry['price']*entry['ssupsingle']/100))*entry['smargess']/100) %}

                                                    {% else %}
                                                        {% set prix = entry['price']+(entry['price']*entry['ssupsingle']/100)+ entry['smargess'] %}
                                                    {% endif %}
                                                {% else %}

                                                    {% if(entry['spersss']) %}

                                                        {% set prix = entry['price']+(entry['price']*0/100) +entry['ssupsingle']+(entry['ssupsingle']*entry['smargess']/100) %}
                                                    {% else %}
                                                        {% set prix = entry['price']+(entry['price']*0/100) +entry['ssupsingle']+entry['smargess'] %}
                                                    {% endif %}

                                                {% endif %}


                                            {% else %}
                                                {% set prix = entry['price']+(entry['price']*0/100) %}
                                            {% endif %}
                                        {% else %}
                                            {% if(item['roomname']=="Chambre single") %}


                                                {% if(entry['sperssupsingle']) %}
                                                    {% if(entry['spersss']) %}
                                                        {% set prix = entry['price']+entry.pricearr[0]['marge']+((entry['price'])*entry['ssupsingle']/100)+(((entry['price'])*entry['ssupsingle']/100)*entry['smargess']/100) %}

                                                    {% else %}
                                                        {% set prix = entry['price']+entry.pricearr[0]['marge']+((entry['price'])*entry['ssupsingle']/100)+ entry['smargess'] %}
                                                    {% endif %}
                                                {% else %}

                                                    {% if(entry['spersss']) %}

                                                        {% set prix = entry['price']+entry.pricearr[0]['marge'] +entry['ssupsingle']+(entry['ssupsingle']*entry['smargess']/100) %}
                                                    {% else %}
                                                        {% set prix = entry['price']+entry.pricearr[0]['marge'] +entry['ssupsingle']+entry['smargess'] %}
                                                    {% endif %}

                                                {% endif %}


                                            {% else %}
                                                {% set prix = entry['price']+entry.pricearr[0]['marge'] %}
                                            {% endif %}
                                        {% endif %}
                                        {{ prix }}
                                        par
                                        personne ou par
                                        chambre en <strong>{{ entry['name'] }}</strong>
                                    </th>


                                </tr>
                                </thead>
                                <tbody id="tb{{ item['nbpersonne'] }}" class="cham">
                                {% endif %}

                                <tr class="">

                                    <td id="{{ item['roomname'] }}" class="span3 td1-res">
                                        <div class="col-md-4 col-xs-6">
                                            <label>Adultes :</label>


                                            {% if item['roomname']=="Chambre single" %}
                                                {% set limitselect=1 %}

                                            {% elseif item['roomname']=="Chambre double" %}
                                                {% set limitselect=2 %}
                                            {% elseif item['roomname']=="Chambre triple" %}
                                                {% set limitselect=3 %}
                                            {% else %}
                                                {% set limitselect=4 %}
                                            {% endif %}


                                            <select
                                                    class="selectmenuforchanging0 select-nbrech2"
                                                    name="adulte[{{ item['id'] }}][{{ k }}][]"
                                                    id="adulte[{{ item['id'] }}][{{ k }}][]"
                                                    onchange="AdulteCalcul(this,{{ item['nbad'] }}, {{ item['id'] }} ,{{ k }})">


                                                {% for xy in 0..item['nbad'] %}
                                                    <option
                                                            {% if xy==item['nbad'] %} selected="selected"{% endif %}
                                                            value="{{ xy }}">{{ xy }}
                                                    </option>
                                                {% endfor %}

                                            </select>
                                        </div>

                                        <div class="col-md-4 col-xs-6">
                                            <label>Enfants :</label>
                                            <select


                                                    class="selectmenuforchanging02 select-nbrech2"
                                                    name="enfant[{{ item['id'] }}][{{ k }}][]"

                                                    onchange="ChildCalcul(this,{{ item['nbenf'] }}, {{ item['id'] }},{{ k }})"
                                                    id="enfant{{ item['id'] }}{{ k }}">
                                                {% for xy in 0..item['nbenf'] %}
                                                    <option

                                                            value="{{ xy }}">{{ xy }}
                                                    </option>
                                                {% endfor %}
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <label>avec :</label>
                                            <select

                                                    class="selectmenuforchanging04 select-nbrech2"
                                                    name="arrangement[{{ item['id'] }}][{{ k }}][]"
                                                    id="arrangement[{{ item['id'] }}][{{ k }}][]">
                                                {% set w=0 %}
                                                {% for xarritem in entry.pricearr %}
                                                    <option value="{{ xarritem["id"] }}"{% if item['arr']==xarritem["idarr"] %} selected="selected"{% endif %}>{{ xarritem["name"] }}{% if w!=0 %} (+
                                                            {% if(xarritem["pers"]) %}

                                                                {% if(xarritem["persm"]) %}
                                                                    {% set prixarr = (entry["price"]*xarritem["price"]/100)+((entry["price"]*xarritem["price"]/100)*xarritem["marge"]/100) %}
                                                                {% else %}
                                                                    {% set prixarr = (entry["price"]*xarritem["price"]/100)+xarritem["marge"] %}

                                                                {% endif %}


                                                            {% else %}
                                                                {% if(xarritem["persm"]) %}

                                                                    {% set prixarr = xarritem["price"]+(xarritem["price"]*xarritem["marge"]/100) %}
                                                                {% else %}
                                                                    {% set prixarr = xarritem["price"]+xarritem["marge"] %}                          {% endif %}

                                                            {% endif %}



                                                            {{ prixarr }}
                                                            ){% endif %}</option>
                                                    {% set w=w+1 %}   {% endfor %}
                                            </select>
                                        </div>

                                    </td>
                                </tr>
                                <tr class="">
                                    <td colspan="3">
                                        <div class="col-md-12 col-xs-12">
                                            {% if item['supplement'] is defined %}
                                                {% if item['supplement']|length>0 %}
                                                    <div class="span6"><label>Suppl&eacute;ment
:</label>
                                                        {% for itemsup in item["supplement"] %}

                                                            {% if itemsup[0] is defined %}
                                                                {% if  (itemsup[0].act==1) and (itemsup[0].supplement.category == "Seul") %}

                                                                    <div class="radiosupp">
                                                                        <input class="supp{{ item['id'] }}{{ k }} radiosuppforchanging0"
                                                                               value="{{ itemsup[1][1] }}"
                                                                               type="radio"
                                                                               name="supp[{{ item['id'] }}][{{ k }}][]"/>&nbsp;&nbsp;{{ itemsup[0].supplement.name }}
                                                                        ( +

                                                                        {% if itemsup[1][0]>0 %}

                                                                            {% set prixsupp = itemsup[1][0] %}

                                                                        {% else %}
                                                                            {% set prixsupp=itemsup[1][0] %}
                                                                        {% endif %}
                                                                        {{ prixsupp }}

                                                                        )<br></div>

                                                                {% endif %}

                                                            {% endif %}



                                                        {% endfor %}

                                                    </div>
                                                    <div class="span6"><h5>Autres Suppl&eacute;ment(s)
                                                            :</h5>
                                                        {% for itemsup in item["supplement"] %}

                                                            {% if itemsup[0] is defined %}
                                                                {% if  (itemsup[0].act==1) and (itemsup[0].supplement.category == "Pleusieur") %}

                                                                    <label class="radiosupp">
                                                                        <input class="supp{{ item['id'] }}{{ k }} radiosuppforchanging0"
                                                                               value="{{ itemsup[1][1] }}"
                                                                               type="checkbox"
                                                                               name="supp[{{ item['id'] }}][{{ k }}][]"/>&nbsp;&nbsp;{{ itemsup[0].supplement.name }}
                                                                        ( +

                                                                        {% if itemsup[1][0]>0 %}

                                                                            {% set prixsupp = itemsup[1][0] %}

                                                                        {% else %}
                                                                            {% set prixsupp=itemsup[1][0] %}
                                                                        {% endif %}
                                                                        {{ prixsupp }}


                                                                        )<br></label>

                                                                {% endif %}

                                                            {% endif %}



                                                        {% endfor %}

                                                    </div>

                                                {% endif %}
                                            {% endif %}
                                            {% if item['events']|length>0 %}
                                                <div class="span6">
                                                    <h5>&Eacute;venements :</h5>
                                                    {% for itemev in item['events'] %}
                                                        <label class="radioevent">
                                                            <input{% if itemev.oblig==1 %} checked="checked" onclick="return false" {% endif %}
                                                                    type="checkbox"
                                                                    class="event{{ item['id'] }}{{ k }} radioeventforchanging0"
                                                                    name="event[{{ item['id'] }}][{{ item['nbpersonne'] }}][]"
                                                                    value="{{ itemev.id }}"/>
                                                            &nbsp;&nbsp;{{ itemev.name }}
                                                            (Adulte
                                                            {% if itemev.pricead>0 %}

                                                                {% if itemev.pers=="1" %}
                                                                    {% if itemev.persm=="1" %}
                                                                        {% set prixev = entry['price']*itemev.pricead/100+((entry['price']*itemev.pricead/100)*itemev.marge/100) %}
                                                                    {% else %}
                                                                        {% set prixev = (entry['price']*itemev.pricead/100)+itemev.marge %}
                                                                    {% endif %}
                                                                {% else %}
                                                                    {% if itemev.persm=="1" %}
                                                                        {% set prixev = itemev.pricead+(itemev.pricead*itemev.marge/100) %}
                                                                    {% else %}
                                                                        {% set prixev = itemev.pricead+itemev.marge %}
                                                                    {% endif %}
                                                                {% endif %}

                                                            {% else %}
                                                                {% if itemev.pers=="1" %}

                                                                    {% if itemev.persm=="1" %}
                                                                        {% set prixev = (entry['price']*itemev.pricead/100)+((entry['price']*itemev.pricead/100)*itemev.marge/100) %}
                                                                    {% else %}
                                                                        {% set prixev = (entry['price']*itemev.pricead/100)+itemev.marge %}
                                                                    {% endif %}
                                                                {% else %}
                                                                    {% if itemev.persm=="1" %}
                                                                        {% set prixev = itemev.pricead+(itemev.pricead*itemev.marge/100) %}
                                                                    {% else %}
                                                                        {% set prixev = itemev.pricead+itemev.marge %}
                                                                    {% endif %}
                                                                {% endif %}
                                                            {% endif %}

                                                            {{ prixev }}
                                                            /
                                                            Enfant
                                                            {% if itemev.pricech >0 %}
                                                                {% if itemev.pers=="1" %}
                                                                    {% if itemev.persm=="1" %}
                                                                        {% set prixev = (entry['price']*itemev.pricech/100)+((entry['price']*itemev.pricech/100)*itemev.marge/100) %}
                                                                    {% else %}
                                                                        {% set prixev = (entry['price']*itemev.pricech/100 )+ itemev.marge %}

                                                                    {% endif %}
                                                                {% else %}

                                                                    {% if itemev.persm=="1" %}
                                                                        {% set prixev = itemev.pricech+(itemev.pricech*itemev.marge/100) %}
                                                                    {% else %}
                                                                        {% set prixev = itemev.pricech+itemev.marge %}

                                                                    {% endif %}


                                                                {% endif %}

                                                            {% else %}
                                                                {% if itemev.pers=="1" %}
                                                                    {% if itemev.persm=="1" %}
                                                                        {% set prixev = entry['price']*itemev.pricech/100+((entry['price']*itemev.pricech/100)*itemev.marge/100) %}
                                                                    {% else %}
                                                                        {% set prixev = (entry['price']*itemev.pricech/100)+itemev.marge %}

                                                                    {% endif %}
                                                                {% else %}
                                                                    {% if itemev.persm=="1" %}
                                                                        {% set prixev = itemev.pricech+(itemev.pricech*itemev.marge/100) %}
                                                                    {% else %}
                                                                        {% set prixev = itemev.pricech+itemev.marge %}

                                                                    {% endif %}
                                                                {% endif %}
                                                            {% endif %}

                                                            )
                                                            MAD )
                                                        </label>
                                                    {% endfor %}
                                                </div>
                                            {% endif %}
                                        </div>
                                    </td>
                                </tr>
                                {% set k= k+1 %}

                                {% set tagduplicated=false %}
                                {% endif %}
                                {% endfor %}
                                </tbody>

                                </table>{% set idtable= idtable +1 %}
                            {% endfor %}
                        {% endif %}
                    </div>
                </div>
            </section>
        </div>

        <div class="col-md-12 col-xs-12" align="right">
            <input type="submit" class="bt-reserver bt-reserver-selection" value="Réserver"/>
        </div>
    </form>
{% endblock %}



*/