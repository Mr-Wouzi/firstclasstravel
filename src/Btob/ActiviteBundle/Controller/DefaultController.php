<?php

namespace Btob\ActiviteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\ActiviteBundle\Entity\Activite;
use Btob\ActiviteBundle\Entity\Resevationact;
use Btob\HotelBundle\Common\Tools;


use Btob\HotelBundle\Form\ClientsType;


use Btob\HotelBundle\Entity\Clients;

class DefaultController extends Controller
{


    public function indexAction($idact)
    {
        $type = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->find($idact);
        $entities = $this->getDoctrine()->getRepository("BtobActiviteBundle:Activite")->findby(array(
            'act' => 1,
            'type'=>$type));
        return $this->render('BtobActiviteBundle:Default:index.html.twig', array('entities' => $entities ,'type'=>$type));
    }

    public function detailAction(Activite $Activite)
    {
        return $this->render('BtobActiviteBundle:Default:detail.html.twig', array('entry' => $Activite));
    }
    public function reservationseactAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobActiviteBundle:Activite")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $datea = new \Datetime(Tools::explodedate($request->request->get('dated'), '/'));
                $nbrad = $request->request->get('nbrad');
                $nbrenf = $request->request->get('nbrenf');
                $bebe = $request->request->get('bebe');
                $remarque = $request->request->get('remarque');

                $reservation = new Resevationact();
                $reservation->setClient($client);
                $reservation->setActivite($entities);
                $reservation->setDated($datea);
                $reservation->setAgent($this->get('security.context')->getToken()->getUser());
                $reservation->setAdulte($nbrad);
                $reservation->setBebe($bebe);
                $reservation->setEnfant($nbrenf);
                $reservation->setCommentaire($remarque);
                $em->persist($reservation);
                $em->flush();


                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notiactiv', 'Votre message a bien été envoyé. Merci.');

                return $this->render('BtobActiviteBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('BtobActiviteBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }


    public function listreservationseactAction($id)
    {
        $entities = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->find($id);

      //  $entities = $this->getDoctrine()->getRepository("BtobActiviteBundle:Resevationact")->findAll();
        return $this->render('BtobActiviteBundle:Default:listreservation.html.twig', array('entities' => $entities));
    }
    public function deletereservationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BtobActiviteBundle:Resevationact')->find($id);
        $idact = $entity->getActivite()->getType()->getId();
        $entities = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->find($idact);
        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('btob_activite_liste_reservation_homepage', array('id' => $idact)));

    }



    public function detailreservationAction(Resevationact $Resevationact)
    {
        $idact = $Resevationact->getActivite()->getType()->getId();
        $entities = $this->getDoctrine()->getRepository("BtobActiviteBundle:Type")->find($idact);
        return $this->render('BtobActiviteBundle:Default:show.html.twig', array('entry' => $Resevationact,'entities' => $entities));
    }


}
