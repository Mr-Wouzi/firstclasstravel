<?php



namespace Front\BtobBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\TeamBundle\Entity\Team;

use Btob\HotelBundle\Common\Tools;

use Btob\OmraBundle\Entity\Omra;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use User\UserBundle\Entity\User;

use Btob\BienetreBundle\Entity\Reservationbienetre;

use Btob\TeamBundle\Entity\Resateam;

use Btob\BienetreBundle\Entity\Bienetre;



class TeamController extends Controller

{



    //  team !!!

    public function indexAction()

    {

        $entities = $this->getDoctrine()->getRepository("BtobTeamBundle:Team")->findby(array('active'=>1));

        $em = $this->getDoctrine()->getManager();


        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        $price_min = 0;
        $price_max = 3000;
        $star = 5;
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
        $dt->modify('+1 day');
        $datef = $dt->format("d/m/Y");
        $nom = "";
        $page = 1;
        $ad = array(0 => 2);
        $enf = array(0 => 0);
        $arr = "";
        // list des pays
        $pays = $this->getDoctrine()->getRepository('BtobHotelBundle:Pays')->findActiv();

        return $this->render('FrontBtobBundle:Team:index.html.twig', array(

            'entities' => $entities,

            'banner' => $banner,

            'pays' => $pays,
            'price_min' => $price_min,
            'price_max' => $price_max,
            'star' => $star,
            'dated' => $dated,
            'datef' => $datef,
            'nom' => $nom,
            'page' => $page,
            'frmad' => $ad,
            'frmenf' => $enf,
            'arr' => $arr,



            ));


    }



    public function detailAction(Team $team)

    {

        return $this->render('FrontBtobBundle:Team:detail.html.twig', array('entry' => $team));

    }

    public function reservationAction(Team $team)

    {

        $em = $this->getDoctrine()->getManager();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();

                $resa=new Resateam();



                $date = Tools::explodedate($request->request->get("dated"),'/');

                $resa->setDated(new \DateTime($date));

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setBebe($request->request->get("bebe"));

                $resa->setAdulte($request->request->get("adulte"));

                $resa->setEnfant($request->request->get("enfant"));

                $resa->setComment($request->request->get("body"));

                $resa->setTeam($team);

                $em->persist($resa);

                $em->flush();

                $request->getSession()->getFlashBag()->add('notifomrafront', 'Votre réservation a bien été envoyé. Merci.');

                return $this->redirect($this->generateUrl('front_btob_team_homepage'));

            } else {

                echo $form->getErrors();

            }

        }

        return $this->render('FrontBtobBundle:Team:reservation.html.twig', array('form' => $form->createView(),'entry'=>$team));

    }







    public function validationAction(){

        return $this->render('BtobTeamBundle:Default:validation.html.twig', array());

    }



}

