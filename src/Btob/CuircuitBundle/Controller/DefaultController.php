<?php

namespace Btob\CuircuitBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\CuircuitBundle\Entity\Resacircui;
use Symfony\Component\Validator\Constraints\DateTime;
use Btob\CuircuitBundle\Entity\Reservationcircuit;

use Btob\CuircuitBundle\Entity\Reservationcdetail;

use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        
        
        
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->listPrice();

       
       
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $circuitprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );        
        
       
        
        $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('BtobCuircuitBundle:Default:index.html.twig', array('entities' => $entities,'circuitprices' => $circuitprices, 'user' => $user));

    }
    
    
    
    

    public function detailAction(Cuircuit $circuit)
    {
        
       $circuitperiode = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());

        return $this->render('BtobCuircuitBundle:Default:detail.html.twig', array('entry' => $circuit,'periods' => $circuitperiode));
    }
    
    
    
    
   public function personalisationAction(Cuircuit $circuit)

    {

       $em = $this->getDoctrine()->getManager();
       
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {
            
            $circuitprice = $request->request->get("circuitprice");
            $session->set('circuitprice', $circuitprice);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
            $nbbebe = $request->request->get("nbbebe");
            $session->set('nbbebe', $nbbebe);
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
            
          return $this->redirect($this->generateUrl('btob_inscrip_reservation_cuircuit', array('id'=>$circuit->getId())));

            
        }
          
       
    return $this->render('BtobCuircuitBundle:Default:personalisation.html.twig', array('entry' => $circuit,'circuitprices' => $circuitprices));

    
    } 
    
    
    public function inscriptionAction(Cuircuit $circuit)

    {

       $em = $this->getDoctrine()->getManager();
       
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());
       $circuitsupp = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Supplementc")->findSupplementByCircuit($circuit->getId());
       
       $circuitreduc = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Reductionc")->findReductionByCircuit($circuit->getId());

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $circuitprice = $session->get("circuitprice");
        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbbebe = $session->get("nbbebe");
        $nbenf = $session->get("nbenf");
        
        $Array = array();
        
        
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                 if(isset($request->request->get("reducad")[$i][$j]))
                 {
                     $adad[$j]['reducad']= $request->request->get("reducad")[$i][$j];
                 }
                 else{
                   $adad[$j]['reducad']=null;  
                 }
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                 if(isset($request->request->get("reducenf")[$i][$k]))
                 {
                     $enf[$k]['reducenf']= $request->request->get("reducenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['reducenf']=null;  
                 }
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
                //bebe
                if(isset($request->request->get("nameb")[$i]))
                {
                     $bebe=array();
                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)
                 {
                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];
                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];
                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];
                
                
                if(isset($request->request->get("suppb")[$i][$l]))
                 {
                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['suppb']=null;  
                 }
                 
                 if(isset($request->request->get("reducb")[$i][$l]))
                 {
                     $bebe[$l]['reducb']= $request->request->get("reducb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['reducb']=null;  
                 }
                 
                 
                 }
                 
                 $Array[$i]['bebe']=$bebe;
                }
                
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('circuitprice', $circuitprice);
            
          

          return $this->redirect($this->generateUrl('btob_resa_reservation_cuircuit', array('id'=>$circuit->getId())));

            
        }
       
       
       
       
    return $this->render('BtobCuircuitBundle:Default:inscription.html.twig', array('entry' => $circuit,'circuitsupp' => $circuitsupp,'circuitreduc' => $circuitreduc,'circuitprices' => $circuitprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf));

    
    }
    
      
    
    
    
/*
    public function reservationAction(Cuircuit $circuit)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $resa=new Resacircui();

                $date = Tools::explodedate($request->request->get("dated"),'/');
                $resa->setDated(new \DateTime($date));
                $resa->setUser($this->get('security.context')->getToken()->getUser());
                $resa->setClient($client);
                $resa->setBebe($request->request->get("bebe"));
                $resa->setAdulte($request->request->get("adulte"));
                $resa->setEnfant($request->request->get("enfant"));
                $resa->setComment($request->request->get("body"));
                $resa->setCircuit($circuit);
                $em->persist($resa);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_circuit_validation'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCuircuitBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$circuit));
    }
    
    
    
    */
    
    
    public function reservationAction(Cuircuit $circuit)

    {

        $em = $this->getDoctrine()->getManager();
      
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $circuitperiode = $session->get("circuitprice");
        
        
       $pricecirc =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuitprice')->find($circuitperiode);   

       
        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           
           $totalreducad=0;
           $totalreducenf=0;
           $totalreducb=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  $reducaddp =0;
               
                  if(isset($val1['reducad']))
                  {
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                  
                  $totalreducad+= $reducaddp;
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              //reduction
              
              
              $reducenfp=0;
                  if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
              $totalreducenf+= $reducenfp;
              
              
              
              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
                  if(isset($valb['suppb']))
                  {
                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
              $totalsuppb+= $suppbp;
              
              
              //reduction
              
              $reducbp=0;
                  if(isset($valb['reducb']))
                  {
                      $reducb=array();
                     foreach ($valb['reducb'] as $k2rb => $val2rb) {
                     $reductionb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2rb);      
                    $reducb[]= $reductionb->getName();
                    $reducbp+= $reductionb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['reducb'] = $reducb;
                  $ArrBase[$key]['bebe'][$k1b]['reducbp'] = $reducbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['reducb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['reducbp'] =0;
                  }
                 
              $totalreducb+= $reducbp;
           }
           }
           
        
          $ArrBase[$key]['price'] = $this->getDoctrine()
                        ->getRepository('BtobCuircuitBundle:Cuircuitprice')
                        ->calcul($pricecirc,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb,$totalreducad,$totalreducenf,$totalreducb);
         
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $circuitprice = $session->get("circuitprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            $User = $this->get('security.context')->getToken()->getUser();

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();
                

                
                
                
                
                
                
                
                

                $resa=new Reservationcircuit();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setCuircuit($circuit);
                $resa->setCuircuitprice($pricecirc);
                
                $resa->setTotal($total);
                  
                $em->persist($resa);

                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           $totalreducad=0;
           $totalreducenf=0;
           $totalreducb=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               $reducaddp =0; 
           
           $resadetail=new Reservationcdetail();
           $resadetail->setReservationcircuit($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  if(isset($val1['reducad']))
                  {
                      
                     

 
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                 
                  $resadetail->setReducad(json_encode($ArrBase[$key]['adult'][$k1]['reducad']));
                  $totalreducad+= $reducaddp;
                  
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               $reducenfp=0;
               
           $resadetails=new Reservationcdetail();
           $resadetails->setReservationcircuit($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
              //reduction
              
              if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
                  
              $resadetails->setReducenf(json_encode($ArrBase[$key]['enf'][$k1e]['reducenf']));
  
              $totalreducenf+= $reducenfp;
              
              
              
           
          
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
               
               $reducbp=0;
               
               $resadetailb=new Reservationcdetail();
           $resadetailb->setReservationcircuit($resa);
           $resadetailb->setChambre($key+1);
           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);
          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);
          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);
                  if(isset($valb['suppb']))
                  {

                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));
              $totalsuppb+= $suppbp;
              
              
              //reduction
              
              if(isset($valb['reducb']))
                  {

                      $reducb=array();
                     foreach ($valb['reducb'] as $k2rb => $val2rb) {
                     $reductionb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2rb);      
                    $reducb[]= $reductionb->getName();
                    $reducbp+= $reductionb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['reducb'] = $reducb;
                  $ArrBase[$key]['bebe'][$k1b]['reducbp'] = $reducbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['reducb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['reducbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setReducb(json_encode($ArrBase[$key]['bebe'][$k1b]['reducb']));
              $totalreducb+= $reducbp;
              

            $em->persist($resadetailb);
            $em->flush();


              
           }
           }
           
        
          $ArrBase[$key]['price'] = $this->getDoctrine()
                        ->getRepository('BtobCuircuitBundle:Cuircuitprice')
                        ->calcul($pricecirc,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb,$totalreducad,$totalreducenf,$totalreducb);
         
          $total+= $ArrBase[$key]['price']; 
          
          

           
       }    
                
                
                
                
                
        
                
               

            } else {
                echo $form->getErrors();
            }

                return $this->redirect($this->generateUrl('btob_circuit_validation'));



        }

        return $this->render('BtobCuircuitBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$circuit,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total));


    }


    
    public function validationAction(){
        return $this->render('BtobCuircuitBundle:Default:validation.html.twig', array());
    }
}
