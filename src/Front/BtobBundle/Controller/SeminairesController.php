<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\ActiviteBundle\Entity\Activite;
use Btob\ActiviteBundle\Entity\Resevationact;
use Btob\HotelBundle\Common\Tools;


use Btob\HotelBundle\Form\ClientsType;


use Btob\HotelBundle\Entity\Clients;

use Btob\CuircuitBundle\Entity\Cuircuit;

use Btob\OmraBundle\Entity\Omra;
use User\UserBundle\Entity\User;
use Btob\HotelBundle\Entity\Hotel;
use Btob\BienetreBundle\Entity\Reservationbienetre;
use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\BienetreBundle\Entity\Bienetre;
use Btob\HotelBundle\Entity\Hotelthemes;
use Btob\SiminaireBundle\Entity\Siminaire;
use Btob\SiminaireBundle\Entity\Reservationsim;
use Btob\SiminaireBundle\Entity\Tsalle;
use Btob\SiminaireBundle\Entity\Reservationcon;
use Btob\SiminaireBundle\Entity\Tacc;

class SeminairesController extends Controller
{
  

    public function reservationsiminairesAction()
    {
        $em = $this->getDoctrine()->getManager();
		$seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Seminaire'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

       
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
               $Users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

                $capacite = $request->request->get('capacite');
                $type = $request->request->get('type');
                $nbsalle = $request->request->get('nbsalle');
                $categorie = $request->request->get('categorie');
                $gala = $request->request->get('gala');
                 if($gala=="on")
                {
                    $gala =true;
                }
                else{
                    
                    $gala =false;
                }
                $incentive = $request->request->get('incentive');
                
                 if($incentive=="on")
                {
                    $incentive =true;
                }
                else{
                    
                    $incentive =false;
                }
                $remarque = $request->request->get('remarque');
                
                
                $nbparticipant = $request->request->get('nbparticipant');
                
                $nbchsingle = $request->request->get('nbchsingle');
                
                $nbchdouble = $request->request->get('nbchdouble');
                
                $nbchtriple = $request->request->get('nbchtriple');
                
                $nbchquadtriple = $request->request->get('nbchquadtriple');
                
                $arrangement = $request->request->get('arrangement');
                
                $lieu = $request->request->get('lieu');
                
                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
                
                $dater = new \Datetime(Tools::explodedate($request->request->get('dater'),'/'));
                
                
                

                $reservation = new Reservationsim();
                $reservation->setAgent($User);
                $reservation->setClient($client);
               
               
                $reservation->setNbsalle($nbsalle);
                $reservation->setCategorie($categorie);
              
                $reservation->setGala($gala);
                
                
                $reservation->setIncentive($incentive);
               
               
                $reservation->setRemarque($remarque);
              
                $reservation->setNbparticipant($nbparticipant);
                
                $reservation->setNbchsingle($nbchsingle);
                
                $reservation->setNbchdouble($nbchdouble);
                
                $reservation->setNbchtriple($nbchtriple);
                
                $reservation->setNbchquadtriple($nbchquadtriple);
                
                $reservation->setArrangement($arrangement);
                $reservation->setLieu($lieu);
                
                $reservation->setDated($dated);
                
                $reservation->setDater($dater);
                
                
                $em->persist($reservation);
                $em->flush();
                
                
                
                
                 foreach( $capacite as $key=>$value){
                    $catg = new Tsalle();
                    $catg->setReservationsim($reservation);
                    $catg->setCapacite($capacite[$key]);
                    $catg->setType($type[$key]);
                   // $catg->setAgeadult(new \Datetime(Tools::explodedate($agead[$key],'/')));
                 
                    $em->persist($catg);
                    $em->flush();

                }
                
                
         $reservationsalles = $this->getDoctrine()->getRepository("BtobSiminaireBundle:Tsalle")->findByRessim($reservation->getId());   

                   
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $reservation->getClient()->getEmail();
               


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "First Class Travel: Réservation Congrés & Séminaire" ;
                $headers = "From:First-Class-Travel infos.firstclass@gmail.com\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">	
				<tbody>				  <tr>				  				<td>		
				<a href="http://www.firstclasstravel.ma/">			
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>		
				</a>							</td>				   <td align="right">					
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>			
				</td>				  </tr>				 </tbody>				</table>';
              	$message1 .='
				 Cher Madame/Monsieur,<br />
				 Merci pour votre réservation. Votre demande a bien été envoyée, nous traiterons votre demande dans les plus brefs délais.<br />
				 <table width="90%"  cellspacing="1" border="0">';
				 
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre commande :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Date de séjour </b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">De ' . $reservation->getDated()->format('Y-m-d') . ' à '. $reservation->getDater()->format('Y-m-d') .' </td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de participants</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbparticipant() . '</td>';
                $message1 .= '</tr>';  

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">La salle plénière</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">';
				foreach( $reservationsalles as $kc=>$valuc){
				   $message1 .= 'Capacité souhaitée: <span>' . $valuc->getCapacite() . '</span> / '; 
          
                     
                      $message1 .= 'Type de salle (auditorium..): <span>' . $valuc->getType() . '</span><br>';
				}
				$message1 .= '</td>';
                $message1 .= '</tr>';                

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de salles souhaitées</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbsalle() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambres single souhaitées</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbchsingle() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambres double  souhaitées</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbchdouble() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambres triple  souhaitées</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbchtriple() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambres quadtriple  souhaitées</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbchquadtriple() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Arrangement souhaité</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getArrangement() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Lieux ou hôtel souhaitée</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getLieu() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Catégorie souhaitée</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getCategorie() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">L\'organisation de visites, soirées ou activités incentive</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">';
				if($reservation->getGala()!=0){
				 $message1 .= 'Soirée de gala - ';
				}
				if($reservation->getIncentive()!=0){
				 $message1 .= 'Activités incentive';
				}
				$message1 .= '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Remarques</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getRemarque(). '</td>';
                $message1 .= '</tr>';
				
				$message1 .= '<tr>';                
				$message1 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed V et Av. Hassen I Bloc B - Imm. SIBAM2 40020 - Marrakech
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                        <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
 </td>';                
				$message1 .= '</tr>';
                $message1 .= '</table>';
                $message1 .= '</body>';



                mail($to, $subject, $message1, $headers);
                //mail('afef.tuninfo@gmail.com', $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservation Congrés & Séminaire" ;
                $header = "From:First-Class-Travel infos.firstclass@gmail.com\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">			
				<tbody>				  <tr>							<td>				 			
				<a href="http://www.firstclasstravel.ma/">		
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>	
				</a>					</td>				   <td align="right">					
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>						</td>				  </tr>				 </tbody>				</table>';
                
				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				 
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre commande :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date de séjour </b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">De ' . $reservation->getDated()->format('Y-m-d') . ' à '. $reservation->getDater()->format('Y-m-d') .' </td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de participants</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbparticipant() . '</td>';
                $message2 .= '</tr>';  

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">La salle plénière</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">';
				foreach( $reservationsalles as $kc=>$valuc){
				   $message2 .= 'Capacité souhaitée: <span>' . $valuc->getCapacite() . '</span> / '; 
          
                     
                      $message2 .= 'Type de salle (auditorium..): <span>' . $valuc->getType() . '</span><br>';
				}
				$message2 .= '</td>';
                $message2 .= '</tr>';                

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de salles souhaitées</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbsalle() . '</td>';
                $message2 .= '</tr>';
$message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">C.I.N / Passeport</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getCin() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civilité</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' .$client->getCiv() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getPname() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getTel() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getPays() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getAdresse() . '</td>';
                $message2 .= '</tr>';
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambres single souhaitées</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbchsingle() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambres double  souhaitées</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbchdouble() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambres triple  souhaitées</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbchtriple() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambres quadtriple  souhaitées</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbchquadtriple() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Arrangement souhaité</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getArrangement() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Lieux ou hôtel souhaitée</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getLieu() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Catégorie souhaitée</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getCategorie() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">L\'organisation de visites, soirées ou activités incentive</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">';
				if($reservation->getGala()!=0){
				 $message2 .= 'Soirée de gala - ';
				}
				if($reservation->getIncentive()!=0){
				 $message2 .= 'Activités incentive';
				}
				$message2 .= '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Remarques</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getRemarque(). '</td>';
                $message2 .= '</tr>';
				
				$message2 .= '<tr>';                
				$message2 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
                Angle Bd. Mohamed V et Av. Hassen I Bloc B - Imm. SIBAM2 40020 - Marrakech
                 <br><br>
                    <font color="orange">RIB :</font></b>145 450 21211 1417080 0006 - 
                        BANQUE POPULAIRE AGENCE EL MENARA  - 
                        <b><font color="orange">Code Swift :</font></b> BCPOMAMC <br>

                        <br> <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a>  - <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a>
 </td>';                
				$message2 .= '</tr>';
                $message2 .= '</table>';
                $message2 .= '</body>';


                mail($too, $subjects, $message2, $header);
                mail('infos.firstclass@gmail.com', $subjects, $message2, $header);
                mail('shamid-fct@menara.ma', $subjects, $message2, $header);
                mail('hmahi-fct@menara.ma', $subjects, $message2, $header);
                mail('firas.benkharrat@gmail.com', $subjects, $message2, $header);
                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);
                
                
                
                
                
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notiSiminaire', 'Votre réservation a été bien prise en compte. Notre conseiller vous contactera dans les plus bref délais.');

                return $this->redirect($this->generateUrl('front_resa_reservation_siminaire'));



            }
        }

        return $this->render('FrontBtobBundle:Siminaire:reservation.html.twig', array('form' => $form->createView()));
    }
	
   public function reservationmedicalAction()
    {
	       $em = $this->getDoctrine()->getManager();
		$seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Seminaire'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta( 'name', 'keywords',$seo->getSeokeyword())
        ;
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
       

       
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
           

               
               $dated = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
               
               $heured = $request->request->get('heured');
               
               $vold = $request->request->get('vold');
               
               
               
            
                $dater = new \Datetime(Tools::explodedate($request->request->get('datef'),'/'));
                $heurea = $request->request->get('heuref');
               
               $vola = $request->request->get('volf');
               
              
               $type = $request->request->get('type');
               
               
               
               if($type=="autres")
                {
                 $type = $request->request->get('autre');
                }
               
                
               
               
               $nomacc = $request->request->get('nom_accompagnant');
               
               
               
               $prenomacc = $request->request->get('prenom_accompagnant');
               
               
               
               
                $hotel1 = $request->request->get('hotel1');
                
                $hotel2 = $request->request->get('hotel2');
                
                
                
                $nbchsingle = $request->request->get('nbchsingle');
                
                $nbnuitesingle = $request->request->get('nbnuitesingle');
                
                $nbchdouble = $request->request->get('nbchdouble');
                
                
                
                $nbnuitedouble = $request->request->get('nbnuitedouble');
   
                $nbchtriple = $request->request->get('nbchtriple');
                
                $nbnuitetwin = $request->request->get('nbnuitetwin');
            
               
                
               $transfert = $request->request->get('transfert');
			   
                if($transfert=="1")
                {
                 $transfert =true;  
                }
                else{
                    
                     $transfert =false;  
                }
               
                
                
                $civ = $request->request->get('civ');
                $organisme = $request->request->get('organisme');
                $prenom = $request->request->get('prenom');
                $nom = $request->request->get('nom');
                $adresse = $request->request->get('adresse');
                $cp = $request->request->get('cp');
                $pays = $request->request->get('pays');
                $ville = $request->request->get('ville');
                $tel = $request->request->get('tel');
                $email = $request->request->get('email');
                $fax = $request->request->get('fax');

                                


                
                
                

                $reservation = new Reservationcon();
                $reservation->setAgent($User);
                
                $reservation->setNbchsingle($nbchsingle);
                $reservation->setNbnuitchsingle($nbnuitesingle);
                $reservation->setNbchdouble($nbchdouble);
                $reservation->setNbnuitchdouble($nbnuitedouble);
                $reservation->setNbchtriple($nbchtriple);
                $reservation->setNbnuitchtriple($nbnuitetwin);
                
                $reservation->setDated($dated);
                $reservation->setDater($dater);
                
                $reservation->setHeurd($heured);
                $reservation->setHeura($heurea);
                
                $reservation->setHotel1($hotel1);
                $reservation->setHotel2($hotel2);
                
                $reservation->setVold($vold);
                $reservation->setVola($vola);
                
                $reservation->setTransfert($transfert);
                $reservation->setType($type);
                
                $reservation->setCiv($civ);
                $reservation->setPname($prenom);
                $reservation->setName($nom);
                $reservation->setEmail($email);
                $reservation->setTel($tel);
                $reservation->setAdresse($adresse);
                $reservation->setCp($cp);
                $reservation->setVille($ville);
                $reservation->setPays($pays);
                $reservation->setOrganisme($organisme);
                $reservation->setFax($fax);               
                
               

                
                $em->persist($reservation);
                $em->flush();
                
                
                
                
                 foreach( $nomacc as $key=>$value){
                    $catg = new Tacc();
                    $catg->setReservationcon($reservation);
                    $catg->setNamead($nomacc[$key]);
                    $catg->setPrenomad($prenomacc[$key]);
                 
                    $em->persist($catg);
                    $em->flush();

                }
                
                
         $reservationsalles = $this->getDoctrine()->getRepository("BtobSiminaireBundle:Tacc")->findByRescon($reservation->getId());   

                 
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $reservation->getEmail();
               


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "First Class Travel: Réservation Congrès ARLAR 2016" ;
                $headers = "From:First-Class-Travel infos.firstclass@gmail.com\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">	
				<tbody>				  <tr>				  				<td>		
				<a href="http://www.firstclasstravel.ma/">			
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>		
				</a>							</td>				   <td align="right">					
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>			
				</td>				  </tr>				 </tbody>				</table>';
              	$message1 .='
				 Cher Madame/Monsieur / Dear Madam, Mister,<br />
				 Merci pour votre réservation. Votre demande a bien été envoyée, nous traiterons votre demande dans les plus brefs délais.<br />
				 Thank you for your reservation. Your request has been sent, we will process your request as soon as possible.<br>
				 <table width="90%"  cellspacing="1" border="0">';
				 
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre commande / Your command :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Date de réservation/Date of reservation </b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d') .' - ' . $reservation->getHeurd() .' --> '. $reservation->getDater()->format('Y-m-d').' - ' . $reservation->getHeura() .' </td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Numéro de Vol Arrivée/Arrival Flight Number </b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30"> '.$reservation->getVold().' </td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Numéro de Vol Départ/Departure Flight Number </b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30"> '.$reservation->getVola().' </td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambre single/Number of single room</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbchsingle() . '</td>';
                $message1 .= '</tr>'; 
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de Nuité chambre single/Number of single room Overnight</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbnuitchsingle() . '</td>';
                $message1 .= '</tr>';  
                
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambre double/Number of double room</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbchdouble() . '</td>';
                $message1 .= '</tr>';  
                
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de Nuité chambre double/Number of double room Overnight</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbnuitchdouble() . '</td>';
                $message1 .= '</tr>';  
                
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambre triple/Number of triple room</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbchtriple() . '</td>';
                $message1 .= '</tr>';  
                
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nombre de Nuité chambre triple/Number of Overnight triple</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getNbnuitchtriple() . '</td>';
                $message1 .= '</tr>';  
                
                
                
                

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Liste des accompagnants/Accompanying list</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">';
				foreach( $reservationsalles as $kc=>$valuc){
				   $message1 .= '<div>' . $valuc->getNamead() .' '. $valuc->getPrenomad() . ' /</div> '; 
          
                     
				}
				$message1 .= '</td>';
                $message1 .= '</tr>';                

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">1er choix d\'hôtel/1st choice of hotel</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getHotel1() . '</td>';
                $message1 .= '</tr>';
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">2ème choix d\'hôtel/2nd choice of hotel</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getHotel2() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Type</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getType() . '</td>';
                $message1 .= '</tr>';
                

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Transfert / Transfer</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">';
				if($reservation->getTransfert()!=0){
				 $message1 .= 'Aéroport/Airport -> Hôtel/Hotel / Hôtel/Hotel -> Aéroport/Airport';
				}else{
				 $message1 .= 'Non';
				}
				
				$message1 .= '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vos coordonnées/Your details :</b></td>';
                $message1 .= '</tr>';
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Civilité/Civility</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getCiv() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom/ First name</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getPname() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom/Last name</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getEmail() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Tél/phone:</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getTel() . '</td>';
                $message1 .= '</tr>';

                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse/Adress:</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getAdresse() . '</td>';
                $message1 .= '</tr>';
                
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Code postal/Post code:</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getCp() . '</td>';
                $message1 .= '</tr>';
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Ville/City:</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getVille() . '</td>';
                $message1 .= '</tr>';
                
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Pays/Country:</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getPays() . '</td>';
                $message1 .= '</tr>';
                
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Organisme:</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getOrganisme() . '</td>';
                $message1 .= '</tr>';
                
                
                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Fax:</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getFax() . '</td>';
                $message1 .= '</tr>';
                
                

               
				
				$message1 .= '<tr>';                
				$message1 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;"> Angle Bd. Mohamed 5 et Av. Hassen 1er N° 7 Bloc B - Imm. SIBAM2 4000 Marrakech Maroc - <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a> <br> <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a> / IBAN/R.I.B : 007 450 00 0098200000160129 / Banque ATTIJARI WAFA BANK <br>Adresse : Boulevard Mohamed VI 40000 Marrakech / Code Swift : BCM AMAMC </td>';                
				$message1 .= '</tr>';
                $message1 .= '</table>';
                $message1 .= '</body>';



                mail($to, $subject, $message1, $headers);
                //mail('afef.tuninfo@gmail.com', $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservation Congrès ARLAR 2016" ;
                $header = "From:First-Class-Travel infos.firstclass@gmail.com\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">			
				<tbody>				  <tr>							<td>				 			
				<a href="http://www.firstclasstravel.ma/">		
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>	
				</a>					</td>				   <td align="right">					
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>						</td>				  </tr>				 </tbody>				</table>';
                
				$message2 .='
				 Cher Madame/Monsieur / Dear Madam, Mister,<br />
				 Merci pour votre réservation. Votre demande a bien été envoyée, nous traiterons votre demande dans les plus brefs délais.<br />
				 Thank you for your reservation. Your request has been sent, we will process your request as soon as possible.<br>
				 <table width="90%"  cellspacing="1" border="0">';
				 
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Votre commande / Your command :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date de réservation/Date of reservation </b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d') .' - ' . $reservation->getHeurd() .' --> '. $reservation->getDater()->format('Y-m-d').' - ' . $reservation->getHeura() .' </td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Numéro de Vol Arrivée/Arrival Flight Number </b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30"> '.$reservation->getVold().' </td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Numéro de Vol Départ/Departure Flight Number </b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30"> '.$reservation->getVola().' </td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambre single/Number of single room</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbchsingle() . '</td>';
                $message2 .= '</tr>'; 
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de Nuité chambre single/Number of single room Overnight</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbnuitchsingle() . '</td>';
                $message2 .= '</tr>';  
                
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambre double/Number of double room</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbchdouble() . '</td>';
                $message2 .= '</tr>';  
                
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de Nuité chambre double/Number of double room Overnight</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbnuitchdouble() . '</td>';
                $message2 .= '</tr>';  
                
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de chambre triple/Number of triple room</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbchtriple() . '</td>';
                $message2 .= '</tr>';  
                
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nombre de Nuité chambre triple/Number of Overnight triple</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getNbnuitchtriple() . '</td>';
                $message2 .= '</tr>';  
                
                
                
                

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Liste des accompagnants/Accompanying list</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">';
				foreach( $reservationsalles as $kc=>$valuc){
				   $message2 .= '<div>' . $valuc->getNamead() .' '. $valuc->getPrenomad() . ' /</div> '; 
          
                     
				}
				$message2 .= '</td>';
                $message2 .= '</tr>';                

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">1er choix d\'hôtel/1st choice of hotel</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getHotel1() . '</td>';
                $message2 .= '</tr>';
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">2ème choix d\'hôtel/2nd choice of hotel</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getHotel2() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Type</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getType() . '</td>';
                $message2 .= '</tr>';
                

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Transfert / Transfer</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">';
				if($reservation->getTransfert()!=0){
				 $message2 .= 'Aéroport/Airport -> Hôtel/Hotel / Hôtel/Hotel -> Aéroport/Airport';
				}else{
				 $message2 .= 'Non';
				}
				
				$message2 .= '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vos coordonnées/Your details :</b></td>';
                $message2 .= '</tr>';
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civilité/Civility</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getCiv() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom/ First name</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getPname() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom/Last name</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getEmail() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél/phone:</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getTel() . '</td>';
                $message2 .= '</tr>';

                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse/Adress:</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getAdresse() . '</td>';
                $message2 .= '</tr>';
                
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Code postal/Post code:</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getCp() . '</td>';
                $message2 .= '</tr>';
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Ville/City:</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getVille() . '</td>';
                $message2 .= '</tr>';
                
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays/Country:</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getPays() . '</td>';
                $message2 .= '</tr>';
                
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Organisme:</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getOrganisme() . '</td>';
                $message2 .= '</tr>';
                
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Fax:</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getFax() . '</td>';
                $message2 .= '</tr>';
                
                

               
				
				$message2 .= '<tr>';                
				$message2 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;"> Angle Bd. Mohamed 5 et Av. Hassen 1er N° 7 Bloc B - Imm. SIBAM2 4000 Marrakech Maroc - <a style="color:#fff;" href="mailto:infos.firstclass@gmail.com" target="_blank">infos.firstclass@gmail.com</a> <br> <a style="color:#fff;" href="http://www.firstclasstravel.ma/">www.firstclasstravel.ma</a> / IBAN/R.I.B : 007 450 00 0098200000160129 / Banque ATTIJARI WAFA BANK <br>Adresse : Boulevard Mohamed VI 40000 Marrakech / Code Swift : BCM AMAMC </td>';                
				$message2 .= '</tr>';
                $message2 .= '</table>';
                $message2 .= '</body>';


                mail($too, $subjects, $message2, $header);
                mail('shamid-fct@menara.ma', $subjects, $message2, $header);
                mail('hmahi-fct@menara.ma', $subjects, $message2, $header);
                mail('firas.benkharrat@gmail.com', $subjects, $message2, $header);
                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);
                
                
                
                
               
                $request->getSession()->getFlashBag()->add('notimedical', 'Votre réservation a été bien prise en compte. Notre conseiller vous contactera dans les plus bref délais.');

                return $this->redirect($this->generateUrl('front_resa_reservation_medical'));



        
        }

        return $this->render('FrontBtobBundle:Siminaire:medical.html.twig');
	
		

	
    }

    public function congresAction()
    {
        $em = $this->getDoctrine()->getManager();
        $seo = $this->getDoctrine()->getRepository('BtobHotelBundle:Configseo')->findOneBy(array('page' => 'Seminaire'));

        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seo->getSeotitle())
            ->addMeta('name', 'description', $seo->getSeodescreption())
            ->addMeta('name', 'keywords', $seo->getSeokeyword());
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');


        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $Users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

            $civ = $request->request->get('civ');
            $organisme = $request->request->get('organisme');
            $prenom = $request->request->get('prenom');
            $nom = $request->request->get('nom');
            $adresse = $request->request->get('adresse');
            $cp = $request->request->get('cp');
            $pays = $request->request->get('pays');
            $ville = $request->request->get('ville');
            $tel = $request->request->get('tel');
            $email = $request->request->get('email');
            $fax = $request->request->get('fax');
            $vold = $request->request->get('vold');
            $dated = $request->request->get('dated');
            $volf = $request->request->get('volf');
            $datef = $request->request->get('datef');
            $hotel1 = $request->request->get('hotel1');
            $hotel2 = $request->request->get('hotel2');
            $nbrsingle = $request->request->get('nbrsingle');
            $nbredoublee = $request->request->get('nbredoublee');
            $transfert = $request->request->get('transfert');
            $msg = $request->request->get('message');

            $double = $request->request->get('double');
            $single = $request->request->get('single');
			
			
			

            if ($transfert == "1") {
                $transfert = "Aéroport <span style='color:#f38f1b;'>/</span> Airport <span style='color:#f38f1b;'>/</span> аэропорт (Marrakech) -> Hôtel <span style='color:#f38f1b;'>/</span> Hotel <span style='color:#f38f1b;'>/</span> отель";
            } else {

                $transfert = "Aéroport <span style='color:#f38f1b;'>/</span> Airport <span style='color:#f38f1b;'>/</span> аэропорт (Casablanca) -> Hôtel <span style='color:#f38f1b;'>/</span> Hotel <span style='color:#f38f1b;'>/</span> отель";
            }


            setlocale(LC_TIME, 'fr_FR', 'fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%d %B %Y ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%A ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));


            $to = $email;


            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject = "First Class Travel: Réservation Séminaire Makroradast";
            $headers = "From:First-Class-Travel infos.firstclass@gmail.com\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">	
				<tbody>				  <tr>				  				<td>		
				<a href="http://www.firstclasstravel.ma/">			
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>		
				</a>							</td>				   <td align="right">					
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>			
				</td>				  </tr>				 </tbody>				</table>';
            $message1 .= '
				 Cher Madame/Monsieur,<br />
				 Merci pour votre réservation. Votre demande a bien été envoyée, nous traiterons votre demande dans les plus brefs délais.<br />
				 Thank you for your reservation. Your request has been sent, we will process your request as soon as possible. <br>
				 Благодарим Вас за бронирование. Ваш запрос был отправлен, мы обработаем Ваш запрос как можно скорее<br>
				 <table width="90%"  cellspacing="1" border="0">';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> INFORMATIONS SUR LE CLIENT <span style="color:#f38f1b;">/</span> CUSTOMER INFORMATION <span style="color:#f38f1b;">/</span> ИНФОРМАЦИЯ О ЗАКАЗЧИКЕ :</b></td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Organisme <span style="color:#f38f1b;">/</span> Organization <span style="color:#f38f1b;">/</span> тело </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $organisme . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Civilité <span style="color:#f38f1b;">/</span> Civility <span style="color:#f38f1b;">/</span> вежливость  </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $civ . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Nom <span style="color:#f38f1b;">/</span> Surname <span style="color:#f38f1b;">/</span> Фамилия </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $nom . ' </td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom <span style="color:#f38f1b;">/</span> Name <span style="color:#f38f1b;">/</span> имя </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $prenom . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse <span style="color:#f38f1b;">/</span> Address <span style="color:#f38f1b;">/</span> адрес  </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $adresse . ' </td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Code postal <span style="color:#f38f1b;">/</span> Post code <span style="color:#f38f1b;">/</span> почтовый индекс  </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $cp . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Pays <span style="color:#f38f1b;">/</span> Country <span style="color:#f38f1b;">/</span> страна  </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $pays . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Ville <span style="color:#f38f1b;">/</span> City <span style="color:#f38f1b;">/</span> город  </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $ville . ' </td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Tél <span style="color:#f38f1b;">/</span> phone <span style="color:#f38f1b;">/</span> телефон  </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $tel . ' </td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> E-mail <span style="color:#f38f1b;">/</span> Электронный адрес  </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $email . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Fax <span style="color:#f38f1b;">/</span> факс  </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $fax . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vol <span style="color:#f38f1b;">/</span> Flight <span style="color:#f38f1b;">/</span> полет :</b></td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Date Départ <span style="color:#f38f1b;">/</span> Дата выезда </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $dated . ' </td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> N° de vol <span style="color:#f38f1b;">/</span> N° of flight <span style="color:#f38f1b;">/</span> Номер рейса </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $vold . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Date Arrivée <span style="color:#f38f1b;">/</span> Arrival Date <span style="color:#f38f1b;">/</span> Дата прибытия </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $datef . ' </td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> N° de vol <span style="color:#f38f1b;">/</span> N° of flight <span style="color:#f38f1b;">/</span> Номер рейса </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $volf . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Hôtels <span style="color:#f38f1b;">/</span> Hotels <span style="color:#f38f1b;">/</span> отели :</b></td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> 1er choix d\'hôtel <span style="color:#f38f1b;">/</span> 1st choice of hotel <span style="color:#f38f1b;">/</span> 1ый выбор отеля </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $hotel1 . ' </td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> 2ème choix d\'hôtel <span style="color:#f38f1b;">/</span> 2nd choice of hotel <span style="color:#f38f1b;">/</span> 2ый выбор отеля </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $hotel2 . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Nombre chambre single <span style="color:#f38f1b;">/</span> Number single room <span style="color:#f38f1b;">/</span> Номер одноместный номер </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $nbrsingle . ' </td>';
            $message1 .= '</tr>';

            if($nbrsingle>0)
            {
			
				foreach( $single as $key=>$value){
						$nbre = $key+1;
						$message1 .= '<tr>';
						$message1 .= '<td height="30"><b style="padding-left:10px;"> Simple <span style="color:#f38f1b;">/</span> Single <span style="color:#f38f1b;">/</span> простой '.$nbre.'  </b></td>';
						$message1 .= '<td height="30"><b>:</b></td>';
						$message1 .= '<td height="30"> ' . $single[$key] . ' </td>';
						$message1 .= '</tr>';
				}
                
            }

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Nombre chambre double <span style="color:#f38f1b;">/</span> Number double room <span style="color:#f38f1b;">/</span> Номер двухместный номер </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $nbredoublee . ' </td>';
            $message1 .= '</tr>';

            if($nbredoublee>0)
            {
			
			
			$nbres=0;
			foreach( $double as $keys=>$valuee){
					if($keys % 2 ==""){
						$nbres = $nbres+1;
					}
					$message1 .= '<tr>';
					$message1 .= '<td height="30"><b style="padding-left:10px;"> Double <span style="color:#f38f1b;">/</span> двойной '.$nbres.'  </b></td>';
                    $message1 .= '<td height="30"><b>:</b></td>';
                    $message1 .= '<td height="30"> ' . $double[$keys] . ' </td>';
                    $message1 .= '</tr>';
			}
                
            }

            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Transfert <span style="color:#f38f1b;">/</span> Transfer <span style="color:#f38f1b;">/</span> перевод :</b></td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Transfert <span style="color:#f38f1b;">/</span> Transfer <span style="color:#f38f1b;">/</span> перевод </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $transfert . ' </td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> INFORMATIONS SUPPLÉMENTAIRES <span style="color:#f38f1b;">/</span> ADDITIONAL INFORMATION <span style="color:#f38f1b;">/</span> ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ:</b></td>';
            $message1 .= '</tr>';
			
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;"> Message <span style="color:#f38f1b;">/</span> сообщение </b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30"> ' . $msg . ' </td>';
            $message1 .= '</tr>';


            $message1 .= '<tr>';
            $message1 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
               <a style="color:#fff;" href="mailto:firstclasstravel@mail.ru" target="_blank">firstclasstravel@mail.ru</a>  - <a style="color:#fff;" href="mailto:zahiri-fct@menara.ma" target="_blank">zahiri-fct@menara.ma</a>
			</td>';
            $message1 .= '</tr>';
            $message1 .= '</table>';
            $message1 .= '</body>';


            mail($to, $subject, $message1, $headers);
            //mail('afef.tuninfo@gmail.com', $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

            $too = $admin->getEmail();

            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subjects = "Réservation Séminaire Makroradast";
            $header = "From:First-Class-Travel infos.firstclass@gmail.com\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message2 = "--$mime_boundary\n";
            $message2 .= "Content-Type: text/html; charset=UTF-8\n";
            $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message2 .= "<html>\n";
            $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
            $message2 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0">			
				<tbody>				  <tr>							<td>				 			
				<a href="http://www.firstclasstravel.ma/">		
				<img src="http://www.firstclasstravel.ma/front/images/logo.png"/>	
				</a>					</td>				   <td align="right">					
				<img src="http://www.firstclasstravel.ma/front/images/telmail2.png"/>						</td>				  </tr>				 </tbody>				</table>';

            $message2 .= '<table width="90%"  cellspacing="1" border="0">';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> INFORMATIONS SUR LE CLIENT <span style="color:#f38f1b;">/</span> CUSTOMER INFORMATION <span style="color:#f38f1b;">/</span> ИНФОРМАЦИЯ О ЗАКАЗЧИКЕ :</b></td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Organisme <span style="color:#f38f1b;">/</span> Organization <span style="color:#f38f1b;">/</span> тело </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $organisme . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;">Civilité <span style="color:#f38f1b;">/</span> Civility <span style="color:#f38f1b;">/</span> вежливость  </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $civ . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;">Nom <span style="color:#f38f1b;">/</span> Surname <span style="color:#f38f1b;">/</span> Фамилия </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $nom . ' </td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom <span style="color:#f38f1b;">/</span> Name <span style="color:#f38f1b;">/</span> имя </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $prenom . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse <span style="color:#f38f1b;">/</span> Address <span style="color:#f38f1b;">/</span> адрес  </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $adresse . ' </td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;">Code postal <span style="color:#f38f1b;">/</span> Post code <span style="color:#f38f1b;">/</span> почтовый индекс  </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $cp . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Pays <span style="color:#f38f1b;">/</span> Country <span style="color:#f38f1b;">/</span> страна  </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $pays . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Ville <span style="color:#f38f1b;">/</span> City <span style="color:#f38f1b;">/</span> город  </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $ville . ' </td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Tél <span style="color:#f38f1b;">/</span> phone <span style="color:#f38f1b;">/</span> телефон  </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $tel . ' </td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> E-mail <span style="color:#f38f1b;">/</span> Электронный адрес  </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $email . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Fax <span style="color:#f38f1b;">/</span> факс  </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $fax . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Vol <span style="color:#f38f1b;">/</span> Flight <span style="color:#f38f1b;">/</span> полет :</b></td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Date Départ <span style="color:#f38f1b;">/</span> Дата выезда </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $dated . ' </td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> N° de vol <span style="color:#f38f1b;">/</span> N° of flight <span style="color:#f38f1b;">/</span> Номер рейса </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $vold . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Date Arrivée <span style="color:#f38f1b;">/</span> Arrival Date <span style="color:#f38f1b;">/</span> Дата прибытия </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $datef . ' </td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> N° de vol <span style="color:#f38f1b;">/</span> N° of flight <span style="color:#f38f1b;">/</span> Номер рейса </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $volf . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Hôtels <span style="color:#f38f1b;">/</span> Hotels <span style="color:#f38f1b;">/</span> отели :</b></td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> 1er choix d\'hôtel <span style="color:#f38f1b;">/</span> 1st choice of hotel <span style="color:#f38f1b;">/</span> 1ый выбор отеля </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $hotel1 . ' </td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> 2ème choix d\'hôtel <span style="color:#f38f1b;">/</span> 2nd choice of hotel <span style="color:#f38f1b;">/</span> 2ый выбор отеля </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $hotel2 . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Nombre chambre single <span style="color:#f38f1b;">/</span> Number single room <span style="color:#f38f1b;">/</span> Номер одноместный номер </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $nbrsingle . ' </td>';
            $message2 .= '</tr>';

            if($nbrsingle>0)
            {
			
				foreach( $single as $key=>$value){
						$nbre = $key+1;
						$message2 .= '<tr>';
						$message2 .= '<td height="30"><b style="padding-left:10px;"> Simple <span style="color:#f38f1b;">/</span> Single <span style="color:#f38f1b;">/</span> простой '.$nbre.'  </b></td>';
						$message2 .= '<td height="30"><b>:</b></td>';
						$message2 .= '<td height="30"> ' . $single[$key] . ' </td>';
						$message2 .= '</tr>';
				}
                
            }

            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Nombre chambre double <span style="color:#f38f1b;">/</span> Number double room <span style="color:#f38f1b;">/</span> Номер двухместный номер </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $nbredoublee . ' </td>';
            $message2 .= '</tr>';

            if($nbredoublee>0)
            {
			
			
			$nbres=0;
			foreach( $double as $keys=>$valuee){
					if($keys % 2 ==""){
						$nbres = $nbres+1;
					}
					$message2 .= '<tr>';
					$message2 .= '<td height="30"><b style="padding-left:10px;"> Double <span style="color:#f38f1b;">/</span> двойной '.$nbres.'  </b></td>';
                    $message2 .= '<td height="30"><b>:</b></td>';
                    $message2 .= '<td height="30"> ' . $double[$keys] . ' </td>';
                    $message2 .= '</tr>';
			}
                
            }

            $message2 .= '<tr>';
            $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> Transfert <span style="color:#f38f1b;">/</span> Transfer <span style="color:#f38f1b;">/</span> перевод :</b></td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Transfert <span style="color:#f38f1b;">/</span> Transfer <span style="color:#f38f1b;">/</span> перевод </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $transfert . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" colspan="3" bgcolor="#2487e3"><b style="color:#fff; padding-left:5px;"> INFORMATIONS SUPPLÉMENTAIRES <span style="color:#f38f1b;">/</span> ADDITIONAL INFORMATION <span style="color:#f38f1b;">/</span> ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ:</b></td>';
            $message2 .= '</tr>';
			
            $message2 .= '<tr>';
            $message2 .= '<td height="30"><b style="padding-left:10px;"> Message <span style="color:#f38f1b;">/</span> сообщение </b></td>';
            $message2 .= '<td height="30"><b>:</b></td>';
            $message2 .= '<td height="30"> ' . $msg . ' </td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="50" colspan="3" bgcolor="#2487e3" align="center" style="color:#fff;">
               <a style="color:#fff;" href="mailto:firstclasstravel@mail.ru" target="_blank">firstclasstravel@mail.ru</a>  - <a style="color:#fff;" href="mailto:zahiri-fct@menara.ma" target="_blank">zahiri-fct@menara.ma</a>
			</td>';
            $message2 .= '</tr>';
            $message2 .= '</table>';
            $message2 .= '</body>';


            //mail($too, $subjects, $message2, $header);
            mail('hamid.firstclasstravel@gmail.com', $subjects, $message2, $header);
            mail('shamid-fct@menara.ma', $subjects, $message2, $header);
            mail('firas.benkharrat@gmail.com', $subjects, $message2, $header);
            mail('zahiri-fct@menara.ma', $subjects, $message2, $header);
            mail('firstclasstravel@mail.ru', $subjects, $message2, $header);
            mail('riahi.ustapha@gmail.com', $subjects, $message2, $header);
            //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);


            $request->getSession()->getFlashBag()->add('notifrontcongres', 'Votre réservation a été bien prise en compte. Notre conseiller vous contactera dans les plus bref délais. / Your reservation has been well considered. Your advisor will contact you shortly. / Ваше бронирование было хорошо продуманы. Ваш консультант свяжется с Вами в ближайшее время.');

            return $this->redirect($this->generateUrl('front_congres'));


        }

        return $this->render('FrontBtobBundle:Siminaire:congres.html.twig');


    }
}
