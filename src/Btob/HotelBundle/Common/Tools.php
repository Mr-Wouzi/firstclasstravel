<?php

namespace Btob\HotelBundle\Common;

class Tools
{
    public static function calculemarge($price, $margepers, $margeval)
    {
        if ($margepers == 1) {
            $price = $price;
        } else {
            $price = $price ;
        }
        return $price;
    }

    public static function array_sort($array, $on, $order = SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    public static function encrypt($data)
    {
        $key = "secret";  // Clé de 8 caractères max
        $data = serialize($data);
        $td = mcrypt_module_open(MCRYPT_DES, "", MCRYPT_MODE_ECB, "");
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $data = base64_encode(mcrypt_generic($td, '!' . $data));
        mcrypt_generic_deinit($td);
        return $data;
    }

    public static function decrypt($data)
    {
        $key = "secret";
        $td = mcrypt_module_open(MCRYPT_DES, "", MCRYPT_MODE_ECB, "");
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $data = mdecrypt_generic($td, base64_decode($data));
        mcrypt_generic_deinit($td);

        if (substr($data, 0, 1) != '!')
            return false;

        $data = substr($data, 1, strlen($data) - 1);
        return unserialize($data);
    }

    public static function getStatus($id)
    {
        $status = array("1" => "Réclamation",
            "2" => "Payement en ligne",
            "3" => "Commande livré",
            "4" => "Confirmé par téléphone",
            "6" => "Confirmation annulé",
            "5" => "En attente",
            "7" => "Payé",
        );

        if (isset($status[$id])) {
            return $status[$id];
        } else {
            return "";
        }
    }

    public static function string2url($str)
    {
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);
        return $str;
    }

    public static function dump($data, $tst = FALSE)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        if ($tst)
            exit;
    }

    public static function RefResa($ref, $id)
    {
        if ($ref == "") {
            return substr_replace("00000000", $id, -strlen($id));
        } else {
            return $ref;
        }
    }
    public static function explodedate($dt,$sep){
        $tab=explode($sep,$dt);
        return $tab[2]."-".$tab[1]."-".$tab[0];
    }
}
