<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Responsablehotel;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\ResponsablehotelType;
use Symfony\Component\HttpFoundation\JsonResponse;

class ResponsablehotelController extends Controller {

    public function indexAction($hotelid) {
        $hotel = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Hotel')
                ->find($hotelid);
        return $this->render('BtobHotelBundle:Responsablehotel:index.html.twig', array('entities' => $hotel->getResponsablehotel(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function addAction($hotelid) {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $Responsablehotel = new Responsablehotel();
        $form = $this->createForm(new ResponsablehotelType(), $Responsablehotel);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Responsablehotel->setHotel($hotel);
                $em->persist($Responsablehotel);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_responsable_homepage', array("hotelid" => $hotelid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Responsablehotel:form.html.twig', array('form' => $form->createView(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function editAction($id, $hotelid) {
        $request = $this->get('request');
        $Responsablehotel = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Responsablehotel')
                ->find($id);
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ResponsablehotelType(), $Responsablehotel);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_responsable_homepage', array("hotelid" => $hotelid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Responsablehotel:form.html.twig', array('form' => $form->createView(), 'id' => $id, "hotelid" => $hotelid, "hotel" => $hotel)
        );
    }

    public function deleteAction(Responsablehotel $Responsablehotel, $hotelid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Responsablehotel) {
            throw new NotFoundHttpException("Promotion non trouvée");
        }
        $em->remove($Responsablehotel);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_responsable_homepage', array("hotelid" => $hotelid)));
    }

}
