<?php 

namespace Btob\EvenementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EvenementpriceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pricead',NULL, array('required' => true, 'label' => "Prix Adulte"))
            ->add('priceenf',NULL, array('required' => true, 'label' => "Prix Enfant"))//,'data' => '0'
            ->add('priceb',NULL, array('required' => true, 'label' => "Prix Bébé"))//,'data' => '0'
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date Evenement",'format' => 'dd/MM/yyyy'))
           

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\EvenementBundle\Entity\Evenementprice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_evenementbundle_evenementprice';
    }
}
