<?php

namespace Btob\VoitureBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\VoitureBundle\Entity\Reservationvoiture;

use Btob\VoitureBundle\Form\ReservationvoitureType;
use Btob\VoitureBundle\Entity\Voiture;



/**
 * Reservationvoiture controller.
 *
 */
class ReservationvoitureController extends Controller
{

    /**
     * Lists all Reservationvoiture entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobVoitureBundle:Reservationvoiture')->findAll();

        return $this->render('BtobVoitureBundle:Reservationvoiture:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Reservationvoiture entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Reservationvoiture();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->SetAct(0);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reservationvoiture_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobVoitureBundle:Reservationvoiture:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Reservationvoiture entity.
     *
     * @param Reservationvoiture $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Reservationvoiture $entity)
    {
        $form = $this->createForm(new ReservationvoitureType(), $entity, array(
            'action' => $this->generateUrl('reservationvoiture_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Reservationvoiture entity.
     *
     */
    public function newAction()
    {
        $entity = new Reservationvoiture();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobVoitureBundle:Reservationvoiture:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Reservationvoiture entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Reservationvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationvoiture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobVoitureBundle:Reservationvoiture:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Reservationvoiture entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Reservationvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationvoiture entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobVoitureBundle:Reservationvoiture:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Reservationvoiture entity.
    *
    * @param Reservationvoiture $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Reservationvoiture $entity)
    {
        $form = $this->createForm(new ReservationvoitureType(), $entity, array(
            'action' => $this->generateUrl('reservationvoiture_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modifier'));

        return $form;
    }
    /**
     * Edits an existing Reservationvoiture entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Reservationvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationvoiture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('reservationvoiture'));
        }

        return $this->render('BtobVoitureBundle:Reservationvoiture:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Reservationvoiture entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobVoitureBundle:Reservationvoiture')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Reservationvoiture entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('reservationvoiture'));
    }

    /**
     * Creates a form to delete a Reservationvoiture entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reservationvoiture_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function delete1Action(Reservationvoiture $reservationvoiture) {
        $em = $this->getDoctrine()->getManager();

        if (!$reservationvoiture) {
            throw new NotFoundHttpException("voiture non trouvée");
        }
        $em->remove($reservationvoiture);
        $em->flush();
        return $this->redirect($this->generateUrl('reservationvoiture'));
    }


}
