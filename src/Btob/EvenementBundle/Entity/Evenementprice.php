<?php

namespace Btob\EvenementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evenementprice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\EvenementBundle\Entity\EvenementpriceRepository")
 */
class Evenementprice {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="pricead", type="float" , nullable=true)
     */
    private $pricead;

     /**
     * @var float
     *
     * @ORM\Column(name="priceenf", type="float" , nullable=true)
     */
    private $priceenf;

     /**
     * @var float
     *
     * @ORM\Column(name="priceb", type="float" , nullable=true)
     */
    private $priceb;   
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

 

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="supplementev")
     * @ORM\JoinColumn(name="evenement_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $evenement;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pricead
     *
     * @param float $pricead
     * @return Evenementprice
     */
    public function setPricead($pricead)
    {
        $this->pricead = $pricead;

        return $this;
    }

    /**
     * Get pricead
     *
     * @return float 
     */
    public function getPricead()
    {
        return $this->pricead;
    }

    /**
     * Set priceenf
     *
     * @param float $priceenf
     * @return Evenementprice
     */
    public function setPriceenf($priceenf)
    {
        $this->priceenf = $priceenf;

        return $this;
    }

    /**
     * Get priceenf
     *
     * @return float 
     */
    public function getPriceenf()
    {
        return $this->priceenf;
    }   
    
    
    /**
     * Set priceb
     *
     * @param float $priceb
     * @return Evenementprice
     */
    public function setPriceb($priceb)
    {
        $this->priceb = $priceb;

        return $this;
    }

    /**
     * Get priceb
     *
     * @return float 
     */
    public function getPriceb()
    {
        return $this->priceb;
    } 
    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Evenementprice
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }


    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Evenementprice
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Evenementprice
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set evenement
     *
     * @param \Btob\EvenementBundle\Entity\Evenement $evenement
     * @return Evenementprice
     */
    public function setEvenement(\Btob\EvenementBundle\Entity\Evenement $evenement = null)
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \Btob\EvenementBundle\Entity\Evenement 
     */
    public function getEvenement()
    {
        return $this->evenement;
    }
    

}
