<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PayementPayement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PayementRepository")
 */
class Payement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="num", type="string", length=150)
     */
    private $num;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actrib", type="boolean")
     */
    private $actrib;


    /**
     * @var boolean
     *
     * @ORM\Column(name="actline", type="boolean")
     */
    private $actline;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actagence", type="boolean")
     */
    private $actagence;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param string $num
     * @return Payement
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }
    public  function  __toString(){
        return $this->num;
    }

    /**
     * Get num
     *
     * @return string 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set actrib
     *
     * @param boolean $actrib
     * @return Payement
     */
    public function setActrib($actrib)
    {
        $this->actrib = $actrib;

        return $this;
    }

    /**
     * Get actrib
     *
     * @return boolean 
     */
    public function getActrib()
    {
        return $this->actrib;
    }


    /**
     * Set actline
     *
     * @param boolean $actline
     * @return Payement
     */
    public function setActline($actline)
    {
        $this->actline = $actline;

        return $this;
    }

    /**
     * Get actline
     *
     * @return boolean
     */
    public function getActline()
    {
        return $this->actline;
    }


    /**
     * Set actagence
     *
     * @param boolean $actagence
     * @return Payement
     */
    public function setActagence($actagence)
    {
        $this->actagence = $actagence;

        return $this;
    }

    /**
     * Get actagence
     *
     * @return boolean
     */
    public function getActagence()
    {
        return $this->actagence;
    }
}
